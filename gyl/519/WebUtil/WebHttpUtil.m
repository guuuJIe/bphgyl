//
//  WebHttpUtil.m
//  qz
//
//  Created by 陈 on 15/11/17.
//  Copyright (c) 2015年 sykj. All rights reserved.
//

#import "WebHttpUtil.h"

#define QZHttpBase @"https://519wz.cn/m/"
#define QZHttpBaseUrl   @"http://www.avfun13.com"
#define QZHttpUpdateUrl   @"http://www.519wz.cn/update.xml"

@interface WebHttpUtil()
@property (nonatomic,copy)NSString *baseUrl;

@end

@implementation WebHttpUtil


+ (instancetype)sharedManager {
    static WebHttpUtil *shared_manager = nil;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        shared_manager = [[self alloc] init];
    });
    return shared_manager;
}

-(void)httpPost:(NSString *)action dic:(NSDictionary *)dic  success:(void (^)(NSURLSessionTask *operation, id responseObject))success
        failure:(void (^)(NSURLSessionTask *operation, NSError *error))failure{
    
    AFHTTPSessionManager *afManager=[[AFHTTPSessionManager alloc]init];
    //设置请求超时
    afManager.requestSerializer.timeoutInterval=10;
    // 设置请求数据格式
    afManager.requestSerializer = [AFJSONRequestSerializer serializer];
    //    [afManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//        [afManager.requestSerializer setValue:@"application/text; charset=gbk" forHTTPHeaderField:@"Content-Type"];
    
    //设置响应数据格式
    afManager.responseSerializer = [AFHTTPResponseSerializer serializer];//NSData
//        afManager.responseSerializer = [AFJSONResponseSerializer serializer];//JSON
//    afManager.responseSerializer=[AFXMLParserResponseSerializer serializer];

    afManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json", @"text/plain", @"text/html", nil];
    
    [afManager POST:[NSString stringWithFormat:@"%@%@",QZHttpBaseUrl,action] parameters:dic success:^(NSURLSessionTask *operation, id responseObject) {
        success(operation,responseObject);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        failure(operation,error);
    }];
}

-(void)httpGet:(NSString*)url success:(void (^)(NSURLSessionTask *operation, id responseObject))success
failure:(void (^)(NSURLSessionTask *operation, NSError *error))failure{
    
    AFHTTPSessionManager *afManager=[[AFHTTPSessionManager alloc]init];
    //设置请求超时
    afManager.requestSerializer.timeoutInterval=10;
    //设置响应数据格式
    afManager.responseSerializer = [AFXMLParserResponseSerializer serializer];//NSData
 
    [afManager GET:url parameters:nil success:^ (NSURLSessionTask * operation, id responseObject) {
        success(operation,responseObject);
    } failure:^ (NSURLSessionTask * operation, NSError * error) {
        failure(operation,error);
    }];
}

-(void)getWelcomeImg:(void (^)(NSURLSessionTask *operation, id responseObject))success
    failure:(void (^)(NSURLSessionTask *operation, NSError *error))failure{
    [self httpPost:@"index_ads" dic:nil success:success failure:failure];
}

-(void)getBaseUrl:(void (^)(NSURLSessionTask *operation, id responseObject))success
          failure:(void (^)(NSURLSessionTask *operation, NSError *error))failure{
    
    NSMutableDictionary *postDada=[[NSMutableDictionary alloc]init];
    [postDada setValue:[self getGBK:@"%D5%E2%CA%C7%C6%C0%BC%DB%D5%E2%CA%C7%C6%C0%BC%DB"] forKey:@"message"];
    [postDada setValue:[self getGBK:@"1473998896"] forKey:@"posttime"];
    [postDada setValue:[self getGBK:@"cb25c17b"] forKey:@"formhash"];
    [postDada setValue:[self getGBK:@"1"] forKey:@"usesig"];
    [postDada setValue:[self getGBK:@"++"] forKey:@"subject"];

    [postDada setValue:[self getGBK:@"post"] forKey:@"mob"];
    [postDada setValue:[self getGBK:@"reply"] forKey:@"action"];
    [postDada setValue:[self getGBK:@"2"] forKey:@"fid"];
    [postDada setValue:[self getGBK:@"32714"] forKey:@"tid"];
    [postDada setValue:[self getGBK:@"page%3D1"] forKey:@"extra"];
    [postDada setValue:[self getGBK:@"yes"] forKey:@"replysubmit"];
        [postDada setValue:[self getGBK:@"yes"] forKey:@"infloat"];
    [postDada setValue:[self getGBK:@"fastpost"] forKey:@"handlekey"];
    [postDada setValue:[self getGBK:@"1"] forKey:@"inajax"];
    
    [self httpPost:@"/forum.php?" dic:postDada success:success failure:failure];
}

-(NSString *)getGBK:(NSString *)str{
    return str;
    return  [str stringByAddingPercentEscapesUsingEncoding:CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000)];
}
@end
