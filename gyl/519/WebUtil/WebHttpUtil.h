//
//  WebHttpUtil.h
//  qz
//
//  Created by 陈 on 15/11/17.
//  Copyright (c) 2015年 sykj. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebHttpUtil : NSObject
+(id)sharedManager;

-(void)getWelcomeImg:(void (^)(NSURLSessionTask *operation, id responseObject))success
    failure:(void (^)(NSURLSessionTask *operation, NSError *error))failure;

-(void)getBaseUrl:(void (^)(NSURLSessionTask *operation, id responseObject))success
          failure:(void (^)(NSURLSessionTask *operation, NSError *error))failure;
@end
