//
//  WXApiManager.h
//  519
//
//  Created by Macmini on 17/1/7.
//  Copyright © 2017年 519. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol WXApiManagerDelegate <NSObject>

@optional
- (void)managerDidRecvAuthResponse:(SendAuthResp *)response;
-(void)managerJunpPay:(PayResp *)respose;

@end
@interface WXApiManager : NSObject<WXApiDelegate>
@property (nonatomic, assign) id<WXApiManagerDelegate> delegate;
+ (instancetype)sharedManager;
@end
