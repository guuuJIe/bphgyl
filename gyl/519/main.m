//
//  main.m
//  519
//
//  Created by 陈 on 16/8/2.
//  Copyright (c) 2016年 519. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
