//
//  AppDelegate.m
//  519
//
//  Created by 陈 on 16/8/2.
//  Copyright (c) 2016年 519. All rights reserved.
//
int64_t    g_groupId;
int64_t    g_staffId;
int64_t    g_questionId;
BOOL    g_openRobotInShuntMode;
#import "AppDelegate.h"
#import "payRequsestHandler.h"
#import <AlipaySDK/AlipaySDK.h>
#import "WXApiManager.h"
#import "AdvertiseView.h"
#import "MainVC.h"
#import "BNCoreServices.h"
#import "MainTabBarController.h"
#import "ServiceSessionListVC.h"
#import "LoginViewController.h"
#import "UITabBar+Badge.h"
#import "MainWebView.h"
@interface AppDelegate ()<UMSocialUIDelegate,WXApiDelegate,UNUserNotificationCenterDelegate,AdvertiseDissmissDelegate,QYConversationManagerDelegate,QYSessionViewDelegate>


@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    
    
    //设置header  useragent头部
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectZero];
    NSString *oldAgent = [webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
    
    NSString *newAgent = [oldAgent stringByAppendingString:@"bOpIhUi90_"];
    
    //注册useraagent
    NSDictionary *dictionary=[[NSDictionary alloc]initWithObjectsAndKeys:newAgent,@"UserAgent" ,nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
    //设置窗口对象
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    self.window.rootViewController = [[MainTabBarController alloc]init];
    //设置背景色
    self.window.backgroundColor = [UIColor whiteColor];
    
//     [[UITabBar appearance] setTranslucent:NO];
    //显示窗口
    [self.window makeKeyAndVisible];
    //添加浮动窗口
//    [self.window.rootViewController.view addSubview:self.btnView];
    
    [WXApi registerApp:WX_APPID withDescription:@"wechat"];
    
    [[QYSDK sharedSDK] registerAppId:QYKEY appName:@"泊啤汇供应链"];
    [[[QYSDK sharedSDK]conversationManager] setDelegate:self];
    
    [UMUtil initUM:launchOptions];
    NSDictionary *remoteNotificationInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (remoteNotificationInfo) {
        [self showChatViewController:remoteNotificationInfo];
    }
    [self registerAPNS];
    
    [[UITabBar appearance] setTranslucent:NO];

    //设置NAV
   // [self initNavBar];
    
    //百度定位
    BOOL ret = [[[BMKMapManager alloc]init]start:BAIDU_KEY  generalDelegate:nil];
    if (!ret) {
        NSLog(@"manager start failed!");
    }
    [BNCoreServices_Instance initServices:BAIDU_KEY];
    [BNCoreServices_Instance startServicesAsyn:nil fail:nil];
    
    
    // 1.判断沙盒中是否存在广告图片，如果存在，直接显示
    NSString *filePath = [self getFilePathWithImageName:[kUserDefaults valueForKey:adImageName]];
    BOOL isExist = [self isFileExistWithFilePath:filePath];
    if (isExist) {// 图片存在

        AdvertiseView *advertiseView = [[AdvertiseView alloc] initWithFrame:self.window.bounds];
        advertiseView.filePath = filePath;
        advertiseView.delegate = self;
        [advertiseView show];

    }else{
        [self dissmiss];
    }
    // 2.无论沙盒中是否存在广告图片，都需要重新调用广告接口，判断广告是否更新
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [self requestUrlImage];
    });
    
    

    return YES;

}


-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    [WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]];
    return YES;
}
-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
    if ([url.host isEqualToString:@"safepay"]) {
        //跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
        }];
    }else{
        return [WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]];
    }
    return YES;

}

//后台进入前台刷新定位
-(void)applicationWillEnterForeground:(UIApplication *)application{
    //[[NSNotificationCenter defaultCenter]postNotificationName:@"refreshLocation" object:nil];
    NSInteger count = [[[QYSDK sharedSDK] conversationManager] allUnreadCount];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:count];
}

#pragma mark 友盟分享回调  UMSocialUIDelegate
-(void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response
{
    //根据`responseCode`得到发送结果,如果分享成功
    if(response.responseCode == UMSResponseCodeSuccess)
    {
        //得到分享到的平台名
        NSLog(@"share to sns name is %@",[[response.data allKeys] objectAtIndex:0]);
    }
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    
    
// NSString *token = [[[[deviceToken description] stringByReplacingOccurrencesOfString: @"<" withString: @""]
//                  stringByReplacingOccurrencesOfString: @">" withString: @""]
//                 stringByReplacingOccurrencesOfString: @" " withString: @""];
//    NSLog(@"%@",token);
//     [[QYSDK sharedSDK] updateApnsToken:deviceToken];
    if (![deviceToken isKindOfClass:[NSData class]]) return;
    const unsigned *tokenBytes = (const unsigned *)[deviceToken bytes];
    NSString *hexToken = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                          ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                          ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                          ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
    NSLog(@"deviceToken:%@",hexToken);
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    //关闭友盟自带的弹出框
    [UMessage setAutoAlert:NO];
    [UMessage didReceiveRemoteNotification:userInfo];
    
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateInactive) {
        [self showChatViewController:userInfo];
    }
}

- (void)showChatViewController:(NSDictionary *)remoteNotificationInfo
{
    id object = [remoteNotificationInfo objectForKey:@"nim"]; //含有“nim”字段，就表示是七鱼的消息
    if (object)
    {
        //所有 UINavigationController 都先 popToRootViewController，
        //然后将聊天窗口 push 进某个 UINavigationController
        MainTabBarController *rootVc = (MainTabBarController *)_window.rootViewController;
        assert(rootVc.viewControllers.count == 2);
        UINavigationController *mainNav = rootVc.viewControllers[0];
        [mainNav popToRootViewControllerAnimated:NO];
        UINavigationController *settingNav = rootVc.viewControllers[1];
        [settingNav popToRootViewControllerAnimated:NO];
        [rootVc setSelectedIndex:1];
        assert(settingNav.viewControllers.count > 0);
//        QYSettingViewController *settingViewController = settingNav.viewControllers[0];
//        [settingViewController onChat];
    }
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    
//    return [WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]];
    
    BOOL result = [UMSocialSnsService handleOpenURL:url];
    if (result == FALSE) {
        //调用其他SDK，例如支付宝SDK等
        if ([url.host isEqualToString:@"safepay"]) {
            
            //跳转支付宝钱包进行支付，处理支付结果
            [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
                NSLog(@"result = %@",resultDic);
            }];
        }else if([url.host isEqualToString:@"pay"]){
            //跳转微信支付进行支付，处理支付结果
            [WXApi handleOpenURL:url delegate:[WXApiManager sharedManager]];
        }
        return YES;
    }
    return result;
}

//iOS10新增：处理前台收到通知的代理方法
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler{
    NSDictionary * userInfo = notification.request.content.userInfo;
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        //应用处于前台时的远程推送接受
        //关闭友盟自带的弹出框
        [UMessage setAutoAlert:NO];
        //必须加这句代码
        [UMessage didReceiveRemoteNotification:userInfo];
        
    }else{
        //应用处于前台时的本地推送接受
    }
    //当应用处于前台时提示设置，需要哪个可以设置哪一个
    completionHandler(UNNotificationPresentationOptionSound|UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionAlert);
}

//iOS10新增：处理后台点击通知的代理方法
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler{
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        //应用处于后台时的远程推送接受
        //必须加这句代码
        [UMessage didReceiveRemoteNotification:userInfo];
        
    }else{
        //应用处于后台时的本地推送接受
    }
    
}


//打开推送消息后处理
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {

}

/**
#pragma mark 导航条
- (void)initNavBar {
    //设置Nav的背景色和title色
    UINavigationBar *navigationBarAppearance = [UINavigationBar appearance];
    
    
    NSDictionary *textAttributes = nil;
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        [navigationBarAppearance setTintColor:[UIColor whiteColor]];//返回按钮的箭头颜色
        [[UITextField appearance] setTintColor:[UIColor colorWithHexString:@"0x3bbc79"]];//设置UITextField的光标颜色
        [[UITextView appearance] setTintColor:[UIColor colorWithHexString:@"0x3bbc79"]];//设置UITextView的光标颜色
        //        [[UISearchBar appearance] setBackgroundImage:[UIImage imageWithColor:kColorNAVBackground] forBarPosition:0 barMetrics:UIBarMetricsDefault];
        textAttributes = @{
                           NSFontAttributeName: FZLanTingHei_L(19),
                           NSForegroundColorAttributeName: APPTabColor,
                           };
    } else {
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
        [[UISearchBar appearance] setBackgroundImage:[UIImage imageWithColor:NavBarBGColor]];
        
        textAttributes = @{
                           FZLanTingHei_B(19),
                           UITextAttributeTextColor:  NavBarTitleColor,
                           UITextAttributeTextShadowColor: [UIColor clearColor],
                           UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetZero],
                           };
#endif
    }
    [navigationBarAppearance setBackgroundImage:[UIImage imageWithColor:APPColor] forBarMetrics:UIBarMetricsDefault];
    [navigationBarAppearance setTitleTextAttributes:textAttributes];
    //消除下方阴影线
    [navigationBarAppearance setShadowImage:[UIImage imageWithColor:APPColor]];
    
}
*/
#pragma mark 广告页部分
/**
 *  判断文件是否存在
 */
- (BOOL)isFileExistWithFilePath:(NSString *)filePath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDirectory = FALSE;
    return [fileManager fileExistsAtPath:filePath isDirectory:&isDirectory];
}

/**
 *  初始化广告页面
 */

-(void)requestUrlImage{
    NSString * imgUrl = [NSString stringWithFormat:@"%@ctl=init",GYLUrl];
        [RequestData requestDataOfUrl:imgUrl success:^(NSDictionary *dic) {
            
            for (NSDictionary * dict in dic[@"start_page"]) {
               
                if (dict[@"img"]) {
                    NSArray *stringArr = [dict[@"img"] componentsSeparatedByString:@"/"];
                    NSString *imageName;
                    for (NSString * str in stringArr) {
                        if ([str rangeOfString:@"?"].location != NSNotFound) {
                            NSArray * imgNameArr = [str componentsSeparatedByString:@"?"];
                            imageName = imgNameArr.firstObject;
                            break;
                        }else{
                            imageName = stringArr.lastObject;
                        }
                    }
                    
                    
                    NSString * imageId;
                    NSString * typeStr;
                    
                    if ([dict[@"type"] isEqualToString:@"0"]) {
                        imageId =dict[@"data"][@"url"];
                    }
                    if ([dict[@"type"] isEqualToString:@"11"]) {
                        imageId = dict[@"data"][@"tid"];
                    }
                    if ([dict[@"type"] isEqualToString:@"12"]) {
                        imageId = dict[@"data"][@"cate_id"];
                    }
                    if ([dict[@"type"] isEqualToString:@"21"]) {
                        imageId = dict[@"data"][@"item_id"];
                    }
                    typeStr = dict[@"type"];

                    
                    // 拼接沙盒路径
                    NSString *filePath = [self getFilePathWithImageName:imageName];
                    
                    NSString * adimagename = [kUserDefaults valueForKey:adImageName];
                    if (!adimagename) {
                        [kUserDefaults setObject:imageName forKey:adImageName];
                    }
                    
                    BOOL isExist = [self isFileExistWithFilePath:filePath];
                    if (!isExist){// 如果该图片不存在，则删除老图片，下载新图片
                        
                        [self downloadAdImageWithUrl:dict[@"img"] imageName:imageName ImageId:imageId withimageType:typeStr];
                        
                    }
                }
            }
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    
}

/**
 *  下载新图片
 */
- (void)downloadAdImageWithUrl:(NSString *)imageUrl imageName:(NSString *)imageName ImageId:(NSString *)imageId withimageType:(NSString *)type
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [self deleteOldImage];
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]];
        UIImage *image = [UIImage imageWithData:data];
        
        NSString *filePath = [self getFilePathWithImageName:imageName]; // 保存文件的名称
        if ([UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES]) {// 保存成功
            [kUserDefaults setValue:imageName forKey:adImageName];
            [kUserDefaults setValue:imageId forKey:@"advImageId"];
            [kUserDefaults setValue:type forKey:@"advImageType"];
            [kUserDefaults synchronize];
        }else{
            NSLog(@"保存失败");
        }
        
    });
}

/**
 *  删除旧图片
 */
- (void)deleteOldImage
{
    NSString *imageName = [kUserDefaults valueForKey:adImageName];
    if (imageName) {
        NSString *filePath = [self getFilePathWithImageName:imageName];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        [fileManager removeItemAtPath:filePath error:nil];
    }
}

/**
 *  根据图片名拼接文件路径
 */
- (NSString *)getFilePathWithImageName:(NSString *)imageName
{
    if (imageName) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:imageName];
        return filePath;
    }
    return nil;
}

/**
 *  客服按钮
 */
-(UIView *)btnView{
    if (!_btnView) {
        _btnView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
        _btnView.center = CGPointMake(Screen_Width-30, Screen_Height*0.65);
        UIPanGestureRecognizer * pan = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePan:)];
        [_btnView addGestureRecognizer:pan];
        
        
        _serviceBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
        _serviceBtn.layer.cornerRadius = 25;
        _serviceBtn.layer.masksToBounds = YES;
        [_serviceBtn setImage:[UIImage imageNamed:@"icon_pink_service"] forState:(UIControlStateNormal)];
        [_serviceBtn setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.8]];
        [_serviceBtn addTarget:self action:@selector(serviceBtnClick) forControlEvents:(UIControlEventTouchUpInside)];
        [_btnView addSubview:_serviceBtn];
        
        _countLabel = [[UILabel alloc]init];
        _countLabel.textColor = [UIColor whiteColor];
        _countLabel.backgroundColor = [UIColor redColor];
        _countLabel.textAlignment = NSTextAlignmentCenter;
        _countLabel.font = [UIFont boldSystemFontOfSize:10];
        _countLabel.layer.cornerRadius = 7;
        _countLabel.layer.masksToBounds = YES;
        [_btnView addSubview:_countLabel];
        
        NSInteger count = [[[QYSDK sharedSDK]conversationManager] allUnreadCount];
        _countLabel.text = [NSString stringWithFormat:@"%ld",(long)count];
        //_countLabel.hidden = NO;
        if (count>20) {
            [_countLabel makeConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(15);
                make.top.mas_equalTo(_btnView.mas_top).with.mas_offset(0);
                make.right.mas_equalTo(_btnView.mas_right).with.mas_offset(0);
            }];
        }else{
            [_countLabel makeConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(15);
                make.width.equalTo(15);
                make.top.mas_equalTo(_btnView.mas_top).with.mas_offset(0);
                make.right.mas_equalTo(_btnView.mas_right).with.mas_offset(0);
            }];
            if (count == 0) {
                _countLabel.hidden = YES;
            }
        }
        
    }
    return _btnView;
}

-(void)serviceBtnClick{
//    [self onchat];
    [self connactCustomer];
}
-(void)handlePan:(UIPanGestureRecognizer *)recognizer{
    CGPoint translation = [recognizer translationInView:self.window.rootViewController.view];
    CGFloat centerX = recognizer.view.center.x + translation.x;
    CGFloat thecenter = 0;
    recognizer.view.center=CGPointMake(centerX,recognizer.view.center.y+ translation.y);
    
    [recognizer setTranslation:CGPointZero inView:self.window.rootViewController.view];
    
    if(recognizer.state==UIGestureRecognizerStateEnded|| recognizer.state==UIGestureRecognizerStateCancelled) {
        
        thecenter=Screen_Width-60/2;

        [UIView animateWithDuration:0.5 animations:^{
            recognizer.view.center=CGPointMake(thecenter,recognizer.view.center.y+ translation.y);
        }];
    }
}

- (void)connactCustomer{
    UITabBarController *tbc = (UITabBarController *)self.window.rootViewController;
    UINavigationController *nav = tbc.viewControllers[tbc.selectedIndex];
    if (EMAIL) {
        NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
       
        // app版本
        NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
        MainWebView * vc = [[MainWebView alloc]init];
        vc.webUrl = [NSString stringWithFormat:@"http://www.bphgyl.com/wap/index.php?ctl=onlinekf&userId=%@&os=ios&appVersion=%@&device=%@&deal_id=0&order_sn=0",EMAIL,app_Version,[[UIDevice currentDevice] model]];
        [nav pushViewController:vc animated:true];
    }else{
        LoginViewController * vc = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
        
        [nav pushViewController:vc animated:true];
    }
   
    
   
    
}

-(void)onchat{
    QYSource *source = [[QYSource alloc] init];
    source.title =  @"泊啤汇";
    source.urlString = @"https://8.163.com/";
    QYSessionViewController *sessionViewController = [[QYSDK sharedSDK] sessionViewController];
    sessionViewController.delegate = self;
    sessionViewController.sessionTitle = @"供应链";
    sessionViewController.source = source;
    sessionViewController.commonQuestionTemplateId = g_questionId;
    sessionViewController.openRobotInShuntMode = g_openRobotInShuntMode;
    if (EMAIL) {
        sessionViewController.staffId = [SALES_OID intValue];
    }else{
        sessionViewController.groupId = 880528;
    }
    [[QYSDK sharedSDK] customUIConfig].bottomMargin = 2;
    BaseNC *nav = [[BaseNC alloc] initWithRootViewController:sessionViewController];
    sessionViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(onBack)];
    nav.navigationBar.tintColor = APPFourColor;
    [nav.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:19], NSForegroundColorAttributeName:APPFourColor}];

    [self.window.rootViewController presentViewController:nav animated:YES completion:^{

    }];
    
//    ServiceSessionListVC * vc = [[ServiceSessionListVC alloc]init];
//    UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:vc];
//    vc.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(onBack)];
//    nav.navigationBar.tintColor = [UIColor whiteColor];
//    [nav.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:19], NSForegroundColorAttributeName:[UIColor whiteColor]}];
//    [nav.navigationBar setBackgroundImage:[UIImage imageWithColor:APPColor] forBarMetrics:(UIBarMetricsDefault)];
//    [self.window.rootViewController presentViewController:nav animated:YES completion:^{
//
//    }];
//
}
/**
 *  点击商铺入口按钮回调
 */
- (void)onTapShopEntrance{

}

/**
 *  点击聊天窗口右边或左边会话列表按钮回调
 */
- (void)onTapSessionListEntrance{

}

-(void)onBack{
    self.countLabel.hidden = YES;
    [self.window.rootViewController dismissViewControllerAnimated:YES completion:^{

    }];
}

-(void)onUnreadCountChanged:(NSInteger)count{
    self.countLabel.hidden = NO;
    self.countLabel.text = [NSString stringWithFormat:@"%ld",(long)count];
    
    if (count>20) {
        [self.countLabel makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(15);
            make.top.mas_equalTo(_serviceBtn.mas_top).with.mas_offset(0);
            make.right.mas_equalTo(_serviceBtn.mas_right).with.mas_offset(0);
        }];
    }else{
        if (count == 0) {
            self.countLabel.hidden = YES;
        }
    }
}

#pragma mark - -- 注册通知
- (void)registerAPNS
{
    float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
    if (sysVer >= 10)
    {
        // iOS 10
        [self registerPush10];
    }
    else if (sysVer >= 8)
    {
        // iOS 8-9
        [self registerPush8to9];
    }
    else {
        // before iOS 8
        [self registerPushBefore8];
    }
#else
    if (sysVer < 8)
    {
        // before iOS 8
        [self registerPushBefore8];
    }
    else
    {
        // iOS 8-9
        [self registerPush8to9];
    }
#endif
}

- (void)registerPush10{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate                  = self;
    [center requestAuthorizationWithOptions:UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted) {
        }
    }];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
#endif
}

- (void)registerPush8to9
{
    UIUserNotificationType types           = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

- (void)registerPushBefore8
{
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
}




#pragma mark 修改状态栏颜色
-(void)dissmiss{
    [self setStatusBarBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0]];
}
//设置状态栏颜色
- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}
@end

