//
//  AdvertiseView.h
//  zhibo
//
//  Created by on 16/5/17.
//  Copyright © 2016年 zhq. All rights reserved.
//



#import <UIKit/UIKit.h>
static NSString *const adImageName = @"adImageName";
static NSString *const adUrl = @"adUrl";

@protocol AdvertiseDissmissDelegate
-(void)dissmiss;
@end


@interface AdvertiseView : UIView
@property (nonatomic,assign)id<AdvertiseDissmissDelegate>delegate;

@property (nonatomic, strong) UIImageView *adView;
/** 显示广告页面方法*/
- (void)show;

/** 图片路径*/
@property (nonatomic, copy) NSString *filePath;



@end
