//
//  AppDelegate.h
//  519
//
//  Created by 陈 on 16/8/2.
//  Copyright (c) 2016年 519. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,strong)UIView * btnView;
@property (nonatomic,strong)UIButton * serviceBtn;
@property (nonatomic,strong)UILabel * countLabel;
@end

