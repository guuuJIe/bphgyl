//
//  UMUtil.m
//  519
//
//  Created by 陈 on 16/8/13.
//  Copyright (c) 2016年 519. All rights reserved.
//

#import "UMUtil.h"

@interface UMUtil()<UNUserNotificationCenterDelegate>

@end

@implementation UMUtil

#pragma mark 初始化友盟
+(void)initUM:(NSDictionary *)launchOptions{
    //设置 AppKey 及 LaunchOptions
    [UMessage startWithAppkey:UM_KEY launchOptions:launchOptions];
    //1.3.0版本开始简化初始化过程。如不需要交互式的通知，下面用下面一句话注册通知即可。
    [UMessage registerForRemoteNotifications];
    //for log
//    [UMessage setLogEnabled:YES];
    
    
    //分享初始化
    [UMSocialData setAppKey:UM_KEY];
    //设置微信AppId、appSecret，分享url
    [UMSocialWechatHandler setWXAppId:WX_APPID appSecret:WX_SSECRET url:@"http://www.umeng.com/social"];
    //设置手机QQ 的AppId，Appkey，和分享URL，需要#import "UMSocialQQHandler.h"
    [UMSocialQQHandler setQQWithAppId:QQ_APPID appKey:QQ_KEY url:@"http://www.umeng.com/social"];
    
    
    //统计初始化
    UMConfigInstance.appKey = UM_KEY;
    UMConfigInstance.channelId =@"新泊啤汇";
    
    [MobClick startWithConfigure:UMConfigInstance];//配置以上参数后调用此方法初始化SDK！
}

#pragma mark 页面统计计时
+(void)mobclickAgentBeginLog:(NSString *)classStr{
    [MobClick beginLogPageView:classStr];//友盟统计
}

#pragma mark 页面统计计时
+(void)mobclickAgentEndLog:(NSString *)classStr{
    [MobClick endLogPageView:classStr];//友盟统计
}

#pragma mark 分享 分享页面需实现 UMSocialUIDelegate, urlImg 存在默认使用urlImg
+(void)shareUM:(UIViewController *)vc delegate:(id)delegate title:(NSString *)title body:(NSString *)body url:(NSString *)url img:(UIImage *)img urlImg:(NSString *)urlImg{
            [UMSocialConfig setFinishToastIsHidden:YES position:UMSocialiToastPositionTop];
    
    //配置该参数 会影响 分享链接点击 页面为该地址 可异步获取image 后分享网络图片
    //    UMSocialUrlResource *urlResource = [[UMSocialUrlResource alloc] initWithSnsResourceType:UMSocialUrlResourceTypeImage url:urlImg];
    //    [UMSocialData defaultData].urlResource = urlResource;
    
//    [UMSocialData defaultData].extConfig.qqData.url=url;
//    [UMSocialData defaultData].extConfig.qqData.title = title;
    
//    [UMSocialData defaultData].extConfig.qzoneData.url=url;
//    [UMSocialData defaultData].extConfig.qzoneData.title =title;
    
    [UMSocialData defaultData].extConfig.wechatSessionData.url=url;
    [UMSocialData defaultData].extConfig.wechatSessionData.title=title;
    
    [UMSocialData defaultData].extConfig.wechatTimelineData.url=url;
    [UMSocialData defaultData].extConfig.wechatTimelineData.title=title;
    
    NSMutableArray *shareToSnsNames=[[NSMutableArray alloc]init];
    if([WXApi isWXAppInstalled]){
        [shareToSnsNames addObject:UMShareToWechatSession];
        [shareToSnsNames addObject:UMShareToWechatTimeline];
    }
    
    if([QQApiInterface isQQInstalled]){
        [shareToSnsNames addObject:UMShareToQQ];
        [shareToSnsNames addObject:UMShareToQzone];
    }

    //包含网络图片 开启异步下载，否则或失败＝默认为图标
    if(urlImg!=nil){
        [[[UIImageView alloc]init] sd_setImageWithURL:[NSURL URLWithString:urlImg] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if(image==nil){
                image=[UIImage imageNamed:@"gylLogo"];
            }
            //随缘下载
            [UMSocialSnsService presentSnsIconSheetView:vc
                                                 appKey:UM_KEY
                                              shareText:body
                                             shareImage:image
                                        shareToSnsNames:shareToSnsNames
                                               delegate:delegate];
        }];
    }else{
        [UMSocialSnsService presentSnsIconSheetView:vc
                                             appKey:UM_KEY
                                          shareText:body
                                         shareImage:[UIImage imageNamed:@"gylLogo"]
                                    shareToSnsNames:shareToSnsNames
                                           delegate:delegate];
    }
    
    
    
    
}

//    [UMUtil logUM:UMShareToWechatSession vc:self isPresentInController:YES back:^(UMSocialResponseEntity *response,UMSocialSnsPlatform *snsPlatform) {
//        if (response.responseCode == UMSResponseCodeSuccess) {
//
//            NSDictionary *dict = [UMSocialAccountManager socialAccountDictionary];
//            UMSocialAccountEntity *snsAccount = [[UMSocialAccountManager socialAccountDictionary] valueForKey:snsPlatform.platformName];
//            NSLog(@"\nusername = %@,\n usid = %@,\n token = %@ iconUrl = %@,\n unionId = %@,\n thirdPlatformUserProfile = %@,\n thirdPlatformResponse = %@ \n, message = %@",snsAccount.userName,snsAccount.usid,snsAccount.accessToken,snsAccount.iconURL, snsAccount.unionId, response.thirdPlatformUserProfile, response.thirdPlatformResponse, response.message);
//
//        }
//    }];

+(void)logUM:(NSString *)snsName vc:(UIViewController *)vc isPresentInController:(BOOL)isPresentInController back:(void (^)(UMSocialResponseEntity *response,UMSocialSnsPlatform *snsPlatform))back{
    UMSocialSnsPlatform *snsPlatform = [UMSocialSnsPlatformManager getSocialPlatformWithName:snsName];
    snsPlatform.loginClickHandler(vc,[UMSocialControllerService defaultControllerService],isPresentInController,^(UMSocialResponseEntity *response){
        back(response,snsPlatform);
    });
}


#pragma  mark 添加标签
+(void)setPushTagUM:(NSArray *)tag callBack:(void(^)(id responseObject, NSInteger remain, NSError *error))callBack{
    [UMessage addTag:tag
            response:^(id responseObject, NSInteger remain, NSError *error) {
                //add your codes
                callBack(responseObject,remain,error);
            }];
}

#pragma mark 获取标签
+(void)getPushTagUM:(void(^)(NSSet *responseTags, NSInteger remain, NSError *error))callBack{
    [UMessage getTags:^(NSSet *responseTags, NSInteger remain, NSError *error) {
        //add your codes
        callBack(responseTags,remain,error);
    }];
}

#pragma mark 清除标签
+(void)removePushTagUM:(void(^)(id responseObject, NSInteger remain, NSError *error))callBack{
    [UMessage removeAllTags:^(id responseObject, NSInteger remain, NSError *error) {
        callBack(responseObject,remain,error);
    }];

}

#pragma mark 添加别名
+(void)setPushAliasUM:(NSString *)alias type:(NSString *)type callBack:(void(^)(id responseObject, NSError *error))callBack{
    [UMessage addAlias:alias type:type response:^(id responseObject, NSError *error) {
        callBack(responseObject,error);
    }];
}

#pragma mark 添加别名
+(void)removePushAliasUM:(NSString *)alias type:(NSString *)type callBack:(void(^)(id responseObject, NSError *error))callBack{
    [UMessage removeAlias:alias type:type response:^(id responseObject, NSError *error) {
        callBack(responseObject,error);
    }];
}

@end
