//
//  PayHelper.h
//  carcool
//
//  Created by 朱子涵 on 15/10/11.
//  Copyright © 2015年 ttsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PayHelper : NSObject

@property (nonatomic,strong) void(^zfbPayBlock)(NSDictionary *);

+(id)sharedManager;

-(void)ZFBPay:(NSString *)orderId price:(NSString *)price title:(NSString *)title info:(NSString *)info;

-(void)WXPay:(NSString *)prepayId;
@end
