//
//  UMUtil.h
//  519
//
//  Created by 陈 on 16/8/13.
//  Copyright (c) 2016年 519. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UMUtil : NSObject

+(void)initUM:(NSDictionary *)launchOptions;

+(void)mobclickAgentBeginLog:(NSString *)classStr;

+(void)mobclickAgentEndLog:(NSString *)classStr;
    
+(void)shareUM:(UIViewController *)vc delegate:(id)delegate title:(NSString *)title body:(NSString *)body url:(NSString *)url img:(UIImage *)img urlImg:(NSString *)urlImg;

//+(void)logUM:(NSString *)snsName vc:(UIViewController *)vc isPresentInController:(BOOL)isPresentInController back:(void (^)(UMSocialResponseEntity *response,UMSocialSnsPlatform *snsPlatform))back;

+(void)setPushTagUM:(NSArray *)tag callBack:(void(^)(id responseObject, NSInteger remain, NSError *error))callBack;

+(void)getPushTagUM:(void(^)(NSSet *responseTags, NSInteger remain, NSError *error))callBack;

+(void)removePushTagUM:(void(^)(id responseObject, NSInteger remain, NSError *error))callBack;

+(void)setPushAliasUM:(NSString *)alias type:(NSString *)type callBack:(void(^)(id responseObject, NSError *error))callBack;

+(void)removePushAliasUM:(NSString *)alias type:(NSString *)type callBack:(void(^)(id responseObject, NSError *error))callBack;
@end
