//
//  RequestData.h
//  519
//
//  Created by Macmini on 16/11/30.
//  Copyright © 2016年 519. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestData : NSObject

+(void)requestDataOfUrl:(NSString *)urlstring success:(void (^)(NSDictionary * dic))success failure:(void (^)(NSError * error))failure;


@end
