//
//  SendWayView.m
//  519
//
//  Created by Macmini on 16/12/6.
//  Copyright © 2016年 519. All rights reserved.
//

#import "SendWayView.h"

@interface SendWayView()
@property (nonatomic,strong)UIView *backView;
@property (nonatomic,strong)UIView * sendView;
@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,strong)NSArray * wayArray;
@property (nonatomic,strong)NSArray * idArray;
@property (nonatomic,strong)NSArray * sendPriceArr;
@end
@implementation SendWayView

-(instancetype)initWithFrame:(CGRect)frame nameArray:(NSArray *)nameArray withIdArray:(NSArray *)idArray withPriceArr:(NSArray *)priceArr{
    if (self = [super initWithFrame:frame]) {
        self.wayArray = nameArray;
        self.idArray = idArray;
        self.sendPriceArr = priceArr;
        [self initView];
    }
    return self;
}



-(void)initView{
    
    self.backgroundColor = [UIColor clearColor];
    
    _backView = [[UIView alloc] initWithFrame:self.frame];
    _backView.backgroundColor = [UIColor blackColor];
    _backView.alpha = 0.6;
    [self addSubview:_backView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hide)];
    [_backView addGestureRecognizer:tap];
    
    _sendView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width-40, 50 * self.wayArray.count)];
    _sendView.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    _sendView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_sendView];

    for (NSInteger i = 0; i<self.wayArray.count; i++) {
        UIView * cellView = [[UIView alloc]initWithFrame:CGRectMake(0, 50*i, _sendView.frame.size.width, 50)];
        [_sendView addSubview:cellView];
        
        UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, cellView.frame.size.width, cellView.frame.size.height)];
        [btn setTitle:self.wayArray[i] forState:(UIControlStateNormal)];
        [btn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
        btn.tag = 7000+i;
        [btn addTarget:self  action:@selector(btnClick:) forControlEvents:(UIControlEventTouchUpInside)];
        [cellView addSubview:btn];
        
    }
}

-(void)btnClick:(UIButton*)btn{
    NSInteger btntag = btn.tag-7000;

    NSString * wayname = self.wayArray[btntag];
    NSString * ids = [NSString stringWithFormat:@"%@",self.idArray[btntag]];

    
    self.sendWayBlock(ids,wayname);
    [self hide];
}


- (void)hide {
    _backView.alpha = 0;
    [UIView animateWithDuration:0.5 animations:^{
        _sendView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width-40, 50 * self.wayArray.count)];
        
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}

@end
