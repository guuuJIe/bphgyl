//
//  OrderConfirmTC.h
//  519
//
//  Created by 陈 on 16/9/22.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"

@protocol OrderConfirmPayDetegate <NSObject>

-(void)changeBtnImage:(NSInteger )tag;

@end



@interface OrderConfirmPayTC : BaseTC


@property(nonatomic,assign)id<OrderConfirmPayDetegate>delegate;
-(void)showData;

-(void)showDataName:(NSString *)name logo:(NSString *)logo withids:(NSString *)ids;


+(CGFloat)getCellHeight;

+(NSString *)getID;
@end
