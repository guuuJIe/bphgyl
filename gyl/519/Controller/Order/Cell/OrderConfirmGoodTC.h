//
//  OrderConfirmGoodTC.h
//  519
//
//  Created by 陈 on 16/9/22.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"
#import "OrderGoodsModel.h"
@interface OrderConfirmGoodTC : BaseTC

@property(nonatomic,strong)OrderGoodsModel * model;

-(void)showDataName:(NSString *)name icon:(NSString *)icon price:(NSString *)price number:(NSString *)number speci:(NSString *)speci;

+(CGFloat)getCellHeight;

+(NSString *)getID;
@end
