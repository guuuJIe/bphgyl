//
//  OrderConfirmTC.m
//  519
//
//  Created by 陈 on 16/9/22.
//  Copyright © 2016年 519. All rights reserved.
//

#import "OrderConfirmPayTC.h"

@interface OrderConfirmPayTC()
@property(nonatomic,strong)UIImageView *img;
@property(nonatomic,strong)UILabel *label;
@property(nonatomic,strong)UIButton *btn;
@property(nonatomic,strong)UIView *line;
@property(nonatomic,strong)NSMutableArray * payWayMuArray;

@end

@implementation OrderConfirmPayTC
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initView];
    }
    
    return  self;
}

-(void)initView{
    self.payWayMuArray = [NSMutableArray new];
    self.img=[[UIImageView alloc] initWithFrame:CGRectMake(Default_Space, Default_Space, 40, 40)];
    self.label=[[UILabel alloc]initWithFrame:CGRectMake(self.img.frame.size.width+self.img.frame.origin.x+Default_Space,([OrderConfirmPayTC getCellHeight]-20)/2, Screen_Width-self.img.frame.size.width-20-Default_Space*4, 20)];
    self.label.textColor=APPFourColor;
    self.label.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.btn =[[UIButton alloc]initWithFrame:CGRectMake(self.label.frame.size.width+self.label.frame.origin.x, self.label.frame.origin.y, 25, 25)];
    [self.btn setImage:[UIImage imageNamed:@"check_0_icon"] forState:UIControlStateNormal];
    [self.btn setImage:[UIImage imageNamed:@"check_1_icon"] forState:UIControlStateSelected];
    self.btn.selected = NO;
//    [self.btn addTarget:self action:@selector(onClick:) forControlEvents:(UIControlEventTouchUpInside)];
    self.btn.userInteractionEnabled = false;
    
    self.line=[[UIView alloc] initWithFrame:CGRectMake(0, [OrderConfirmPayTC getCellHeight]-Default_Line, Screen_Width, Default_Line)];
    self.line.backgroundColor=APPBGColor;
    
    [self addSubview:self.img];
    [self addSubview:self.label];
    [self addSubview:self.btn];
    [self addSubview:self.line];
}


-(void)showDataName:(NSString *)name logo:(NSString *)logo withids:(NSString *)ids{
    [self.img sd_setImageWithURL:[NSURL URLWithString:logo]];
    self.label.text = name;
    NSInteger inter = [ids integerValue];
    self.btn.tag = 6000+inter;

}

-(void)showData{
    self.img.image=[UIImage imageNamed:@"icon_120x120"];
    self.label.text=@"支付宝";
    
}

-(void)onClick:(UIButton *)btn{
   
    [self.delegate changeBtnImage:btn.tag];
}


+(CGFloat)getCellHeight{
    return 60;
}

+(NSString *)getID{
    return @"OrderConfirmTC";
}
@end
