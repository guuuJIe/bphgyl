//
//  AddGoodTC.m
//  519
//
//  Created by 陈 on 16/9/21.
//  Copyright © 2016年 519. All rights reserved.
//

#import "AddGoodTC.h"

@interface AddGoodTC()
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UIView *tagView;
@property(nonatomic,strong)UIView *line;
@property (nonatomic,strong)NSMutableArray * btnMuArray;
@property (nonatomic,strong)NSArray * priceArray;
@property (nonatomic,strong)NSArray * idArray;
@end

@implementation AddGoodTC

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initView];
    }
    
    return  self;
}

-(void)initView{
    self.btnMuArray = [NSMutableArray new];
    self.backgroundColor=[UIColor whiteColor];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    self.titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, Screen_Width-Default_Space*2, 20)];
    self.titleLabel.textColor=APPFourColor;
    self.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    
    self.tagView=[[UIView alloc]init];
    
    self.line=[[UIView alloc]init];
    self.line.backgroundColor=APPBGColor;
    
    [self addSubview:self.titleLabel];
    [self addSubview:self.tagView];
    [self addSubview:self.line];
}

-(void)showData:(NSString *)title btnTitle:(NSArray *)btnTitle btnPrice:(NSArray *)btnPrice btnids:(NSArray *)btnId witnIndex:(NSInteger)index{
    
    self.priceArray = btnPrice;
    self.idArray = btnId;
    
    self.titleLabel.text=title;
    
    self.tagView.frame=CGRectMake(0, self.titleLabel.frame.size.height+Default_Space, Screen_Width, [AddGoodTC getCellHeight:btnTitle]-self.titleLabel.frame.size.height-Default_Space*3);
    
    self.line.frame=CGRectMake(0,[AddGoodTC getCellHeight:btnTitle]-Default_Space,Screen_Width, Default_Space);
    
    for (UIView *itemView in self.tagView.subviews) {
        [itemView removeFromSuperview];
    }
    
    CGFloat evaTagNowX=0;
    CGFloat evaTagItemHeight=36;
    int evaTagNowLine=0;
//    NSMutableArray * newMuArray = [NSMutableArray new];
    for(int i=0;i<btnTitle.count;i++){
        UIFont *titleFont=[UIFont systemFontOfSize:FONT_SIZE_M];
        NSString *titleStr=[btnTitle objectAtIndex:i];
        CGFloat itemWidth=Default_Space+[NSString sizeWithText:titleStr font:titleFont maxSize:Max_Size].width+20+Default_Space;
        
        if(evaTagNowX+itemWidth>Screen_Width){
            evaTagNowLine+=1;
            evaTagNowX=0;
        }
        
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(evaTagNowX, evaTagNowLine*evaTagItemHeight, itemWidth, evaTagItemHeight)];
        view.backgroundColor=[UIColor clearColor];
        
        UIButton *title=[[UIButton alloc]initWithFrame:CGRectMake(Default_Space, 5, itemWidth-Default_Space*2, evaTagItemHeight-Default_Space)];

        [title setTitle:titleStr forState:UIControlStateNormal];
        title.titleLabel.font=titleFont;
        title.layer.cornerRadius=5;
        title.layer.borderWidth=Default_Line;
        title.layer.masksToBounds=YES;
        [title setTitleColor:APPFourColor forState:UIControlStateNormal];
        [title setTitleColor:APPColor forState:UIControlStateSelected];
        title.layer.borderColor=APPFourColor.CGColor;
        title.tag = 9000 + index*100 +i;
        [title addTarget:self action:@selector(titleOnClick:) forControlEvents:(UIControlEventTouchUpInside)];
        [view addSubview:title];
        [self.btnMuArray addObject:title];
        [self.tagView addSubview:view];
        evaTagNowX+=itemWidth;
    }
}

-(void)titleOnClick:(UIButton *)btn{

    if(btn.selected == YES){
        btn.selected = NO;
        [btn setTitleColor:APPFourColor forState:UIControlStateNormal];
        btn.layer.borderColor=APPFourColor.CGColor;
        
        NSInteger number = btn.tag%100;
        NSArray * subArray = @[self.priceArray[number],self.idArray[number]];
        
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"subPriceOn" object:subArray];
    }else{
        NSString * btnId = @"";
        NSNumber *btnPrice;
        NSNumber *oldPrice  = @0;
        NSString * oldId = @"";
        for (UIButton * subBtn in self.btnMuArray) {
            if (subBtn.selected == YES) {
                NSInteger number = subBtn.tag%100;
                oldPrice = self.priceArray[number];
                oldId = self.idArray[number];
            }
            subBtn.selected = NO;
            [subBtn setTitleColor:APPFourColor forState:UIControlStateNormal];
            subBtn.layer.borderColor = APPFourColor.CGColor;
        }
        NSInteger nowBtnTag  = btn.tag%100;
        btnId = self.idArray[nowBtnTag];
        btnPrice = self.priceArray[nowBtnTag];
        
        btn.selected = YES;
        btn.layer.borderColor=APPColor.CGColor;
     
        if(oldPrice == nil){
            oldPrice = @0;
        }
        NSArray * notiArray = @[btnId,btnPrice,oldPrice,oldId];
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"addPriceOn" object:notiArray];
    }
}

+(CGFloat)getCellHeight:(NSArray *)array{
    CGFloat tagNowX=0;
    CGFloat tagItemHeight=36;
    int tagNowLine=0;
    for(int i=0;i<array.count;i++){
        UIFont *titleFont=[UIFont systemFontOfSize:FONT_SIZE_M];
        NSString *titleStr=[array objectAtIndex:i];
        CGFloat itemWidth=Default_Space+[NSString sizeWithText:titleStr font:titleFont maxSize:Max_Size].width+20+Default_Space;
        
        if(tagNowX+itemWidth>Screen_Width){
            tagNowLine+=1;
            tagNowX=0;
        }
        tagNowX+=itemWidth;
    }
    
    return (tagNowLine+1)*tagItemHeight+Default_Space*3+20;
}

+(NSString *)getID{
    return @"AddGoodTC";
}
@end
