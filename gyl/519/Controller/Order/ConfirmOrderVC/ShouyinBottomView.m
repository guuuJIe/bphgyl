//
//  ShouyinBottomView.m
//  519
//
//  Created by Macmini on 2019/4/24.
//  Copyright © 2019 519. All rights reserved.
//

#import "ShouyinBottomView.h"

@interface ShouyinBottomView()
@property(nonatomic,strong)UIButton * collectionBtn;
@property(nonatomic,strong)UIButton * serviceBtn;
@property(nonatomic,strong)UIButton *payBtn;
@end

@implementation ShouyinBottomView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    _serviceBtn = [[UIButton alloc]init];
    _serviceBtn.backgroundColor = [UIColor whiteColor];
    [_serviceBtn setImage:[UIImage imageNamed:@"icon_service"] forState:(UIControlStateNormal)];
    [_serviceBtn setTitle:@"客服" forState:(UIControlStateNormal)];
    [_serviceBtn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
    _serviceBtn.titleLabel.font = [UIFont systemFontOfSize:9];
    _serviceBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 15, 17, 0);
    _serviceBtn.titleEdgeInsets = UIEdgeInsetsMake(17, 0, 0, 17);
    [_serviceBtn addTarget:self action:@selector(kefuAction) forControlEvents:(UIControlEventTouchUpInside)];
    _serviceBtn.tag = 2005;
    [self addSubview:_serviceBtn];
    [_serviceBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(0);
        make.top.offset(0);
        make.height.offset(49);
        make.width.offset(55);
    }];
    
    
    _collectionBtn = [[UIButton alloc]init];
    _collectionBtn.backgroundColor = [UIColor whiteColor];
    [_collectionBtn setImage:[UIImage imageNamed:@"icon_order"] forState:(UIControlStateNormal)];
    [_collectionBtn setTitle:@"订单" forState:(UIControlStateNormal)];
    [_collectionBtn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
    _collectionBtn.titleLabel.font = [UIFont systemFontOfSize:9];
    _collectionBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 15, 17, 0);
    _collectionBtn.titleEdgeInsets = UIEdgeInsetsMake(17, 0, 0, 17);
    [_collectionBtn addTarget:self action:@selector(orderClick) forControlEvents:(UIControlEventTouchUpInside)];
    _collectionBtn.tag = 2000;
    [self addSubview:_collectionBtn];
    [_collectionBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_serviceBtn.mas_right).offset(1);
        make.top.offset(0);
        make.width.offset(55);
        make.height.offset(49);
    }];
    
    
    _payBtn = [[UIButton alloc]init];
    [_payBtn setBackgroundColor:APPColor];
    [_payBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_payBtn setTitle:@"确认" forState:UIControlStateNormal];
    _payBtn.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    [_payBtn addTarget:self action:@selector(payBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_payBtn];
    [_payBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_collectionBtn.mas_right);
        make.right.offset(0);
         make.top.offset(0);
        make.height.offset(50);
      
    }];
}

- (void)kefuAction{
    if (self.kefu) {
        self.kefu();
    }
}

- (void)orderClick{
    if (self.order) {
        self.order();
    }
}

- (void)payBtnOnClick{
    if (self.submitOrder) {
        self.submitOrder();
    }
}

@end
