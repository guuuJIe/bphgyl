//
//  AddressView.m
//  519
//
//  Created by Macmini on 2017/12/5.
//  Copyright © 2017年 519. All rights reserved.
//

#import "AddressView.h"


@implementation AddressView

-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
        self.backgroundColor = [UIColor whiteColor];
        [self createUI];
    }
    return self;
}

-(void)createUI{
    UIImageView * locationImgV = [[UIImageView alloc]init];
    locationImgV.image = [UIImage imageNamed:@"location_black_icon"];
    [self addSubview:locationImgV];
    [locationImgV makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).with.mas_offset(10);
        make.centerY.equalTo(self.mas_centerY);
        make.width.and.height.equalTo(25);
    }];
    
    
    _namelabel = [[UILabel alloc]init];
    _namelabel.textColor = APPFourColor;
    _namelabel.textAlignment = NSTextAlignmentLeft;
    _namelabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:_namelabel];
    [_namelabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(locationImgV.mas_right).with.mas_offset(10);
        make.top.mas_equalTo(self.mas_top).with.mas_offset(10);
        make.height.equalTo(30);
    }];
    
    
    _phoneLabel = [[UILabel alloc]init];
    _phoneLabel.textColor = APPFourColor;
    _phoneLabel.textAlignment = NSTextAlignmentRight;
    _phoneLabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:_phoneLabel];
    [_phoneLabel makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).with.mas_offset(-50);
        make.top.mas_equalTo(self.mas_top).with.mas_offset(10);
        make.height.equalTo(30);
    }];
    
    
    _addressLabel = [[UILabel alloc]init];
    _addressLabel.textColor = APPFourColor;
    _addressLabel.textAlignment = NSTextAlignmentLeft;
    _addressLabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:_addressLabel];
    [_addressLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(locationImgV.mas_right).with.mas_offset(10);
        make.top.mas_equalTo(_namelabel.mas_bottom).with.mas_offset(0);
        make.height.equalTo(30);
    }];
    
    _neverAddresslLbel = [[UILabel alloc]init];
    _neverAddresslLbel.textAlignment = NSTextAlignmentLeft;
    _neverAddresslLbel.text = @"新增收货地址";
    _neverAddresslLbel.font = [UIFont systemFontOfSize:14];
    _neverAddresslLbel.textColor = APPFourColor;
    _neverAddresslLbel.hidden = YES;
    [self addSubview:_neverAddresslLbel];
    [_neverAddresslLbel makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(locationImgV.mas_right).with.mas_offset(10);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.height.equalTo(30);
    }];
    
    
    UIImageView * rightImageV = [[UIImageView alloc]init];
    rightImageV.image = [UIImage imageNamed:@"right_black_icon"];
    [self addSubview:rightImageV];
    [rightImageV makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).with.mas_offset(-10);
        make.centerY.equalTo(self.mas_centerY);
        make.width.and.height.equalTo(25);
    }];
    
    UIImageView * downImageV = [[UIImageView alloc]init];
    downImageV.backgroundColor=[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"address_line"]];
    [self addSubview:downImageV];
    [downImageV makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.bottom.right.equalTo(0);
        make.height.equalTo(3);
    }];
    
    
}
@end
