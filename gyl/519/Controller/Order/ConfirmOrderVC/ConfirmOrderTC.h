//
//  ConfirmOrderTC.h
//  519
//
//  Created by Macmini on 2017/12/5.
//  Copyright © 2017年 519. All rights reserved.
//

typedef void(^changeDeliveryBlock)(NSArray * idArray,NSArray * nameArray,NSString * supplierId);
typedef void(^endEditBlock)(NSString * supplierId,NSString * content);
typedef void(^clickCellTagBlock)(NSInteger tag,BOOL isFold);//点击tf回调
typedef void(^foldBlock)(BOOL isFold,NSInteger goodsnum);//折叠回调
#import <UIKit/UIKit.h>
#import "ConfirmOrderModel.h"
@interface ConfirmOrderTC : UITableViewCell
@property(nonatomic,copy)changeDeliveryBlock block;
@property(nonatomic,copy)endEditBlock editblock;
@property(nonatomic,copy)clickCellTagBlock tagblock;
@property(nonatomic,copy)foldBlock foldBlock;
@property(nonatomic,strong)CartListGroupModel * model;

@property(nonatomic,strong)UIImageView * titleImageV;//选择btn
@property(nonatomic,strong)UITableView * tableViewtc;//商品列表
@property(nonatomic,strong)UILabel * storeNameLabel;//门店名
@property(nonatomic,strong)UIButton * sendWayBtn;//配送方式
@property(nonatomic,strong)UITextField * textField;//备注
@property(nonatomic,strong)UILabel * priceLabel;    //最后价格
@property(nonatomic,strong)UILabel * postalLabel;//邮费


@property(nonatomic,strong)NSArray * supplierIdArr;
@property (nonatomic,copy)NSString * supplierId;
@property(nonatomic,assign) NSInteger is_score;//是否是积分商品
@end
