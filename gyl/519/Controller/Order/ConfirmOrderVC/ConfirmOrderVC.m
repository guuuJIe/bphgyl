//
//  ConfirmOrderVC.m
//  519
//
//  Created by Macmini on 2017/12/5.
//  Copyright © 2017年 519. All rights reserved.
//
 static NSString * cellIde =@"confirmtcIde";
#import "ConfirmOrderVC.h"
#import "AddressView.h"
#import "ConfirmOrderModel.h"
#import "ConfirmOrderTC.h"
#import "DeliveryView.h"
#import "AddressVC.h"
#import "AddressEditVC.h"
#import "OrderPayVC.h"
#import "PayResultVC.h"
@interface ConfirmOrderVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSString * _totalPrice;
    NSString * _buyNowId;
    NSString * _buyNowNumber;
    NSString * _buynowAttr;
    
}
@property (nonatomic,strong)AddressView * addressView;
@property (nonatomic,strong)DeliveryView * deliveryView;
@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,strong)NSMutableArray * dataArray;//所有数据
@property (nonatomic,strong)NSMutableArray * cartList;//门店
@property (nonatomic,strong)NSMutableArray * cartList2;//门店
@property (nonatomic,strong)UIView * endView;//最后提交订单view
@property (nonatomic,strong)UILabel * totalPriceLabel;

@property (nonatomic,copy)NSString * allPrice;//总价
@property(nonatomic,strong)NSMutableArray * idMuarray;
@property(nonatomic,strong)NSMutableArray * contentArray;
@property(nonatomic,strong)NSMutableArray * supplierArr;
@property(nonatomic,strong)NSMutableArray * yfArr;
@property(nonatomic,strong)NSMutableDictionary * mudic;


@property(nonatomic,assign)NSInteger cellNum;
@property(nonatomic,assign)BOOL isfold;
@property(nonatomic,copy)NSString * totalPrice;

@property(nonatomic,assign)NSInteger is_score;//是否是积分商品
@end

@implementation ConfirmOrderVC


- (void)viewWillAppear:(BOOL)animated{
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = APPBGColor;
    self.navigationItem.title = @"提交订单";
    [self createUI];
    [self requestOrderData];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(keyboardWillShow:)
                                                name:UIKeyboardWillShowNotification
                                              object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(keyboarkDidShow:)
                                                name:UIKeyboardWillHideNotification
                                              object:nil];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)createUI{
    self.cartList = [NSMutableArray new];
    self.cartList2 = [NSMutableArray new];
    
    self.idMuarray = [NSMutableArray new];
    self.contentArray = [NSMutableArray new];
    self.supplierArr = [NSMutableArray new];
    self.yfArr = [NSMutableArray new];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.endView];
    [self.endView makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.bottom.offset(0);
        make.height.offset(50+BottomAreaHeight);
    }];
}

-(void)requestOrderData{
    _buyNowId = [NSString stringWithFormat:@"%@",_dic[@"buynow_id"]];
    _buyNowNumber = [NSString stringWithFormat:@"%@",_dic[@"buynow_number"]];
    _buynowAttr = [NSString stringWithFormat:@"%@",_dic[@"buynow_attr"]];
    _is_score = [_dic[@"is_score"] integerValue];
    //订单地址
    if (![_dic[@"consignee_info"]isEqual:[NSNull null]]) {
        _addressView.namelabel.text = _dic[@"consignee_info"][@"consignee"];
        _addressView.phoneLabel.text = _dic[@"consignee_info"][@"mobile"];
        NSString * lv2 = _dic[@"consignee_info"][@"region_lv2_name"];
        NSString * lv3 = _dic[@"consignee_info"][@"region_lv3_name"];
        NSString * lv4 = _dic[@"consignee_info"][@"region_lv4_name"];
        NSString * address = _dic[@"consignee_info"][@"address"];
        _addressView.addressLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@",lv2,lv3,lv4,address];
    }else{
        _addressView.neverAddresslLbel.hidden = NO;
    }
    
    
    //商品内容
    NSDictionary * subdic = _dic[@"cart_list_group"];
    NSArray * keyArr = subdic.allKeys;
    for (NSInteger i = 0; i<keyArr.count; i++) {
        NSString * key = keyArr[i];
        NSDictionary * subdic2 = subdic[key];
        
        CartListGroupModel * model = [[CartListGroupModel alloc]init];
        model.goods_list = subdic2[@"goods_list"];
        model.delivery_list = subdic2[@"delivery_list"];
        model.supplier = subdic2[@"supplier"];
        model.supplier_id = subdic2[@"supplier_id"];
        model.total_price = subdic2[@"total_price"];
        [_cartList addObject:model];
        [self.supplierArr addObject:model.supplier_id];
    }
    
   
    //是积分商品
    if (_is_score == 1) {
        _allPrice = [NSString stringWithFormat:@"%@",_dic[@"total_data"][@"return_total_score"]];
        _totalPriceLabel.text =[NSString stringWithFormat:@"总价: %.2lf积分",[_allPrice doubleValue]];
    }else{
        //总价
         _allPrice = [NSString stringWithFormat:@"%@",_dic[@"total_data"][@"total_price"]];
        _totalPriceLabel.text =[NSString stringWithFormat:@"总价: ￥%.2lf",[_allPrice doubleValue]];
    }
    
   
    [_tableView reloadData];
}

#pragma  mark -- tableViewdelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.cartList.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    ConfirmOrderTC * cell = [tableView dequeueReusableCellWithIdentifier:cellIde];
    [cell setSelectionStyle:(UITableViewCellSelectionStyleNone)];
    if (cell == nil) {
        cell = [[ConfirmOrderTC alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:cellIde];
    }
    cell.tag = 3200+indexPath.row;
    CartListGroupModel * model = _cartList[indexPath.row];
    cell.model = model;
    
    //选择配送方式
    __weak typeof(self)weself = self;
    __weak typeof(ConfirmOrderTC)*weakcell = cell;
    weakcell.block = ^(NSArray *idArray, NSArray *nameArray, NSString * supplierid) {
    weself.deliveryView = [[DeliveryView alloc]initWithFrame:weself.view.bounds
                                                  deliveryId:idArray
                                            withDeliveryName:nameArray
                                              withSupplierId:model.supplier_id
                                                      goodId:_buyNowId
                                                     goodNum:_buyNowNumber];
        weself.deliveryView.buyNow = self.isbuynow;
//        weself.deliveryView.Ids = [NSString stringWithFormat:@"%@",model.ids];
        [weself.view addSubview:_deliveryView];
        //选择回调
        weself.deliveryView.block = ^(NSInteger num, NSString * postalPrice,id nowTotalPrice) {
            NSString * supplierid = [NSString stringWithFormat:@"%@",weself.supplierArr[indexPath.row]];
            NSString * deliceryId = [NSString stringWithFormat:@"%@",idArray[num]];
            if (weself.idMuarray.count>0) {
                for (NSInteger i = 0; i<weself.idMuarray.count; i++) {
                    NSArray * subarr = weself.idMuarray[i];
                    if ([subarr[0] isEqualToString:supplierid]) {
                        [weself.idMuarray removeObject:subarr];
                        [weself.yfArr removeObject:_yfArr[i]];
                    }
                }
            }
            [weself.yfArr addObject:@[supplierid, postalPrice]];
            [weself.idMuarray addObject:@[supplierid,@{@"delivery_id":deliceryId}]];
            [weself.deliveryView removeFromSuperview];
            [weakcell.sendWayBtn setTitle:[NSString stringWithFormat:@"%@",nameArray[num]] forState:(UIControlStateNormal)];
            [weakcell.sendWayBtn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
            weakcell.postalLabel.text = postalPrice;
            
            
            //计算总价
            float allprice = [_allPrice floatValue];
            for (NSInteger i = 0 ; i<_yfArr.count; i++) {
                NSArray * sunArr = _yfArr[i];
                float yf = [sunArr[1] floatValue];
                NSLog(@"lf  %lf",yf);
                allprice += yf;
            }
            if (_is_score == 1) {
                
            }else{
                weakcell.priceLabel.text = [NSString stringWithFormat:@"￥%.2lf",[nowTotalPrice doubleValue]];
                 weself.totalPriceLabel.text =[NSString stringWithFormat:@"总价: ￥%.2lf",allprice];
            }
           
        };
    };
    //备注编辑回调
    weakcell.editblock = ^(NSString *supplierId, NSString *content) {
        if (weself.contentArray.count>0) {
            for (NSInteger i = 0; i<weself.contentArray.count; i++) {
                NSArray * subarr = weself.contentArray[i];
                if ([subarr[0] isEqualToString:supplierId]) {
                    [weself.contentArray removeObject:subarr];
                }
            }
        }
        [weself.contentArray addObject:@[supplierId,@{@"content":content}]];
    };
    
    weakcell.tagblock = ^(NSInteger tag , BOOL isfold) {
        weself.cellNum = tag-3200;
        weself.isfold = isfold;
    };
    
    weakcell.foldBlock = ^(BOOL isFold,NSInteger goodsnum) {
         weself.isfold = isFold;
//        if (isFold) {
//            weakcell.height = 100*goodsnum+260;
//        }else{
//
//        }
        
        [weself.tableView reloadData];
    };
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CartListGroupModel * model = _cartList[indexPath.row];
    NSArray * goodlist = model.goods_list;
    
    if (self.isfold) {
        return 100*goodlist.count+350;
    }
    
    if (goodlist.count >3) {

         return 100*3+350;
    }else{

         return 100*goodlist.count+350;
    }
   
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%ld",(long)indexPath.row);
}


#pragma  mark -- 事件点击
//提交订单
-(void)clickUploadBtn:(UIButton *)btn{
    
    [MBProgressHUD showHUD];
    NSMutableArray * array = [NSMutableArray new];
    for (NSArray * idArr in _idMuarray) {
        [array addObject:idArr];
    }
    for (NSArray * content in _contentArray) {
        [array addObject:content];
    }
    
    NSMutableDictionary * idDic = [NSMutableDictionary dictionary];
    NSMutableArray * dicArr = [NSMutableArray new];
    
    for (NSInteger i = 0; i<_supplierArr.count; i++) {
        NSString * ids = [NSString stringWithFormat:@"%@",_supplierArr[i]];
        NSMutableDictionary * mutabDic = [NSMutableDictionary dictionary];
        for (NSArray * subArr in array) {
            NSString * subIds = [NSString stringWithFormat:@"%@",subArr[0]];
            if ([ids isEqualToString:subIds]) {
                [mutabDic addEntriesFromDictionary:subArr[1]];
            }
        }
        [dicArr addObject:mutabDic];
    }
    
    for (NSInteger i = 0; i<dicArr.count; i++) {
        NSString * ids = [NSString stringWithFormat:@"%@",_supplierArr[i]];
        [idDic setObject:dicArr[i] forKey:ids];
    }
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [self uploadSubmit:[self dictionaryToJson:idDic]];
    });
    
}

//点击地址view跳转
-(void)clickAddressView{
    if (_addressView.neverAddresslLbel.hidden == NO) {
        AddressEditVC * vc = [AddressEditVC new];
        vc.isedit = @"0";
        vc.isBuy = @"1";
        vc.typeStr=@"添加地址";
        __weak typeof (self)weself = self;
        vc.pushBlock = ^(NSString * name,NSString * phone,NSString * address){
            weself.addressView.neverAddresslLbel.hidden=YES;
            weself.addressView.namelabel.text=name;
            weself.addressView.addressLabel.text=address;
            weself.addressView.phoneLabel.text = phone;
        };
        
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        AddressVC * vc = [AddressVC new];
        vc.pushStr = @"0";
        __weak typeof (self)weself = self;
        vc.pushBlock = ^(NSString * name,NSString * phone,NSString * address,NSString * ids){
            weself.addressView.namelabel.text=name;
            weself.addressView.addressLabel.text=address;
            weself.addressView.phoneLabel.text = phone;
        };
        [self.navigationController pushViewController:vc animated:YES];
    }
}

//提交订单接口请求
-(void)uploadSubmit:(NSString *)submit{
    NSString * email = [kUserDefaults objectForKey:@"email"];
    NSString * pwd = [kUserDefaults objectForKey:@"user_pwd"];
    
    NSString * submitUrl ;
    if (_isbuynow) {
        submitUrl = [NSString stringWithFormat:@"%@ctl=cart&act=done&params_list=%@&buynow_id=%@&buynow_attr=%@&buynow_number=%@&email=%@&pwd=%@",GYLUrl,submit,_buyNowId,_buynowAttr,_buyNowNumber,email,pwd];
    }else{
        submitUrl = [NSString stringWithFormat:@"%@ctl=cart&act=done&params_list=%@&email=%@&pwd=%@",GYLUrl,submit,email,pwd];
    }
    [RequestData requestDataOfUrl:submitUrl success:^(NSDictionary *dic) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([dic[@"status"]  isEqual: @1]) {
                if ([dic[@"pay_status"] isEqual:@1]) {
                    PayResultVC *vc = [PayResultVC new];
                    vc.number = @"2";
                    vc.price = self.totalPriceLabel.text;
                    [self.navigationController pushViewController:vc animated:true];
                    
                    
                }else{
                    OrderPayVC * vc = [OrderPayVC new];
                    vc.orderId = [NSString stringWithFormat:@"%@",dic[@"order_id"]];
                    if (!_isbuynow) {
                        vc.paytype = @"carPay";
                    }
                    vc.isFromJump = self.isJump;
                    [self.navigationController pushViewController:vc animated:YES];
                    
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshCart" object:nil];
                }
                
           
                
            }else{
                [MBProgressHUD showError:dic[@"info"] toView:self.view];
            }
            [MBProgressHUD dissmiss];
        });
    } failure:^(NSError *error) {
        [MBProgressHUD dissmiss];
    }];
}

- (NSString*)dictionaryToJson:(NSDictionary *)dic
{
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}
#pragma mark -- 键盘弹出隐藏
- (void)keyboardWillShow:(NSNotification *)notification{
    
//    if (self.isfold) {
//        CGRect keyboardFrame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
//        CGFloat height = keyboardFrame.origin.y-50;
//
//        NSInteger tcCellNum = 0;
//
//        CGFloat textField_maxY = 220*(0+1)+100*tcCellNum+100-30;
//        CGFloat space = - self.tableView.contentOffset.y + textField_maxY;
//        CGFloat transformY = height - space;
//        if (transformY < 0) {
//            CGRect frame = self.tableView.frame;
//            frame.origin.y = transformY ;
//            self.tableView.frame = frame;
//        }
//    }else{
//        CGRect keyboardFrame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
//        CGFloat height = keyboardFrame.origin.y-50;
//
//        NSInteger tcCellNum = 0;
//        for (NSInteger i = 0; i<=_cellNum; i++) {
//            CartListGroupModel * model = _cartList[i];
//            NSArray * goodlist = model.goods_list;
//            tcCellNum += goodlist.count;
//        }
//        CGFloat textField_maxY = 220*(_cellNum+1)+100*tcCellNum+100-30;
//        CGFloat space = - self.tableView.contentOffset.y + textField_maxY;
//        CGFloat transformY = height - space;
//        if (transformY < 0) {
//            CGRect frame = self.tableView.frame;
//            frame.origin.y = transformY ;
//            self.tableView.frame = frame;
//        }
//    }
    
//    CGRect keyboardFrame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
//    CGFloat height = keyboardFrame.origin.y-50;
    
//    CGRect keyboardRect = [aValue CGRectValue];
//    int height = keyboardRect.size.height;
    //    NSInteger tcCellNum = 0;
//
//    CGFloat textField_maxY = 220*(0+1)+100*tcCellNum+100-30;
//    CGFloat space = - self.tableView.contentOffset.y + textField_maxY;
//    CGFloat transformY = height - space;
//    if (transformY < 0) {
    //获取键盘的高度
    NSDictionary *userInfo = [notification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    int height = keyboardRect.size.height;
        CGRect frame = self.tableView.frame;
        frame.origin.y = -height ;
        self.tableView.frame = frame;

    
}
- (void)keyboarkDidShow:(NSNotification *)notification {
    CGRect frame = self.tableView.frame;
    frame.origin.y = 0;
    self.tableView.frame = frame;
}

#pragma mark -- 懒加载
-(AddressView *)addressView{
    if (!_addressView) {
        _addressView = [[AddressView alloc]initWithFrame:CGRectMake(0, 10, Screen_Width, 80)];
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickAddressView)];
        [_addressView addGestureRecognizer:tap];
    }
    return _addressView;
}

-(UITableView *)tableView{
    if (!_tableView) {
        if (_isbuynow) {
            _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight-50-BottomAreaHeight) style:(UITableViewStylePlain)];
        }else{
            _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-50-BottomAreaHeight) style:(UITableViewStylePlain)];
        }
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = APPBGColor;
        _tableView.separatorStyle = UITableViewScrollPositionNone;
        [_tableView registerClass:[ConfirmOrderTC class] forCellReuseIdentifier:cellIde];
        _tableView.tableHeaderView = self.addressView;
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        NSLog(@"%f",_tableView.frame.size.height);
    }
    return _tableView;
}
-(UIView *)endView{
    if (!_endView) {
//        if (_isbuynow) {
//            if ([self.isJump isEqualToString:@"jumpFromTypeAndCar"]) {
//                _endView = [[UIView alloc]initWithFrame:CGRectMake(0, Screen_Height-50, Screen_Width, 50)];
//            }else{
//                _endView = [[UIView alloc]initWithFrame:CGRectMake(0, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight-50, Screen_Width, 50)];
//            }
//        }else{
//            _endView = [[UIView alloc]initWithFrame:CGRectMake(0, Screen_Height-50, Screen_Width, 50)];
//        }
//
        _endView = [[UIView alloc]init];
        _endView.backgroundColor = [UIColor whiteColor];
        _totalPriceLabel = [[UILabel alloc]init];
        _totalPriceLabel.text = @"总价:";
        _totalPriceLabel.textAlignment = NSTextAlignmentCenter;
        _totalPriceLabel.textColor = APPFourColor;
        _totalPriceLabel.font = [UIFont systemFontOfSize:15];
        [_endView addSubview:_totalPriceLabel];
        [_totalPriceLabel makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.offset(0);
            make.height.offset(50);
            make.width.offset(Screen_Width/3*2);
        }];
        
        UIButton * uploadBtn = [[UIButton alloc]init];
        [uploadBtn setTitle:@"提交订单" forState:(UIControlStateNormal)];
        [uploadBtn setTitleColor: [UIColor whiteColor] forState:(UIControlStateNormal)];
        [uploadBtn setBackgroundColor:APPColor];
        uploadBtn.tag = 3010;
        [uploadBtn addTarget:self action:@selector(clickUploadBtn:) forControlEvents:(UIControlEventTouchUpInside)];
        [_endView addSubview:uploadBtn];
        [uploadBtn makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_totalPriceLabel.mas_right).offset(0);
            make.top.offset(0);
            make.width.offset(Screen_Width/3);
            make.height.offset(50);
        }];
    }
    return _endView;
}



@end
