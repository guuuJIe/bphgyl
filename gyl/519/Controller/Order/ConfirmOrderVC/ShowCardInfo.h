//
//  ShowCardInfo.h
//  519
//
//  Created by Macmini on 2019/8/15.
//  Copyright © 2019 519. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShowCardInfo : UIView

- (void)show;

@property (nonatomic) UIViewController *contrc;

- (void)dimiss;

- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
