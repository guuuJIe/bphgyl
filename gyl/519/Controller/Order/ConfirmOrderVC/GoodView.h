//
//  GoodView.h
//  519
//
//  Created by Macmini on 2019/5/11.
//  Copyright © 2019 519. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^foldBlock)(BOOL isFold,NSInteger goodsnum);//折叠回调
NS_ASSUME_NONNULL_BEGIN

@interface GoodView : UIView

@property (nonatomic,strong) NSMutableArray *goodsList;
@property (nonatomic,copy) foldBlock fold;

@end

NS_ASSUME_NONNULL_END
