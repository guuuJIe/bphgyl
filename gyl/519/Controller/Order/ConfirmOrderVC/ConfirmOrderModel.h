//
//  ConfirmOrderModel.h
//  519
//
//  Created by Macmini on 2017/12/5.
//  Copyright © 2017年 519. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConfirmOrderModel : NSObject
@property(nonatomic,copy)NSString * status;
@property(nonatomic,copy)NSArray * cart_list_group;
@property(nonatomic,copy)NSArray * total_data;
@property(nonatomic,copy)NSString * is_score;
@property(nonatomic,copy)NSArray  * consignee_info;//地址


@end

//delivery_list
@interface DeliveryListModel:NSObject
@property (nonatomic,strong)NSArray * Id;
@property (nonatomic,strong)NSArray * name;

@end


@interface CartListGroupModel:NSObject

@property (nonatomic,strong)NSArray  * goods_list;
@property (nonatomic,copy)NSString * supplier_id;
@property (nonatomic,copy)NSString * supplier;
@property (nonatomic,strong)NSArray * delivery_list;
@property (nonatomic,copy)NSString * total_price;
@property (nonatomic,copy)NSString * return_total_score;
@end

@interface GoodsListModel:NSObject
@property (nonatomic,copy)NSString * return_score;
@property (nonatomic,copy)NSString * return_total_score;
@property (nonatomic,copy)NSString * unit_price;
@property (nonatomic,copy)NSString * total_price;
@property (nonatomic,copy)NSString * number;
@property (nonatomic,copy)NSString * deal_id;
@property (nonatomic,copy)NSString * attr_str;
@property (nonatomic,copy)NSString * name;
@property (nonatomic,copy)NSString * max;
@property (nonatomic,copy)NSString * supplier_id;
@property (nonatomic,copy)NSString * icon;
@property (nonatomic,copy)NSString * Id;
@end
