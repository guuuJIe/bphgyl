//
//  GoodView.m
//  519
//
//  Created by Macmini on 2019/5/11.
//  Copyright © 2019 519. All rights reserved.
//

#import "GoodView.h"
#import "ConfirmOrderTCTC.h"
@interface GoodView()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)UITableView *tableViewtc;
@property (nonatomic,strong)UIButton *foldButton;
@property (nonatomic,assign) BOOL isClick;
@end

@implementation GoodView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self initView];
    }
    return self;
}

- (void)initView{
    _tableViewtc = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 0)];
    _tableViewtc.delegate = self;
    _tableViewtc.dataSource = self;
    _tableViewtc.separatorStyle = UITableViewScrollPositionNone;
    _tableViewtc.showsHorizontalScrollIndicator =  YES;
    _tableViewtc.showsVerticalScrollIndicator = YES;
    _tableViewtc.scrollEnabled = NO;
    _tableViewtc.backgroundColor = [UIColor whiteColor];
    [_tableViewtc registerNib:[UINib nibWithNibName:@"ConfirmOrderTCTC" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"confirmorderTCTCIde"];
    [self addSubview:_tableViewtc];
//    [_tableViewtc mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.equalTo(self);
//    }];

    UIButton *button = [[UIButton alloc] init];
    [button setBackgroundColor:[UIColor whiteColor]];
    [button setTitle:@"查看更多" forState:0];
    [button setImage:[UIImage imageNamed:@"down_icon"] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:0];
    [button.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [button setTitle:@"立即收起" forState:UIControlStateSelected];
    [button setImage:[UIImage imageNamed:@"up_icon"] forState:UIControlStateSelected];
    [button addTarget:self action:@selector(reload:) forControlEvents:UIControlEventTouchUpInside];
    
    [button setImageEdgeInsets:UIEdgeInsetsMake(0, 140, 0, 0)];
    [button setTitleEdgeInsets:UIEdgeInsetsMake(0, -35, 0, 0)];
    [button.titleLabel setTextAlignment:NSTextAlignmentCenter];
    _foldButton = button;
    [self addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_tableViewtc.mas_bottom).with.mas_offset(1);
        make.left.and.right.mas_equalTo(self).with.mas_offset(0);
        make.height.equalTo(40);
    }];
    
    
}


- (void)reload:(UIButton *)sender{
    
    sender.selected = !sender.selected;
    
    if(sender.selected){
        self.tableViewtc.frame = CGRectMake(0, 0, Screen_Width, _goodsList.count*100);
    }else{
        self.tableViewtc.frame = CGRectMake(0, 0, Screen_Width, 3*100);
    }
    
    
   
    
    
    self.isClick = sender.selected;
    if (self.fold) {
        self.fold(self.isClick, _goodsList.count);
    }
    [self.tableViewtc reloadData];
}


- (void)setGoodsList:(NSMutableArray *)goodsList{
    
    _goodsList = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *subArray = [NSMutableArray arrayWithCapacity:0];
    for (NSDictionary *dic in goodsList) {
         GoodsListModel * model = [[GoodsListModel alloc]init];
        [model setValuesForKeysWithDictionary:dic];
        [subArray addObject:model];
    }
    
    _goodsList = subArray;
    if (goodsList.count>3) {
        self.foldButton.hidden = false;

        self.tableViewtc.frame = CGRectMake(0, 0, Screen_Width, 300);
    }else{
        self.foldButton.hidden = true;
        self.tableViewtc.frame = CGRectMake(0, 0, Screen_Width, goodsList.count*100);
    }
    
   
    [self.tableViewtc reloadData];
    
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _isClick ? _goodsList.count : _goodsList.count > 3 ? 3:_goodsList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * cellIde = @"confirmorderTCTCIde";
    ConfirmOrderTCTC * cell = [tableView dequeueReusableCellWithIdentifier:cellIde];
    [cell setSelectionStyle:(UITableViewCellSelectionStyleNone)];
    if (cell == nil) {
        cell = [[ConfirmOrderTCTC alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:cellIde];
    }
    GoodsListModel * model = _goodsList[indexPath.row];
    cell.listmodel = model;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

@end
