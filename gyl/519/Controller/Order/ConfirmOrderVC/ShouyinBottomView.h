//
//  ShouyinBottomView.h
//  519
//
//  Created by Macmini on 2019/4/24.
//  Copyright © 2019 519. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^kefuBlock)(void);
typedef void(^orderBlock)(void);
typedef void(^submitOrderBlock)(void);
NS_ASSUME_NONNULL_BEGIN

@interface ShouyinBottomView : UIView

@property (nonatomic,copy)kefuBlock kefu;

@property (nonatomic,copy)orderBlock order;

@property (nonatomic,copy)submitOrderBlock submitOrder;
@end

NS_ASSUME_NONNULL_END
