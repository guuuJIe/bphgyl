//
//  ConfirmOrderTCTC.h
//  519
//
//  Created by Macmini on 2017/12/5.
//  Copyright © 2017年 519. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConfirmOrderModel.h"
@interface ConfirmOrderTCTC : UITableViewCell
@property (nonatomic,strong)GoodsListModel * listmodel;

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *goodTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *onePriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@end
