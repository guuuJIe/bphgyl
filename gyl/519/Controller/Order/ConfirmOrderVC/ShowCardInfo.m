//
//  ShowCardInfo.m
//  519
//
//  Created by Macmini on 2019/8/15.
//  Copyright © 2019 519. All rights reserved.
//

#import "ShowCardInfo.h"

@interface ShowCardInfo()
@property (nonatomic) UIView *infoView;
@property (nonatomic,strong) UILabel *carlbl;
@end

@implementation ShowCardInfo

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
       self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.55] ;
        [self initLayout];
        
    }
    
    return self;
    
}

- (void)initLayout{
    
    self.infoView = [[UIView alloc] initWithFrame:CGRectMake(35, self.frame.size.height, self.frame.size.width - 70, 290)];
    self.infoView.backgroundColor = [UIColor whiteColor];
    self.infoView.layer.cornerRadius = 10;
    self.infoView.layer.masksToBounds = true;
    
    [self addSubview:self.infoView];
    self.carlbl = [[UILabel alloc] init];
    [self.infoView addSubview:self.carlbl];
    self.carlbl.textColor = APPFourColor;
    self.carlbl.font = [UIFont systemFontOfSize:14];
    self.carlbl.numberOfLines = 0;
    [self.carlbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(12);
        make.centerY.equalTo(self.infoView);
    }];
    
    UIButton *button = [[UIButton alloc] init];
    [button setImage:[UIImage imageNamed:@"Exit"] forState:0];
    button.userInteractionEnabled = false;
    [self addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.infoView.mas_bottom).offset(20);
        make.centerX.mas_equalTo(self);
    }];
    
}


- (void)setupData:(NSDictionary *)dic{
    
    NSString *urlString = dic[@"memo"];
    NSString *str = [urlString stringByReplacingOccurrencesOfString:@"\n" withString:@"\r\n"];
    self.carlbl.text = str;
    
}


- (void)initAnimation{
    
   
    [UIView animateWithDuration:0.5 animations:^{
        //将view.frame 设置在屏幕上方
        self.infoView.frame = CGRectMake(35, self.frame.size.height/2 - 150, self.frame.size.width-70, 290);
    }];
}

-(void)exitAni{
   
    [UIView animateWithDuration:0.5 animations:^{
        self.infoView.frame = CGRectMake(35, self.frame.size.height, self.frame.size.width - 70, 290);
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
        }
    }];
}


- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
   
    [self exitAni];
    
}

- (void)show{
    
    [_contrc.view addSubview:self];
    [self initAnimation];
    
}


@end
