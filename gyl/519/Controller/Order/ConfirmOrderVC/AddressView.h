//
//  AddressView.h
//  519
//
//  Created by Macmini on 2017/12/5.
//  Copyright © 2017年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressView : UIView
@property (nonatomic,strong)UILabel * namelabel;
@property (nonatomic,strong)UILabel * phoneLabel;
@property (nonatomic,strong)UILabel * addressLabel;

@property (nonatomic,strong)UILabel * neverAddresslLbel;
@end
