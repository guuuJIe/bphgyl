//
//  DeliveryView.m
//  519
//
//  Created by Macmini on 2017/12/6.
//  Copyright © 2017年 519. All rights reserved.
//

#import "DeliveryView.h"

@implementation DeliveryView

-(instancetype)initWithFrame:(CGRect)frame deliveryId:(NSArray *)deliveryId withDeliveryName:(NSArray *)deliveryName withSupplierId:(NSString *)supplierId goodId:(NSString *)goodId goodNum:(NSString *)goodsNum{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.5];
        self.deliveryName = deliveryName;
        self.deliveryId = deliveryId;
        self.supplierId = supplierId;
        self.Ids = goodId;
        self.num = goodsNum;
        [self createUI];
    }
    return self;
}
-(void)createUI{
    
    UIView * whiteView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 50*self.deliveryId.count+self.deliveryId.count)];
    whiteView.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    whiteView.backgroundColor = [UIColor whiteColor];
    [self addSubview:whiteView];
    
    for (NSInteger i=0; i<self.deliveryId.count; i++) {
        UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 50*i+1, self.frame.size.width, 50)];
        [btn setTitle:self.deliveryName[i] forState:(UIControlStateNormal)];
        [btn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
        [btn setBackgroundColor:[UIColor whiteColor]];
        btn.tag = 4010+i;
        [btn addTarget:self action:@selector(clickBtn:) forControlEvents:(UIControlEventTouchUpInside)];
        [whiteView addSubview:btn];
    }
    
}

-(void)clickBtn:(UIButton *)btn{
    NSInteger i = btn.tag-4010;
    [self requestDeliveryPrice:i];
}

-(void)requestDeliveryPrice:(NSInteger)num{
    NSString * email = [kUserDefaults objectForKey:@"email"];
    NSString * pwd = [kUserDefaults objectForKey:@"user_pwd"];
    NSString * freightUrl ;
    if (!_buyNow) {
        freightUrl =[NSString stringWithFormat:@"%@ctl=cart&act=count_buy_total&delivery_id=%@&supplier_id=%@&email=%@&pwd=%@",GYLUrl,_deliveryId[num],_supplierId,email,pwd];
    }else{
        freightUrl =[NSString stringWithFormat:@"%@ctl=cart&act=count_buy_total&delivery_id=%@&supplier_id=%@&buynow_id=%@&buynow_attr=%@&buynow_number=%@&email=%@&pwd=%@",GYLUrl,_deliveryId[num],_supplierId,self.Ids,@"",self.num,email,pwd];
    }
    [RequestData requestDataOfUrl:freightUrl success:^(NSDictionary *dic) {
        NSLog(@"%@",dic);
        NSString * freight = [NSString stringWithFormat:@"%@",dic[@"delivery_fee"]];
        NSString * isSupport = [NSString stringWithFormat:@"%@",dic[@"is_support"]];
        if([isSupport doubleValue] < 0){
            [MBProgressHUD showError:@"部分商品暂不支持配送" toView:self];
        }else{
            if([dic[@"status"] isEqual:@1]){
                NSString * postalPrice = [NSString stringWithFormat:@"%.0lf元",[freight doubleValue]];
                
                if (self.block) {
                    self.block(num,postalPrice,dic[@"pay_price"]);
                }
            }
        }
    } failure:^(NSError *error) {
        
    }];
}




-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self removeFromSuperview];
}

@end
