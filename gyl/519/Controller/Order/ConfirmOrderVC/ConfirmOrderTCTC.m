//
//  ConfirmOrderTCTC.m
//  519
//
//  Created by Macmini on 2017/12/5.
//  Copyright © 2017年 519. All rights reserved.
//

#import "ConfirmOrderTCTC.h"

@implementation ConfirmOrderTCTC




-(void)setListmodel:(GoodsListModel *)listmodel{
    _listmodel = listmodel;
    [_icon sd_setImageWithURL:[NSURL URLWithString:listmodel.icon]];
    _titleLabel.text = listmodel.name;
    if ([listmodel.return_score integerValue] != 0) {
         _onePriceLabel.text = [NSString stringWithFormat:@"%@积分",listmodel.return_score];
    }else{
         _onePriceLabel.text = [NSString stringWithFormat:@"￥%@",listmodel.unit_price];
    }
   
    _countLabel.text = [NSString stringWithFormat:@"x%@",listmodel.number];
    _goodTypeLabel.text = @"";
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
