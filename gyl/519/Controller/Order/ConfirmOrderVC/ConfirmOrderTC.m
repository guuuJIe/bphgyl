//
//  ConfirmOrderTC.m
//  519
//
//  Created by Macmini on 2017/12/5.
//  Copyright © 2017年 519. All rights reserved.
//

#import "ConfirmOrderTC.h"
#import "ConfirmOrderTCTC.h"
@interface ConfirmOrderTC()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)NSMutableArray * goodsList;
@property (nonatomic,strong)NSMutableArray * deliveryId;
@property (nonatomic,strong)NSMutableArray * deliveryName;
@property (nonatomic,strong)NSString * goodsId;
@property (nonatomic,strong)NSString * goodNum;
@property (nonatomic,strong)NSMutableDictionary * muDic;
@property (nonatomic,assign)int sendWayPrice;
@property (nonatomic,assign)BOOL isFold;
@property (nonatomic) UIButton *foldButton;
@property (nonatomic) UIView *peisongView;
@end

@implementation ConfirmOrderTC

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.goodsList = [NSMutableArray new];
        self.deliveryId = [NSMutableArray new];
        self.deliveryName = [NSMutableArray new];
        self.backgroundColor =APPBGColor;
        [self createUI];
    }
    return self;
}

-(void)createUI{
    UIView * headView = [[UIView alloc]init];
    headView.backgroundColor = [UIColor whiteColor];
    [self addSubview:headView];
    [headView makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).with.mas_offset(10);
        make.left.and.right.mas_equalTo(self).with.mas_offset(0);
        make.height.equalTo(50);
    }];
    
//    _titleImageV = [[UIImageView alloc]init];
//    _titleImageV.image = [UIImage imageNamed:@""];
//    [headView addSubview:_titleImageV];
//    [_titleImageV makeConstraints:^(MASConstraintMaker *make) {
//        make.top.and.left.equalTo(15);
//        make.width.and.height.equalTo(20);
//    }];
    
    _storeNameLabel = [[UILabel alloc]init];
    _storeNameLabel.text = @"门店自营";
    _storeNameLabel.textColor = APPFourColor;
    _storeNameLabel.textAlignment = NSTextAlignmentLeft;
    _storeNameLabel.font = [UIFont boldSystemFontOfSize:16];
    [headView addSubview:_storeNameLabel];
    [_storeNameLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.mas_left).with.mas_offset(10);
        make.top.mas_equalTo(headView.mas_top).with.mas_offset(10);
        make.height.equalTo(30);
    }];
    
    

    _tableViewtc = [[UITableView alloc]initWithFrame:CGRectMake(0, 61, Screen_Width, 200) style:(UITableViewStylePlain)];
    _tableViewtc.delegate = self;
    _tableViewtc.dataSource = self;
    _tableViewtc.separatorStyle = UITableViewScrollPositionNone;
    _tableViewtc.showsHorizontalScrollIndicator =  YES;
    _tableViewtc.showsVerticalScrollIndicator = YES;
    _tableViewtc.scrollEnabled = NO;
    [_tableViewtc registerNib:[UINib nibWithNibName:@"ConfirmOrderTCTC" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"confirmorderTCTCIde"];
    [self addSubview:_tableViewtc];
    
    
    UIButton *button = [[UIButton alloc] init];
    [button setBackgroundColor:[UIColor whiteColor]];
    [button setTitle:@"查看更多" forState:0];
    [button setImage:[UIImage imageNamed:@"down_icon"] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:0];
    [button.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [button setTitle:@"立即收起" forState:UIControlStateSelected];
    [button setImage:[UIImage imageNamed:@"up_icon"] forState:UIControlStateSelected];
    [button addTarget:self action:@selector(reload:) forControlEvents:UIControlEventTouchUpInside];
    
    [button setImageEdgeInsets:UIEdgeInsetsMake(0, 140, 0, 0)];
    [button setTitleEdgeInsets:UIEdgeInsetsMake(0, -35, 0, 0)];
    [button.titleLabel setTextAlignment:NSTextAlignmentCenter];
    _foldButton = button;
    [self addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_tableViewtc.mas_bottom).with.mas_offset(1);
        make.left.and.right.mas_equalTo(self).with.mas_offset(0);
        make.height.equalTo(40);
    }];
    
    UIView * sendWayView = [[UIView alloc]init];
    sendWayView.backgroundColor = [UIColor whiteColor];
    [self addSubview:sendWayView];
    _peisongView = sendWayView;
    [sendWayView makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(button.mas_bottom).with.mas_offset(1);
        make.left.and.right.mas_equalTo(self).with.mas_offset(0);
        make.height.equalTo(40);
    }];
    
    
    UILabel * sendTitle = [[UILabel alloc]init];
    sendTitle.text = @"配送方式";
    sendTitle.textColor = APPFourColor;
    sendTitle.textAlignment = NSTextAlignmentLeft;
    sendTitle.font = [UIFont systemFontOfSize:15];
    [sendWayView addSubview:sendTitle];
    [sendTitle makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(sendWayView.mas_left).with.mas_offset(10);
        make.top.mas_equalTo(sendWayView.mas_top).with.mas_offset(5);
        make.height.equalTo(30);
    }];
    
    _sendWayBtn = [[UIButton alloc]init];
    [_sendWayBtn setTitle:@"配送方式" forState:(UIControlStateNormal)];
    [_sendWayBtn setTitleColor:APPThreeColor forState:(UIControlStateNormal)];
    _sendWayBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [_sendWayBtn addTarget:self action:@selector(changeSendWay) forControlEvents:(UIControlEventTouchUpInside)];
    [sendWayView addSubview:_sendWayBtn];
    [_sendWayBtn makeConstraints:^(MASConstraintMaker *make) {
       make.right.mas_equalTo(sendWayView.mas_right).with.mas_offset(-30);
        make.top.mas_equalTo(sendWayView.mas_top).with.mas_offset(5);
        make.height.equalTo(30);
    }];
    
    UIImageView * turnRight = [UIImageView new];
    turnRight.image = [UIImage imageNamed:@"right_black_icon"];
    [sendWayView addSubview:turnRight];
    [turnRight makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_sendWayBtn.mas_centerY);
        make.right.mas_equalTo(self.mas_right).with.mas_offset(-5);
        make.width.and.height.equalTo(20);
    }];
    //备注
    UIView * remarksView = [[UIView alloc]init];
    remarksView.backgroundColor  =[UIColor whiteColor];
    [self addSubview:remarksView];
    [remarksView makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(sendWayView.mas_bottom).with.mas_offset(1);
        make.left.and.right.mas_equalTo(self).with.mas_offset(0);
        make.height.equalTo(70);
    }];
    
    
    UILabel * remarksLabel = [[UILabel alloc]init];
    remarksLabel.text = @"备注";
    remarksLabel.textColor = APPFourColor;
    remarksLabel.textAlignment = NSTextAlignmentLeft;
    remarksLabel.font = [UIFont systemFontOfSize:15];
    [remarksView addSubview:remarksLabel];
    [remarksLabel makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(remarksView.mas_top).with.mas_offset(5);
        make.left.mas_equalTo(remarksView.mas_left).with.mas_equalTo(10);
        make.height.equalTo(30);
    }];;
    
    _textField = [[UITextField alloc]init];
    _textField.placeholder = @"请输入你的留言";
    _textField.delegate = self;
    _textField.returnKeyType = UIReturnKeyDone;
    _textField.font = [UIFont systemFontOfSize:14];
    [remarksView addSubview:_textField];
    [_textField makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(remarksLabel.mas_bottom).with.mas_offset(5);
        make.left.mas_equalTo(remarksView.mas_left).with.mas_offset(10);
        make.right.mas_equalTo(remarksView.mas_right).with.mas_offset(-10);
        make.height.equalTo(30);
    }];
    
    UIView * priceView = [[UIView alloc]init];
    priceView.backgroundColor = [UIColor whiteColor];
    [self addSubview:priceView];
    [priceView makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(remarksView.mas_bottom).with.mas_offset(1);
        make.left.and.right.mas_equalTo(self).with.mas_offset(0);
        make.height.equalTo(40);
    }];
    
    _postalLabel = [[UILabel alloc]init];
    _postalLabel.textColor = APPFourColor;
    _postalLabel.text = @"0.00元";
    _postalLabel.textAlignment = NSTextAlignmentLeft;
    _postalLabel.font = [UIFont systemFontOfSize:15];
    [priceView addSubview:_postalLabel];
    [_postalLabel makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(priceView.mas_top).with.mas_offset(5);
        make.right.mas_equalTo(priceView.mas_right).with.mas_offset(-10);
        make.height.equalTo(30);
    }];
    UILabel * postalTitle = [[UILabel alloc]init];
    postalTitle.textColor = APPFourColor;
    postalTitle.text = @"邮费:";
    postalTitle.textAlignment = NSTextAlignmentLeft;
    postalTitle.font = [UIFont systemFontOfSize:15];
    [priceView addSubview:postalTitle];
    [postalTitle makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(priceView.mas_top).with.mas_offset(5);
        make.right.mas_equalTo(_postalLabel.mas_left).with.mas_offset(-5);
        make.height.equalTo(30);
    }];
    
    _priceLabel = [[UILabel alloc]init];
    _priceLabel.textColor = [UIColor redColor];
    
    _priceLabel.textAlignment = NSTextAlignmentLeft;
    _priceLabel.font = [UIFont systemFontOfSize:15];
    [priceView addSubview:_priceLabel];
    [_priceLabel makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(priceView.mas_top).with.mas_offset(5);
        make.right.mas_equalTo(postalTitle.mas_left).with.mas_offset(-10);
        make.height.equalTo(30);
    }];
    
    UILabel * totalPriceLabel = [[UILabel alloc]init];
    totalPriceLabel.textColor = APPFourColor;
    totalPriceLabel.text = @"小计:";
    totalPriceLabel.textAlignment = NSTextAlignmentLeft;
    totalPriceLabel.font = [UIFont systemFontOfSize:15];
    [priceView addSubview:totalPriceLabel];
    [totalPriceLabel makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(priceView.mas_top).with.mas_offset(5);
        make.right.mas_equalTo(_priceLabel.mas_left).with.mas_offset(-5);
        make.height.mas_equalTo(30);
    }];
    
    UILabel * promptLabel = [[UILabel alloc]init];
    promptLabel.textColor = APPFourColor;
    promptLabel.text = @"温馨提示：我司批发的进口啤酒都配有中文标，请在上架或销售前每瓶必须加贴合格中文标签，若没有中文标可拒收，若销售中出现未贴中文标签，我司概不负责。";
    promptLabel.textAlignment = NSTextAlignmentLeft;
    promptLabel.font = [UIFont systemFontOfSize:15];
    promptLabel.numberOfLines = 0;
    [priceView addSubview:promptLabel];
    [promptLabel makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(totalPriceLabel.mas_bottom).with.mas_offset(12);
        make.left.mas_equalTo(remarksView.mas_left).with.mas_offset(10);
        make.right.mas_equalTo(remarksView.mas_right).with.mas_offset(-10);
//        make.height.mas_equalTo(30);
    }];

}

- (void)reload:(UIButton *)sender{
    sender.selected = !sender.selected;
    if (sender.selected) {
       
        _tableViewtc.frame = CGRectMake(0, 61,Screen_Width, _goodsList.count*100);
        
    }else{
        
        if (_goodsList.count > 3) {
            _tableViewtc.frame = CGRectMake(0, 61,Screen_Width, 3*100);
        }else{
            _tableViewtc.frame = CGRectMake(0, 61,Screen_Width, _goodsList.count*100);
        }
        
       
    }
  
    
    _isFold = sender.selected;
    if (self.foldBlock) {
        self.foldBlock(_isFold,_goodsList.count);
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _goodsList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * cellIde = @"confirmorderTCTCIde";
    ConfirmOrderTCTC * cell = [tableView dequeueReusableCellWithIdentifier:cellIde];
    [cell setSelectionStyle:(UITableViewCellSelectionStyleNone)];
    if (cell == nil) {
        cell = [[ConfirmOrderTCTC alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:cellIde];
    }
    GoodsListModel * model = _goodsList[indexPath.row];
    cell.listmodel = model;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [_textField resignFirstResponder];
}
-(void)setModel:(CartListGroupModel *)model{
    [_goodsList removeAllObjects];
    [_deliveryId removeAllObjects];
    [_deliveryName removeAllObjects];
    _model = model;
    NSLog(@"%@",model.supplier);
    _storeNameLabel.text = model.supplier;
    _supplierId = model.supplier_id;
    NSDictionary *dic = model.goods_list.firstObject;
    if ([dic[@"return_score"] integerValue] != 0) {
        _priceLabel.text = [NSString stringWithFormat:@"%.2lf积分",[dic[@"return_total_score"] doubleValue]];
    }else{
       _priceLabel.text = [NSString stringWithFormat:@"￥%.2lf",[model.total_price doubleValue]];
    }
    
    
    
    for (NSDictionary * subdic in model.goods_list) {
        GoodsListModel * model = [[GoodsListModel alloc]init];
        _goodsId = model.Id = [NSString stringWithFormat:@"%@",subdic[@"id"]];
        model.deal_id = subdic[@"deal_id"];
        model.return_score = subdic[@"return_score"];
        _goodNum = model.number = [NSString stringWithFormat:@"%@",subdic[@"number"]];
        model.supplier_id = subdic[@"supplier_id"];
        model.icon = subdic[@"icon"];
        model.name = subdic[@"name"];
        model.unit_price = subdic[@"unit_price"];
        [_goodsList addObject:model];
        
    }
    for (NSDictionary * subdic in model.delivery_list) {
        [_deliveryId addObject:subdic[@"id"]];
        [_deliveryName addObject:subdic[@"name"]];
    }

    CGRect frame = _tableViewtc.frame;
    if (_goodsList.count>3) {
        self.foldButton.hidden = false;
        if (self.isFold) {
             _tableViewtc.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, _goodsList.count * 100);
        
        }else{
             _tableViewtc.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 3 * 100);
        }
        
        [_peisongView updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.foldButton.mas_bottom).with.mas_offset(1);
            //            make.left.and.right.mas_equalTo(self).with.mas_offset(0);
            //            make.height.equalTo(40);
        }];

    }else{
        self.foldButton.hidden = true;
        _tableViewtc.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, _goodsList.count * 100);
        [_peisongView updateConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_tableViewtc.mas_bottom).with.mas_offset(1);
//            make.left.and.right.mas_equalTo(self).with.mas_offset(0);
//            make.height.equalTo(40);
        }];
       
      
    }
    
    [self updateFocusIfNeeded];
    [self updateConstraintsIfNeeded];
   
    [_tableViewtc reloadData];
}

-(void)changeSendWay{
    [_textField resignFirstResponder];
    if (self.block) {
        self.block(_deliveryId, _deliveryName,_supplierId);
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (self.tagblock) {
        self.tagblock(self.tag,self.isFold);
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSString * text;
    if(!textField.text){
        text = @"";
    }else{
        text = textField.text;
    }
    if (self.editblock) {
        self.editblock(_supplierId, text);
    }
    [textField resignFirstResponder];
    return  YES;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [_textField resignFirstResponder];
}

@end
