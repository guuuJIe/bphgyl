//
//  ConfirmOrderVC.h
//  519
//
//  Created by Macmini on 2017/12/5.
//  Copyright © 2017年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmOrderVC : UIViewController
@property(nonatomic,strong)NSDictionary * dic;
@property(nonatomic,assign)BOOL isbuynow;//1:立即购买 0:购物车购买
@property (nonatomic,copy)NSString * isJump;
@property (nonatomic,copy)NSString * goodId;
@property (nonatomic,copy)NSString * goodNum;
@end
