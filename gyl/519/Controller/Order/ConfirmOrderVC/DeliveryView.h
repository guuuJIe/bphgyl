//
//  DeliveryView.h
//  519
//
//  Created by Macmini on 2017/12/6.
//  Copyright © 2017年 519. All rights reserved.
//
typedef void(^clickDeliveyBlock)(NSInteger num, NSString * postalPrice,id nowTotalPrice);
#import <UIKit/UIKit.h>

@interface DeliveryView : UIView
@property (nonatomic,copy)clickDeliveyBlock block;
@property(nonatomic,strong)NSArray * deliveryId;
@property(nonatomic,strong)NSArray * deliveryName;
@property (nonatomic,strong)NSString * supplierId;
@property(nonatomic,assign)BOOL buyNow;
@property(nonatomic,copy)NSString * Ids;
@property(nonatomic,copy)NSString * num;
-(instancetype)initWithFrame:(CGRect)frame deliveryId:(NSArray *)deliveryId withDeliveryName:(NSArray *)deliveryName  withSupplierId:(NSString *)supplierId goodId:(NSString *)goodId goodNum:(NSString *)goodsNum;
@end
