//
//  PayResultVC.h
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PayResultVC : UIViewController
@property(nonatomic,copy)NSString * number;
@property(nonatomic,copy)NSString * price;
@end
