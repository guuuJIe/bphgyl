//
//  OrderPayVC.h
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderPayVC : UIViewController

@property(nonatomic,copy)NSString * orderId;
@property(nonatomic,copy)NSString * paytype;
@property(nonatomic,copy)NSString * isFromJump;
@end
