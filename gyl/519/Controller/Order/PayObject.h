//
//  PayObject.h
//  519
//
//  Created by Macmini on 16/12/7.
//  Copyright © 2016年 519. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^callBack)(NSDictionary *result);
typedef void(^wxpayblock)(Boolean result);
@interface PayObject : NSObject

-(void)getParInfoWithURL:(NSDictionary *)dic callback:(callBack)block;

-(void)WXPayinfoWithDic:(NSDictionary *)dic callback:(wxpayblock)block;
@end
