//
//  SendWayView.h
//  519
//
//  Created by Macmini on 16/12/6.
//  Copyright © 2016年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SendWayView : UIView

-(instancetype)initWithFrame:(CGRect)frame nameArray:(NSArray *)nameArray withIdArray:(NSArray *)idArray withPriceArr:(NSArray *)priceArr;

@property(nonatomic,copy)void(^sendWayBlock)(NSString * wayId,NSString * wayName);

@end
