//
//  TimerView.m
//  519
//
//  Created by Macmini on 16/12/6.
//  Copyright © 2016年 519. All rights reserved.
//

#import "TimerView.h"

@interface TimerView()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView * dayTableView;
@property (nonatomic,strong)UITableView * timerTableView;
@property (nonatomic,strong)NSArray * dayArray;
@property (nonatomic,strong)NSArray * timerArr;
@property (nonatomic,strong)NSArray * numberArr;
@property (nonatomic,strong)UIView *backView;
@property (nonatomic,strong)UIView *timerView;
@property (nonatomic,strong)UIView *headView;
@property (nonatomic,copy)NSString * hourStr;
@property (nonatomic,copy)NSString * numberStr;
@property (nonatomic,copy)NSString * dayStr;
@end


@implementation TimerView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self initView];
    }
    return self;
}

-(void)initView{
    self.backgroundColor = [UIColor clearColor];
    
    _backView = [[UIView alloc] initWithFrame:self.frame];
    _backView.backgroundColor = [UIColor blackColor];
    _backView.alpha = 0.6;
    [self addSubview:_backView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hide)];
    [_backView addGestureRecognizer:tap];
    
    self.dayArray = @[@"今天",@"明天"];
    
    
    _timerView= [[UIView alloc]initWithFrame:CGRectMake(0, self.frame.size.height-300, self.frame.size.width, 300)];
    _timerView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_timerView];
    
    _headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,self.frame.size.width,43)];
    _headView.backgroundColor = [UIColor colorWithRed:245/255.0 green:245/255.0 blue:245/255.0 alpha:1.0];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(10, 0, 50, 43.5);
    button.backgroundColor = [UIColor clearColor];
    button.titleLabel.font = [UIFont systemFontOfSize:16];
    [button setTitleColor:[UIColor colorWithRed:0/255.0 green:122/255.0 blue:255/255.0 alpha:1.0] forState:UIControlStateNormal];
    [button setTitle:@"取消" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(cancleButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [_headView addSubview:button];
    
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(self.frame.size.width - 50, 0, 50, 43.5);
    button.backgroundColor = [UIColor clearColor];
    button.titleLabel.font = [UIFont systemFontOfSize:16];
    [button setTitleColor:[UIColor colorWithRed:0/255.0 green:122/255.0 blue:255/255.0 alpha:1.0] forState:UIControlStateNormal];
    [button setTitle:@"确定" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(completionButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [_headView addSubview:button];
    
    [_timerView addSubview:_headView];
    
    _dayTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 43, _timerView.frame.size.width/2, _timerView.frame.size.height-43)];
    _dayTableView.delegate = self;
    _dayTableView.dataSource = self;
    _dayTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    [_timerView addSubview:_dayTableView];
    
    _timerTableView = [[UITableView alloc]initWithFrame:CGRectMake(_timerView.frame.size.width/2, 43, _timerView.frame.size.width/2, _timerView.frame.size.height-43)];
    _timerTableView.dataSource = self;
    _timerTableView.delegate = self;
    _timerTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    [_timerView addSubview:_timerTableView];
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == _dayTableView) {
        if (self.dayArray.count>0) {
            return self.dayArray.count;
        }
        return 2;
        
    }else{
        if (self.timerArr.count>0) {
            return self.timerArr.count;
        }
        return 2;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _dayTableView) {
        NSString * cellIDE = @"dayIDE";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIDE];
        if (cell == nil) {
            cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:cellIDE];
        }
        cell.textLabel.text = self.dayArray[indexPath.row];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        
        
        NSMutableArray * hourArr = [NSMutableArray new];
        NSMutableArray * numberArr = [NSMutableArray new];
        for (NSArray * subArr in self.alltimerArr) {
            NSMutableArray * hourArr1 = [NSMutableArray new];
            NSMutableArray * numberArr1 = [NSMutableArray new];
            for (NSArray * thirdArr in subArr) {
                [hourArr1 addObject:thirdArr[0]];
                [numberArr1 addObject:thirdArr[1]];
            }
            [hourArr addObject:hourArr1];
            [numberArr addObject:numberArr1];
        }
        self.timerArr = hourArr[0];
         self.numberArr = numberArr[0];
        [_timerTableView reloadData];
        
        
        NSInteger selectedIndex = 0;
        NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:selectedIndex inSection:0];
        [_dayTableView selectRowAtIndexPath:selectedIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        self.dayStr = [NSString stringWithFormat:@"%ld",(long)selectedIndex];
        return cell;
    }else{
        NSString * cellIDE = @"timerIDE";
        UITableViewCell * cell  = [tableView dequeueReusableCellWithIdentifier:cellIDE];
        if (cell == nil) {
            cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:cellIDE];
        }
       
        cell.textLabel.text = self.timerArr[indexPath.row];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        
        NSInteger selectedIndex = 0;
        NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:selectedIndex inSection:0];
        [_timerTableView selectRowAtIndexPath:selectedIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        self.hourStr = self.timerArr[selectedIndex];
        self.numberStr = self.numberArr[selectedIndex];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.dayTableView) {
        NSInteger i = indexPath.row;
        
        NSMutableArray * hourArr = [NSMutableArray new];
        NSMutableArray * numberArr = [NSMutableArray new];
        for (NSArray * subArr in self.alltimerArr) {
            NSMutableArray * hourArr1 = [NSMutableArray new];
            NSMutableArray * numberArr1 = [NSMutableArray new];
            for (NSArray * thirdArr in subArr) {
                [hourArr1 addObject:thirdArr[0]];
                [numberArr1 addObject:thirdArr[1]];
            }
            [hourArr addObject:hourArr1];
            [numberArr addObject:numberArr1];
        }
        self.timerArr = hourArr[i];
        self.numberArr = numberArr[i];
        [_timerTableView reloadData];
        
        self.dayStr = [NSString stringWithFormat:@"%ld",indexPath.row];
    }else{
        self.hourStr = self.timerArr[indexPath.row];
        self.numberStr = self.numberArr[indexPath.row];
       
    }
}


- (void)completionButtonAction:(UIButton *)sender {
    if (self.timerdelegate && [self.timerdelegate respondsToSelector:@selector(selectTimerDay:hour:withnumber:)]) {
        [self.timerdelegate selectTimerDay:self.dayStr hour:self.hourStr withnumber:self.numberStr];
    }else{
        NSLog(@"TimeView 代理未实现");
    }
    
    [self hide];
}

- (void)cancleButtonAction:(UIButton *)sender {
    [self hide];
}


- (void)hide {
    _backView.alpha = 0;
    [UIView animateWithDuration:0.5 animations:^{
        self.timerView.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height+44, self.frame.size.width, 300);
        
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
}
@end
