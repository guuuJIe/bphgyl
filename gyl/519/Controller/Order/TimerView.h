//
//  TimerView.h
//  519
//
//  Created by Macmini on 16/12/6.
//  Copyright © 2016年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectTimerDelegate <NSObject>

-(void)selectTimerDay:(NSString *)day hour:(NSString *)hour withnumber:(NSString *)number;

@end


@interface TimerView : UIView
@property (nonatomic,assign)id<SelectTimerDelegate>timerdelegate;
@property (nonatomic,strong)NSArray * alltimerArr;

@end
