//
//  OrderPayVC.m
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//

#import "OrderPayVC.h"
#import "OrderConfirmPayTC.h"
#import "PayResultVC.h"
#import "base64.h"
#import <AlipaySDK/AlipaySDK.h>
#import "PayObject.h"
#import "WXApiManager.h"
#import "MKPAlertView.h"
#import "GoodInfoVC.h"
#import "MainWebView.h"
#import "UIViewController+BackButtonHandler.h"
#import "ShouyinBottomView.h"
#import "OrderListVC.h"
#import "ConnactView.h"
#import "GoodView.h"
#import "ShowCardInfo.h"
@interface OrderPayVC()<UITableViewDataSource,UITableViewDelegate,OrderConfirmPayDetegate,WXApiManagerDelegate>
@property(nonatomic,strong)UIButton *backBtn;

@property(nonatomic,strong)UIScrollView *scrollView;

@property(nonatomic,strong)UIView *orderInfoView;
@property(nonatomic,strong)UILabel *orderNumberTitleLabel;
@property(nonatomic,strong)UILabel *orderNumberLabel;
@property(nonatomic,strong)UIView *orderInfoLine;
@property(nonatomic,strong)UIView *orderInfoLine2;
@property(nonatomic,strong)UILabel *orderPriceTitleLabel;
@property(nonatomic,strong)UILabel *orderPriceLabel;
@property(nonatomic,strong)UILabel *wayPriceLabel;
@property(nonatomic,strong)UILabel *wayMoneyLabel;

@property(nonatomic,strong)UIView *payTitleView;
@property(nonatomic,strong)UILabel *payTitleLabel;

@property(nonatomic,strong)UITableView *payTableView;
@property(nonatomic,strong)UIButton *payBtn;

@property(nonatomic,copy)NSString * order_id;
@property(nonatomic,copy)NSString * order_sn;
@property(nonatomic,copy)NSString * payMoney;
@property(nonatomic,copy)NSString * payId;
@property(nonatomic,copy)NSString * onlineId;
@property(nonatomic,strong)NSMutableArray * payNameMuarray;
@property(nonatomic,strong)NSMutableArray * payIdMuArray;
@property(nonatomic,strong)NSMutableArray * payLogoMuArray;
@property(nonatomic,strong)NSMutableArray * payOnlineidMuArray;
@property (nonatomic,assign)BOOL payState;

@property (nonatomic,strong)ShouyinBottomView *bottomView;

@property (nonatomic,strong)ConnactView *conView;

@property (nonatomic,strong) GoodView *goodsView;

@property (nonatomic,strong) NSArray *goodsArr;
@property (nonatomic)ShowCardInfo *carView;
@end

@implementation OrderPayVC
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"收银台";
    self.payIdMuArray = [NSMutableArray new];
    self.payNameMuarray = [NSMutableArray new];
    self.payLogoMuArray = [NSMutableArray new];
    self.payOnlineidMuArray = [NSMutableArray new];
    [self initView];
    [self payInitBar];
    [self requestDataForOrder];
}
-(void)payInitBar{
    UIView * contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 70, 40)];
    UIButton * btn = [[UIButton alloc]initWithFrame:contentView.bounds];
    [btn setImage:[UIImage imageNamed:@"返回"] forState:(UIControlStateNormal)];
    btn.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    [btn setTitle:@"返回" forState:(UIControlStateNormal)];
    [btn setTitleColor:APPFourColor forState:(UIControlState)UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    btn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [btn addTarget:self action:@selector(popVC) forControlEvents:(UIControlEventTouchUpInside)];
    [contentView addSubview:btn];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:contentView];
    
    UIView * refreshView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 60, 40)];
    UIButton * refreshBtn = [[UIButton alloc]initWithFrame:refreshView.bounds];
//    [refreshBtn setImage:[UIImage imageNamed:@"refresh"] forState:(UIControlStateNormal)];
    [refreshBtn setTitle:@"刷新" forState:(UIControlStateNormal)];
    [refreshBtn setTitleColor:APPFourColor forState:(UIControlState)UIControlStateNormal];
    refreshBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    refreshBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    refreshBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [refreshBtn addTarget:self action:@selector(refreshData) forControlEvents:(UIControlEventTouchUpInside)];
    [refreshView addSubview:refreshBtn];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:refreshView];;
}

- (void)refreshData{
    [self requestDataForOrder];
}

-(void)popVC{
    MKPAlertView *alertView = [[MKPAlertView alloc]initWithTitle:@"订单已生成,确定取消支付" message:nil sureBtn:@"确认" cancleBtn:@"取消"];
    alertView.resultIndex = ^(NSInteger index)
    {
        if ([self.paytype isEqualToString:@"nowPay"]) {
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            for (UIViewController *controller in self.navigationController.viewControllers) {
                if ([controller isKindOfClass:[GoodInfoVC class]]) {
                    GoodInfoVC *revise =(GoodInfoVC *)controller;
                    [self.navigationController popToViewController:revise animated:YES];
                }else{
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
        }
        
    };
    [alertView showMKPAlertView];
}


-(void)requestDataForOrder{
    
    [MBProgressHUD showHUD];
    NSString * orderUrl = [NSString stringWithFormat:@"%@ctl=payment&act=done&id=%@&email=%@&pwd=%@",GYLUrl,self.orderId,EMAIL,USER_PWD];
    self.payIdMuArray = [NSMutableArray new];
    self.payNameMuarray = [NSMutableArray new];
    self.payLogoMuArray = [NSMutableArray new];
   [RequestData requestDataOfUrl:orderUrl success:^(NSDictionary *dic) {
       [MBProgressHUD dissmiss];
       self.goodsArr = dic[@"cart_list_group"][@"1"][@"goods_list"];
       self.order_sn = dic[@"order_sn"];
      
       self.payMoney = dic[@"total_price"];
       self.order_id = dic[@"order_id"];
       
       self.orderNumberLabel.text=self.order_sn;
       if ([dic[@"return_total_score"] integerValue] != 0) {
           self.orderPriceLabel.text=[NSString stringWithFormat:@"￥ %@元 %@积分",self.payMoney,dic[@"return_total_score"]];
       }else{
           self.orderPriceLabel.text=[NSString stringWithFormat:@"￥ %@",self.payMoney];
       }
       
       NSString * deliveryStr = [NSString stringWithFormat:@"%@",dic[@"delivery_fee"]];
       if ([deliveryStr isEqualToString:@"<null>"]) {
           deliveryStr = @"0";
       }
       self.wayMoneyLabel.text = [NSString stringWithFormat:@"￥ %@",deliveryStr];
       
       for ( NSDictionary * subdic in dic[@"payment_list"]) {
           [self.payNameMuarray addObject: subdic[@"name"]];
           [self.payIdMuArray addObject:subdic[@"id"]];
           [self.payLogoMuArray addObject:subdic[@"logo"]];
           [self.payOnlineidMuArray addObject:subdic[@"online_pay"]];
       }
      
      
//
       [self showData];
   } failure:^(NSError *error) {
       NSLog(@"%@",error);
   }];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)initView{
    self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-40)];
    self.scrollView.showsVerticalScrollIndicator=NO;
    self.scrollView.backgroundColor=APPBGColor;
    
    self.goodsView = [[GoodView alloc] initWithFrame:CGRectMake(0, Default_Space, Screen_Width, 100)];
    
    self.orderInfoView=[[UIView alloc] initWithFrame:CGRectMake(0, self.goodsView.frame.size.height+Default_Space, Screen_Width, 120)];
    self.orderInfoView.backgroundColor=[UIColor whiteColor];
    
    self.orderNumberTitleLabel=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space,Default_Space, Screen_Width-Default_Space*2, 20)];
    self.orderNumberTitleLabel.textColor=APPFourColor;
    self.orderNumberTitleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.orderNumberTitleLabel.text=@"订单号：";
    self.orderNumberTitleLabel.textAlignment=NSTextAlignmentLeft;
    self.wayPriceLabel = [[UILabel alloc]initWithFrame:CGRectMake(Default_Space, self.orderNumberTitleLabel.frame.origin.y+self.orderNumberTitleLabel.frame.size.height+Default_Space*2, Screen_Width-Default_Space*2, 20)];
    self.wayPriceLabel.textColor=APPFourColor;
    self.wayPriceLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.wayPriceLabel.textAlignment=NSTextAlignmentLeft;
    self.wayPriceLabel.text=@"运费：";
    self.orderPriceTitleLabel=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space, self.wayPriceLabel.frame.origin.y+self.wayPriceLabel.frame.size.height+Default_Space*2, Screen_Width-Default_Space*2, 20)];
    self.orderPriceTitleLabel.textColor=APPFourColor;
    self.orderPriceTitleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.orderPriceTitleLabel.textAlignment=NSTextAlignmentLeft;
    self.orderPriceTitleLabel.text=@"订单金额：";
    
    self.orderInfoLine=[[UIView alloc] initWithFrame:CGRectMake(0, 40, Screen_Width, Default_Line)];
    self.orderInfoLine.backgroundColor=APPBGColor;
    self.orderInfoLine2=[[UIView alloc] initWithFrame:CGRectMake(0, 80, Screen_Width, Default_Line)];
    self.orderInfoLine2.backgroundColor=APPBGColor;
    
    self.orderNumberLabel=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space,Default_Space, Screen_Width-Default_Space*2, 20)];
    self.orderNumberLabel.textColor=APPFourColor;
    self.orderNumberLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.orderNumberLabel.textAlignment=NSTextAlignmentRight;
    self.wayMoneyLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, self.orderNumberLabel.frame.origin.y+self.orderNumberLabel.frame.size.height+Default_Space*2, Screen_Width-Default_Space*2, 20)];
    self.wayMoneyLabel.textColor=APPColor;
    self.wayMoneyLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.wayMoneyLabel.textAlignment=NSTextAlignmentRight;
    self.orderPriceLabel=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space, self.wayMoneyLabel.frame.origin.y+self.wayMoneyLabel.frame.size.height+Default_Space*2, Screen_Width-Default_Space*2, 20)];
    self.orderPriceLabel.textColor=APPColor;
    self.orderPriceLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.orderPriceLabel.textAlignment=NSTextAlignmentRight;
    
    [self.orderInfoView addSubview:self.orderNumberLabel];
    [self.orderInfoView addSubview:self.orderPriceLabel];
    [self.orderInfoView addSubview:self.orderNumberTitleLabel];
    [self.orderInfoView addSubview:self.orderPriceTitleLabel];
    [self.orderInfoView addSubview:self.orderInfoLine];
    [self.orderInfoView addSubview:self.orderInfoLine2];
    [self.orderInfoView addSubview:self.wayPriceLabel];
    [self.orderInfoView addSubview:self.wayMoneyLabel];
    
    self.payTitleView=[[UIView alloc] initWithFrame:CGRectMake(0, self.orderInfoView.frame.origin.y+self.orderInfoView.frame.size.height+Default_Space, Screen_Width, 40)];
    self.payTitleView.backgroundColor=[UIColor whiteColor];
    self.payTitleLabel=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space, Default_Space, Screen_Width-Default_Space*2, 20)];
    self.payTitleLabel.textColor=APPFourColor;
    self.payTitleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.payTitleLabel.text=@"支付方式";
    [self.payTitleView addSubview:self.payTitleLabel];
    
    self.payTableView=[[UITableView alloc]init];
    [self.payTableView registerClass:[OrderConfirmPayTC class] forCellReuseIdentifier:[OrderConfirmPayTC getID]];
    self.payTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.payTableView.delegate=self;
    self.payTableView.dataSource=self;
    self.payTableView.scrollEnabled=NO;
    self.payTableView.showsVerticalScrollIndicator=NO;
    
//    self.payBtn = [[UIButton alloc]init];
//    [self.payBtn setBackgroundColor:APPColor];
//    [self.payBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [self.payBtn setTitle:@"确认" forState:UIControlStateNormal];
//    self.payBtn.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
//    [self.payBtn addTarget:self action:@selector(payBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.bottomView = [[ShouyinBottomView alloc] init];
    
    [self.scrollView addSubview:self.goodsView];
    [self.scrollView addSubview:self.orderInfoView];
    [self.scrollView addSubview:self.payTitleView];
    [self.scrollView addSubview:self.payTableView];
    
    [self.view addSubview:self.scrollView];
    [self.view addSubview:self.payBtn];
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.equalTo(0);
        make.height.offset(50+BottomAreaHeight);
        make.width.equalTo(Screen_Width);
    }];

    
    WS(weself);
    self.bottomView.kefu = ^{
        
        
        NSString * kefuInterface = [NSString stringWithFormat:@"%@ctl=user&act=kf&email=%@&pwd=%@",GYLUrl,EMAIL,USER_PWD];
        [RequestData requestDataOfUrl:kefuInterface success:^(NSDictionary *dic) {
            NSLog(@"%@",dic);
            if([dic[@"user_login_status"] integerValue] == 0){//未登入
                MainWebView * vc = [[MainWebView alloc]init];
                NSString* nowSysVer = [[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleShortVersionString"];
                vc.webUrl = [NSString stringWithFormat:@"http://www.bphapp.com/wap/index.php?ctl=onlinekf&userId=%@&os=ios&appVersion=%@&device=%@&deal_id=0&order_sn=0",EMAIL,nowSysVer,[[UIDevice currentDevice] model]];
                [weself.navigationController pushViewController:vc animated:true];
                //                if (EMAIL) {
                //
                //                }else{
                //                    [weself.navigationController pushViewController:[LoginViewController new] animated:YES];
                //                }
            }else{
                _conView = [[ConnactView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
                [weself.conView setupData:dic];
                [weself.view addSubview:weself.conView];
            }
        } failure:^(NSError *error) {
            
        }];
    };
    
    self.bottomView.order = ^{
        [weself allOrderVIewOnClick:0];
    };
    
    self.bottomView.submitOrder = ^{
        [weself payBtnOnClick];
    };
    
    
    self.goodsView.fold = ^(BOOL isFold, NSInteger goodsnum) {
//        if (isFold) {
//
//        }else{
//
//        }
        
        [weself foldData:isFold];
    };
    
}


- (void)foldData:(BOOL) isFold{
    
    if (isFold) {
         self.goodsView.frame = CGRectMake(0, Default_Space, Screen_Width, self.goodsArr.count*100+40);
    }else{
         self.goodsView.frame = CGRectMake(0, Default_Space, Screen_Width, 3*100+40);
    }
   
    self.orderInfoView.frame = CGRectMake(0, self.goodsView.frame.size.height+self.goodsView.origin.y, Screen_Width, 120);
    
    self.payTitleView.frame = CGRectMake(0, self.orderInfoView.frame.origin.y+self.orderInfoView.frame.size.height+Default_Space, Screen_Width, 40);
    self.payTableView.frame=CGRectMake(0, self.payTitleView.frame.origin.y+self.payTitleView.frame.size.height+Default_Line, Screen_Width, self.payNameMuarray.count*[OrderConfirmPayTC getCellHeight]);
    
    self.scrollView.contentSize=CGSizeMake(Screen_Width, self.payTableView.frame.origin.y+self.payTableView.frame.size.height+80+BottomAreaHeight);
}


-(void)showData{
    
    if (self.goodsArr.count>3) {
        self.goodsView.frame = CGRectMake(0, Default_Space, Screen_Width, 3*100+40);
    }else{
        self.goodsView.frame = CGRectMake(0, Default_Space, Screen_Width, self.goodsArr.count*100);
    }
     self.goodsView.goodsList = [NSMutableArray arrayWithArray:self.goodsArr];
    
    self.orderInfoView.frame = CGRectMake(0, self.goodsView.frame.size.height+self.goodsView.origin.y, Screen_Width, 120);
   
     self.payTitleView.frame = CGRectMake(0, self.orderInfoView.frame.origin.y+self.orderInfoView.frame.size.height+Default_Space, Screen_Width, 40);
    self.payTableView.frame=CGRectMake(0, self.payTitleView.frame.origin.y+self.payTitleView.frame.size.height+Default_Line, Screen_Width, self.payNameMuarray.count*[OrderConfirmPayTC getCellHeight]);
    
    self.scrollView.contentSize=CGSizeMake(Screen_Width, self.payTableView.frame.origin.y+self.payTableView.frame.size.height+80+BottomAreaHeight);
    
    [self.payTableView reloadData];
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.payIdMuArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderConfirmPayTC *cell=[tableView dequeueReusableCellWithIdentifier:[OrderConfirmPayTC getID] forIndexPath:indexPath];
    if(cell==nil){
        cell=[[OrderConfirmPayTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[OrderConfirmPayTC getID]];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell showDataName:self.payNameMuarray[indexPath.row] logo:self.payLogoMuArray[indexPath.row] withids:self.payIdMuArray[indexPath.row]];
    cell.delegate = self;

    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [OrderConfirmPayTC getCellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self changeBtnImage:[_payIdMuArray[indexPath.row] integerValue]+6000];
    self.onlineId = self.payOnlineidMuArray[indexPath.row];
    self.payId = self.payIdMuArray[indexPath.row];
    NSLog(@"%@ %@",self.payIdMuArray[indexPath.row],self.payOnlineidMuArray[indexPath.row]);
    
}

-(void)changeBtnImage:(NSInteger)tag{
    for (NSInteger i = 0; i<self.payIdMuArray.count; i++) {
        UIButton * btn = [self.view viewWithTag:[self.payIdMuArray[i] integerValue]+6000];
        [btn setImage:[UIImage imageNamed:@"check_0_icon"] forState:(UIControlStateNormal)];
    }
     UIButton * btn = [self.view viewWithTag:tag];
    [btn setImage:[UIImage imageNamed:@"check_1_icon"] forState:(UIControlStateNormal)];
   
}



#pragma mark onclick delegate
-(void)payBtnOnClick{
    [MBProgressHUD showHUD];
    __weak typeof (self)weself = self;
    NSString * payUrl = [NSString stringWithFormat:@"%@ctl=payment&act=get_payment_code&order_id=%@&payment_id=%@&email=%@&pwd=%@",GYLUrl,self.order_id,self.payId,EMAIL,USER_PWD];
    [RequestData requestDataOfUrl:payUrl success:^(NSDictionary *dic) {
        
        //余额支付
        if ([_payId isEqualToString:@"17"]) {
            if ([dic[@"status"] isEqual:@1]) {
                [MBProgressHUD dissmiss];
                PayResultVC * payresult = [PayResultVC new];
                payresult.number = @"1";
                payresult.price = weself.payMoney;
              [self.navigationController pushViewController:payresult animated:YES];
            }else{
                [MBProgressHUD dissmiss];
                [MBProgressHUD showError:dic[@"info"] toView:weself.view];
            }
        }else{
            //第三方支付
//            3=app支付宝 4=app微信 5=余额支付 2=web微信 9=线下支付
            NSString * payname = dic[@"payment_code"][@"class_name"];
            PayObject * obj = [PayObject new];
            //支付宝
            if ([self.onlineId isEqualToString:@"3"]) {
                [obj getParInfoWithURL:dic callback:^(NSDictionary *result) {
                    NSLog(@"pay -- %@",result);
                    NSString * resultStatus = result[@"resultStatus"];
                    if ([resultStatus isEqualToString:@"9000"]) {
                        [MBProgressHUD dissmiss];
                        PayResultVC * payresult = [PayResultVC new];
                        payresult.number = @"1";
                        payresult.price = [NSString stringWithFormat:@"￥%@",weself.payMoney];
                        [self.navigationController pushViewController:payresult animated:YES];
                    }else if ([resultStatus integerValue]>=6000 && [resultStatus integerValue] < 7000){
                        [MBProgressHUD dissmiss];
                        [MBProgressHUD showError:@"订单已取消" toView:weself.view];
                    }
                    NSLog(@"result %@",result);
                }];
            }else if([self.onlineId isEqualToString:@"4"]){
                //微信
                [obj WXPayinfoWithDic:dic callback:^(Boolean result) {
                    NSLog(@"%hhu",result);
                    if ([WXApi isWXAppInstalled]) {
                        [WXApiManager sharedManager].delegate = self;
                        
                    }else{
                        [MBProgressHUD dissmiss];
                        [MBProgressHUD showError:@"未安装微信" toView:self.view];
                    }
                }];
            }
//            else if([payname isEqualToString:@"Zhaoshang"]){
//                //招商银行
//                NSString * pay_action = dic[@"payment_code"][@"pay_action"];
//                MainWebView * webview = [[MainWebView alloc]init];
//                webview.webUrl = pay_action;
//                webview.runjump = @"zhaoshangJump";
//               [self.navigationController pushViewController:webview animated:YES];
//                
//            }
            else if ([self.onlineId isEqualToString:@"9"]){
                [self.carView show];
                [self.carView setupData:dic[@"payment_code"]];
            }else{
                [MBProgressHUD dissmiss];
                PayResultVC * payresult = [PayResultVC new];
                payresult.number = @"1";
                payresult.price = weself.payMoney;
            }
        }
        [MBProgressHUD dissmiss];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];

}

-(void)managerJunpPay:(PayResp *)respose{
    NSLog(@"resp %@",respose);
    PayResp*response=(PayResp*)respose;
    switch(response.errCode){
        case WXSuccess:{
            PayResultVC * payresult = [PayResultVC new];
            payresult.number = @"1";
            payresult.price = [NSString stringWithFormat:@"￥%@",self.payMoney];
           [self.navigationController pushViewController:payresult animated:YES];
        }
            break;
        default:
            [MBProgressHUD showError:@"支付失败" toView:self.view];
            NSLog(@"支付失败，retcode=%d",respose.errCode);
            break;
    }
}

/**
 *  index  all:0  1,2,3,4
 */
-(void)allOrderVIewOnClick:(NSInteger)index{
    NSArray *allOrderTitleArray=[[NSArray alloc]initWithObjects:@"全部",@"待付款",@"待配送",@"待评价",@"售后", nil];
    NSMutableArray *itemTitleWidthArray=[[NSMutableArray alloc]init];
    NSMutableArray *vcArray=[[NSMutableArray alloc]init];
    for (int i=0; i<allOrderTitleArray.count;i++){
        Class class=[OrderListVC class];
        [vcArray addObject:class];
        
        NSNumber *itemWidth=[NSNumber numberWithFloat:[NSString sizeWithText:[allOrderTitleArray objectAtIndex:i] font:[UIFont systemFontOfSize:FONT_SIZE_M] maxSize:Max_Size].width+Default_Space];
        [itemTitleWidthArray addObject:itemWidth];
    }
    
    WMPageController *pageController=[[WMPageController alloc] initWithViewControllerClasses:vcArray andTheirTitles:allOrderTitleArray];
    pageController.postNotification = YES;
    pageController.bounces = NO;
    pageController.menuBGColor=[UIColor whiteColor];
    pageController.menuHeight=40;
    pageController.menuViewStyle = WMMenuViewStyleLine;
    pageController.progressHeight = 2;
    pageController.itemsWidths=itemTitleWidthArray;
    pageController.progressViewWidths = itemTitleWidthArray;
    pageController.titleSizeSelected = FONT_SIZE_M;
    pageController.titleSizeNormal=FONT_SIZE_M;
    //    pageController.titleFontName=@"Helvetica-Bold";
    pageController.titleColorSelected=APPColor;
    pageController.titleColorNormal=APPFourColor;
    pageController.selectIndex = (int)index;
    [pageController initBar];
    
    [self.navigationController pushViewController:pageController animated:YES];
    
    
}

- (ShowCardInfo *)carView{
    
    if (!_carView) {
        self.carView = [[ShowCardInfo alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        
        _carView.contrc = self;
        [self.view addSubview:self.carView];
    }
    return _carView;
    
}

@end
