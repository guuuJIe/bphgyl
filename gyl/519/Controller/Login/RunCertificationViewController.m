///Users/macmini/Desktop/gylbb/519.xcodeproj
//  RunCertificationViewController.m
//  519
//
//  Created by Macmini on 2017/12/11.
//  Copyright © 2017年 519. All rights reserved.
//

#import "RunCertificationViewController.h"
#import "CertificationViewController.h"
@interface RunCertificationViewController ()
@property (weak, nonatomic) IBOutlet UIView *isRunCertificationView;
@property (weak, nonatomic) IBOutlet UIButton *recycleBtn;
@property (weak, nonatomic) IBOutlet UIView *noRunView;
@property (weak, nonatomic) IBOutlet UIView *runView;
@property (weak, nonatomic) IBOutlet UILabel *reasonLabel;
@property (weak, nonatomic) IBOutlet UIButton *backHome;
@property (weak, nonatomic) IBOutlet UIView *yesRunView;
@property (weak, nonatomic) IBOutlet UILabel *statueLbl;
@property (weak, nonatomic) IBOutlet UIImageView *verifyPic;

@end

@implementation RunCertificationViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
   
    self.navigationItem.title = @"企业认证";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"" style:(UIBarButtonItemStyleDone) target:nil action:nil];
     [MBProgressHUD showHUD];
    self.yesRunView.hidden = true;
    if (_rz_status == 2) {
//        self.noRunView.hidden = YES;
//        self.recycleBtn.hidden = YES;
       
    }else if (_rz_status == 3) {
//        self.runView.hidden = YES;
       
    }
    else{
       
//        self.noRunView.hidden = YES;
//        self.recycleBtn.hidden = YES;
//        [self.verifyPic setImage:[UIImage imageNamed:@""]];
    }
     if ([self.isJumpFrom isEqualToString:@"fromGoodInfo"]) {
         [_backHome setTitle:@"返回" forState:(UIControlStateNormal)];
     }
    [self requestDataForCertifivation];
}
- (IBAction)recycleUpload:(id)sender {
//    for (UIViewController *controller in self.navigationController.viewControllers) {
//        if ([controller isKindOfClass:[CertificationViewController class]]) {
//            CertificationViewController *revise =(CertificationViewController *)controller;
//            [self.navigationController popToViewController:revise animated:YES];
//        }else{
            CertificationViewController * vc = [[CertificationViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
//        }
//    }
}

- (IBAction)goHome:(id)sender {
    LoginViewController * vc = [[LoginViewController alloc]init];
    if (vc.isFormJump == 1) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        if ([self.isJumpFrom isEqualToString:@"fromGoodInfo"]) {
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}
-(void)requestDataForCertifivation{
    NSString * urlstring = [NSString stringWithFormat:@"http://www.bphgyl.com/cxb_api/index.php?ctl=user&act=renzheng&email=%@&pwd=%@",EMAIL,USER_PWD];
    [RequestData requestDataOfUrl:urlstring success:^(NSDictionary *dic) {
        NSLog(@"%@",dic);
        [MBProgressHUD dissmiss];
        if ([dic[@"status"] isEqual: @1]) {
            _rz_status = [[NSString stringWithFormat:@"%@",dic[@"data"][@"rz_status"]]integerValue];
            [kUserDefaults setObject:[NSString stringWithFormat:@"%ld",(long)_rz_status] forKey:@"rz_status"];
            if ([[NSString stringWithFormat:@"%@",dic[@"data"][@"rz_status"]] isEqualToString:@"3"]) {
                 self.runView.hidden = YES;
                self.statueLbl.text = @"抱歉，您的审核未通过";
                [self.reasonLabel setText:dic[@"data"][@"rz_info"]];
                 [self.verifyPic setImage:[UIImage imageNamed:@"verifyFail"]];
            }else if ([[NSString stringWithFormat:@"%@",dic[@"data"][@"rz_status"]] isEqualToString:@"2"]){
                self.noRunView.hidden = false;
                self.recycleBtn.hidden = YES;
                self.statueLbl.text = @"提交成功，请耐心等待管理员审核!";
                [self.reasonLabel setText:@"预计将在1-3个工作日内审核完成"];
                [self.verifyPic setImage:[UIImage imageNamed:@"verifying"]];
            }else if ([[NSString stringWithFormat:@"%@",dic[@"data"][@"rz_status"]] isEqualToString:@"1"]){
                self.noRunView.hidden = false;
                self.recycleBtn.hidden = YES;
                self.statueLbl.text = @"恭喜你通过审核,审核已通过";
                [self.reasonLabel setText:@""];
                [self.verifyPic setImage:[UIImage imageNamed:@"verifySucess"]];
            }else if ([[NSString stringWithFormat:@"%@",dic[@"data"][@"rz_status"]] isEqualToString:@"0"]){
                self.statueLbl.text = @"未审核";
            }
            
        }
    } failure:^(NSError *error) {
        
    }];
}






-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    AppDelegate * appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appdelegate.serviceBtn.hidden = YES;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];

    AppDelegate * appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appdelegate.serviceBtn.hidden = NO;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
