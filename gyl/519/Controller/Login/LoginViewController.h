//
//  LoginViewController.h
//  519
//
//  Created by Macmini on 2017/11/30.
//  Copyright © 2017年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController

/**
 *  isFromJump 1:首页客服按钮未登录跳转
 */
@property (nonatomic,assign)NSInteger isFormJump;


@end
