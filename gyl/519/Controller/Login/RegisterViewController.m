//
//  RegisterViewController.m
//  519
//
//  Created by Macmini on 2017/12/2.
//  Copyright © 2017年 519. All rights reserved.
//

#import "RegisterViewController.h"
#import "CertificationViewController.h"
@interface RegisterViewController ()
{
    NSTimer * _timer;
    int sendMessageTime;
}
@property (weak, nonatomic) IBOutlet UITextField *mabileTextField;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UITextField *pwdTextField;
@property (weak, nonatomic) IBOutlet UILabel *provisionLabel;

@property (weak, nonatomic) IBOutlet UIButton *sendMessageBtn;
@property (weak, nonatomic) IBOutlet UIButton *nestBtn;
@property (weak, nonatomic) IBOutlet UIButton *clauseBtn;



@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
     self.edgesForExtendedLayout = UIRectEdgeNone;
//    [self.navigationController setNavigationBarHidden:false animated:true];
//    self.view.backgroundColor = [uic]
    if (self.isRegister == 0) {
        self.navigationItem.title = @"注册";
    }else{
        if (self.isRegister == 1) {
            self.navigationItem.title = @"重置密码";
        }else{
            self.navigationItem.title = @"修改密码";
        }
        
        _provisionLabel.hidden = YES;
        _clauseBtn.hidden = YES;
    }
    sendMessageTime = 60;
}

-(BOOL)navigationShouldPopOnBackButton{
    LoginViewController *vc = [LoginViewController new];
    if (vc.isFormJump == 1) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    return NO;
}

//是否显示密码
- (IBAction)showPwdBtn:(UIButton *)sender {
    if (sender.selected) {
        sender.selected = NO;
        self.pwdTextField.secureTextEntry = YES;
        [sender setImage:[UIImage imageNamed:@"button_不显示密码"] forState:(UIControlStateNormal)];
    }else{
        sender.selected = YES;
        self.pwdTextField.secureTextEntry = NO;
        [sender setImage:[UIImage imageNamed:@"button_显示密码"] forState:(UIControlStateNormal)];
    }
}

//下一步
- (IBAction)nextBtn:(id)sender {
    NSString * reginUrl;
    
    if (_isRegister == 0){
        reginUrl = [NSString stringWithFormat:@"%@ctl=user&act=dophlogin&mobile=%@&sms_verify=%@&user_pwd=%@&is_reg=1",GYLUrl,self.mabileTextField.text,self.codeTextField.text,self.pwdTextField.text];
    }
    if (_isRegister == 1){
        reginUrl = [NSString stringWithFormat:@"%@ctl=user&act=phmodifypassword&mobile=%@&sms_verify=%@&new_pwd=%@",GYLUrl,self.mabileTextField.text,self.codeTextField.text,self.pwdTextField.text];
    }


    [RequestData requestDataOfUrl:reginUrl success:^(NSDictionary *dic) {
        NSLog(@"%@",dic);
        NSString * info = dic[@"info"];
        if ([dic[@"status"] isEqual:@1]) {
            [MBProgressHUD showSuccess:info toView:self.view];
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                [kUserDefaults setObject:dic[@"user_name"] forKey:@"user_name"];
                [kUserDefaults setObject:dic[@"user_pwd"] forKey:@"user_pwd"];
                [kUserDefaults setObject:dic[@"mobile"] forKey:@"email"];
                [kUserDefaults setObject:[NSString stringWithFormat:@"%@",dic[@"sales_oid"]] forKey:@"sales_oid"];
                [kUserDefaults setObject:[NSString stringWithFormat:@"%@",dic[@"group_id"]] forKey:@"group_id"];
                [kUserDefaults synchronize];
            });
            [[QYSDK sharedSDK]logout:^{
                
            }];
            [self onchat];
            if (_isRegister == 0) {
                CertificationViewController * vc = [CertificationViewController new];
                vc.isJumpCertiricat = @"isfromRegister";
                [self.navigationController pushViewController:vc animated:YES];
            }
            if (_isRegister == 1) {
                double delayInSeconds = 1.0;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                LoginViewController *login = [[LoginViewController alloc] init];
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [self.navigationController pushViewController:login animated:true];
                });
            }
        }else{
            [MBProgressHUD showError:info toView:self.view];
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
-(void)onchat{
    QYSource *source = [[QYSource alloc] init];
    source.title =  @"泊啤汇";
    source.urlString = @"https://8.163.com/";
    QYSessionViewController *sessionViewController = [[QYSDK sharedSDK] sessionViewController];
   // sessionViewController.delegate = self;
    sessionViewController.sessionTitle = @"供应链";
    sessionViewController.source = source;
    
    if (EMAIL && SALES_OID) {
        sessionViewController.staffId = [SALES_OID intValue];
    }else{
        sessionViewController.groupId = 880528;
    }
    [[QYSDK sharedSDK] customUIConfig].bottomMargin = 2;
}


//发送验证码
- (IBAction)sendMessageBtn:(UIButton *)sender {
    if (_mabileTextField.text.length == 11) {
        NSString * sendUrl;
        if(_isRegister == 0){
            sendUrl = [NSString stringWithFormat:@"%@ctl=sms&act=send_sms_code&mobile=%@&unique=1",GYLUrl,self.mabileTextField.text];
        }
        if (_isRegister == 1) {
            sendUrl = [NSString stringWithFormat:@"%@ctl=sms&act=send_sms_code&mobile=%@&unique=2",GYLUrl,self.mabileTextField.text];
        }
        [RequestData requestDataOfUrl:sendUrl success:^(NSDictionary *dic) {
            
            if ([dic[@"status"] isEqual:@1]) {
                _timer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(changeSMSTime) userInfo:nil repeats:YES];
                [_timer fire];
                sender.enabled = NO;
                [MBProgressHUD showSuccess:dic[@"info"] toView:self.view];
            }else{
                [MBProgressHUD showError:dic[@"info"] toView:self.view];
            }
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }
}
-(void)changeSMSTime{
    sendMessageTime -= 1;
    if (sendMessageTime == 0) {
        sendMessageTime = 60;
        [_timer invalidate];
        self.sendMessageBtn.titleLabel.text = @"重新发送验证码";
        [self.sendMessageBtn setTitle:@"重新发送验证码" forState:(UIControlStateNormal)];
        self.sendMessageBtn.enabled = YES;
    }else{
        self.sendMessageBtn.titleLabel.text = [NSString stringWithFormat:@"%d秒后重新获取",sendMessageTime];
        [self.sendMessageBtn setTitle:[NSString stringWithFormat:@"%d秒后重新获取",sendMessageTime] forState:(UIControlStateNormal)];
    }
}

//条款点击
- (IBAction)bphClause:(UIButton *)sender {
    
}



-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{

    if (_mabileTextField.text.length>2 && _codeTextField.text.length>2 && _pwdTextField.text.length>3) {
        [_nestBtn setBackgroundColor:[UIColor orangeColor]];
        _nestBtn.enabled = YES;
    }else{
        [_nestBtn setBackgroundColor:APPThreeColor];
        _nestBtn.enabled = NO;
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return  YES;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
