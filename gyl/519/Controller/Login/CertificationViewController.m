//
//  CertificationViewController.m
//  519
//
//  Created by Macmini on 2017/12/2.
//  Copyright © 2017年 519. All rights reserved.
//

#import "CertificationViewController.h"
#import "EnterpriceTableViewCell.h"
#import "BossInfomationTableViewCell.h"
#import "ChangeCrameView.h"
#import "RunCertificationViewController.h"
#import "base64.h"
#import "RegisterViewController.h"
@interface CertificationViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    NSString * _enterpriceName;
    NSString * _enterpriceNumber;
    
    NSString * _bossName;
    NSString * _bossSFZNumber;
    NSString * _molibeNumber;
    NSString * _bossAddress;
}
@property (nonatomic,strong)UITableView * certificationTableView;
@property (nonatomic,strong)UIView * endView;
@property (nonatomic,strong)EnterpriceTableViewCell * enterpriceCell;
@property (nonatomic,strong)BossInfomationTableViewCell * bossInfoCell;
@property (nonatomic,strong)ChangeCrameView * crameView;
@property (nonatomic,strong)UITextField * editTextf;
@property (nonatomic,strong)UIButton * clickBtn;
@property (nonatomic,copy)NSString * imagekey;
@property (nonatomic,strong)NSMutableArray * imageValueArray;
@property (nonatomic,strong)UIView * bgView;
@property (nonatomic,strong)UILabel * numLabel;
@property (nonatomic,strong)UITextField * toolText;
@property (nonatomic,assign)BOOL isFirst;
@property (nonatomic,strong)NSMutableDictionary *saveDic;
@end

@implementation CertificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [MBProgressHUD showHUD];
    _imageValueArray = [NSMutableArray new];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    self.navigationItem.title = @"企业认证";
    [self payInitBar];
    [self createConfigUI];
    [self requestCertificationVCData];
    
    _isFirst = true;
    _saveDic = [NSMutableDictionary new];
    
}
-(void)payInitBar{
    // 左上角的返回按钮
    UIView * btnView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 50,30)];
    
    UIButton *backButton = [[UIButton alloc]initWithFrame:btnView.bounds];
    [backButton setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [backButton setTitle:@"返回" forState:(UIControlStateNormal)];
    [backButton setTitleColor:APPFourColor forState:(UIControlState)UIControlStateNormal];
    backButton.titleLabel.font = [UIFont systemFontOfSize:15];
    backButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    backButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [backButton addTarget:self action:@selector(onback) forControlEvents:UIControlEventTouchUpInside];
    backButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0); // 这里微调返回键的位置可以让它看上去和左边紧贴
    [btnView addSubview:backButton];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:btnView];
}
-(void)onback{
    if ([self.isJumpCertiricat isEqualToString:@"isfromRegister"] || [self.isJumpCertiricat isEqualToString:@"isfromLogin"]) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
}

-(void)createConfigUI{
    [self.view addSubview:self.endView];
    _endView.backgroundColor = [UIColor redColor];
   
    [self.endView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.mas_equalTo(50);
    }];
    UIButton * uploadBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 50)];
    [uploadBtn setTitle:@"提交" forState:(UIControlStateNormal)];
    [uploadBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [uploadBtn setBackgroundColor:APPColor];
    [uploadBtn addTarget:self action:@selector(clickUploadBtn) forControlEvents:(UIControlEventTouchUpInside)];
    [self.endView addSubview:uploadBtn];
    [self.view addSubview:self.certificationTableView];
    [self.certificationTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.bottom.mas_equalTo(self.endView.mas_top).offset(0);
    }];
   
    
}

-(void)requestCertificationVCData{
    NSString * urlstring = [NSString stringWithFormat:@"http://www.bphgyl.com/cxb_api/index.php?ctl=user&act=renzheng&email=%@&pwd=%@",EMAIL,USER_PWD];
    [RequestData requestDataOfUrl:urlstring success:^(NSDictionary *dic) {
        NSLog(@"%@",dic);
        if ([dic[@"status"] isEqual:@1]) {
            NSString * rz_status = [NSString stringWithFormat:@"%@",dic[@"data"][@"rz_status"]];
            if ([rz_status isEqualToString:@"3"]) {
                self.enterpriceCell.enterpriceNameTF.text = dic[@"data"][@"company"];
                self.enterpriceCell.enterpriceNumberTF.text = dic[@"data"][@"company_id"];
                self.bossInfoCell.userNameTextField.text = dic[@"data"][@"company_faren"];
                self.bossInfoCell.molibeTextField.text = dic[@"data"][@"faren_mobile"];
                self.bossInfoCell.sfzNumberTextField.text = dic[@"data"][@"person_id"];
                self.bossInfoCell.addressTextField.text = dic[@"data"][@"address"];
                self.bossInfoCell.saleManId.text = dic[@"data"][@"sales_id"];
                for (NSInteger i = 0; i<4; i++) {
                    UIButton * btn = [self.view viewWithTag:70+i];
                    NSString * btnImage;
                    NSString * btnKey;
                    switch (btn.tag) {
                        case 70:
                            btnKey = @"company_id_img";
                            btnImage = dic[@"data"][@"company_id_img"];
                            break;
                        case 71:
                            btnKey = @"shop_img";
                            btnImage = dic[@"data"][@"shop_img"];
                            break;
                        case 72:
                            btnKey = @"person_id_img1";
                            btnImage = dic[@"data"][@"person_id_img1"];
                            break;
                        case 73:
                            btnKey = @"person_id_img2";
                            btnImage = dic[@"data"][@"person_id_img2"];
                            break;
                        default:
                            break;
                    }
                    
                    [_imageValueArray addObject:[NSString stringWithFormat:@"%@=%@",btnKey,btnImage]];
                    if (btnImage.length>0) {
                        [btn setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:btnImage]]] forState:(UIControlStateNormal)];
                    }
                }
            }
            
        }
        [self.certificationTableView reloadData];

        [MBProgressHUD dissmiss];
    } failure:^(NSError *error) {
        [MBProgressHUD dissmiss];
    }];
}

- (IBAction)recycleUpload:(id)sender {
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[RegisterViewController class]]) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else if ([controller isKindOfClass:[LoginViewController class]]) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        _enterpriceCell = [tableView dequeueReusableCellWithIdentifier:@"enterpriceCell"];
        _enterpriceCell.enterpriceNameTF.delegate = self;
        _enterpriceCell.enterpriceNumberTF.delegate = self;
        [_enterpriceCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        if (_enterpriceCell == nil) {
            _enterpriceCell = [[EnterpriceTableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"enterpriceCell"];
        }
        __weak typeof(self)weself = self;
        _enterpriceCell.uploadBlock = ^(UIButton *btn) {
            weself.clickBtn = btn;
            [weself.view addSubview:weself.crameView];
        };
        return  _enterpriceCell;
    }
    if (indexPath.row == 1) {
        _bossInfoCell = [tableView dequeueReusableCellWithIdentifier:@"bossinfoCell"];
        [_bossInfoCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        _bossInfoCell.userNameTextField.delegate = self;
        _bossInfoCell.sfzNumberTextField.delegate = self;
        _bossInfoCell.molibeTextField.delegate = self;
        _bossInfoCell.addressTextField.delegate = self;
        _bossInfoCell.saleManId.delegate = self;
        if (_bossInfoCell == nil) {
            _bossInfoCell = [[BossInfomationTableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"bossinfoCell"];
        }
        __weak typeof(self)weself = self;
        _bossInfoCell.uploadBlock = ^(UIButton *btn) {
            weself.clickBtn = btn;
            [weself.view addSubview:weself.crameView];
        };
        return  _bossInfoCell;
    }
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cellIde"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"cellIde"];
    }
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 400;
    }
    if (indexPath.row == 1) {
        return 600;
    }
    return 2;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    _editTextf = textField;
    
    
    if ([_editTextf isEqual:_bossInfoCell.saleManId]) {
        _editTextf.inputAccessoryView = [self addToolbar];
    }
    
    NSLog(@"1");
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
/*  键盘将要显示
*  @param notification 通知
*/
-(void)keyboardWillShow:(NSNotification *)notification
{
    //这样就拿到了键盘的位置大小信息frame，然后根据frame进行高度处理之类的信息
    CGRect frame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat height = frame.origin.y-50;
    
    CGFloat textHeight = 0.0;
    switch (_editTextf.tag) {
        case 50:
            textHeight =  110;
            break;
        case 51:
            textHeight =  160;
            break;
        case 52:
            textHeight =  400+100;
            break;
        case 53:
            textHeight =  400+150;
            break;
        case 54:
            textHeight =  400+200;
            break;
        case 55:
            textHeight =  400+250;
            break;
        case 56:
        {
//            [UIView animateWithDuration:0.5 animations:^{
//                self.certificationTableView.frame = CGRectMake(self.certificationTableView.frame.origin.x, self.certificationTableView.frame.origin.y-frame.size.height, self.certificationTableView.frame.size.width, self.certificationTableView.frame.size.height);
//            }];
        }
            break;
            
        default:
            break;
    }
    
   
    
    CGFloat space = -self.certificationTableView.contentOffset.y + textHeight;
    CGFloat transformY = height - space;
    NSLog(@"%lf",-self.certificationTableView.contentOffset.y);
    NSLog(@"%lf,%lf,%lf",height,space,transformY);
    if (transformY < 0) {
        CGRect frame = _certificationTableView.frame;
        frame.origin.y = transformY ;
        _certificationTableView.frame = frame;
    }
    
   
}
/**
 *  键盘将要隐藏
 *  @param notification 通知
 */
-(void)keyboardWillHidden:(NSNotification *)notification
{
    CGRect frame = self.certificationTableView.frame;
    frame.origin.y = 0;
    self.certificationTableView.frame = frame;
}

-(UITableView *)certificationTableView{
    if (!_certificationTableView) {
        _certificationTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
        _certificationTableView.delegate = self;
        _certificationTableView.dataSource = self;
        _certificationTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _certificationTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
        [_certificationTableView registerNib:[UINib nibWithNibName:@"EnterpriceTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"enterpriceCell"];
        [_certificationTableView registerNib:[UINib nibWithNibName:@"BossInfomationTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"bossinfoCell"];
        _certificationTableView.backgroundColor =APPBGColor;
        _certificationTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeNone;
    }
    return _certificationTableView;
}
-(UIView *)endView{
    if (!_endView) {
        _endView = [[UIView alloc]init];
        
        
        
    }
    return _endView;
}


-(void)clickUploadBtn{
//    [MBProgressHUD showHUD];
    //textfield.tag : 50--55
    NSMutableArray * textfStrArr = [NSMutableArray new];
    for (NSInteger i = 0; i<7; i++) {
        UITextField * textf = (UITextField *)[self.view viewWithTag:50+i];
        NSLog(@"%@",textf.text);
        if (textf.text.length == 0) {
            textf.text = @"";
        }
        [textfStrArr addObject:textf.text];
    }
    NSString * uploadImageS = @"";
    for (NSInteger i = 0; i<_imageValueArray.count; i++) {
        NSLog(@"%@",_imageValueArray[i]);
        
        if (uploadImageS.length<0) {
            
            uploadImageS = @"";
        }
        
        uploadImageS = [NSString stringWithFormat:@"%@&%@",uploadImageS,_imageValueArray[i]];
    }
    NSString * certificationUrl = [NSString stringWithFormat:@"%@&ctl=user&act=do_renzheng&company=%@&company_id=%@&company_faren=%@&person_id=%@&faren_mobile=%@&address=%@&%@&email=%@&pwd=%@&sales_id=%@",GYLUrl,textfStrArr[0],textfStrArr[1],textfStrArr[2],textfStrArr[3],textfStrArr[4],textfStrArr[5],uploadImageS,EMAIL,USER_PWD,textfStrArr[6]];
    NSLog(@"%@",certificationUrl);
    [RequestData requestDataOfUrl:certificationUrl success:^(NSDictionary *dic) {
        NSLog(@"%@",dic);
        [MBProgressHUD dissmiss];
        if ([dic[@"status"] isEqual:@1]) {
            RunCertificationViewController * vc = [[RunCertificationViewController alloc]initWithNibName:@"RunCertificationViewController" bundle:nil];
            vc.rz_status = 2;
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            [MBProgressHUD showError:dic[@"info"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD dissmiss];
    }];
    
    
}
-(ChangeCrameView *)crameView{
    if (!_crameView) {
        _crameView = [[ChangeCrameView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-Screen_NavBarHeight-Screen_StatusBarHeight)];
        __weak typeof(self)weself = self;
        _crameView.cameraBlock = ^(NSInteger num) {
            if (num == 0) {
                [weself addCamera];
            }
            if (num == 1) {
                [weself libraryAction];
            }
        };
    }
    return _crameView;
}
-(void)addCamera{
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    UIImagePickerController * pick = [[UIImagePickerController alloc]init];
    pick.delegate = self;
    pick.allowsEditing = YES;
    pick.sourceType = sourceType;
    [self presentViewController:pick animated:YES completion:^{
        [_crameView removeFromSuperview];
    }];
}

//点击相册
- (void)libraryAction {
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;//指定数据来源为相册
    imagePicker.delegate = self;
//    imagePicker.allowsEditing = YES;
    [self presentViewController:imagePicker animated:YES completion:nil];
     [_crameView removeFromSuperview];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    [picker dismissViewControllerAnimated:YES completion:^{
    
    }];
    
    UIImage *image = nil;
    if (picker.allowsEditing) {
        image = [info objectForKey:UIImagePickerControllerEditedImage];
    }else{
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    

    [self performSelector:@selector(saveImage:) withObject:image afterDelay:0.5];
}


#pragma mark------------------查看图片大小
- (void)calulateImageFileSize:(UIImage *)image {
    NSData *data = UIImagePNGRepresentation(image);
    if (!data) {
        data = UIImageJPEGRepresentation(image, 0.5);//需要改成0.5才接近原图片大小，原因请看下文
    }
    double dataLength = [data length] * 1.0;
    NSArray *typeArray = @[@"bytes",@"KB",@"MB",@"GB",@"TB",@"PB", @"EB",@"ZB",@"YB"];
    NSInteger index = 0;
    while (dataLength > 1024) {
        dataLength /= 1024.0;
        index ++;
    }
    NSLog(@"image = %.3f %@",dataLength,typeArray[index]);
}



//点击取消是调用的方法
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:^{

    }];
}
-(void)saveImage:(UIImage *)image{
    NSLog(@"image==%@",image);
    switch (_clickBtn.tag) {
        case 70:
            _imagekey = @"company_id_img";
            break;
        case 71:
            _imagekey = @"shop_img";
            break;
        case 72:
            _imagekey = @"person_id_img1";
            break;
        case 73:
            _imagekey = @"person_id_img2";
            break;
        default:
            break;
    }
    
    [self.view addSubview:self.bgView];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSString * urlString = [NSString stringWithFormat:@"%@&ctl=user&act=upload_image&email=%@&pwd=%@",GYLUrl,EMAIL,USER_PWD];
        //AFN3.0+基于封住HTPPSession的句柄
        AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //formData: 专门用于拼接需要上传的数据,在此位置生成一个要上传的数据体
        [manager POST:urlString  parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            
            // 压缩图片
            
            NSData *data=UIImageJPEGRepresentation(image, 1.0);
            if (data.length>300*1024) {
                if (data.length>1024*1024) {//1M以及以上
                    data=UIImageJPEGRepresentation(image, 0.1);
                }else if (data.length>512*1024) {//0.5M-1M
                    data=UIImageJPEGRepresentation(image, 0.5);
                }else if (data.length>300*1024) {//0.25M-0.5M
                    data=UIImageJPEGRepresentation(image, 0.9);
                }
            }
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            formatter.dateFormat = @"yyyyMMddHHmmss";
            NSString *str = [formatter stringFromDate:[NSDate date]];
            NSString *fileName = [NSString stringWithFormat:@"%@.jpg", str];
            [formData appendPartWithFileData:data name:@"file" fileName:fileName mimeType:@"image/jpg"];
            
        } progress:^(NSProgress * _Nonnull uploadProgress) {
            
            //上传进度
            // @property int64_t totalUnitCount;     需要下载文件的总大小
            // @property int64_t completedUnitCount; 当前已经下载的大小
            //
            // 给Progress添加监听 KVO
            NSLog(@"%f %lld",1.0 * uploadProgress.completedUnitCount / uploadProgress.totalUnitCount,uploadProgress.totalUnitCount);
            // 回到主队列刷新UI,用户自定义的进度条
            float pro =1.0 * uploadProgress.completedUnitCount / uploadProgress.totalUnitCount;
            dispatch_async(dispatch_get_main_queue(), ^{
                _numLabel.text = [NSString stringWithFormat:@"%.0f%%",pro*100];
                if (pro == 1.0) {
                    _numLabel.hidden = YES;
                    [MBProgressHUD showHUD];
                }
            });
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [MBProgressHUD dissmiss];
            [self.bgView removeFromSuperview];
            [_clickBtn setImage:image forState:(UIControlStateNormal)];
            NSString *base64Decoded = [[NSString alloc]
                                       initWithData:responseObject encoding:NSUTF8StringEncoding];
            
            NSData * jsondatastring = [Base64 decodeString:base64Decoded];
            
            NSDictionary * dic = [NSJSONSerialization JSONObjectWithData:jsondatastring options:NSJSONReadingMutableLeaves error:nil];
            NSString * acatar = dic[@"url"];
            NSString * imageDic = [NSString stringWithFormat:@"%@=%@",self.imagekey,acatar];
            
            NSInteger index = _clickBtn.tag-70;
            if (_imageValueArray.count==4) {
                
                [_imageValueArray replaceObjectAtIndex:index withObject:imageDic];
                
            }else{
                
                if ([_saveDic objectForKey:self.imagekey] == nil) {
                    [_imageValueArray addObject:imageDic];
                    [_saveDic setObject:imageDic forKey:self.imagekey];
                }else{
                    [_imageValueArray replaceObjectAtIndex:index withObject:imageDic];
                    
                }
                
               
               
             
               
                
               
                
            }
           
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self.bgView removeFromSuperview];
            [MBProgressHUD showError:@"请求超时，上传失败" toView:self.view];
            [MBProgressHUD dissmiss];
            NSLog(@"上传失败 %@", error);
        }];
    });
}
-(UIView *)bgView{
    if (!_bgView) {
        _bgView = [[UIView alloc]initWithFrame:self.view.bounds];
        _bgView.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.7];
        _numLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 30)];
        _numLabel.textColor = [UIColor whiteColor];
        _numLabel.center = _bgView.center;
        _numLabel.font = [UIFont systemFontOfSize:15];
        _numLabel.textAlignment = NSTextAlignmentCenter;
        [_bgView addSubview:_numLabel];
    }
    return _bgView;
}


-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [_editTextf resignFirstResponder];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    AppDelegate * appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appdelegate.serviceBtn.hidden = YES;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    AppDelegate * appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appdelegate.serviceBtn.hidden = NO;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    
}

- (UIToolbar *)addToolbar
{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 40)];
    toolbar.tintColor = [UIColor blueColor];
    toolbar.backgroundColor = [UIColor whiteColor];
    
    UITextField *text = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame)-100, 40)];
    text.placeholder = @"请输入推广员ID";
    text.delegate = self;
    self.toolText = text;
    
//    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithCustomView:text];
    
    UIBarButtonItem *bar = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(textFieldDone)];
    bar.tintColor = APPColor;
    toolbar.items = @[space,bar];
    return toolbar;
}

- (void)textFieldDone{
    [self.view endEditing:true];
}

@end
