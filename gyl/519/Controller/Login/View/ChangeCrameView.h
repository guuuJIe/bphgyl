//
//  ChangeCrameView.h
//  519
//
//  Created by Macmini on 2017/12/9.
//  Copyright © 2017年 519. All rights reserved.
//

typedef void(^addCameraAndPhotoBlock)(NSInteger num);
#import <UIKit/UIKit.h>

@interface ChangeCrameView : UIView
@property(nonatomic,copy)addCameraAndPhotoBlock cameraBlock;
@end
