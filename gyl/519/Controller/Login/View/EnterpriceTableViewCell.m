//
//  EnterpriceTableViewCell.m
//  519
//
//  Created by Macmini on 2017/12/2.
//  Copyright © 2017年 519. All rights reserved.
//

#import "EnterpriceTableViewCell.h"

@interface EnterpriceTableViewCell()<UITextFieldDelegate>

@end

@implementation EnterpriceTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) {
        self.enterpriceNameTF.delegate = self;
        self.enterpriceNumberTF.delegate = self;
        
    }
    return self;
}

//上传营业执照
- (IBAction)uploadLicence:(UIButton *)sender {
    [self endEditing:YES];
    if (self.uploadBlock) {
        self.uploadBlock(sender);
    }
}
//上传营业执照
- (IBAction)uploadStorePhoto:(UIButton *)sender {
    [self endEditing:YES];
    if (self.uploadBlock) {
        self.uploadBlock(sender);
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason{

}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"begin");
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSLog(@"%@",string);
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return  YES;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
