//
//  BossInfomationTableViewCell.h
//  519
//
//  Created by Macmini on 2017/12/2.
//  Copyright © 2017年 519. All rights reserved.
//
typedef void(^uploadImageBlock)(UIButton * btn);
#import <UIKit/UIKit.h>

@interface BossInfomationTableViewCell : UITableViewCell
@property (nonatomic,copy)uploadImageBlock uploadBlock;
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *sfzNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *molibeTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;

@property (weak, nonatomic) IBOutlet UITextField *saleManId;

@end
