//
//  EnterpriceTableViewCell.h
//  519
//
//  Created by Macmini on 2017/12/2.
//  Copyright © 2017年 519. All rights reserved.
//

typedef void(^uploadImageBlock)(UIButton * btn);

#import <UIKit/UIKit.h>
@interface EnterpriceTableViewCell : UITableViewCell

@property (nonatomic,copy)uploadImageBlock uploadBlock;
@property (weak, nonatomic) IBOutlet UITextField *enterpriceNameTF;
@property (weak, nonatomic) IBOutlet UITextField *enterpriceNumberTF;



@end
