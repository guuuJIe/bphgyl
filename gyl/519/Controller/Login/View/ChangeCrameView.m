//
//  ChangeCrameView.m
//  519
//
//  Created by Macmini on 2017/12/9.
//  Copyright © 2017年 519. All rights reserved.
//

#import "ChangeCrameView.h"

@interface ChangeCrameView()

@end


@implementation ChangeCrameView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.5];
        [self createUI];
    }
    return self;
}
-(void)createUI{
    UIView * whiteView = [[UIView alloc]initWithFrame:CGRectMake(0, self.height-200, self.width, 200)];
    whiteView.backgroundColor = [UIColor whiteColor];
    [self addSubview:whiteView];
    
    NSArray * imageArr = @[@"button_拍照",@"button_相册"];
    NSArray * titleArr = @[@"相机",@"相册"];
    for (NSInteger i = 0; i<2; i++) {
        UIImageView * cameraImageV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 60, 60)];
        cameraImageV.center = CGPointMake(self.width/4+self.width/2*i, whiteView.frame.size.height/2-30) ;
        cameraImageV.image = [UIImage imageNamed:imageArr[i]];
        cameraImageV.userInteractionEnabled = YES;
        cameraImageV.tag = 60+i;
        [whiteView addSubview:cameraImageV];
        
        UILabel * titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 50, 40)];
        titleLabel.text = titleArr[i];
        titleLabel.center = CGPointMake(self.width/4+self.width/2*i, CGRectGetMaxY(cameraImageV.frame)+25);
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor = APPFourColor;
        titleLabel.font = [UIFont systemFontOfSize:14];
        [whiteView addSubview:titleLabel];
        
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
        [cameraImageV addGestureRecognizer:tap];
    }
    
    UIButton * backBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    backBtn.center = CGPointMake(self.width/2, self.height-30);
    [backBtn setImage:[UIImage imageNamed:@"button_取消"] forState:(UIControlStateNormal)];
    [backBtn addTarget:self action:@selector(cancleBtn) forControlEvents:(UIControlEventTouchUpInside)];
    [self addSubview:backBtn];
    
}

-(void)tap:(UITapGestureRecognizer *)tap{
    NSInteger tag = tap.view.tag;
    switch (tag) {
        case 60:
            if (self.cameraBlock) {
                self.cameraBlock(0);
            }
            break;
        case 61:
            if (self.cameraBlock) {
                self.cameraBlock(1);
            }
            break;
        default:
            break;
    }
}





-(void)cancleBtn{
    [self removeFromSuperview];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    NSSet *allTouches = [event allTouches];    //返回与当前接收者有关的所有的触摸对象
    UITouch *touch = [allTouches anyObject];   //视图中的所有对象
    CGPoint point = [touch locationInView:self]; //返回触摸点在视图中的当前坐标
    CGFloat y = point.y;
    
    if (y < CGRectGetMaxY(self.frame)-200) {
        [UIView animateWithDuration:0.5 animations:^{
            [self removeFromSuperview];
        }];
    }
}
@end
