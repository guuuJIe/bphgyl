//
//  AddUserInfoViewController.h
//  519
//
//  Created by Macmini on 2017/12/27.
//  Copyright © 2017年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddUserInfoViewController : UIViewController
@property(nonatomic,copy)NSString * userName;
@property(nonatomic,copy)NSString * nickIcon;
@property(nonatomic,copy)NSString * unionid;
@end
