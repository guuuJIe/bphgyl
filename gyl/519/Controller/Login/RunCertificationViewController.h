//
//  RunCertificationViewController.h
//  519
//
//  Created by Macmini on 2017/12/11.
//  Copyright © 2017年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RunCertificationViewController : UIViewController

/**
 *  rz_status  2:等待审核  3:审核失败
 */
@property(nonatomic,assign)NSInteger rz_status;

/**
 *  isJumpFrom  fromGoodInfo:从goodInfovc 跳转
 */
@property(nonatomic,strong)NSString * isJumpFrom;

@end
