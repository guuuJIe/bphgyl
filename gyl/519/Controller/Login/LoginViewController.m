//
//  LoginViewController.m
//  519
//
//  Created by Macmini on 2017/11/30.
//  Copyright © 2017年 519. All rights reserved.
//



#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "CertificationViewController.h"
#import "RunCertificationViewController.h"
#import "WXApiManager.h"
#import "AddUserInfoViewController.h"
@interface LoginViewController ()<UITextFieldDelegate,WXApiManagerDelegate>
{
    /**
     * 0:账号登陆  1:短信登陆
     */
    int isAccoundLogin;
    /**
     *  重新发送验证码时间
     */
    int sendMessageTime;
    
    NSTimer * _timer;
    
    /**
     *  0:返回 1：前进
     */
    int isContious;
}

@property (weak, nonatomic) IBOutlet UIButton *accountBtn;
@property (weak, nonatomic) IBOutlet UIButton *phoneBtn;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *sendMessageBtn;

@property (weak, nonatomic) IBOutlet UIView *accoundDownView;
@property (weak, nonatomic) IBOutlet UIView *phoneDownView;
@property (weak, nonatomic) IBOutlet UIView *accoundLoginView;
@property (weak, nonatomic) IBOutlet UIView *phoneLoginView;
//账号登陆
@property (weak, nonatomic) IBOutlet UITextField *accountTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwoldTextField;
//短信登陆
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UIView *elseLoginWay;



@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"登陆";
    self.view.backgroundColor = [UIColor whiteColor];
//    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault; self.navigationController.navigationBar.tintColor = [UIColor blackColor];
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:(UIBarMetricsDefault)];
    isAccoundLogin = 0;
    sendMessageTime = 60;
    
    
    BOOL iswx = [WXApi isWXAppInstalled];
    if (iswx == NO) {
        self.elseLoginWay.hidden = YES;
    }
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    AppDelegate * appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appdelegate.serviceBtn.hidden = YES;
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
//    if (isContious == 0) {
//        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent; self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
//        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:APPColor] forBarMetrics:(UIBarMetricsDefault)];
//    }
    AppDelegate * appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appdelegate.serviceBtn.hidden = NO;
}

-(BOOL)navigationShouldPopOnBackButton{
    if (self.isFormJump == 1) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    return NO;
}

- (IBAction)userLogin:(UIButton *)sender {
    switch (sender.tag) {
        case 5000:
            isAccoundLogin=0;
            self.accoundLoginView.hidden = NO;
            self.accoundDownView.hidden = NO;
            self.phoneLoginView.hidden = YES;
            self.phoneDownView.hidden = YES;
            [self.accountBtn setTitleColor: [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1/1.0] forState:(UIControlStateNormal)];
            [self.phoneBtn setTitleColor:  [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1/1.0] forState:(UIControlStateNormal)];
            break;
        case 5001:
            isAccoundLogin=1;
            self.accoundLoginView.hidden = YES;
            self.accoundDownView.hidden = YES;
            self.phoneLoginView.hidden = NO;
            self.phoneDownView.hidden = NO;
            [self.phoneBtn setTitleColor: [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1/1.0] forState:(UIControlStateNormal)];
            [self.accountBtn setTitleColor:  [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1/1.0] forState:(UIControlStateNormal)];
            break;
        default:
            break;
    }
}
//查看密码
- (IBAction)checkPassword:(UIButton *)sender {
    if (sender.selected) {
        sender.selected = NO;
        self.passwoldTextField.secureTextEntry = YES;
        [sender setImage:[UIImage imageNamed:@"button_不显示密码"] forState:(UIControlStateNormal)];
        NSLog(@"1");
    }else{
        
        sender.selected = YES;
        self.passwoldTextField.secureTextEntry = NO;
        [sender setImage:[UIImage imageNamed:@"button_显示密码"] forState:(UIControlStateNormal)];
        NSLog(@"0");
    }
        
}

//登陆
- (IBAction)clickLoginBtn:(UIButton *)sender {
    NSLog(@"1");
    NSString * account;
    NSString * pwdOrCode;
    NSString * loginUrl;
    if (isAccoundLogin == 0) {
        account = _accountTextField.text;
        pwdOrCode = _passwoldTextField.text;
        loginUrl = [NSString stringWithFormat:@"%@&ctl=user&act=dologin&user_key=%@&user_pwd=%@",GYLUrl,account,pwdOrCode];
    }
    if (isAccoundLogin == 1) {
        account = _phoneTextField.text;
        pwdOrCode = _codeTextField.text;
        loginUrl = [NSString stringWithFormat:@"%@&ctl=user&act=dophlogin&mobile=%@&sms_verify=%@",GYLUrl,account,pwdOrCode];
    }
    
    [RequestData requestDataOfUrl:loginUrl success:^(NSDictionary *dic) {
        
        if ([dic[@"status"] isEqual:@1]) {
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                [kUserDefaults setObject:dic[@"user_pwd"] forKey:@"user_pwd"];
                [kUserDefaults setObject:dic[@"user_name"] forKey:@"user_name"];
                [kUserDefaults setObject:dic[@"mobile"] forKey:@"email"];
                [kUserDefaults setObject:[NSString stringWithFormat:@"%@",dic[@"rz_status"]] forKey:@"rz_status"];
                [kUserDefaults setObject:[NSString stringWithFormat:@"%@",dic[@"sales_oid"]] forKey:@"sales_oid"];
                [kUserDefaults setObject:[NSString stringWithFormat:@"%@",dic[@"group_id"]] forKey:@"group_id"];
                kUserDefaults.synchronize;
            });
            [[QYSDK sharedSDK] logout:^(){}];
            [self onchat];
            /**
             *  rz_status   0=未审核 1=通过审核 2=等待审核 3=审核失败
             */
            NSString * rz_status = [NSString stringWithFormat:@"%@",dic[@"rz_status"]];
            if ([rz_status isEqualToString:@"0"]) {
                CertificationViewController * vc = [[CertificationViewController alloc]init];
                vc.isJumpCertiricat = @"isfromLogin";
                [self.navigationController pushViewController:vc animated:YES];
            }
            if ([rz_status isEqualToString:@"1"]) {
                [self.navigationController popToRootViewControllerAnimated:YES];
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"loginRefresh" object:nil];
            }
            if ([rz_status isEqualToString:@"2"]) {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
            if ([rz_status isEqualToString:@"3"]) {
                RunCertificationViewController * vc = [[RunCertificationViewController alloc]initWithNibName:@"RunCertificationViewController" bundle:nil];
                vc.rz_status = 3;
                [self.navigationController pushViewController:vc animated:YES];
            }
            
            [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshCart" object:nil];
//
            
        }else{
            [MBProgressHUD showError:dic[@"info"] toView:self.view];
        }
    } failure:^(NSError *error) {
        
    }];
}

//注册
- (IBAction)clickRegisterBtn:(id)sender {
    isContious = 1;
    RegisterViewController * registe = [[RegisterViewController alloc]initWithNibName:@"RegisterViewController" bundle:nil];
    registe.isRegister = 0;
    [self.navigationController pushViewController:registe animated:YES];
}

//忘记密码
- (IBAction)clickBtnByForgetPasswold:(id)sender {
    isContious = 1;
    RegisterViewController * registe = [[RegisterViewController alloc]initWithNibName:@"RegisterViewController" bundle:nil];
    registe.isRegister = 1;
    [self.navigationController pushViewController:registe animated:YES];
}

//获取验证码
- (IBAction)obtianCodeByMessage:(UIButton *)sender {
    if (isAccoundLogin == 1) {
        if (_phoneTextField.text.length>10) {
            NSString * sendUrl = [NSString stringWithFormat:@"%@ctl=sms&act=send_sms_code&mobile=%@&unique=2",GYLUrl,self.phoneTextField.text];
            [RequestData requestDataOfUrl:sendUrl success:^(NSDictionary *dic) {
                
                if ([dic[@"status"] isEqual:@1]) {
                    sender.enabled = NO;
                    _timer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(changeSMSTime) userInfo:nil repeats:YES];
                    [_timer fire];
                    [MBProgressHUD showSuccess:dic[@"info"] toView:self.view];
                }else{
                    [MBProgressHUD showError:dic[@"info"] toView:self.view];
                }
                
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
            }];
        }
    }
    
}
-(void)changeSMSTime{
    sendMessageTime -= 1;
    if (sendMessageTime == 0) {
        sendMessageTime = 60;
        [_timer invalidate];
        self.sendMessageBtn.titleLabel.text = @"发送验证码";
        [self.sendMessageBtn setTitle:@"发送验证码" forState:(UIControlStateNormal)];
        self.sendMessageBtn.enabled = YES;
    }else{
        self.sendMessageBtn.titleLabel.text = [NSString stringWithFormat:@"%d秒后重新获取",sendMessageTime];
        [self.sendMessageBtn setTitle:[NSString stringWithFormat:@"%d秒后重新获取",sendMessageTime] forState:(UIControlStateNormal)];
    }
}
//微信登陆
- (IBAction)clickBtnByWeChat:(UIButton *)sender {
    [self umLogin:UMShareToWechatSession];
}


-(void)umLogin:(NSString*)type{
    
    
    //构造SendAuthReq结构体
    SendAuthReq* req =[[SendAuthReq alloc ] init];
    req.scope = @"snsapi_userinfo" ;
    req.state = @"123" ;
    req.openID = WX_APPID;
    //第三方向微信终端发送一个SendAuthReq消息结构
    [WXApiManager sharedManager].delegate = self;
    [WXApi sendReq:req];
    
    
}
- (void)managerDidRecvAuthResponse:(SendAuthResp *)response {
    [self getWeiXinOpenId:response.code];
}
//通过code获取access_token，openid，unionid
- (void)getWeiXinOpenId:(NSString *)code{
    [MBProgressHUD showHUD];
    NSString *url =[NSString stringWithFormat:@"https://api.weixin.qq.com/sns/oauth2/access_token?appid=%@&secret=%@&code=%@&grant_type=authorization_code",WX_APPID,WX_SSECRET,code];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *zoneUrl = [NSURL URLWithString:url];
        NSString *zoneStr = [NSString stringWithContentsOfURL:zoneUrl encoding:NSUTF8StringEncoding error:nil];
        NSData *data = [zoneStr dataUsingEncoding:NSUTF8StringEncoding];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data){
                NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                NSString *openID = dic[@"openid"];
                NSString * access_token=  dic[@"access_token"];
                [self requestUserdata:access_token withopenid:openID];
            }
        });
    });
}
-(void)requestUserdata:(NSString *)access_token withopenid:(NSString *)openid{
    
    NSString * userdataurl = [NSString stringWithFormat:@"https://api.weixin.qq.com/sns/userinfo?access_token=%@&openid=%@",access_token,openid];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *zoneUrl = [NSURL URLWithString:userdataurl];
        NSString *zoneStr = [NSString stringWithContentsOfURL:zoneUrl encoding:NSUTF8StringEncoding error:nil];
        NSData *data = [zoneStr dataUsingEncoding:NSUTF8StringEncoding];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data){
                NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                [self requestIsLogin:dic[@"unionid"] nickname:dic[@"nickname"] withheadimgurl:dic[@"headimgurl"]];
                
            }
        });
    });
}
//判断是否已经绑定手机号
-(void)requestIsLogin:(NSString *)unionid nickname:(NSString *)nickname  withheadimgurl:(NSString *)headimgurl{
    NSString * url = [NSString stringWithFormat:@"%@ctl=synclogin&act=weixin&unionid=%@",GYLUrl,unionid];
    [RequestData requestDataOfUrl:url success:^(NSDictionary *dic) {
        
        NSString * status = [NSString stringWithFormat:@"%@",dic[@"status"]];
        if (![status isEqualToString:@"1"]) {
            [MBProgressHUD dissmiss];
            AddUserInfoViewController * vc = [[AddUserInfoViewController alloc]initWithNibName:@"AddUserInfoViewController" bundle:nil];
            vc.nickIcon = headimgurl;
            vc.userName = nickname;
            vc.unionid = unionid;
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            [MBProgressHUD dissmiss];
            
            [[NSUserDefaults standardUserDefaults]setObject:nickname forKey:@"user_name"];
            [[NSUserDefaults standardUserDefaults]setObject:headimgurl forKey:@"headimgurl"];
            [kUserDefaults setObject:dic[@"mobile"] forKey:@"email"];
            [[NSUserDefaults standardUserDefaults]setObject:dic[@"user_pwd"] forKey:@"user_pwd"];
            [kUserDefaults setObject:[NSString stringWithFormat:@"%@",dic[@"sales_oid"]] forKey:@"sales_oid"];
            [kUserDefaults setObject:[NSString stringWithFormat:@"%@",dic[@"group_id"]] forKey:@"group_id"];
            kUserDefaults.synchronize;
            [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshCart" object:nil];
            [self onchat];
            [self.navigationController popToRootViewControllerAnimated:true];
        }
        
    } failure:^(NSError *error) {
        
    }];
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (isAccoundLogin == 0) {
        if (_accountTextField.text.length>=3 && _passwoldTextField.text.length>=3) {
            [_loginBtn setBackgroundColor:[UIColor orangeColor]];
            _loginBtn.enabled = YES;
        }else{
            [_loginBtn setBackgroundColor:APPThreeColor];
            _loginBtn.enabled = NO;
        }
    }
    if (isAccoundLogin == 1) {
        if (_phoneTextField.text.length == 11 && _codeTextField.text.length>=4) {
            [_loginBtn setBackgroundColor:[UIColor orangeColor]];
            _loginBtn.enabled = YES;
        }else{
            [_loginBtn setBackgroundColor:APPThreeColor];
            _loginBtn.enabled = NO;
        }
    }
    
    
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return  YES;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(void)onchat{
    QYSource *source = [[QYSource alloc] init];
    source.title =  @"泊啤汇";
    source.urlString = @"https://8.163.com/";
    QYSessionViewController *sessionViewController = [[QYSDK sharedSDK] sessionViewController];
    sessionViewController.sessionTitle = @"供应链x";
    sessionViewController.source = source;
    if(SALES_OID){
        sessionViewController.staffId = [SALES_OID intValue];
    }else{
        sessionViewController.groupId = 880528;
    }
    sessionViewController.hidesBottomBarWhenPushed = YES;
    
    UIBarButtonItem * leftIten = [[UIBarButtonItem alloc]initWithTitle:@"返回" style:(UIBarButtonItemStylePlain) target:self action:@selector(onback)];
    sessionViewController.navigationItem.leftBarButtonItem = leftIten;
}
-(void)onback{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
