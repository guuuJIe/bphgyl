//
//  AddUserInfoViewController.m
//  519
//
//  Created by Macmini on 2017/12/27.
//  Copyright © 2017年 519. All rights reserved.
//

#import "AddUserInfoViewController.h"
#import "CertificationViewController.h"
@interface AddUserInfoViewController ()
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTF;
@property (weak, nonatomic) IBOutlet UITextField *codeMessageTF;
@property (weak, nonatomic) IBOutlet UIButton *sendMessageBtn;
@property(nonatomic,assign)int sendMessageTime;
@property(nonatomic,strong)NSTimer *timer;
@end

@implementation AddUserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"绑定手机";
    _sendMessageTime = 60;
}

- (IBAction)sendSms:(UIButton*)sender {
    if (_phoneNumberTF.text.length>10) {
        NSString * sendUrl = [NSString stringWithFormat:@"%@ctl=sms&act=send_sms_code&mobile=%@&unique=0",GYLUrl,_phoneNumberTF.text];
        [RequestData requestDataOfUrl:sendUrl success:^(NSDictionary *dic) {
            
            if ([dic[@"status"] isEqual:@1]) {
                sender.enabled = NO;
                _timer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(changeSMSTime) userInfo:nil repeats:YES];
                [_timer fire];
                [MBProgressHUD showSuccess:dic[@"info"] toView:self.view];
            }else{
                [MBProgressHUD showError:dic[@"info"] toView:self.view];
            }
            
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }
}
-(void)changeSMSTime{
    _sendMessageTime -= 1;
    if (_sendMessageTime == 0) {
        _sendMessageTime = 60;
        [_timer invalidate];
        self.sendMessageBtn.titleLabel.text = @"发送验证码";
        [self.sendMessageBtn setTitle:@"发送验证码" forState:(UIControlStateNormal)];
        self.sendMessageBtn.enabled = YES;
    }else{
        self.sendMessageBtn.titleLabel.text = [NSString stringWithFormat:@"%d秒后重新获取",_sendMessageTime];
        [self.sendMessageBtn setTitle:[NSString stringWithFormat:@"%d秒后重新获取",_sendMessageTime] forState:(UIControlStateNormal)];
    }
}
- (IBAction)upload:(id)sender {
    if (self.phoneNumberTF.text.length>0 &&  self.codeMessageTF.text.length) {
        NSString * sendUrl = [NSString stringWithFormat:@"%@ctl=user&act=dophbind_register&mobile=%@&sms_verify=%@&unionid=%@&avatar=%@&user_name=%@",GYLUrl,self.phoneNumberTF.text,self.codeMessageTF.text,self.unionid,self.nickIcon,self.userName];
        
        [RequestData requestDataOfUrl:sendUrl success:^(NSDictionary *dic) {
            
            if([dic[@"status"] isEqual:@1]){
                [MBProgressHUD showSuccess:dic[@"info"] toView:self.view];
//                dispatch_async(dispatch_get_global_queue(0, 0), ^{
                    [[NSUserDefaults standardUserDefaults]setObject:dic[@"user_name"] forKey:@"user_name"];
                    [[NSUserDefaults standardUserDefaults]setObject:dic[@"user_pwd"] forKey:@"user_pwd"];
                    [kUserDefaults setObject:dic[@"mobile"] forKey:@"email"];
                    [kUserDefaults setObject:[NSString stringWithFormat:@"%@",dic[@"sales_oid"]] forKey:@"sales_oid"];
                kUserDefaults.synchronize;
//                });
                [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshCart" object:nil];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popToRootViewControllerAnimated:YES];
                });
//                CertificationViewController *  vc = [[CertificationViewController alloc]init];
//                vc.isJumpCertiricat = @"isfromRegister";
//                [self.navigationController pushViewController:vc animated:YES];
                
            }else{
                [MBProgressHUD showError:dic[@"info"] toView:self.view];
            }
        } failure:^(NSError *error) {
            [MBProgressHUD showError:@"网络错误" toView:self.view];
        }];
    }
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
