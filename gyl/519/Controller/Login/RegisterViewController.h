//
//  RegisterViewController.h
//  519
//
//  Created by Macmini on 2017/12/2.
//  Copyright © 2017年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController

/**
 *  isRegister  0:注册  1:忘记密码
 */
@property (nonatomic,assign)NSInteger isRegister; 
@end
