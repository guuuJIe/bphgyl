//
//  Type.m
//  519
//
//  Created by 陈 on 16/8/29.
//  Copyright © 2016年 519. All rights reserved.
//
static NSString * cellIde = @"typeIde";
#import "TypeVC.h"
#import "SelectVC.h"
#import "GoodListVC.h"
#import "GoodListView.h"
#import "CarModel.h"
#import "GoodInfoVC.h"
@interface TypeVC ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UIButton * searchBtn;
@property (nonatomic,strong)UITextField * searchfield;
@property (nonatomic,strong)GoodListView * listView;

@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSArray *tagArray;
@property (nonatomic,strong)NSMutableArray * goodTypeMuArr;
@property (nonatomic,strong)NSMutableArray * labelArr;

@property (nonatomic,strong)NSMutableArray * subGoodMuArray;
@property (nonatomic,strong)NSMutableArray * imgMuArray;
@property (nonatomic,strong)NSMutableArray * idMuArray;
@property (nonatomic,strong)NSMutableArray * secondIdMuArray;
@end

@implementation TypeVC

- (void)viewDidLoad {
    [super viewDidLoad];

    
    _goodTypeMuArr  = [NSMutableArray new];
    _labelArr = [NSMutableArray new];
    _subGoodMuArray = [NSMutableArray new];
    [self initBar];
    [self initView];
    
    [self requestDataOfTypeVC];
}

-(void)requestDataOfTypeVC{
    
    NSString * url = [NSString stringWithFormat:@"%@ctl=cate",GYLUrl];
    [RequestData requestDataOfUrl:url success:^(NSDictionary *dic) {
        NSString * titleStr;
        NSString * idStr;
        for (NSDictionary * dict in dic[@"bcate_list"]) {
            CateListModel * model = [[CateListModel alloc]init];
            [model setValuesForKeysWithDictionary:dict];
            [_goodTypeMuArr addObject:model];
            if (!titleStr) {
                titleStr = dict[@"name"];
            }
            if (!idStr) {
                idStr = [NSString stringWithFormat:@"%@",dict[@"id"]];
            }
            NSMutableArray * newMuArray = [NSMutableArray new];
            for (NSDictionary * namedic in dict[@"bcate_type"]) {
                CateGoodTypeModel * model = [[CateGoodTypeModel alloc]init];
                [model setValuesForKeysWithDictionary:namedic];
                [newMuArray addObject:model];
            }
            [_subGoodMuArray addObject:newMuArray];
        }
        [self.tableView reloadData];
        self.listView.typeLabel.text = titleStr;
        self.listView.allGoodsId = idStr;
        NSIndexPath *ip=[NSIndexPath indexPathForRow:0 inSection:0];
        [_tableView selectRowAtIndexPath:ip animated:YES scrollPosition:UITableViewScrollPositionBottom];
        NSArray * indexArr = _subGoodMuArray[0];
        self.listView.collectionArray = indexArr;
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [MBProgressHUD showError:@"网络错误" toView:self.view];
        
    }];
}

-(void)addRefreshWithNoInternet{
    
}


-(void)initBar{
    
    self.navigationController.navigationBar.translucent = true;
    UIView * searview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 40)];
    searview.backgroundColor = [UIColor clearColor];
    self.navigationItem.titleView = searview;
    
    self.searchBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 5, searview.frame.size.width-35, 30)];
    [self.searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    self.searchBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [self.searchBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.searchBtn setBackgroundColor:APPBGColor];
    [self.searchBtn setImage:[UIImage imageNamed:@"icon_搜索"] forState:(UIControlStateNormal)];
    [self.searchBtn addTarget:self action:@selector(onClickSearchBtn) forControlEvents:UIControlEventTouchUpInside];
    self.searchBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    self.searchBtn.layer.cornerRadius = 5;
    self.searchBtn.layer.masksToBounds = YES;
    [searview addSubview:self.searchBtn];
    
}

-(void)onClickSearchBtn{
    SelectVC *vc=[[SelectVC alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)initView{
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.listView];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _goodTypeMuArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIde];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:cellIde];
    }
    
    //cell.selectionStyle = UITableViewCellSelectionStyleNone;
    CateListModel * model = self.goodTypeMuArr[indexPath.row];
    
    cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
    cell.selectedBackgroundView.backgroundColor = APPBGColor;

    UILabel * celllabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 90, 50)];
    if (indexPath.row == 0) {
        celllabel.textColor = APPColor;
    }else{
        celllabel.textColor = APPFourColor;
   }
    celllabel.text = model.name;
    //elllabel.highlightedTextColor = APPColor;
    celllabel.textAlignment = NSTextAlignmentCenter;
    celllabel.font = [UIFont systemFontOfSize:15];
    [cell addSubview:celllabel];
    [self.labelArr addObject:celllabel];
    
    
    UIView * lineView = [[UIView alloc]init];
    lineView.backgroundColor = APPBGColor;
    [cell addSubview:lineView];
    [lineView makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.offset(0);
        make.height.offset(1);
    }];
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    for (NSInteger i=0;i<_labelArr.count; i++) {
        UILabel * label = _labelArr[i];
        if (i == indexPath.row) {
            label.textColor = APPColor;
        }else{
            label.textColor = APPFourColor;
        }
    }
    
    CateListModel * model = self.goodTypeMuArr[indexPath.row];
    self.listView.typeLabel.text = model.name;
    self.listView.allGoodsId = [NSString stringWithFormat:@"%@",model.Id];
    self.listView.collectionArray = _subGoodMuArray[indexPath.row];
    
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:APPThreeColor]];
}


-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 90, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight)];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.scrollEnabled = YES;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
        
    }
    return _tableView;
}
-(GoodListView *)listView{
    if (!_listView) {
        _listView = [[GoodListView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.tableView.frame), Screen_StatusBarHeight+Screen_NavBarHeight, Screen_Width-90, self.view.frame.size.height-Screen_NavBarHeight-Screen_StatusBarHeight-65)];
        __weak typeof(self)weself = self;
        _listView.block = ^(NSString *ids) {
            __strong typeof(weself)self = weself;
            GoodListVC * vc = [[GoodListVC alloc]init];
            vc.idStr = ids;
            [self.navigationController pushViewController:vc animated:YES];
        };
    }
    return _listView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
