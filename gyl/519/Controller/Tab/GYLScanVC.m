//
//  GYLScanVC.m
//  519
//
//  Created by Macmini on 2019/6/15.
//  Copyright © 2019 519. All rights reserved.
//
#define JSCGlobalTheme0f70044 [UIColor colorWithHexString:@"f70044"]
#import "GYLScanVC.h"
#import "LBXScanResult.h"
#import "LBXScanWrapper.h"
#import "LBXScanVideoZoomView.h"
#import "LBXAlertAction.h"
#import "GoodInfoVC.h"
#import "GoodListVC.h"
#import "MainWebView.h"
@interface GYLScanVC ()
@property (nonatomic, strong) UILabel *promptLabel;
@end

@implementation GYLScanVC
-(void)initViewController{
    //创建参数对象
    LBXScanViewStyle *style = [[LBXScanViewStyle alloc]init];
    
    //矩形区域中心上移，默认中心点为屏幕中心点
    style.centerUpOffset = 44;
    
    //扫码框周围4个角的类型,设置为外挂式
    style.photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle_Outer;
    
    //扫码框周围4个角绘制的线条宽度
    style.photoframeLineW = 3;
    
    //扫码框周围4个角的宽度
    style.photoframeAngleW = 24;
    
    //扫码框周围4个角的高度
    style.photoframeAngleH = 24;
    
    //扫码框内 动画类型 --线条上下移动
    style.anmiationStyle = LBXScanViewAnimationStyle_LineMove;
    
    //    //线条上下移动图片
    style.animationImage = [UIImage imageNamed:@"CodeScan.bundle/qrcode_scan_part_net"];;
    
    //码框周围4个角的颜色
    style.colorAngle = JSCGlobalTheme0f70044;
    //矩形框颜色
    style.colorRetangleLine = JSCGlobalTheme0f70044;
    
    self.style = style;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initViewController];
    [self setupNavigationBar];
    [self.view addSubview:self.promptLabel];
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    //    self.view.backgroundColor = [UIColor blackColor];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
  
        //[self setNavigationAlpha:0.01];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:(UIBarMetricsDefault)];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:APPThreeColor]];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
}

- (void)setupNavigationBar {
    self.navigationItem.title = @"扫一扫";
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"相册" style:(UIBarButtonItemStyleDone) target:self action:@selector(openPhoto)];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:APPColor] forBarMetrics:UIBarMetricsDefault];
}

- (void)dealloc{
    NSLog(@"销毁了");
}

- (void)showError:(NSString*)str
{
    
    [LBXAlertAction showAlertWithTitle:@"提示" msg:str buttonsStatement:@[@"知道了"] chooseBlock:nil];
    
}


- (void)scanResultWithArray:(NSArray<LBXScanResult*>*)array
{
    if (array.count < 1)
    {
        [self popAlertMsgWithScanResult:nil];
        
        return;
    }
    
    //经测试，可以同时识别2个二维码，不能同时识别二维码和条形码
    for (LBXScanResult *result in array) {
        NSLog(@"scanResult:%@",result.strScanned);
    }
    
    LBXScanResult *scanResult = array[0];
    
    NSString*strResult = scanResult.strScanned;
    
    self.scanImage = scanResult.imgScanned;
    
    //震动提醒
     [LBXScanWrapper systemVibrate];
    
    if (!strResult) {
        
        [self popAlertMsgWithScanResult:nil];
        
        return;
    }
    
     [self showNextVCWithScanResult:scanResult];
    
}

- (void)showNextVCWithScanResult:(LBXScanResult*)strResult{

    NSString *scanRes = strResult.strScanned;
    [self reStartDevice];
    if ([scanRes hasPrefix:@"http"]) {
        NSArray * arr = [scanRes componentsSeparatedByString:@"?"];
        NSArray *lastArr = [arr.lastObject componentsSeparatedByString:@"&"];
        
        if ([scanRes rangeOfString:@"ctl=list"].location != NSNotFound) {
            NSArray * arr = [scanRes componentsSeparatedByString:@"?"];
            NSArray *lastArr = [arr.lastObject componentsSeparatedByString:@"&"];
            NSString * las;
            for (NSString * substr in lastArr) {
                NSString * str;
                if ([substr isEqualToString:lastArr.firstObject]) {
                    str= [substr stringByReplacingOccurrencesOfString:@"f[" withString:@"\""];
                }else{
                    str= [substr stringByReplacingOccurrencesOfString:@"f[" withString:@""];
                }
                
                str = [str stringByReplacingOccurrencesOfString:@"]=" withString:@"\":\""];
                if (!las) {
                    las = str;
                }else{
                    las = [NSString stringWithFormat:@"%@\",\"%@",las,str];
                }
            }
            NSString * url = [NSString stringWithFormat:@"%@f={%@",GYLUrl,las];
            url = [url stringByReplacingOccurrencesOfString:@"ctl=list\",\"" withString:@"}&ctl=goods&"];
            GoodListVC * vc = [GoodListVC new];
//            vc.isFromWeb = YES;
//            vc.url = url;
            [self.navigationController pushViewController:vc animated:true];
            
            
        }else if([scanRes rangeOfString:@"ctl=item"].location != NSNotFound){
            NSArray * urlArr = [scanRes componentsSeparatedByString:@"="];
            GoodInfoVC *goodsInfo = [GoodInfoVC new];
            goodsInfo.goodsId = urlArr.lastObject;
            [self.navigationController pushViewController:goodsInfo animated:true];
        }else{
            NSLog(@"%@",[NSString stringWithFormat:@"%@", scanRes]);
            
            MainWebView *jumpVC = [[MainWebView alloc] init];
            jumpVC.webUrl = [NSString stringWithFormat:@"%@", scanRes];
            [self.navigationController pushViewController:jumpVC animated:true];
        }
        
       
//        GoodInfoVC *infoVC = [GoodInfoVC new];
//        infoVC.goodsId = []
        
    }else{
        
        
    }

}



- (void)popAlertMsgWithScanResult:(NSString*)strResult
{
    if (!strResult) {
        
        strResult = @"识别失败";
    }
    
    __weak __typeof(self) weakSelf = self;
    [LBXAlertAction showAlertWithTitle:@"扫码内容" msg:strResult buttonsStatement:@[@"知道了"] chooseBlock:^(NSInteger buttonIdx) {
        [weakSelf reStartDevice];
    }];
    
    
    
}

@end
