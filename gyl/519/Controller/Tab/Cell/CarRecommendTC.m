//
//  CarRecommendTC.m
//  519
//
//  Created by Macmini on 2018/1/17.
//  Copyright © 2018年 519. All rights reserved.
//

//为你推荐cell
#import "CarRecommendTC.h"
#import "MainHotTCCC.h"

@interface CarRecommendTC()<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong)UICollectionView * collectionView;
@property(nonatomic,strong)UIView * headView;

@end
@implementation CarRecommendTC

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = APPBGColor;
        [self createUI];
    }
    return self;
}

-(void)createUI{
    self.headView = [[UIView alloc]init];
    self.headView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.headView];
    [self.headView makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.top.offset(10);
        make.height.offset(40);
    }];
    UIImageView * iconImgV = [[UIImageView alloc]init];
    iconImgV.image = [UIImage imageNamed:@"为你推荐"];
    iconImgV.contentMode = UIViewContentModeScaleAspectFit;
    [self.headView addSubview:iconImgV];
    [iconImgV makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(10);
        make.top.offset(10);
        make.height.offset(20);
        make.width.offset(70);
    }];
    UILabel * titlelabel = [[UILabel alloc]init];
    titlelabel.text = @"精选爆款好酒";
    titlelabel.textColor = APPThreeColor;
    titlelabel.textAlignment = NSTextAlignmentLeft;
    titlelabel.font = [UIFont systemFontOfSize:14];
    [self.headView addSubview:titlelabel];
    [titlelabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(iconImgV.mas_right).offset(10);
        make.top.offset(10);
        make.height.offset(20);
    }];
    
    UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake((Screen_Width)/2, Screen_Width/2+60);
    layout.minimumLineSpacing = 1;
    layout.minimumInteritemSpacing = 1;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    _collectionView  = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
    [_collectionView registerClass:[CarAndMineBottomCell class] forCellWithReuseIdentifier:[CarAndMineBottomCell getID]];
    _collectionView.backgroundColor = APPBGColor;
    _collectionView.delegate = self;
    _collectionView.dataSource =self;
    _collectionView.scrollEnabled = NO;
    [self addSubview:_collectionView];
    [_collectionView makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headView.mas_bottom).offset(1);
        make.left.right.offset(0);
    }];
}
-(void)setDataArray:(NSArray *)dataArray{
    _dataArray = dataArray;
    [_collectionView reloadData];
    [_collectionView updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headView.mas_bottom).offset(1);
        make.left.right.offset(0);
        make.height.offset((Screen_Width/2+120)*(dataArray.count+1)/2);
    }];
}
#pragma collection delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _dataArray.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize itemSize=[self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:indexPath];
    
    CarAndMineBottomCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[CarAndMineBottomCell getID] forIndexPath:indexPath];
    if(cell==nil){
        cell=[[CarAndMineBottomCell alloc] initWithFrame:CGRectMake(0, 0,itemSize.width,itemSize.height)];
    }
    
    BestDealModel * model = self.dataArray[indexPath.row];
    cell.model = model;
    cell.clickBlock = ^(BestDealModel *model) {
        CGRect cellInCollection = [_collectionView convertRect:cell.frame toView:_collectionView];
        [self.delegate addIncar:model withPosition:cellInCollection withCollectionView:_collectionView andCurrentCell:cell];
    };
    return  cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(Screen_Width/2-.5, Screen_Width/2+120);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0,0,0,0);
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    BestDealModel * model = self.dataArray[indexPath.row];
//    [self.delegate clickHotCell:model.Id];
    if (self.block) {
        self.block(model.Id);
    }
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
+(NSString *)getID{
    return @"carRecommendTC";
}
@end
