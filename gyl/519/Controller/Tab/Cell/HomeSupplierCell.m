//
//  HomeSupplierCell.m
//  gylPT
//
//  Created by Macmini on 2017/8/29.
//  Copyright © 2017年 wzBPH. All rights reserved.
//

#import "HomeSupplierCell.h"
#import "HomeSupplierCC.h"
@interface HomeSupplierCell()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic,strong)UICollectionView * collectionview;
@property (nonatomic,strong)UIView * headView;
@property (nonatomic,strong)NSMutableArray * supplierArr;
@end


@implementation HomeSupplierCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.supplierArr = [NSMutableArray new];
        [self initView];
    }
    return self;
}

-(void)initView{
    
    self.headView = [[UIView alloc]init];
    self.headView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.headView];
    [self.headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).with.offset(0);
        make.left.equalTo(self).with.offset(5);
        make.right.equalTo(self).with.offset(-5);
        make.height.mas_equalTo(40);
    }];
    UIImageView * iconImg = [[UIImageView alloc] init];
    iconImg.contentMode = UIViewContentModeScaleAspectFit;
    iconImg.image = [UIImage imageNamed:@"热门供应商"];
    [self.headView addSubview:iconImg];
    [iconImg makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(10);
        make.centerY.equalTo(self.headView.mas_centerY);
        make.height.offset(20);
        make.width.offset(80);
    }];
    
    UILabel * nameLabel = [[UILabel alloc]init];
    nameLabel.text = @"更多供应商正在入驻";
    nameLabel.textColor = APPThreeColor;
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.font = [UIFont systemFontOfSize:13];
    [self.headView addSubview:nameLabel];
    
    UIButton * moreBtn = [[UIButton alloc]init];
    moreBtn.titleLabel.font = [UIFont  systemFontOfSize:11];
    moreBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    moreBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
    [moreBtn setTitle:@"更多 >>" forState:(UIControlStateNormal)];
    [moreBtn setTitleColor:APPThreeColor forState:(UIControlStateNormal)];
    [moreBtn addTarget:self action:@selector(moreBtnClick) forControlEvents:(UIControlEventTouchUpInside)];
    [self.headView addSubview:moreBtn];
    
    UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.collectionview = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
    self.collectionview.backgroundColor = [UIColor whiteColor];
    [self.collectionview registerClass:[HomeSupplierCC class] forCellWithReuseIdentifier:[HomeSupplierCC getIde]];
    self.collectionview.delegate= self;
    self.collectionview.dataSource = self;
    self.collectionview.scrollEnabled = NO;
    [self addSubview:self.collectionview];
    
    
    
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.headView.mas_centerY);
        make.left.equalTo(iconImg.mas_right).with.offset(10);
        //make.width.mas_equalTo(Screen_Width/2);
        make.height.mas_equalTo(40);
    }];
    [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.equalTo(self.headView).with.offset(5);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(30);
    }];

    [self.collectionview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(Screen_Width);
        //        make.height.mas_equalTo(Screen_Width/750*200*(self.supplierArr.count/2));
        make.bottom.equalTo(self.mas_bottom).with.offset(0);
        make.left.equalTo(self.mas_left).with.offset(0);
        make.top.equalTo(self.headView.mas_bottom).with.offset(0);
    }];
    
}

-(void)moreBtnClick{
    [self.deleate clickmoreBtn];
}

#pragma mark collection delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{

    return _supplierArr.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize itemSize=[self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:indexPath];
    
    HomeSupplierCC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[HomeSupplierCC getIde] forIndexPath:indexPath];
    if(cell==nil){
        cell=[[HomeSupplierCC alloc] initWithFrame:CGRectMake(0, 0,itemSize.width,itemSize.width)];
    }
    
    SupplierModel * model = self.supplierArr[indexPath.row];
    cell.model = model;
    return  cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(Screen_Width/2, Screen_Width/750*200);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    SupplierModel * model = self.supplierArr[indexPath.row];
    [self.deleate clickSupplierCell:model.Id];
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)showDataForSupplier:(NSArray *)supplierArr{
    [self.supplierArr removeAllObjects];
    if (supplierArr.count>0) {
        for (SupplierModel * model in supplierArr) {
            [self.supplierArr addObject: model];
        }
    }else{
        
    }
   
    
    
    [self.collectionview reloadData];
}



+(NSString *)getIDE{
    return @"supplierIDE";
}



@end
