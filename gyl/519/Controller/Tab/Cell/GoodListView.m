//
//  GoodListView.m
//  gylPT
//
//  Created by Macmini on 2018/1/9.
//  Copyright © 2018年 wzBPH. All rights reserved.
//

//typevc 界面有方自定义view
static NSString * collectionIde = @"goodListCollectionIde";
#import "GoodListView.h"

@interface GoodListView()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic,strong)UICollectionView * collectionView;
@end

@implementation GoodListView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = APPBGColor;
        [self createUI];
    }
    return self;
}

-(void)createUI{
    UIView * whiteView = [[UIView alloc]init];
    whiteView.backgroundColor = [UIColor whiteColor];
    [self addSubview:whiteView];
    [whiteView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.offset(10);
        make.width.offset(self.frame.size.width-20) ;
        make.height.offset(40);
    }];
    
    self.typeLabel = [[UILabel alloc]init];
    _typeLabel.textColor =APPFourColor;
    _typeLabel.textAlignment = NSTextAlignmentLeft;
    _typeLabel.font = [UIFont systemFontOfSize:13];
    [whiteView addSubview:self.typeLabel];
    [_typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.offset(0);
        make.left.offset(10);
    }];
    UIButton * moreBtn = [[UIButton alloc]init];
    [moreBtn setTitle:@"全部" forState:(UIControlStateNormal)];
    moreBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [moreBtn setTitleColor:APPThreeColor forState:(UIControlStateNormal)];
    [moreBtn addTarget:self action:@selector(mobtnClick) forControlEvents:(UIControlEventTouchUpInside)];
    [whiteView addSubview:moreBtn];
    [moreBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.offset(0);
        make.right.offset(-10);
    }];
    
    
    UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake((Screen_Width-110-3)/3, 100) ;
    layout.minimumLineSpacing = 1;
    layout.minimumInteritemSpacing = 1;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    _collectionView  = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
    _collectionView.backgroundColor = APPBGColor;
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.scrollEnabled = YES;
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:collectionIde];
    [self addSubview:_collectionView];
    [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(whiteView.mas_bottom).with.offset(10);
        make.left.offset(10);
        make.right.offset(-10);
        make.bottom.offset(-10);
    }];
}
-(void)mobtnClick{
    if (self.block) {
        self.block(self.allGoodsId);
    }
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _collectionArray.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:collectionIde forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[UICollectionViewCell alloc]initWithFrame:CGRectMake(0, 0, (Screen_Width-110-3)/3, 100)];
    }else{
        for (UIView * subView in cell.subviews) {
            [subView removeFromSuperview];
        }
    }
    _model = self.collectionArray[indexPath.row];
    cell.backgroundColor = [UIColor whiteColor];
    UIImageView * iconImgV = [[UIImageView alloc]init];
    iconImgV.contentMode = UIViewContentModeScaleAspectFit;
    [iconImgV sd_setImageWithURL:[NSURL URLWithString:_model.cate_img] placeholderImage:[UIImage imageNamed:@"default_Img"]];
    [cell addSubview:iconImgV];
    [iconImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(5);
        make.centerX.equalTo(cell.mas_centerX);
        make.height.offset(70);
        make.width.offset(70);
    }];
    
    UILabel * label = [[UILabel alloc]init];
    label.text = _model.name;
    label.textColor = APPFourColor;
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:12];
    [cell addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(iconImgV.mas_bottom).with.offset(0);
        make.height.offset(20);
        make.width.offset(cell.frame.size.width-20);
        make.centerX.equalTo(cell.mas_centerX);
    }];
    
    
    
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    _model = self.collectionArray[indexPath.row];
    if (self.block) {
        self.block(self.model.Id);
    }
    
}
-(void)setCollectionArray:(NSArray *)collectionArray{
    _collectionArray = collectionArray;
    
    [_collectionView reloadData];
}


@end
