//
//  MainTimeTC.h
//  519
//
//  Created by 陈 on 16/8/30.
//  Copyright © 2016年 519. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarModel.h"
@protocol MainTimeTCDelegate
-(void)allTimeGood;
-(void)clickTimerCell:(NSString *)timerID;
@end

@interface MainTimeTC : BaseTC
@property(nonatomic,strong)id<MainTimeTCDelegate> delagete;
@property(nonatomic,strong)BestDealModel * model;
-(void)showTime:(NSString *)time;
-(void)showData:(NSArray *)timerData;
+(NSString*)getID;
@end
