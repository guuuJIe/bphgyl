//
//  MainHotTCCC.h
//  519
//
//  Created by 陈 on 16/8/31.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseCC.h"
#import "CarModel.h"



@interface MainHotTCCC : BaseCC
@property(nonatomic,strong)BestDealModel * model;

+(NSString *)getID;

@property(nonatomic,copy)void (^clickBlock)(BestDealModel *model);
@property(nonatomic,strong)UIImageView *img;
@end
