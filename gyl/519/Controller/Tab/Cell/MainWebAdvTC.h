//
//  MainWebAdvTC.h
//  519
//
//  Created by 陈 on 16/8/31.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"

@protocol MainWebDelegate <NSObject>

-(void)webPush:(NSString *)pushUrl withUrlId:(NSString *)urlId;

@end


@interface MainWebAdvTC : BaseTC
@property(nonatomic,strong)UIWebView *webView;
@property(nonatomic,assign)id<MainWebDelegate>delegate;
-(void)showData:(NSString *)webString;

+(NSString *)getID;
@end
