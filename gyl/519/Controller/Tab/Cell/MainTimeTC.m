//
//  MainTimeTC.m
//  519
//
//  Created by 陈 on 16/8/30.
//  Copyright © 2016年 519. All rights reserved.
//

#import "MainTimeTC.h"
#import "MainTimeTCCC.h"
#import <sys/utsname.h>
@interface MainTimeTC()<UICollectionViewDataSource,UICollectionViewDelegate>
@property(nonatomic,strong)NSArray *data;

@property (nonatomic,strong)UIView *marLine;
@property (nonatomic,strong)UIView *moreView;
@property (nonatomic,strong)UIView *redLine;
@property(nonatomic,copy)NSString *xsqgStr;
@property(nonatomic,strong)UIFont *xsqgFont;
@property(nonatomic,strong)UILabel *xsqgLabel;
@property(nonatomic,copy)NSString *timeStr;
@property(nonatomic,strong)UIFont *timeFont;
@property(nonatomic,strong)UILabel *timeLabel;
@property(nonatomic,strong)NSString *moreStr;
@property(nonatomic,strong)UIFont *moreFont;
@property(nonatomic,strong)UILabel *moreLabel;

@property(nonatomic,strong)UIView *line;

@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,assign)CGFloat collectionViewHeight;
@property(nonatomic,assign)CGFloat xpoint;
@property(nonatomic,assign)int index;
@end

@implementation MainTimeTC

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        _index = 1;
        _xpoint = 0;
        [self initView];
    }
    
    return  self;
}

-(void)initView{
    self.backgroundColor=APPBGColor;
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    self.collectionViewHeight=Screen_Width/3.3+40;
    
    self.moreView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 40)];
    //self.moreView.backgroundColor=[UIColor whiteColor];
    UITapGestureRecognizer *moreViewTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(moreViewOnClick)];
    [self.moreView addGestureRecognizer:moreViewTap];
    
    self.marLine=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Default_Space)];
    self.marLine.backgroundColor=APPBGColor;
    [self.moreView addSubview:self.marLine];
    
    UIImageView * iconImgV = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 60, 20)];
    iconImgV.image = [UIImage imageNamed:@"限时抢购"];
    iconImgV.contentMode = UIViewContentModeScaleAspectFit;
    [self.moreView addSubview:iconImgV];
    
    self.timeStr=@"距结束 00:00:00";
    self.timeFont=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(iconImgV.frame)+10, Default_Space, [NSString sizeWithText:self.timeStr font:self.timeFont maxSize:Max_Size].width,  20)];
    self.timeLabel.textColor=[UIColor whiteColor];
    self.timeLabel.textAlignment=NSTextAlignmentCenter;
    self.timeLabel.backgroundColor=APPThreeColor;
    self.timeLabel.layer.cornerRadius=5;
    self.timeLabel.layer.masksToBounds=YES;
    self.timeLabel.font=self.timeFont;
    [self.moreView addSubview:self.timeLabel];
    UIImageView * moreImgV = [[UIImageView alloc]initWithFrame:CGRectMake(Screen_Width-110, 10, 90, 20)];
    moreImgV.image = [UIImage imageNamed:@"button_超值热卖"];
    moreImgV.contentMode = UIViewContentModeScaleAspectFit;
    [self.moreView addSubview:moreImgV];
    
    self.scrollView =[[UIScrollView alloc]initWithFrame:CGRectMake(0,self.moreView.frame.origin.y+self.moreView.frame.size.height+0.1, Screen_Width, self.collectionViewHeight)];
    self.scrollView.showsHorizontalScrollIndicator=NO;
    self.scrollView.backgroundColor=[UIColor whiteColor];
    self.scrollView.bounces=NO;
    
    UICollectionViewFlowLayout *layout=[[ UICollectionViewFlowLayout alloc ] init ];
    layout.itemSize = CGSizeMake(Screen_Width/3.3, Screen_Width/3.3);
    layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = 0;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, 0, 0) collectionViewLayout:layout];
    self.collectionView.backgroundColor=[UIColor whiteColor];
    [self.collectionView registerClass:[MainTimeTCCC class] forCellWithReuseIdentifier:[MainTimeTCCC getID]];
    self.collectionView.delegate=self;
    self.collectionView.dataSource=self;
    self.collectionView.scrollEnabled=NO;
    
    [self.scrollView addSubview:self.collectionView];
    
    self.line=[[UIView alloc]initWithFrame:CGRectMake(0, self.moreView.frame.size.height+self.moreView.frame.origin.y-Default_Line, Screen_Width, Default_Line)];
    self.line.backgroundColor=APPBGColor;

    [self addSubview:self.marLine];
    [self addSubview:self.moreView];
    [self addSubview:self.line];
    [self addSubview:self.scrollView];
    
}

-(void)showTime:(NSString *)time{
    self.timeStr=time;
    self.timeLabel.frame=CGRectMake(90, Default_Space, [NSString sizeWithText:self.timeStr font:self.timeFont maxSize:Max_Size].width+Default_Space,  20);
    self.timeLabel.text=self.timeStr;
}

-(void)showData:(NSArray *)timerData{
    self.data = timerData;
    self.collectionView.frame=CGRectMake(0, 0, (Screen_Width/3.3)*self.data.count+(self.data.count*1), self.collectionViewHeight);
    self.scrollView.contentSize=CGSizeMake((Screen_Width/3.3)*self.data.count+(self.data.count*1), self.collectionViewHeight);
    [self.collectionView reloadData];
}

#pragma collection delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.data.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize itemSize=[self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:indexPath];
    
    MainTimeTCCC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[MainTimeTCCC getID] forIndexPath:indexPath];
    if(cell==nil){
        cell=[[MainTimeTCCC alloc] initWithFrame:CGRectMake(0, 0,itemSize.width,itemSize.height)];
    }
    BestDealModel * model = _data[indexPath.row];
    cell.model = model;
    return  cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(Screen_Width/3.3, self.collectionViewHeight);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    BestDealModel * model = _data[indexPath.row];
    [self.delagete clickTimerCell:[NSString stringWithFormat:@"%@",model.Id]];
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(CGFloat )collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
#pragma mark delagete
-(void)moreViewOnClick{
    [self.delagete allTimeGood];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

+(NSString *)getID{
    return @"MainTimeTC";
}

@end
