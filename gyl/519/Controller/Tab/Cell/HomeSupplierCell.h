//
//  HomeSupplierCell.h
//  gylPT
//
//  Created by Macmini on 2017/8/29.
//  Copyright © 2017年 wzBPH. All rights reserved.
//

#import "HomeSupplierCell.h"

@protocol HomeSupplierCellDelegate <NSObject>

-(void)clickSupplierCell:(NSString *)supplierId;
-(void)clickmoreBtn;
@end

@interface HomeSupplierCell : UITableViewCell
@property(nonatomic,assign)id<HomeSupplierCellDelegate>deleate;
-(void)showDataForSupplier:(NSArray *)supplierArr;
+(NSString *)getIDE;
@end
