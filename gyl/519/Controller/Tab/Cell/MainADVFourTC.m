//
//  MainADVFourTC.m
//  519
//
//  Created by Macmini on 16/11/24.
//  Copyright © 2016年 519. All rights reserved.
//

#import "MainADVFourTC.h"

@interface MainADVFourTC()<UIScrollViewDelegate>
@property (nonatomic,strong)SDCycleScrollView *cycleScrollView;

@end

@implementation MainADVFourTC

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initView];
    }
    return  self;
}

-(void)initView{
    self.backgroundColor=APPBGColor;
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    

    
    self.cycleScrollView = [[SDCycleScrollView alloc]init];
    [self.cycleScrollView setAutoScrollTimeInterval:5];
    [self.cycleScrollView setAutoScroll:YES];
    [self.cycleScrollView setInfiniteLoop:YES];
    self.cycleScrollView.currentPageDotImage=[UIImage imageNamed:@"pageControlCurrentDot"];
    self.cycleScrollView.pageDotImage=[UIImage imageNamed:@"pageControlDot"];
    
    [self addSubview:self.cycleScrollView];
}

-(void)showImgOfAdvFour:(NSArray *)imgs withTag:(NSInteger)tag{
    
    self.cycleScrollView.delegate=self.delegate;
    self.cycleScrollView.tag = tag;
    [self.cycleScrollView setFrame:CGRectMake(10, 10, Screen_Width-20, (Screen_Width-20)/640*180)];
    self.cycleScrollView.layer.cornerRadius = 20;
    self.cycleScrollView.layer.masksToBounds = YES;
    [self.cycleScrollView setImageURLStringsGroup:imgs];
}



-(void)selectViewOnClickOfFour{
//    [self.delegate selectViewOnClickOfFour];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


+(NSString *)getID{
    return @"MainADVFourTC";
}
@end
