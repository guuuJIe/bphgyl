//
//  MainNavTC.m
//  519
//
//  Created by 陈 on 16/8/30.
//  Copyright © 2016年 519. All rights reserved.
//

#define ActionHeight self.frame.size.height-40
#define OneCollectionLine Screen_Width*0.5/2
#define navNum 5
#import "MainNavTC.h"
#import "MainNavTCCC.h"
#import <YYWebImage.h>
@interface MainNavTC()<UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSInteger _inter;
    UIImageView * _imgView;
    UIView * _lineView;
}
@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,strong)NSArray *data;
@property (nonatomic,strong)NSArray * nameData;
@property (nonatomic,strong)NSArray * newsTitleArr;
@property (nonatomic,strong)NSArray * newsUtlArr;
@property (nonatomic,strong)NSArray * typeArr;
@property (nonatomic,strong)UICollectionViewFlowLayout *layout;
@property (nonatomic,strong)UIButton * newsbtn;
@property (nonatomic,strong)NSTimer * timer;
@property (nonatomic,strong)NSArray * color;
//action
@property (nonatomic,strong)UIImageView * imgView;

@property (nonatomic,strong)UIImageView * threeImageView;
@end

@implementation MainNavTC

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        _inter = 0;
        [self initView];
    }
    return  self;
}

-(void)initView{
    
    self.backgroundColor=[UIColor whiteColor];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    _layout=[[ UICollectionViewFlowLayout alloc ] init ];
    _layout.itemSize = CGSizeMake(Screen_Width/navNum, ActionHeight);
    _layout.minimumInteritemSpacing = 0;
    _layout.minimumLineSpacing = 0;
    _layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    

    _imgView = [[UIImageView alloc]init];
    [self addSubview:_imgView];
    
    self.collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Width*0.5/2) collectionViewLayout:_layout];
    self.collectionView.backgroundColor=[UIColor colorWithWhite:0 alpha:0];
    [self.collectionView registerClass:[MainNavTCCC class] forCellWithReuseIdentifier:[MainNavTCCC getID]];
    self.collectionView.delegate=self;
    self.collectionView.dataSource=self;
    self.collectionView.scrollEnabled=NO;
    
//    _lineView = [[UIView alloc]init];
//    _lineView.backgroundColor = APPBGColor;
//    [self addSubview:_lineView];
    [self addSubview:self.collectionView];

    
    //活动
    self.actionView = [[UIView alloc]init];
    [self addSubview:self.actionView];
    
    
    HRAdView * view = [[HRAdView alloc]init];
    view.textAlignment = NSTextAlignmentLeft;//默认
    view.isHaveTouchEvent = YES;
    view.labelFont = [UIFont systemFontOfSize:14];
    view.color = APPFourColor;
    view.time = 4.0f;
    view.defaultMargin = 10;
    view.numberOfTextLines = 2;
    view.edgeInsets = UIEdgeInsetsMake(8, 8 , 8, 10);
    view.headImg = [UIImage imageNamed:@"icon_供应链头条"];
    [self addSubview:view];
    self.adView = view;
 
    
    __weak typeof(self) weakself = self;
    self.adView.clickAdBlock = ^(NSUInteger index){
        [[NSNotificationCenter defaultCenter]postNotificationName:@"bphNewsJump" object:@[weakself.newsUtlArr[index],weakself.typeArr[index]]];
    };
    
}


-(void)showData:(NSArray *)data withName:(NSArray *)nameArray newsNameArr:(NSArray *)newsArr newsUrl:(NSArray *)newsUrl withNewsType:(NSArray *)newsType actionType:(NSArray *)typeArr actionId:(NSArray *)idArr actionImage:(NSArray *)imageArr actionBgImage:(NSString *)bgUrl navTextColor:(NSArray *)color{
    self.data=data;
    self.nameData = nameArray;
    self.newsTitleArr = newsArr;
    self.newsUtlArr = newsUrl;
    self.typeArr = newsType;
    self.color = color;
    
    //活动背景图
    _imgView.frame = CGRectMake(0, 0, Screen_Width, Screen_Width*0.5/2*((nameArray.count+3)/4));
//    _imgView.backgroundColor = [UIColor redColor];
    [_imgView sd_setImageWithURL:[NSURL URLWithString:bgUrl]];
    
    
    
    //nav
    if (imageArr.count>0) {
        NSInteger countIn = nameArray.count/5;
        
        CGFloat coutF = countIn + (nameArray.count%5);
//        self.collectionView.frame = CGRectMake(0, 0, Screen_Width, Screen_Width*0.5/2*((nameArray.count+4)/navNum));
        self.collectionView.frame = CGRectMake(0, 0, Screen_Width, coutF*ActionHeight);
    }else{
        self.collectionView.frame = CGRectMake(0, 0, Screen_Width, ActionHeight);
    }
    
    [self.collectionView reloadData];
    
    
    if (imageArr.count>0) {
        //获取image宽高
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageArr[0]]];
        UIImage *image = [UIImage imageWithData:data];
        self.actionView.frame = CGRectMake(0, self.collectionView.frame.size.height, Screen_Width, (Screen_Width/(image.size.width*imageArr.count)*image.size.height));
        if (self.threeImageView) {
            for (UIView * subView in self.actionView.subviews) {
                [subView removeFromSuperview];
            }
        }
        for (NSInteger i = 0; i<imageArr.count; i++) {
            _threeImageView = [[UIImageView alloc]initWithFrame:CGRectMake(Screen_Width/imageArr.count*i, 0, Screen_Width/imageArr.count, self.actionView.frame.size.height)];
            _threeImageView.tag = 1000+i;
            [_threeImageView yy_setImageWithURL:[NSURL URLWithString:imageArr[i]] options:0];
//            [_threeImageView sd_setImageWithURL:[NSURL URLWithString:imageArr[i]]];
            [self.actionView addSubview:_threeImageView];
            
            UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionOnclick:)];
            [_threeImageView addGestureRecognizer:tap];
        }
    }else{
        if (_threeImageView) {
            for (UIView * subview in self.actionView.subviews) {
                [subview removeFromSuperview];
            }
        }
    }
    //泊啤汇头条
    if (self.adView.oneLabel) {
        [self.adView.oneLabel removeFromSuperview];
    }
    [self.adView addLabelTitle:_newsTitleArr];
    self.adView.numberOfTextLines = 1;
    [self.adView beginScroll];
    self.adView.frame = CGRectMake(0, CGRectGetMaxY(self.collectionView.frame), Screen_Width, 34);
    
}

-(void)actionOnclick:(UITapGestureRecognizer *)tap{
    NSInteger i = tap.view.tag-1000;
    [self.delegate mainActiontype:i];
}

#pragma mark collection delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.data.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize itemSize=[self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:indexPath];

    MainNavTCCC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[MainNavTCCC getID] forIndexPath:indexPath];
    if(cell==nil){
        cell=[[MainNavTCCC alloc] initWithFrame:CGRectMake(0, 0,itemSize.width,itemSize.width)];
    }
    [cell showData:_data[indexPath.row] withName:_nameData[indexPath.row] withNameColor:self.color[indexPath.row]];
//    cell.backgroundColor= [UIColor colorWithRed:arc4random_uniform(255)/255.0 green:arc4random_uniform(255)/255.0 blue:arc4random_uniform(255)/255.0 alpha:1];
    return  cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(Screen_Width/navNum, OneCollectionLine);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"navNotification" object:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(NSString *)getID{
    return @"MainNavTC";
}

@end
