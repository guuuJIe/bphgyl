//
//  MainViewPageTC.m
//  519
//
//  Created by 陈 on 16/8/29.
//  Copyright © 2016年 519. All rights reserved.
//

#import "MainViewPageTC.h"

@interface MainViewPageTC()
@property (nonatomic,strong)SDCycleScrollView *cycleScrollView;
@property(nonatomic,strong)UIView *cityView;
@property(nonatomic,strong)UILabel *cityLabel;
@property(nonatomic,strong)UIImageView *bottomImg;

@property(nonatomic,strong)UIImageView *selectImg;
@end

@implementation MainViewPageTC

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initView];
    }
    return  self;
}

-(void)initView{
    self.backgroundColor=[UIColor whiteColor];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    self.cycleScrollView = [[SDCycleScrollView alloc]init];
    [self.cycleScrollView setAutoScrollTimeInterval:5];
    [self.cycleScrollView setAutoScroll:YES];
    [self.cycleScrollView setInfiniteLoop:YES];
    self.cycleScrollView.currentPageDotImage=[UIImage imageNamed:@"pageControlCurrentDot"];
    self.cycleScrollView.pageDotImage=[UIImage imageNamed:@"pageControlDot"];

    [self addSubview:self.cycleScrollView];

}

-(void)showImg:(NSArray *)imgs withTag:(NSInteger)tag{
    self.cycleScrollView.delegate=self.delegate;
    self.cycleScrollView.tag = tag;
    [self.cycleScrollView setFrame:CGRectMake(0, 0, Screen_Width, self.frame.size.height)];
    [self.cycleScrollView setImageURLStringsGroup:imgs];
}

-(void)showShopName:(NSString *)shopName{
    self.selectLabel.text = shopName;
}

-(void)selectViewOnClick{
    [self.mainViewPageDelagate selectViewOnClick];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

+(NSString *)getID{
    return @"MainViewPageTC";
}

@end
