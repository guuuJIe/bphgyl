//
//  MainNavTC.h
//  519
//
//  Created by 陈 on 16/8/30.
//  Copyright © 2016年 519. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HRAdView.h"
@protocol MainNewsDelegate <NSObject>

-(void)mainActiontype:(NSInteger)inter;

@end

@interface MainNavTC : BaseTC
@property(nonatomic,assign)id <MainNewsDelegate> delegate;
@property (nonatomic, strong) HRAdView *adView;
@property (nonatomic,strong)UIView * actionView;
-(void)showData:(NSArray *)data
       withName:(NSArray *)nameArray
    newsNameArr:(NSArray *)newsArr
        newsUrl:(NSArray *)newsUrl
   withNewsType:(NSArray *)newsType
     actionType:(NSArray *)typeArr
       actionId:(NSArray *)idArr
    actionImage:(NSArray *)imageArr 
  actionBgImage:(NSString *)bgUrl
   navTextColor:(NSArray *)color;

+(NSString *)getID;

@end
