//
//  MainTimeTCCC.m
//  519
//
//  Created by 陈 on 16/8/31.
//  Copyright © 2016年 519. All rights reserved.
//

#import "MainTimeTCCC.h"

@interface MainTimeTCCC()
@property(nonatomic,strong)UIImageView *img;
@property(nonatomic,strong)UILabel *title;
@property(nonatomic,strong)UILabel *price;
@property(nonatomic,strong)UILabel * buycount;
@end

@implementation MainTimeTCCC

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

-(void)initView{
    self.backgroundColor = [UIColor whiteColor];
    self.img=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space,Default_Space, self.frame.size.width-20, self.frame.size.width-20)];
    self.img.contentMode = UIViewContentModeScaleAspectFit;
    self.title=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, self.img.frame.origin.y+self.img.frame.size.height, self.frame.size.width-20, 20)];
    self.title.font=[UIFont systemFontOfSize:14];
    self.title.textColor=APPFourColor;
    self.title.lineBreakMode=NSLineBreakByTruncatingTail;
//    self.title.numberOfLines=2;
    self.price=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space, self.title.frame.origin.y+self.title.frame.size.height, self.frame.size.width-20, 20)];
    self.price.font=[UIFont systemFontOfSize:13];
    self.price.textColor=APPOneColor;
    
    CGFloat countWidth =self.price.frame.size.width+self.price.frame.origin.x;
    self.buycount = [[UILabel alloc]initWithFrame:CGRectMake( countWidth, self.price.frame.origin.y, self.frame.size.width - countWidth, 20)];
    self.buycount.textAlignment = NSTextAlignmentRight;
    self.buycount.textColor = APPThreeColor;
    self.buycount.font = [UIFont systemFontOfSize:FONT_SIZE_S];
    
    
    
    [self addSubview:self.img];
    [self addSubview:self.title];
    [self addSubview:self.price];
    [self addSubview:self.buycount];
}

-(void)setModel:(BestDealModel *)model{
    _model = model;
    [self.img sd_setImageWithURL:[NSURL URLWithString:model.icon]];
    self.title.text = model.name;
    self.price.text = [NSString stringWithFormat:@"￥%@",model.current_price];
    CGFloat priceWidth = [NSString sizeWithText:self.price.text font:[UIFont systemFontOfSize:11] maxSize:CGSizeMake(self.frame.size.width/3, 20)].width;
    self.price.frame = CGRectMake(Default_Space, self.title.frame.origin.y+self.title.frame.size.height, priceWidth+10, 20);
    self.buycount.text = [NSString stringWithFormat:@"%@人已买",model.buy_count];
    self.buycount.frame = CGRectMake( priceWidth+15, self.price.frame.origin.y, self.frame.size.width - priceWidth-20, 20);
}

+(NSString *)getID{
    return @"MainTimeTCCC";
}
@end
