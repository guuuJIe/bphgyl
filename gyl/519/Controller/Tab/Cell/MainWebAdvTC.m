//
//  MainWebAdvTC.m
//  519
//
//  Created by 陈 on 16/8/31.
//  Copyright © 2016年 519. All rights reserved.
//

#import "MainWebAdvTC.h"
#import "MainWebView.h"
@interface MainWebAdvTC()<UIWebViewDelegate>

@property (nonatomic,strong)UILabel * label;
@property(nonatomic,copy)NSString * htmlString;
@property (strong, nonatomic) NSTimer    *timer; // 顶部轮播图片定时器
@property (nonatomic,assign)BOOL isadd;
@end

@implementation MainWebAdvTC

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initView];
    }
    return  self;
}

-(void)initView{
    self.backgroundColor=APPBGColor;
    self.selectionStyle=UITableViewCellSelectionStyleNone;

    UIView * whiteView = [[UIView alloc]initWithFrame:CGRectMake(0, 10, Screen_Width, 40)];
    whiteView.backgroundColor =[ UIColor whiteColor];
    [self addSubview:whiteView];
    
    UIImageView * iconImg = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 80, 20)];
    iconImg.contentMode = UIViewContentModeScaleAspectFit;
    iconImg.image = [UIImage imageNamed:@"供应链推荐"];
    [whiteView addSubview:iconImg];
    
    UILabel * titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(iconImg.frame)+5, 12, 150, 15)];
    titleLabel.text = @"为您精心挑选的好货";
    titleLabel.textColor = APPThreeColor;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.font = [UIFont systemFontOfSize:13];
    [whiteView addSubview:titleLabel];
    
    self.webView=[[UIWebView alloc]initWithFrame:CGRectMake(0, 50, Screen_Width, Screen_Width/750*600)];
    self.webView.backgroundColor=[UIColor whiteColor];
    self.webView.scrollView.scrollEnabled=NO;
    self.webView.delegate = self;
    [self addSubview:self.webView];
}

-(void)showData:(NSString *)webString{

//    dispatch_async(dispatch_get_main_queue(), ^{
    [self.webView loadHTMLString:webString baseURL:nil];
//    });
    
}


#pragma mark webviewDelegate
-(void)webViewDidStartLoad:(UIWebView *)webView{
    
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
}
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSLog(@"%@",request.URL);
    NSString * urlStr = [NSString stringWithFormat:@"%@",request.URL];
    NSArray * urlArr = [urlStr componentsSeparatedByString:@"="];
    if ([urlStr rangeOfString:@"http://"].location != NSNotFound) {
        if ([urlStr rangeOfString:@"ctl=item"].location != NSNotFound) {
            [self.delegate webPush:@"item" withUrlId:urlArr.lastObject];
        }else {
            [self.delegate webPush:@"web" withUrlId:urlStr];
        }
        return NO;
    }
    
    return YES;
}

+(NSString *)getID{
    return @"MainWebAdvTC";
}

@end
