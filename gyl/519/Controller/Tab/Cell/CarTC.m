//
//  CarTC.m
//  519
//
//  Created by 陈 on 16/9/20.
//  Copyright © 2016年 519. All rights reserved.
//

/**
 *  购物车cell
 */
#import "CarTC.h"

@interface CarTC()<UITextFieldDelegate>
@property (nonatomic,assign)NSInteger num;
@property(nonatomic,strong)UIView *line;

@property(nonatomic,strong)UIImageView *img;
@property(nonatomic,strong)UILabel *nameLabel;
@property(nonatomic,strong)UILabel *infoLabel;
@property(nonatomic,strong)UILabel *priceLabel;
@property(nonatomic,strong)UILabel *countLabel;
@property(nonatomic,strong)UITextField * textField;
@property(nonatomic,strong)UIButton *addBtn;
@property(nonatomic,strong)UIButton *removeBtn;
@property(nonatomic,strong)UIButton *deleteBtn;

@end

@implementation CarTC

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self createView];
    }
    
    return  self;
}
-(void)createView{
   
    self.line=[[UIView alloc]init];
    self.line.backgroundColor=APPBGColor;
    [self addSubview:self.line];
    [self.line makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.and.right.mas_equalTo(self).with.mas_offset(0);
        make.height.mas_offset(10);
    }];
    
    _img = [[UIImageView  alloc]init];
    _img.layer.borderWidth = .5;
    _img.layer.borderColor = APPBGColor.CGColor;
    [self addSubview:_img];
    [_img makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).with.mas_offset(10);
        make.top.mas_equalTo(self.line.mas_bottom).with.mas_offset(5);
        make.width.and.height.equalTo(100);
    }];
    
    _nameLabel = [[UILabel alloc]init];
    _nameLabel.textColor = APPFourColor;
    _nameLabel.textAlignment = NSTextAlignmentLeft;
    _nameLabel.font = [UIFont systemFontOfSize:14];
    _nameLabel.numberOfLines = 2;
    [self addSubview:_nameLabel];
    [_nameLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.img.mas_right).with.mas_offset(10);
        make.top.mas_equalTo(self.line.mas_bottom).with.mas_offset(10);
        make.right.equalTo(-10);
    }];
    
    _priceLabel = [[UILabel alloc]init];
    _priceLabel.textColor = [UIColor redColor];
    _priceLabel.textAlignment = NSTextAlignmentLeft;
    _priceLabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:_priceLabel];
    [_priceLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.img.mas_right).with.mas_offset(10);
        make.bottom.mas_equalTo(self.mas_bottom).with.mas_offset(-10);
        make.height.equalTo(20);
    }];
    
    _deleteBtn = [[UIButton alloc]init];
    [_deleteBtn setImage:[UIImage imageNamed:@"button_删除"] forState:(UIControlStateNormal)];
    [_deleteBtn addTarget:self action:@selector(clickDelectBtn) forControlEvents:(UIControlEventTouchUpInside)];
    [_deleteBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    _deleteBtn.layer.cornerRadius = 3;
    _deleteBtn.layer.masksToBounds = YES;
    _deleteBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:_deleteBtn];
    [_deleteBtn makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.mas_right).with.mas_offset(-15);
        make.bottom.mas_equalTo(self.mas_bottom).with.mas_offset(-10);
        make.height.equalTo(30);
        make.width.equalTo(30);
    }];
    
    _addBtn = [[UIButton alloc]init];
    [_addBtn setImage:[UIImage imageNamed:@"button_加"] forState:(UIControlStateNormal)];
    [_addBtn addTarget:self action:@selector(addNum) forControlEvents:(UIControlEventTouchUpInside)];
    [self addSubview:_addBtn];
    [_addBtn makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_deleteBtn.mas_left).with.mas_offset(-20);
        make.bottom.mas_equalTo(self.mas_bottom).with.mas_equalTo(-10);
        make.height.and.width.equalTo(25);
    }];
    
    _textField = [[UITextField alloc]init];
    _textField.textColor = APPFourColor;
    _textField.textAlignment =NSTextAlignmentCenter;
    _textField.delegate = self;
    _textField.font = [UIFont systemFontOfSize:12];
    _textField.layer.borderWidth = 1;
    _textField.layer.borderColor = APPBGColor.CGColor;
    _textField.keyboardType = UIKeyboardTypeNumberPad;
    [self addSubview:_textField];
    [_textField makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_addBtn.mas_left).with.mas_offset(1);
        make.bottom.mas_equalTo(self.mas_bottom).with.mas_offset(-10);
        make.height.equalTo(25);
        make.width.equalTo(40);
    }];
    
    _removeBtn = [[UIButton alloc]init];
    [_removeBtn setImage:[UIImage imageNamed:@"button_减_高亮"] forState:(UIControlStateNormal)];
    [_removeBtn addTarget:self action:@selector(removeGoodsNum) forControlEvents:(UIControlEventTouchUpInside)];
    [self addSubview:_removeBtn];
    [_removeBtn makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_textField.mas_left).with.mas_offset(1);
        make.bottom.mas_equalTo(self.mas_bottom).with.mas_equalTo(-10);
        make.height.and.width.equalTo(25);
    }];
    [_deleteBtn setEnlargeEdgeWithTop:10 left:10 bottom:10 right:10];
}

-(void)clickDelectBtn{
    if (self.deleteBlcok) {
        self.deleteBlcok();
    }
    
}
-(void)removeGoodsNum{
    
    if (_num <= 1) {
        
    }else{
        _num--;
        self.textField.text = [NSString stringWithFormat:@"%ld",(long)_num];
        if (_num == 1) {
            [_removeBtn setImage:[UIImage imageNamed:@"button_减_默认"] forState:(UIControlStateNormal)];
        }
        [self updateAllPrice:_num];
    }
}
-(void)addNum{
    
    if (_num>=999) {
    
    }else{
        _num++;
        self.textField.text = [NSString stringWithFormat:@"%ld",(long)_num];
         [_removeBtn setImage:[UIImage imageNamed:@"button_减_高亮"] forState:(UIControlStateNormal)];
        [self updateAllPrice:_num];
    }
}

-(void)setModel:(CarModel *)model{
    _model = model;
    _num = [_model.number integerValue];
    if (_num == 1) {
         [_removeBtn setImage:[UIImage imageNamed:@"button_减_默认"] forState:(UIControlStateNormal)];
    }
    [self.img sd_setImageWithURL:[NSURL URLWithString:model.icon]];
    self.nameLabel.text = model.name;
    self.priceLabel.text = [NSString stringWithFormat:@"￥%.2lf",[model.unit_price doubleValue]];
    self.textField.text = [NSString stringWithFormat:@"%@",model.number];
}

-(void)updateAllPrice:(NSInteger)number{
    NSString * addShoppingCarUrl = [NSString stringWithFormat:@"%@ctl=cart&act=update&id=%@&num=%ld&email=%@&pwd=%@",GYLUrl,_model.ids,(long)number,EMAIL,USER_PWD];
    [RequestData requestDataOfUrl:addShoppingCarUrl success:^(NSDictionary *dic) {
        NSLog(@"%@",dic);
        if([dic[@"status"] isEqual:@1]){
            if (self.block) {
                self.block(dic[@"cart_total_price"]);
            }
            self.priceLabel.text = [NSString stringWithFormat:@"￥%@",dic[@"unit_price"]];
        }else{
            [MBProgressHUD showError:dic[@"info"] toView:nil];
        }
    } failure:^(NSError *error) {
        
    }];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{

    [self.delegate addToolBarForTextField:textField];

}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    _num = [textField.text integerValue];
    if(_num <=1){
        _num = 1;
    }
    if (_num>=999) {
        _num = 999;
    }
    [self updateAllPrice:_num];
}


+(NSString *)getID{
    return @"CarTC";
}
@end
