//
//  SessionListTableViewCell.h
//  519
//
//  Created by Macmini on 2017/12/20.
//  Copyright © 2017年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SessionListTableViewCell : UITableViewCell

@property (nonatomic,strong)UIImageView * iconImage;
@property (nonatomic,strong)UILabel * countLabel;
@property (nonatomic,strong)UILabel * nameLabel;
@property (nonatomic,strong)UILabel * messageLabel;
@property (nonatomic,assign)NSInteger unreadCount;
@property (nonatomic,assign)QYSessionStatus status;
@property (nonatomic,assign)NSTimeInterval lestMessageTime;
@property (nonatomic,strong)UILabel * timeLabel;

@end
