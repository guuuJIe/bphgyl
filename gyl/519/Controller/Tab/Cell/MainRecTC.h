//
//  MainAdvTC.h
//  519
//
//  Created by 陈 on 16/8/31.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"
#import "CarModel.h"

@protocol mainRectDelegate <NSObject>
-(void)clickMainTectCell:(NSString *)rectId;
@end
@interface MainRecTC : BaseTC
@property(nonatomic,assign)id<mainRectDelegate>delegate;
-(void)showData:(NSArray *)data;

+(NSString *)getID;
@end
