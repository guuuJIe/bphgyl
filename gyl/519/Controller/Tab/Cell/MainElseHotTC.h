//
//  MainElseHotTC.h
//  519
//
//  Created by Macmini on 17/3/23.
//  Copyright © 2017年 519. All rights reserved.
//

#import "BaseTC.h"
#import "CarModel.h"
@class BestDealModel;

@protocol ElseHotDelegate <NSObject>
-(void)clickHotCell:(NSString *)hotId;
-(void)hotGoodsaddIncar:(BestDealModel *)model andNSIndexPath:(NSIndexPath *)indexpath;
@end

typedef void(^addIncar)(BestDealModel *modal,UIImageView *imageView);

@interface MainElseHotTC : BaseTC
@property(nonatomic,strong)id<ElseHotDelegate>delegate;
+(NSString *)getelseId;
-(void)showHotTcData:(NSArray *)elseHotData;
@property (nonatomic,copy)addIncar addAction;

@end
