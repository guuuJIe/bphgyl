//
//  MainViewPageTC.h
//  519
//
//  Created by 陈 on 16/8/29.
//  Copyright © 2016年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MainViewPageDelegate
-(void)selectViewOnClick;
//-(void)pageHomeOnClick;

@end


@interface MainViewPageTC : BaseTC

@property(nonatomic,strong)id<SDCycleScrollViewDelegate> delegate;
@property(nonatomic,strong)id<MainViewPageDelegate> mainViewPageDelagate;
@property(nonatomic,strong)UILabel *selectLabel;
@property(nonatomic,strong)UIView *homeView;
@property(nonatomic,strong)UIImageView *letDownImg;

-(void)showImg:(NSArray *)imgs withTag:(NSInteger)tag;
-(void)showShopName:(NSString * )shopName;
+(NSString *)getID;
@end
