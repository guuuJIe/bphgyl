//
//  CarAndMineBottomCell.m
//  519
//
//  Created by Macmini on 2019/7/29.
//  Copyright © 2019 519. All rights reserved.
//

#import "CarAndMineBottomCell.h"

@interface CarAndMineBottomCell()

@property(nonatomic,strong)UILabel *title;
@property(nonatomic,strong)UILabel *price;
@property(nonatomic,strong)UIImageView *car;
@property(nonatomic,strong)UILabel * oldPrice;
@property(nonatomic,strong)UIView * bgview;
@property(nonatomic,strong)UILabel * buyNowLabel;
@property(nonatomic,strong)UIImageView *addImage;
@end

@implementation CarAndMineBottomCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

-(void)initView{
    self.backgroundColor = [UIColor whiteColor];
    NSString * priceText = @"￥11.1";
    CGFloat priceWidth = [NSString sizeWithText:priceText font:[UIFont systemFontOfSize:15] maxSize:self.price.frame.size].width;
    CGFloat lineWidth =[NSString sizeWithText:@"￥110" font:[UIFont systemFontOfSize:15] maxSize:self.price.frame.size].width;
    self.img=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space,Default_Space, self.frame.size.width-20, self.frame.size.width-20)];
    self.img.contentMode = UIViewContentModeScaleAspectFit;
    self.title=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, self.img.frame.origin.y+self.img.frame.size.height+Default_Space+Default_Space, self.frame.size.width-20, 40)];

    self.title.font=[UIFont systemFontOfSize:14];
    self.title.textColor=APPFourColor;
    self.title.lineBreakMode=NSLineBreakByTruncatingTail;
    self.title.numberOfLines=2;
    self.price=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space, self.title.frame.origin.y+self.title.frame.size.height, priceWidth, 20)];

    self.price.font=[UIFont systemFontOfSize:15];
    self.price.lineBreakMode = NSLineBreakByWordWrapping;
    self.price.textColor=APPOneColor;
    self.oldPrice = [[UILabel alloc]initWithFrame:CGRectMake(Default_Space,self.price.frame.origin.y+self.title.frame.size.height+1,  priceWidth, 20)];

    self.oldPrice.textColor = APPThreeColor;
    self.oldPrice.textAlignment = NSTextAlignmentLeft;
    self.oldPrice.font = [UIFont systemFontOfSize:FONT_SIZE_X];
//    _bgview = [[UIView alloc]initWithFrame:CGRectMake(0,0,lineWidth, 1)];
//    _bgview.center = CGPointMake(self.oldPrice.frame.size.width/2, self.oldPrice.frame.size.height/2);
//    _bgview.backgroundColor = APPThreeColor;
    [self.oldPrice addSubview:_bgview];
    
    self.buyNowLabel = [[UILabel alloc]initWithFrame:CGRectMake(priceWidth*2+Default_Space+5, self.oldPrice.frame.origin.y+1, self.frame.size.width-priceWidth*2-Default_Space*2-5, 20)];
    self.buyNowLabel.textColor = APPFourColor;
    self.buyNowLabel.textAlignment = NSTextAlignmentRight;
    self.buyNowLabel.font = [UIFont systemFontOfSize:FONT_SIZE_X];
    self.buyNowLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self addSubview:_buyNowLabel];
    
    [self addSubview:self.img];
    [self addSubview:self.title];
    [self addSubview:self.price];
    [self addSubview:self.oldPrice];
    [self addSubview:self.buyNowLabel];
    [self addSubview:self.addImage];
    
    
    
    
}
-(void)setModel:(BestDealModel *)model{
    _model = model;
    //积分商品
    if ([model.buy_type isEqualToString:@"1"]) {
        self.price.text = [NSString stringWithFormat:@"%.2f积分",model.deal_score];
    }else{
        self.price.text = [NSString stringWithFormat:@"￥%@",model.current_price];
    }
    [self.img sd_setImageWithURL:[NSURL URLWithString:model.icon]];
    self.title.text = model.name;
    
    self.oldPrice.text = [NSString stringWithFormat:@"￥%@",model.origin_price];
    self.buyNowLabel.text = [NSString stringWithFormat:@"%@人已买",model.buy_count];
    
    CGFloat priceWidth = [NSString sizeWithText:self.price.text font:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(self.frame.size.width/3, 20)].width;
    if (priceWidth>self.frame.size.width/3) {
        priceWidth = self.frame.size.width/3;
    }
    
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.img.mas_bottom).offset(Default_Space);
        make.left.equalTo(10);

        make.right.equalTo(-10);
        //        make.height.mas_equalTo(30);
    }];
    
    
    
    [self.buyNowLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-10);
        make.bottom.equalTo(self.contentView).offset(-20);
    }];
    
    [self.oldPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(Default_Space);
        
        make.centerY.equalTo(self.buyNowLabel);
    }];
    
    [self.price mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(Default_Space);
        make.bottom.equalTo(self.oldPrice.mas_top).offset(-18);

//        make.centerY.equalTo(self.buyNowLabel);
    }];
    
   
    [self.addImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.price);

        make.right.equalTo(-10);
        make.size.mas_equalTo(CGSizeMake(28, 28));
    }];
    //    self.price.frame = CGRectMake(Default_Space,self.title.frame.origin.y+self.title.frame.size.height, priceWidth, 20);
    //
    //    CGFloat moneyWidth = [NSString sizeWithText:self.oldPrice.text font:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(self.frame.size.width/3, 20)].width;
    //    self.oldPrice.frame = CGRectMake(Default_Space, self.price.frame.origin.y+self.price.frame.size.height, moneyWidth, 20);
    //
    //    self.buyNowLabel.frame = CGRectMake(priceWidth+moneyWidth+Default_Space, self.oldPrice.frame.origin.y, self.frame.size.width-priceWidth-moneyWidth-Default_Space-10, 20);
    //    self.bgview.frame = CGRectMake(0, 0, moneyWidth, 1);
    //    self.bgview.center = CGPointMake(self.oldPrice.frame.size.width/2, self.oldPrice.frame.size.height/2);
    
    //中划线
    NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"￥%@",model.origin_price] attributes:attribtDic];
    
    self.oldPrice.attributedText = attribtStr;
}

- (void)addIncar{
    if (self.clickBlock) {
        self.clickBlock(self.model);
    }
}

- (UIImageView *)addImage{
    if (!_addImage) {
        _addImage = [[UIImageView alloc] init];
        _addImage.userInteractionEnabled = true;
        [_addImage addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addIncar)]];
        _addImage.image = [UIImage imageNamed:@"icon_add"];
    }
    return _addImage;
}

+(NSString *)getID{
    return @"CarAndMineBottomCell";
}

@end
