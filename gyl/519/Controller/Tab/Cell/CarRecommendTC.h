//
//  CarRecommendTC.h
//  519
//
//  Created by Macmini on 2018/1/17.
//  Copyright © 2018年 519. All rights reserved.
//
typedef void(^recommendGoodBlock)(NSString * Ids);



#import <UIKit/UIKit.h>
@class BestDealModel;
@class CarAndMineBottomCell;
#import "CarAndMineBottomCell.h"
@protocol recommandDelegate <NSObject>
-(void)addIncar:(BestDealModel *)model withPosition:(CGRect)position withCollectionView:(UICollectionView *)collectionView andCurrentCell:(CarAndMineBottomCell *)cell;
@end

@interface CarRecommendTC : UITableViewCell
@property (nonatomic,copy)recommendGoodBlock block;
@property (nonatomic,strong)NSArray * dataArray;

@property (nonatomic,weak)id<recommandDelegate>delegate;

+(NSString *)getID;

@end
