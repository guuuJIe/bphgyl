//
//  MainHotTCCC.m
//  519
//
//  Created by 陈 on 16/8/31.
//  Copyright © 2016年 519. All rights reserved.
//

#import "MainHotTCCC.h"
#import "UIButton+Edge.h"
@interface MainHotTCCC()

@property(nonatomic,strong)UILabel *title;
@property(nonatomic,strong)UILabel *price;
@property(nonatomic,strong)UIImageView *car;
@property(nonatomic,strong)UILabel * oldPrice;
@property(nonatomic,strong)UIView * bgview;
@property(nonatomic,strong)UILabel * buyNowLabel;
@property(nonatomic,strong)UIButton *addImage;
@property(nonatomic,strong)UILabel *buycountLabel;
@end

@implementation MainHotTCCC

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

-(void)initView{
    self.backgroundColor = [UIColor whiteColor];
    NSString * priceText = @"￥11.1";
    CGFloat priceWidth = [NSString sizeWithText:priceText font:[UIFont systemFontOfSize:15] maxSize:self.price.frame.size].width;
    CGFloat lineWidth =[NSString sizeWithText:@"￥110" font:[UIFont systemFontOfSize:15] maxSize:self.price.frame.size].width;
    self.img=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space,Default_Space, 80, 80)];
    self.img.contentMode = UIViewContentModeScaleAspectFit;
    self.title=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space+CGRectGetMaxX(self.img.frame), Default_Space, self.frame.size.width-20, 40)];
    self.title.font=[UIFont systemFontOfSize:14];
    self.title.textColor=APPFourColor;
    self.title.lineBreakMode=NSLineBreakByTruncatingTail;
    self.title.numberOfLines=2;
    self.price=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space+CGRectGetMaxX(self.img.frame), self.title.frame.origin.y+self.title.frame.size.height, priceWidth, 20)];
    self.price.font=[UIFont systemFontOfSize:15];
    self.price.lineBreakMode = NSLineBreakByWordWrapping;
    self.price.textColor=APPOneColor;
    self.oldPrice = [[UILabel alloc]initWithFrame:CGRectMake(Default_Space+CGRectGetMaxX(self.price.frame),CGRectGetMinY(self.price.frame)+1,  priceWidth, 20)];
    self.oldPrice.textColor = APPThreeColor;
    self.oldPrice.textAlignment = NSTextAlignmentLeft;
    self.oldPrice.font = [UIFont systemFontOfSize:FONT_SIZE_X];
//    _bgview = [[UIView alloc]initWithFrame:CGRectMake(0,0,lineWidth, 1)];
//    _bgview.center = CGPointMake(self.oldPrice.frame.size.width/2, self.oldPrice.frame.size.height/2);
//    _bgview.backgroundColor = APPThreeColor;
    [self.oldPrice addSubview:_bgview];

    self.buyNowLabel = [[UILabel alloc]initWithFrame:CGRectMake(priceWidth*2+Default_Space+5, self.oldPrice.frame.origin.y+1, self.frame.size.width-priceWidth*2-Default_Space*2-5, 20)];
    self.buyNowLabel.textColor = APPFourColor;
    self.buyNowLabel.textAlignment = NSTextAlignmentRight;
    self.buyNowLabel.font = [UIFont systemFontOfSize:FONT_SIZE_X];
    self.buyNowLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self addSubview:_buyNowLabel];
    
    _buycountLabel = [[UILabel alloc]init];
    _buycountLabel.textColor = APPThreeColor;
    _buycountLabel.textAlignment = NSTextAlignmentRight;
    _buycountLabel.font = [UIFont systemFontOfSize:13];;
    [self addSubview:_buycountLabel];
   
    
    
    [self addSubview:self.img];
    [self addSubview:self.title];
    [self addSubview:self.price];
    [self addSubview:self.oldPrice];
    [self addSubview:self.buyNowLabel];
    [self addSubview:self.addImage];
   
    
    
    
}
-(void)setModel:(BestDealModel *)model{
    _model = model;
    [self.img sd_setImageWithURL:[NSURL URLWithString:model.icon]];
    self.title.text = model.name;
    self.price.text = [NSString stringWithFormat:@"￥%@",model.current_price];
    self.oldPrice.text = [NSString stringWithFormat:@"￥%@",model.origin_price];
    self.buyNowLabel.text = [NSString stringWithFormat:@"库存:%@件",model.max_bought];
    self.buycountLabel.text = [NSString stringWithFormat:@"保质期:%@",model.expiry_date];
    CGFloat priceWidth = [NSString sizeWithText:self.price.text font:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(self.frame.size.width/3, 20)].width;
    if (priceWidth>self.frame.size.width/3) {
        priceWidth = self.frame.size.width/3;
    }
    
    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.img).offset(9);
        make.left.equalTo(self.img.mas_right).offset(10);
        make.right.equalTo(-10);
//        make.height.mas_equalTo(30);
    }];
    
    
    
    [self.buyNowLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-10);
        make.bottom.equalTo(self.contentView).offset(-18);
    }];
    
   
    
    [self.price mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.title);
        make.centerY.equalTo(self.buyNowLabel);
    }];
    
    [self.oldPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.price.mas_right).offset(5);
        make.centerY.equalTo(self.buyNowLabel);
    }];
    [self.addImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.right.equalTo(-10);
        make.size.mas_equalTo(CGSizeMake(28, 28));
    }];
    
    [self.buycountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.title);
        make.centerY.equalTo(self.addImage);
    }];
    
    //中划线
    NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"￥%@",model.origin_price] attributes:attribtDic];
    
    self.oldPrice.attributedText = attribtStr;
//    self.price.frame = CGRectMake(Default_Space,self.title.frame.origin.y+self.title.frame.size.height, priceWidth, 20);
//    
//    CGFloat moneyWidth = [NSString sizeWithText:self.oldPrice.text font:[UIFont systemFontOfSize:15] maxSize:CGSizeMake(self.frame.size.width/3, 20)].width;
//    self.oldPrice.frame = CGRectMake(Default_Space, self.price.frame.origin.y+self.price.frame.size.height, moneyWidth, 20);
//    
//    self.buyNowLabel.frame = CGRectMake(priceWidth+moneyWidth+Default_Space, self.oldPrice.frame.origin.y, self.frame.size.width-priceWidth-moneyWidth-Default_Space-10, 20);
//    self.bgview.frame = CGRectMake(0, 0, moneyWidth, 1);
//    self.bgview.center = CGPointMake(self.oldPrice.frame.size.width/2, self.oldPrice.frame.size.height/2);
}

- (void)addIncar{
    if (self.clickBlock) {
        self.clickBlock(self.model);
    }
}

- (UIButton *)addImage{
    if (!_addImage) {
        _addImage = [[UIButton alloc] init];
       
        [_addImage addTarget:self action:@selector(addIncar) forControlEvents:UIControlEventTouchUpInside];
//        [_addImage addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addIncar)]];
//        _addImage.image = [UIImage imageNamed:@"icon_add"];
        [_addImage setImage:[UIImage imageNamed:@"icon_add"] forState:0];
        [_addImage setEnlargeEdgeWithTop:20 left:20 bottom:20 right:20];
        
    }
    return _addImage;
}

+(NSString *)getID{
    return @"MainHotTCCC";
}
@end
