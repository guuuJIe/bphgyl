//
//  CarTC.h
//  519
//
//  Created by 陈 on 16/9/20.
//  Copyright © 2016年 519. All rights reserved.
//
typedef void(^carVCAllPriceBlock)(NSString *totalPrice);
typedef void(^carDeleteDataBlock)(void);
#import "BaseTC.h"
#import "CarModel.h"

@protocol carBorderKeyDelegate
-(void)addToolBarForTextField:(UITextField *)textField;
@end

@interface CarTC : BaseTC

@property (nonatomic,strong)CarModel * model;
@property (nonatomic,copy)carVCAllPriceBlock block;
@property (nonatomic,copy)carDeleteDataBlock deleteBlcok;
@property (nonatomic,assign)id<carBorderKeyDelegate>delegate;



+(NSString *)getID;
@end
