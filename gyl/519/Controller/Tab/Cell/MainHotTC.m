//
//  MainHotTC.m
//  519
//
//  Created by 陈 on 16/8/31.
//  Copyright © 2016年 519. All rights reserved.
//

#import "MainHotTC.h"
#import "MainHotTCCC.h"
@interface MainHotTC()<UICollectionViewDataSource,UICollectionViewDelegate>
@property(nonatomic,strong)NSArray * data;

@property(nonatomic,copy)NSString * cellName;

@property(nonatomic,strong)UIView *moreView;
@property(nonatomic,strong)UIView *redLine;
@property(nonatomic,copy)NSString *bktjStr;
@property(nonatomic,strong)UIFont *bktjFont;
@property(nonatomic,strong)UILabel *bktjLabel;
@property(nonatomic,strong)UIButton * moreButton;

@property(nonatomic,strong)UICollectionView *collectionView;
@property (nonatomic,assign)BOOL isadd;

@end

@implementation MainHotTC
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        _isadd = NO;
        [self initView];
    }
    
    return  self;
}

-(void)initView{
    self.backgroundColor=APPBGColor;
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    self.moreView=[[UIView alloc]initWithFrame:CGRectMake(0, Default_Space, Screen_Width, 40)];
    self.moreView.backgroundColor=[UIColor whiteColor];
    
    UIImageView * titleImageV = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 60, 20)];
    titleImageV.contentMode = UIViewContentModeScaleAspectFit;
    titleImageV.image = [UIImage imageNamed:@"爆款推荐"];
    [self.moreView addSubview:titleImageV];
    
    UILabel * infoLable = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(titleImageV.frame)+5, 0, 150, self.moreView.frame.size.height)];
    infoLable.text=@"精选爆款好酒";
    infoLable.textAlignment=NSTextAlignmentLeft;
    infoLable.font=[UIFont systemFontOfSize:13];
    infoLable.textColor=APPThreeColor;
    
    CGFloat morebtn_x = _bktjLabel.frame.size.width + _bktjLabel.frame.origin.x;
    self.moreButton = [[UIButton alloc]initWithFrame:CGRectMake(morebtn_x, 0, Screen_Width - morebtn_x-10, self.moreView.frame.size.height)];
    [self.moreButton setTitle:@"更多>>" forState:(UIControlStateNormal)];
    [self.moreButton setTitleColor:APPThreeColor forState:(UIControlStateNormal)];
    self.moreButton.titleLabel.font = [UIFont systemFontOfSize:FONT_SIZE_X];
    self.moreButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [self.moreButton addTarget:self action:@selector(moreBuyGoods) forControlEvents:(UIControlEventTouchUpInside)];
    
    [self.moreView addSubview:infoLable];
    [self.moreView addSubview:self.moreButton];
    
    UICollectionViewFlowLayout *layout=[[ UICollectionViewFlowLayout alloc ] init ];
    layout.itemSize = CGSizeMake(Screen_Width, 120);
    layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = 1;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, self.moreView.frame.origin.y+self.moreView.frame.size.height+0.5, Screen_Width,0) collectionViewLayout:layout];
    self.collectionView.backgroundColor=APPBGColor;
    [self.collectionView registerClass:[MainHotTCCC class] forCellWithReuseIdentifier:[MainHotTCCC getID]];
    self.collectionView.delegate=self;
    self.collectionView.dataSource=self;
    self.collectionView.scrollEnabled=NO;

    [self addSubview:self.moreView];
    [self addSubview: self.collectionView];
}

-(void)showHotTcData:(NSArray *)hotData{
    self.data = hotData;
//    self.collectionView.frame=CGRectMake(0, self.moreView.frame.origin.y+self.moreView.frame.size.height+0.5, Screen_Width, ((Screen_Width-5*3)/2+120));
    self.collectionView.frame=CGRectMake(0, self.moreView.frame.origin.y+self.moreView.frame.size.height+0.5, Screen_Width, 120*hotData.count);
    [self.collectionView reloadData];
}


-(void)moreBuyGoods{
    [self.delegate moregoodsJump];
}


#pragma collection delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _data.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize itemSize=[self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:indexPath];
    
    MainHotTCCC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[MainHotTCCC getID] forIndexPath:indexPath];
    if(cell==nil){
        cell=[[MainHotTCCC alloc] initWithFrame:CGRectMake(0, 0,itemSize.width,itemSize.height)];
    }
    
    BestDealModel * model = self.data[indexPath.row];
    cell.model = model;
    
    __weak typeof(cell) weakcell  = cell;
    cell.clickBlock = ^(BestDealModel *model) {
//        __strong typeof(weself)self = weself;
        clickBlock click = self.clikAction;
        if (click) {
            self.clikAction(model, indexPath.row, weakcell.img);
        }
    };
    return  cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(Screen_Width, 120);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(1, 1, 1, 1);
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    BestDealModel * model = self.data[indexPath.row];
    [self.delegate clickHotCell:model.Id];
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

+(NSString *)getID{
    return @"MainHotTC";
}

@end
