//
//  MainElseHotTC.m
//  519
//
//  Created by Macmini on 17/3/23.
//  Copyright © 2017年 519. All rights reserved.
//

#import "MainElseHotTC.h"
#import "MainHotTCCC.h"

@interface MainElseHotTC()<UICollectionViewDataSource,UICollectionViewDelegate>
@property(nonatomic,strong)NSArray *hotdata;
@property(nonatomic,copy)NSString * cellName;
@property(nonatomic,strong)UICollectionView *collectionView;
@property (nonatomic,assign)BOOL isadd;

@end

@implementation MainElseHotTC
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        _isadd = NO;
        [self initView];
    }
    
    return  self;
}

-(void)initView{
    self.backgroundColor=APPBGColor;
    self.selectionStyle=UITableViewCellSelectionStyleNone;

    UICollectionViewFlowLayout *layout=[[ UICollectionViewFlowLayout alloc ] init ];
//    layout.itemSize = CGSizeMake((Screen_Width-1*3)/2, (Screen_Width-1*2)/2+120);
    layout.itemSize = CGSizeMake(Screen_Width, 120);
    layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = 1;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0,0.5, Screen_Width,0) collectionViewLayout:layout];
    self.collectionView.backgroundColor=APPBGColor;
    [self.collectionView registerClass:[MainHotTCCC class] forCellWithReuseIdentifier:[MainHotTCCC getID]];
    self.collectionView.delegate=self;
    self.collectionView.dataSource=self;
    self.collectionView.scrollEnabled=NO;
    
    [self addSubview: self.collectionView];
}

-(void)showHotTcData:(NSArray *)elseHotData{
    self.hotdata = elseHotData;
//    self.collectionView.frame=CGRectMake(0, 0.5, Screen_Width, ((Screen_Width-5*3)/2+120));
    self.collectionView.frame = CGRectMake(0, 0.5, Screen_Width, 120);
    [self.collectionView reloadData];
}
#pragma collection delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _hotdata.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize itemSize=[self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:indexPath];
    
    MainHotTCCC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[MainHotTCCC getID] forIndexPath:indexPath];
    if(cell==nil){
        cell=[[MainHotTCCC alloc] initWithFrame:CGRectMake(0, 0,itemSize.width,itemSize.height)];
    }
 
    BestDealModel * model = self.hotdata[indexPath.row];
    cell.model = model;
    WS(weself);
    cell.clickBlock = ^(BestDealModel *model) {
        addIncar click = self.addAction;
        
        if (click) {
            self.addAction(model,cell.img);
        }
        
//        [weself.delegate hotGoodsaddIncar:model andNSIndexPath:indexPath];
    };
    return  cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    return CGSizeMake((Screen_Width-1*3)/2, (Screen_Width-1*2)/2+120);
    return  CGSizeMake(Screen_Width, 120);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(1, 1, 1, 1);
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    BestDealModel * model = self.hotdata[indexPath.row];
    [self.delegate clickHotCell:model.Id];
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


+(NSString *)getelseId{
    return @"ElseMainHotTC";
}

@end
