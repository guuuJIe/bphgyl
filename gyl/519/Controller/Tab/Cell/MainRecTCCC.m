//
//  MainAdvTCCC.m
//  519
//
//  Created by 陈 on 16/8/31.
//  Copyright © 2016年 519. All rights reserved.
//

#import "MainRecTCCC.h"

@interface MainRecTCCC()
@property(nonatomic,strong)UIImageView *img;
@property(nonatomic,strong)UILabel * titleLabel;
@property(nonatomic,strong)UILabel * priceLabel;
@property(nonatomic,strong)UILabel * lookLabel;

@end

@implementation MainRecTCCC
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self initView];
    }
    return self;
}

-(void)initView{
    _img = [[UIImageView alloc]init];
    _img.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:_img];
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = APPFourColor;
    _titleLabel.numberOfLines= 0;
    _titleLabel.textAlignment = NSTextAlignmentLeft;
    _titleLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:_titleLabel];
    
    [_img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).with.offset(5);
        make.centerX.mas_equalTo(self.mas_centerX);
        make.height.mas_equalTo(80);
        make.width.mas_equalTo(100);
    }];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset(10);
        make.right.equalTo(self).with.offset(-10);
        make.top.equalTo(_img.mas_bottom).with.offset(0);
        make.height.mas_equalTo(20);
    }];
}



-(void)setModel:(BestDealModel *)model{
    _model = model;
    [_img sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.icon]]];
    _titleLabel.text = [NSString stringWithFormat:@"%@",model.name];
}

+(NSString*)getID{
    return @"MainRecTCCC";
}

@end
