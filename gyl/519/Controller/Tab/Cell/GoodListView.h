//
//  GoodListView.h
//  gylPT
//
//  Created by Macmini on 2018/1/9.
//  Copyright © 2018年 wzBPH. All rights reserved.
//

typedef void(^goodListToInfoBlock)(NSString * ids);

#import <UIKit/UIKit.h>
#import "CarModel.h"

@interface GoodListView : UIView
@property (nonatomic,strong)NSArray * collectionArray;
@property (nonatomic,strong)UILabel * typeLabel;
@property (nonatomic,strong)CateGoodTypeModel * model;
@property (nonatomic,copy)goodListToInfoBlock block;
@property (nonatomic,copy)NSString * allGoodsId;
@end
