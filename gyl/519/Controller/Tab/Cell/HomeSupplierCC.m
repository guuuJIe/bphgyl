//
//  HomeSupplierCC.m
//  gylPT
//
//  Created by Macmini on 2017/8/29.
//  Copyright © 2017年 wzBPH. All rights reserved.
//

#import "HomeSupplierCC.h"

@interface HomeSupplierCC()

@property (nonatomic,strong)UIImageView * imageView;

@end

@implementation HomeSupplierCC

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self initview];
    }
    return self;
}

-(void)initview{
    self.imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width/2, Screen_Width/750*200)];
    [self addSubview:self.imageView];
}
-(void)setModel:(SupplierModel *)model{
    _model = model;
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:model.preview]];
}

+(NSString *)getIde{
    return @"supplierCC";
}
@end
