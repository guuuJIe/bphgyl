//
//  MainAdvTC.m
//  519
//
//  Created by 陈 on 16/8/31.
//  Copyright © 2016年 519. All rights reserved.
//

#import "MainAdvTC.h"

@interface MainAdvTC()
@property (nonatomic,strong)SDCycleScrollView *cycleScrollView;

@property(nonatomic,strong)UIImageView *img;
@property (nonatomic,assign)BOOL isadd;
@end

@implementation MainAdvTC

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        _isadd = NO;
        [self initView];
    }
    return  self;
}

-(void)initView{
    self.backgroundColor=APPBGColor;
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    self.cycleScrollView = [[SDCycleScrollView alloc]init];
    [self.cycleScrollView setAutoScrollTimeInterval:5];
    [self.cycleScrollView setAutoScroll:YES];
    [self.cycleScrollView setInfiniteLoop:YES];
    self.cycleScrollView.currentPageDotImage=[UIImage imageNamed:@"pageControlCurrentDot"];
    self.cycleScrollView.pageDotImage=[UIImage imageNamed:@"pageControlDot"];
    
    [self addSubview:self.cycleScrollView];
}

-(void)showImgOfAdvFour:(NSArray *)imgs withTag:(NSInteger)tag{

        self.cycleScrollView.delegate=self.delegate;
        self.cycleScrollView.tag = tag;
        [self.cycleScrollView setFrame:CGRectMake(0, Default_Space, Screen_Width, Screen_Width/640*180)];
        [self.cycleScrollView setImageURLStringsGroup:imgs];
    
}



+(NSString *)getID{
    return @"MainAdvTC";
}
@end
