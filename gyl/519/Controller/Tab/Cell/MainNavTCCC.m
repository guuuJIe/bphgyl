//
//  MainNavTCCC.m
//  519
//
//  Created by 陈 on 16/8/30.
//  Copyright © 2016年 519. All rights reserved.
//

#import "MainNavTCCC.h"

@interface MainNavTCCC()
@property (nonatomic,strong)UIImageView *img;
@property(nonatomic,strong)UILabel *title;


@end

@implementation MainNavTCCC
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

-(void)initView{
    self.img=[[UIImageView alloc]initWithFrame:CGRectMake(25,12, self.frame.size.width/2, self.frame.size.width/2)];
    self.img.center = CGPointMake(self.frame.size.width/2, self.frame.size.width/2.2);
    
    self.title=[[UILabel alloc]initWithFrame:CGRectMake(0, _img.frame.size.height+_img.frame.origin.y+5, self.frame.size.width, 15)];
    self.title.textAlignment=NSTextAlignmentCenter;
    self.title.font=[UIFont systemFontOfSize:FONT_SIZE_XX];
    
    [self addSubview:self.img];
    [self addSubview:self.title];

}

-(void)showData:(NSString *)imageurl withName:(NSString *)imageName withNameColor:(NSString *)color{
    [_img sd_setImageWithURL:[NSURL URLWithString:imageurl]];
    _title.text = imageName;
    if (color.length<1) {
        _title.textColor = APPFourColor;
    }else{
         color = [color stringByReplacingOccurrencesOfString:@"#" withString:@"0x"];
        _title.textColor = [UIColor colorFromHexRGB:color];
    }
}

+(NSString*)getID{
    return @"MainNavTCCC";
}
@end
