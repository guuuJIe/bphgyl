//
//  CarAndMineBottomCell.h
//  519
//
//  Created by Macmini on 2019/7/29.
//  Copyright © 2019 519. All rights reserved.
//

#import "BaseTC.h"
#import "CarModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CarAndMineBottomCell : BaseCC
@property(nonatomic,strong)BestDealModel * model;

+(NSString *)getID;
@property(nonatomic,strong)UIImageView *img;
@property(nonatomic,copy)void (^clickBlock)(BestDealModel *model);
@end

NS_ASSUME_NONNULL_END
