//
//  MainAdvTC.m
//  519
//
//  Created by 陈 on 16/8/31.
//  Copyright © 2016年 519. All rights reserved.
//

#define itemWidth (Screen_Width-20-3)/3
#import "MainRecTC.h"
#import "MainRecTCCC.h"

@interface MainRecTC() <UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSArray * _rectArray;
}
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UICollectionView *collectionView;
@end

@implementation MainRecTC

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initView];
    }
    return  self;
}

-(void)initView{
    
    self.backgroundColor=[UIColor whiteColor];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 10)];
    lineView.backgroundColor = APPBGColor;
    [self addSubview:lineView];
    UIView * headView = [[UIView alloc]initWithFrame:CGRectMake(0, 10, Screen_Width, 40)];
    headView.backgroundColor = [UIColor whiteColor];
    [self addSubview:headView];
    
    UIImageView * iconImgV = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 60, 20)];
    iconImgV.image = [UIImage imageNamed:@"为你推荐"];
    iconImgV.contentMode = UIViewContentModeScaleAspectFit;
    [headView addSubview:iconImgV];
    
    UILabel * historyLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(iconImgV.frame)+5, 0, 150, 40)];
    historyLabel.text = @"历史浏览有喜欢的吗";
    historyLabel.textColor = APPThreeColor;
    historyLabel.textAlignment = NSTextAlignmentLeft;
    historyLabel.font = [UIFont systemFontOfSize:13];
    [headView addSubview:historyLabel];
   
    UICollectionViewFlowLayout *layout=[[ UICollectionViewFlowLayout alloc ] init ];
    layout.itemSize = CGSizeMake(itemWidth, 110);
    layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = 0.5;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(10,CGRectGetMaxY(headView.frame), Screen_Width-20,itemWidth*2+20) collectionViewLayout:layout];
    [self.collectionView registerClass:[MainRecTCCC class] forCellWithReuseIdentifier:[MainRecTCCC getID]];
    self.collectionView.backgroundColor = APPBGColor;
    self.collectionView.delegate=self;
    self.collectionView.dataSource=self;
    self.collectionView.scrollEnabled=NO;
    [self addSubview:self.collectionView];
}

-(void)showData:(NSArray *)data {
    _rectArray = data;
    CGRect frame = self.collectionView.frame;
    frame.size.height = (data.count+2)/3*110+2;
    self.collectionView.frame =frame;
    [self.collectionView reloadData];
}

#pragma collection delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _rectArray.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize itemSize=[self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:indexPath];

    MainRecTCCC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[MainRecTCCC getID] forIndexPath:indexPath];
    if(cell==nil){
        cell=[[MainRecTCCC alloc] initWithFrame:CGRectMake(0, 0,itemSize.width,itemSize.height)];
    }
    
    BestDealModel * model = _rectArray[indexPath.row];
    cell.model = model;
//    cell.alpha = 0.1;
//    [cell showData:self.datas[indexPath.row]];
    return  cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(itemWidth-.5,110);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0.5,0.5,0.5,0.5);
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    BestDealModel * model = _rectArray[indexPath.row];
    [self.delegate clickMainTectCell:[NSString stringWithFormat:@"%@",model.Id]];
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

+(NSString *)getID{
    return @"MainRecTC";
}
@end
