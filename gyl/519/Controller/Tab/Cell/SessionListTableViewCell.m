//
//  SessionListTableViewCell.m
//  519
//
//  Created by Macmini on 2017/12/20.
//  Copyright © 2017年 519. All rights reserved.
//


//七鱼聊天列表cell  未用到
#import "SessionListTableViewCell.h"

@implementation SessionListTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier] ) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
    }
    return self;
}

-(void)createUI{
    
    
    _iconImage = [[UIImageView alloc]init];
    _iconImage.layer.cornerRadius = 5;
    _iconImage.image = [UIImage imageNamed:@"bph_logo"];
    _iconImage.layer.masksToBounds = YES;
    [self addSubview:_iconImage];
    [_iconImage makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).with.mas_offset(10);
        make.left.mas_equalTo(self).with.mas_offset(10);
        make.width.and.height.equalTo(40);
    }];
    
    _countLabel = [[UILabel alloc]init];
    _countLabel.textAlignment = NSTextAlignmentCenter;
    _countLabel.textColor = [UIColor whiteColor];
    _countLabel.backgroundColor = [UIColor redColor];
    _countLabel.layer.cornerRadius = 7;
    _countLabel.layer.masksToBounds = YES;
    _countLabel.font = [UIFont boldSystemFontOfSize:11];
    [self addSubview:_countLabel];
    
    
    
    _nameLabel = [[UILabel alloc]init];
    _nameLabel.textColor = APPFourColor;
    _nameLabel.textAlignment = NSTextAlignmentLeft;
    _nameLabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:_nameLabel];
    [_nameLabel makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).with.mas_offset(5);
        make.left.mas_equalTo(_iconImage.mas_right).mas_offset(20);
        make.height.equalTo(25);
    }];
    
    _messageLabel =[[UILabel alloc]init];
    _messageLabel.textAlignment = NSTextAlignmentLeft;
    _messageLabel.textColor = APPThreeColor;
    _messageLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:_messageLabel];
    [_messageLabel makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_nameLabel.mas_bottom).with.mas_offset(0);
        make.left.mas_equalTo(_iconImage.mas_right).with.mas_offset(20);
        make.width.equalTo(Screen_Width-150);
        make.height.equalTo(20);
    }];
    
    _timeLabel = [[UILabel alloc]init];
    _timeLabel.textColor = APPThreeColor;
    _timeLabel.textAlignment = NSTextAlignmentRight;
    _timeLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:_timeLabel];
    [_timeLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(_nameLabel.mas_centerY);
        make.right.mas_equalTo(self).with.mas_offset(-10);
        make.height.equalTo(25);
    }];
}

@end
