//
//  MainHotTC.h
//  519
//
//  Created by 陈 on 16/8/31.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"
#import "CarModel.h"
@class BestDealModel;

@protocol MoreBuyGoodsDelegate <NSObject>

-(void)moregoodsJump;
-(void)clickHotCell:(NSString *)hotId;
-(void)addIncar:(BestDealModel *)model withImageView:(UIImageView *)imageView;
@end

typedef void(^clickBlock)(BestDealModel *model,NSInteger index,UIImageView *imageView);

@interface MainHotTC : BaseTC

@property(nonatomic,assign)id<MoreBuyGoodsDelegate>delegate;

-(void)showHotTcData:(NSArray *)hotData;

+(NSString *)getID;

@property (nonatomic,copy) clickBlock clikAction;
@end
