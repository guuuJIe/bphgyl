//
//  UserInfoView.m
//  519
//
//  Created by Macmini on 2018/1/8.
//  Copyright © 2018年 519. All rights reserved.
//

#import "UserInfoView.h"

@interface UserInfoView()


@end

@implementation UserInfoView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self == [super initWithFrame:frame]) {
       
        [self createInfoUI];
//        self.userInteractionEnabled = true;
//        UITapGestureRecognizer * imgtap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
//        [self addGestureRecognizer:imgtap];
    }
    return  self;
}

-(void)createInfoUI{
    UIImageView * imagV = [[UIImageView alloc]init];
    imagV.image = [UIImage imageNamed:@"img_我的"];
    [self addSubview:imagV];
    [imagV makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.and.bottom.offset(0);
    }];
    
    UIView * bacView = [[UIView alloc]init];
    bacView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];
    bacView.layer.cornerRadius = (10+44)/2;
    bacView.layer.masksToBounds = YES;
    [imagV addSubview:bacView];
    [bacView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset(30);
        make.bottom.equalTo(self).with.offset(-17);
        make.width.and.height.offset(10+44);
    }];
    
    self.userImgView = [[UIImageView alloc]init];
    self.userImgView.image = [UIImage imageNamed:@"user_white"];
    self.userImgView.layer.cornerRadius = (5+44)/2;
    self.userImgView.layer.masksToBounds = YES;
//    self.userImgView.userInteractionEnabled = true;
    [imagV addSubview:self.userImgView];
    
//    [self.userImgView addGestureRecognizer:imgtap];
    [self.userImgView makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(bacView.mas_centerX);
        make.centerY.equalTo(bacView.mas_centerY);
        make.width.and.height.offset(5+44);
    }];
    
    
    
    _RZStatus = [[UIButton alloc]init];
    [_RZStatus setTitle:@"认证" forState:0];
    [_RZStatus setTitleColor:six3 forState:UIControlStateNormal];
    [_RZStatus.titleLabel setFont:[UIFont systemFontOfSize:11]];
    _RZStatus.layer.borderColor = six3.CGColor;
    _RZStatus.layer.cornerRadius = 5;
    _RZStatus.layer.borderWidth = 1;
    _RZStatus.layer.masksToBounds = true;
    [self addSubview:_RZStatus];
    [_RZStatus addTarget:self action:@selector(verifyClick:) forControlEvents:UIControlEventTouchUpInside];
   
   
    
    //yu e
    self.elseMoneyLabel = [[UILabel alloc]init];
    self.elseMoneyLabel.textColor = [UIColor whiteColor];
    self.elseMoneyLabel.textAlignment = NSTextAlignmentLeft;
    self.elseMoneyLabel.font = [UIFont systemFontOfSize:12];
    [imagV addSubview:self.elseMoneyLabel];
    [self.elseMoneyLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.userImgView.mas_right).with.offset(20);
        make.bottom.mas_equalTo(self).with.offset(-20);
        make.height.offset(15);
    }];
    
    self.userNameLabel = [[UILabel alloc]init];
    self.userNameLabel.text = @"登陆/注册";
    self.userNameLabel.textColor = [UIColor whiteColor];
    self.userNameLabel.textAlignment = NSTextAlignmentLeft;
    self.userNameLabel.font = [UIFont systemFontOfSize:14];
//    self.userNameLabel.userInteractionEnabled = YES;
    self.userNameLabel.numberOfLines = 1;
    [imagV addSubview:self.userNameLabel];
//    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
//    [self.userNameLabel addGestureRecognizer:tap];
    [self.userNameLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userImgView.mas_right).with.offset(20);
        make.bottom.mas_equalTo(self.elseMoneyLabel.mas_top).with.offset(-5);
        make.height.offset(20);
        make.width.mas_lessThanOrEqualTo(100);
//        make.width.offset(140);
    }];
    
    
    self.levelLabel = [[UILabel alloc]init];
    self.levelLabel.text = @"";
    self.levelLabel.textColor = [UIColor whiteColor];
    self.levelLabel.textAlignment = NSTextAlignmentCenter;
    self.levelLabel.font = [UIFont systemFontOfSize:12];
    //    self.userNameLabel.userInteractionEnabled = YES;
    self.levelLabel.numberOfLines = 1;
    [imagV addSubview:self.levelLabel];
    //    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
    //    [self.userNameLabel addGestureRecognizer:tap];
   
    
    
}

-(void)tap{
//    if (self.block) {
//        self.block();
//    }
}


- (void)verifyClick:(UIButton *)sender{
    if (self.RZblock) {
        self.RZblock();
    }
}






@end
