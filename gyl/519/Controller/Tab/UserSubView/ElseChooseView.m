//
//  ElseChooseView.m
//  519
//
//  Created by Macmini on 2018/1/8.
//  Copyright © 2018年 519. All rights reserved.
//

#import "ElseChooseView.h"

@implementation ElseChooseView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createChooseUI];
    }
    return self;
}

-(void)createChooseUI{
    self.backgroundColor = [UIColor whiteColor];
    
    NSArray * imgArr = @[@"button_我的收藏",@"button_历史浏览",@"button_地址管理",@"button_客服热线",@"button_招商入驻"];
    NSArray * titleArr = @[@"我的收藏",@"历史浏览",@"地址管理",@"客服热线",@"招商入驻"];
    for (NSInteger i = 0 ; i<5; i++) {
        UIView * btnView = [[UIView alloc]init];
        [self addSubview:btnView];
        btnView.tag = 4010+i;
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
        [btnView addGestureRecognizer:tap];
        [btnView makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.offset(Screen_Width/5*i+Screen_Width/10-Screen_Width/2);
            make.top.offset(0);
            make.width.offset(Screen_Width/5);
            make.height.offset(self.height);
        }];
        
        UIImageView * imgv = [[UIImageView alloc]init];
        imgv.image = [UIImage imageNamed:imgArr[i]];
        [btnView addSubview:imgv];
        [imgv makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(btnView.mas_centerX);
            make.top.offset(20);
            make.width.and.height.offset(25);
        }];
        UILabel * label = [[UILabel alloc]init];
        label.text = titleArr[i];
        label.textColor = APPFourColor;
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:13];
        [btnView addSubview:label];
        [label makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(imgv.mas_bottom).with.offset(10);
            make.centerX.equalTo(btnView.mas_centerX);
            make.height.offset(20);
        }];
    }
    
}

-(void)tap:(UITapGestureRecognizer *)tap{
    NSInteger index = tap.view.tag-4010;
    if (self.block) {
        self.block(index);
    }
}
@end
