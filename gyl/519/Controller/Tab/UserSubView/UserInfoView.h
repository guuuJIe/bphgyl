//
//  UserInfoView.h
//  519
//
//  Created by Macmini on 2018/1/8.
//  Copyright © 2018年 519. All rights reserved.
//
typedef void(^UserInfoBlock)(void);
typedef void(^RZStatuesBlock)(void);
#import <UIKit/UIKit.h>

@interface UserInfoView : UIView

@property (nonatomic,copy)UserInfoBlock block;
@property (nonatomic,copy)RZStatuesBlock RZblock;
@property (nonatomic,strong)UILabel * userNameLabel;
@property (nonatomic,strong)UIImageView * userImgView;
@property (nonatomic,strong)UILabel * elseMoneyLabel;
@property (nonatomic,strong)UILabel * levelLabel;
@property (nonatomic,strong)UIButton *RZStatus;
@end
