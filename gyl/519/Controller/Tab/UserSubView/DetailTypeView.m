//
//  DetailTypeView.m
//  519
//
//  Created by Macmini on 2018/1/8.
//  Copyright © 2018年 519. All rights reserved.
//

#import "DetailTypeView.h"

@interface DetailTypeView()



@end


@implementation DetailTypeView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self == [super initWithFrame:frame]) {
        self.backgroundColor  = [UIColor whiteColor];
        [self createDetailUI];
    }
    return self;
}
-(void)createDetailUI{
    UIView * leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,Screen_Width/ 5*4, self.frame.size.height)];
    [self addSubview:leftView];

    NSArray * titleArr = @[@"待付款",@"待收货",@"待评价",@"售后"];
    NSArray * imgArr = @[@"button_待付款",@"button_待收货",@"button_待评价",@"button_售后"];
    for (NSInteger i = 0; i<4; i++) {
        UIView * typeView = [[UIView alloc]initWithFrame:CGRectMake((Screen_Width/ 5*4)/4*i, 0, (Screen_Width/ 5*4)/4, leftView.height)];
        typeView.tag = 4001+i;
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
        [typeView addGestureRecognizer:tap];
        [leftView addSubview:typeView];
        
        UIImageView * imgV = [[UIImageView alloc]init];
        imgV.image = [UIImage imageNamed:imgArr[i]];
        [typeView addSubview:imgV];
        [imgV makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(typeView.mas_top).with.offset(20);
            make.centerX.mas_equalTo(typeView.mas_centerX);
            make.width.height.offset(25);
        }];
//
        UILabel * titleLabel = [[UILabel alloc]init];
        titleLabel.text = titleArr[i];
        titleLabel.textColor = APPFourColor;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.font = [UIFont systemFontOfSize:13];
        [typeView addSubview:titleLabel];
        [titleLabel makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(imgV.mas_bottom).with.offset(10);
            make.centerX.equalTo(typeView.mas_centerX);
            make.height.offset(20);
        }];
        
    }
    
    
    UIView * rightView = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(leftView.frame), 0, self.frame.size.width/5, self.frame.size.height)];
    rightView.tag = 4000;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
    [rightView addGestureRecognizer:tap];
    [self addSubview:rightView];
    
    UIImageView * leftimg = [[UIImageView alloc]init];
    leftimg.image = [UIImage imageNamed:@"icon_线"];
    [rightView addSubview:leftimg];
    [leftimg makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(0);
        make.top.offset(10);
        make.bottom.offset(-10);
        make.width.offset(5);
    }];
    UIImageView * imgV = [[UIImageView alloc]init];
    imgV.image = [UIImage imageNamed:@"button_全部订单"];
    [rightView addSubview:imgV];
    [imgV makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(rightView.mas_top).with.offset(20);
        make.centerX.mas_equalTo(rightView.mas_centerX);
        make.width.height.offset(25);
    }];
    
    UILabel * titleLabel = [[UILabel alloc]init];
    titleLabel.text = @"全部订单";
    titleLabel.textColor = APPFourColor;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont systemFontOfSize:13];
    [rightView addSubview:titleLabel];
    [titleLabel makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imgV.mas_bottom).with.offset(10);
        make.centerX.equalTo(rightView.mas_centerX);
        make.height.offset(20);
    }];
}

-(void)tap:(UITapGestureRecognizer *)tap{
    NSInteger index = tap.view.tag - 4000;
    if (self.block) {
        self.block(index);
    }
}

@end
