//
//  ServiceSessionListVC.m
//  519
//
//  Created by Macmini on 2017/12/20.
//  Copyright © 2017年 519. All rights reserved.
//
static NSString * cellIde = @"sessionListIde";
#import "ServiceSessionListVC.h"
#import "SessionListTableViewCell.h"
@interface ServiceSessionListVC ()<UITableViewDelegate,UITableViewDataSource,QYConversationManagerDelegate>
{
    NSInteger count;
}
@property (nonatomic,strong)NSArray * dataArr;
@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,strong)UILabel * neverLabel;

@end

@implementation ServiceSessionListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"消息盒";
    self.view.backgroundColor = APPBGColor;
    [[[QYSDK sharedSDK]conversationManager]setDelegate:self];
    self.dataArr = [[[QYSDK sharedSDK]conversationManager]getSessionList];
    if (self.dataArr.count>0) {
        [self.view addSubview:self.tableView];
    }else{
        [self.view addSubview:self.neverLabel];
    }
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SessionListTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIde];
    if (cell == nil) {
        cell = [[SessionListTableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:cellIde];
    }
    if (self.dataArr.count>0) {
        QYSessionInfo * info = self.dataArr[indexPath.row];
        cell.nameLabel.text = @"奇盟客服";
        cell.messageLabel.text = info.lastMessageText;
        cell.timeLabel.text = [self stringWithTimerInterval:info.lastMessageTimeStamp];
        NSInteger  newCount = info.unreadCount;
        if (newCount == 0) {
            cell.countLabel.hidden = YES;
        }else{
            cell.countLabel.hidden = NO;
            cell.countLabel.text = [NSString stringWithFormat:@"%ld",newCount];
        }
        if (cell.countLabel.text.length > 1) {
            [cell.countLabel makeConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(15);
                make.centerX.mas_equalTo(cell.iconImage.mas_right).with.mas_offset(0);
                make.centerY.mas_equalTo(cell.iconImage.mas_top).with.mas_offset(0);
            }];
        }else{
            [cell.countLabel makeConstraints:^(MASConstraintMaker *make) {
                make.height.and.width.equalTo(15);
                make.centerX.mas_equalTo(cell.iconImage.mas_right).with.mas_offset(0);
                make.centerY.mas_equalTo(cell.iconImage.mas_top).with.mas_offset(0);
            }];
        }
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    QYSource *source = [[QYSource alloc] init];
    source.title =  @"泊啤汇";
    source.urlString = @"https://8.163.com/";
    QYSessionViewController *sessionViewController = [[QYSDK sharedSDK] sessionViewController];
    sessionViewController.sessionTitle = @"供应链";
    sessionViewController.source = source;
    sessionViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:sessionViewController animated:YES];
}

-(NSString *)stringWithTimerInterval:(NSTimeInterval)interval{
    NSDate * newDate = [NSDate date];
    NSTimeInterval  newTime =  [newDate timeIntervalSince1970] - interval;
    if (newTime<86400) {
        NSInteger sec = (NSInteger)newTime%60;
        NSInteger min = (NSInteger)newTime/60%60;
        NSInteger hour = (NSInteger)newTime/60/60%24;
        if (hour>0) {
            return [NSString stringWithFormat:@"%ld小时前",(long)hour];
        }
        if (min>0){
            return [NSString stringWithFormat:@"%ld分钟前",(long)min];
        }
        if (sec>0){
            return [NSString stringWithFormat:@"%ld秒前",(long)sec];
        }
    }else if (newTime>86400 && newTime<86400*2){
        return @"一天前";
    }else if(newTime>86400*2 && newTime<86400*3){
        return @"两天前";
    }else{
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd";
        NSString *dateString = [formatter stringFromDate:date];
        return dateString;
    }
    return @"";
}



-(UITableView *)tableView{
    if (!(_tableView)) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height) style:(UITableViewStylePlain)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
        [_tableView registerClass:[SessionListTableViewCell class] forCellReuseIdentifier:cellIde];
    }
    return _tableView;
}

-(UILabel *)neverLabel{
    if (!_neverLabel) {
        _neverLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 30)];
        _neverLabel.center = CGPointMake(Screen_Width/2, Screen_Height/4);
        _neverLabel.text = @"暂无消息";
        _neverLabel.textColor = APPThreeColor;
        _neverLabel.font = [UIFont boldSystemFontOfSize:14];
        _neverLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _neverLabel;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)onSessionListChanged:(NSArray<QYSessionInfo*> *)sessionList{
    self.dataArr = sessionList;
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
