//
//  MainWebView.m
//  519
//
//  Created by Macmini on 16/12/20.
//  Copyright © 2016年 519. All rights reserved.
//

#import "MainWebView.h"
#import "OrderListVC.h"
#import "GoodInfoVC.h"
#import <cmbkeyboard/CMBWebKeyboard.h>
#import <cmbkeyboard/NSString+Additions.h>
#import <WebKit/WebKit.h>
@interface MainWebView ()<UIWebViewDelegate,UIGestureRecognizerDelegate,WKUIDelegate,WKNavigationDelegate>
@property (nonatomic,strong)UIButton * backBtn;
@property (nonatomic,strong)UIWebView * webView;
@property (nonatomic,strong)UIBarButtonItem *closeItem;
@property (nonatomic) WKWebView *wkwebView;
@property (assign,nonatomic) BOOL isFirst;
@property (nonatomic,strong)NSString *content;
@end

@implementation MainWebView

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [MBProgressHUD showHUD];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
//    [self initview];
    [self initWkwebView];
     [self initbacBar];
    if ([_webUrl rangeOfString:@"act=doubleEleven"].location != NSNotFound) {
//       [self initbacBar];
    }else if([_webUrl equals:@"http://www.bphgyl.com/download/"]){
//
    }
    _isFirst = true;
    [self initshareBtn];
//    self.navigationItem.leftBarButtonItems = @[self.closeItem];
    [MBProgressHUD showHUD];
}
-(void)initbacBar{
    UIView * contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    UIButton * btn = [[UIButton alloc]initWithFrame:contentView.bounds];
    [btn setImage:[UIImage imageNamed:@"返回"] forState:(UIControlStateNormal)];
    [btn addTarget:self action:@selector(popVC) forControlEvents:(UIControlEventTouchUpInside)];
    [contentView addSubview:btn];
    UIBarButtonItem * item = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    item.width = -15;
    self.navigationItem.leftBarButtonItems = @[item,[[UIBarButtonItem alloc]initWithCustomView:contentView],self.closeItem];
    
   
}

- (void)initshareBtn{
    UIView * shareView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton * shareBtn = [[UIButton alloc]initWithFrame:shareView.bounds];
    [shareBtn setImage:[UIImage imageNamed:@"分享-1"] forState:(UIControlStateNormal)];
    [shareBtn addTarget:self action:@selector(shareItem) forControlEvents:(UIControlEventTouchUpInside)];
    [shareView addSubview:shareBtn];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:shareView];;
}


- (void)shareItem{
    [UMUtil shareUM:self delegate:self title:self.navigationItem.title body:self.content?self.content:@"" url:self.webUrl img:nil urlImg:nil];
}

#pragma mark bar

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[CMBWebKeyboard shareInstance] hideKeyboard];
    if ([_webUrl rangeOfString:@"act=doubleEleven"].location != NSNotFound) {
        [UIApplication  sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:(UIBarMetricsDefault)];
        [self setStatusBarBackgroundColor:[UIColor whiteColor]];
    }
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[CMBWebKeyboard shareInstance] hideKeyboard];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:APPColor] forBarMetrics:(UIBarMetricsDefault)];
//    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//    [self setStatusBarBackgroundColor:APPColor];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:APPThreeColor]];
}

-(void)popVC{
//    if ([self.runjump isEqualToString:@"runjump"]) {
//        [self dismissViewControllerAnimated:YES completion:nil];
//        [[NSNotificationCenter defaultCenter]postNotificationName:@"runjumpmain" object:nil];
//    }else{
    
    if ([self.wkwebView canGoBack]) {
        [self.wkwebView goBack];
    }else{
        [self backVC];
    }
//        [self.navigationController popViewControllerAnimated:YES];
//    }
}
-(void)initview{
    _webView = [[UIWebView alloc]initWithFrame:CGRectMake(0,0, Screen_Width, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight)];
    NSString *encodedString=[self.webUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:encodedString]]];
    [_webView request];
    _webView.delegate = self;
    _webView.backgroundColor = [UIColor whiteColor];
//    _webView.opaque = NO;
    [self.view addSubview:_webView];
    
    if (_webUrl.length<3) {
        UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 40)];
        label.text = @"暂无内容";
        label.textColor = APPThreeColor;
        label.textAlignment = NSTextAlignmentCenter;
        label.center = CGPointMake(Screen_Width/2, 150);
        [_webView addSubview:label];
    }
}


- (void)initWkwebView{
    
    [self.view addSubview:self.wkwebView];
   
    
    NSString *encodedString=[self.webUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:encodedString]];
    NSArray *cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage].cookies;
    //Cookies数组转换为requestHeaderFields
    NSDictionary *requestHeaderFields = [NSHTTPCookie requestHeaderFieldsWithCookies:cookies];
    //设置请求头
    request.allHTTPHeaderFields = requestHeaderFields;
    [self.wkwebView loadRequest:request];
    
    if (_webUrl.length<3) {
        UILabel * label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 40)];
        label.text = @"暂无内容";
        label.textColor = APPThreeColor;
        label.textAlignment = NSTextAlignmentCenter;
        label.center = CGPointMake(Screen_Width/2, 150);
        [_wkwebView addSubview:label];
    }
    
    
    
    [self.wkwebView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
   
    
}

#pragma mark ---- WKWebView_DELEGATE---
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    
    
    if ([keyPath isEqualToString:@"title"] && object == _wkwebView) {
        
        self.navigationItem.title = _wkwebView.title;
    }else if([keyPath isEqualToString:@"estimatedProgress"]){
        NSLog(@"网页加载进度 = %f",_wkwebView.estimatedProgress);
//        self.progressView.progress = _wkwebView.estimatedProgress;
//        if (_wkwebView.estimatedProgress >= 1.0f) {
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                self.progressView.progress = 0;
//            });
//
//
//        }
    }

}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
   
    NSString * urlstring = [NSString stringWithFormat:@"%@",navigationAction.request.URL];
    
    if (_isFirst) {
        _isFirst = false;
        decisionHandler(WKNavigationActionPolicyAllow);
    }else{
        if ([urlstring rangeOfString:@"bphgyl.com"].location != NSNotFound) {
            if ([urlstring rangeOfString:@"ctl=item"].location != NSNotFound || [urlstring rangeOfString:@"ctl=list"].location != NSNotFound){
                decisionHandler(WKNavigationActionPolicyCancel);
            }else{
                decisionHandler(WKNavigationActionPolicyAllow);
            }
        }else{
            decisionHandler(WKNavigationActionPolicyAllow);
        }
    }
    
    if ([urlstring rangeOfString:@"http://cmbnprm/"].location != NSNotFound) {
        self.backBtn.hidden = YES;
        [self allOrderVIewOnClick:0];
    }
    if ([urlstring rangeOfString:@"bphgyl.com"].location != NSNotFound) {
        if ([urlstring rangeOfString:@"ctl=item"].location != NSNotFound) {
            NSArray * urlArr = [urlstring componentsSeparatedByString:@"="];
            GoodInfoVC * vc = [GoodInfoVC new];
            vc.goodsId = urlArr.lastObject;
            [self.navigationController pushViewController:vc animated:YES];
           return;
        }
    }
    
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    
    [MBProgressHUD dissmiss];
    [self.wkwebView evaluateJavaScript:@"description_text();" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        self.content = result;
    }];
    
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    [MBProgressHUD dissmiss];
    [MBProgressHUD showError:@"加载错误" toView:self.view];
}


#pragma mark----UIWebViewDelegate------depracted
-(void)webViewDidStartLoad:(UIWebView *)webView{

}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    //    获取当前页面的title
    self.navigationItem.title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    self.content =  [self.webView stringByEvaluatingJavaScriptFromString:@"description_text();"];
    
    [MBProgressHUD dissmiss];
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [MBProgressHUD dissmiss];
    
    [MBProgressHUD showError:@"请求错误" toView:self.view];
    
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSLog(@"%@\n%@\n%@",request.URL,request.HTTPMethod,request.URL.host);
    if ([request.URL.host isCaseInsensitiveEqualToString:@"cmbls"]) {
        CMBWebKeyboard *secKeyboard = [CMBWebKeyboard shareInstance];
        [secKeyboard showKeyboardWithRequest:request];
        secKeyboard.webView = webView;
        
        UITapGestureRecognizer* myTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
        [self.view addGestureRecognizer:myTap]; //这个可以加到任何控件上,比如你只想响应WebView，我正好填满整个屏幕
        myTap.delegate = self;
        myTap.cancelsTouchesInView = NO;
        return NO;
    }
    NSString * urlstring = [NSString stringWithFormat:@"%@",request.URL];
    if ([urlstring rangeOfString:@"http://cmbnprm/"].location != NSNotFound) {
        self.backBtn.hidden = YES;  
        [self allOrderVIewOnClick:0];
    }
    if ([urlstring rangeOfString:@"bphgyl.com"].location != NSNotFound) {
        if ([urlstring rangeOfString:@"ctl=item"].location != NSNotFound) {
            NSArray * urlArr = [urlstring componentsSeparatedByString:@"="];
            GoodInfoVC * vc = [GoodInfoVC new];
            vc.goodsId = urlArr.lastObject;
            [self.navigationController pushViewController:vc animated:YES];
            return NO;
        }
    }
    
    return  YES;
}


-(void)backVC{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)handleSingleTap:(UITapGestureRecognizer *)sender{
    [[CMBWebKeyboard shareInstance] hideKeyboard];
    
}

-(void)allOrderVIewOnClick:(NSInteger)index{
    NSArray *allOrderTitleArray=[[NSArray alloc]initWithObjects:@"全部",@"待付款",@"待配送",@"待评价",@"售后", nil];
    NSMutableArray *itemTitleWidthArray=[[NSMutableArray alloc]init];
    NSMutableArray *vcArray=[[NSMutableArray alloc]init];
    for (int i=0; i<allOrderTitleArray.count;i++){
        Class class=[OrderListVC class];
        [vcArray addObject:class];
        
        NSNumber *itemWidth=[NSNumber numberWithFloat:[NSString sizeWithText:[allOrderTitleArray objectAtIndex:i] font:[UIFont systemFontOfSize:FONT_SIZE_M] maxSize:Max_Size].width+Default_Space];
        [itemTitleWidthArray addObject:itemWidth];
    }
    
    WMPageController *pageController=[[WMPageController alloc] initWithViewControllerClasses:vcArray andTheirTitles:allOrderTitleArray];
    pageController.postNotification = YES;
    pageController.bounces = NO;
    pageController.menuBGColor=[UIColor whiteColor];
    pageController.menuHeight=40;
    pageController.menuViewStyle = WMMenuViewStyleLine;
    pageController.progressHeight = 2;
    pageController.itemsWidths=itemTitleWidthArray;
    pageController.progressViewWidths = itemTitleWidthArray;
    pageController.titleSizeSelected = FONT_SIZE_M;
    pageController.titleSizeNormal=FONT_SIZE_M;
//    pageController.titleFontName=@"Helvetica-Bold";
    pageController.titleColorSelected=APPColor;
    pageController.titleColorNormal=APPFourColor;
    pageController.selectIndex = (int)index;
    pageController.payJumpPage = @"payJumpPage";
    [pageController initBar];
    
   [self.navigationController pushViewController:pageController animated:YES];
    
    
}


- (UIBarButtonItem *)closeItem{
    if (!_closeItem) {
        _closeItem = [[UIBarButtonItem alloc] initWithTitle:@"关闭" style:UIBarButtonItemStylePlain target:self action:@selector(backVC)];
        [_closeItem setTintColor:[UIColor blackColor]];
    }
    return _closeItem;
}



#pragma mark - dealloc
- (void)dealloc
{
    [[CMBWebKeyboard shareInstance] hideKeyboard];
    _webView.delegate = nil;
    [_webView stopLoading];
    [_wkwebView removeObserver:self forKeyPath:@"title"];
}

- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (WKWebView *)wkwebView {
    if (!_wkwebView) {
        WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
        // 偏好设置
        config.preferences = [[WKPreferences alloc] init];
        config.preferences.minimumFontSize = 10;
        config.preferences.javaScriptEnabled = YES;
        config.preferences.javaScriptCanOpenWindowsAutomatically = NO;
        
        
        WKUserContentController *userContentController = [[WKUserContentController alloc] init];
        //        WKUserScript *cookieScript = [[WKUserScript alloc] initWithSource:cookie injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:NO];
        //        [userContentController addUserScript:cookieScript];
        
//        [userContentController addScriptMessageHandler:self name:@"share"];
//        [userContentController addScriptMessageHandler:self name:@"login"];
        
        config.userContentController = userContentController;
        config.selectionGranularity = WKSelectionGranularityDynamic;
        config.allowsInlineMediaPlayback = YES;
//        config.mediaTypesRequiringUserActionForPlayback = false;
        
        
        _wkwebView = [[WKWebView alloc] initWithFrame:CGRectMake(0,0, Screen_Width, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight) configuration:config];
        
        _wkwebView.navigationDelegate = self;//导航代理
        _wkwebView.UIDelegate = self;//UI代理
        _wkwebView.backgroundColor = [UIColor whiteColor];
        _wkwebView.allowsBackForwardNavigationGestures = YES;
        
    }
    return _wkwebView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
