//
//  CarModel.h
//  519
//
//  Created by Macmini on 16/12/2.
//  Copyright © 2016年 519. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarModel : NSObject

@property (nonatomic,copy)NSString * attr;
@property (nonatomic,copy)NSString * attr_str;
@property (nonatomic,copy)NSString * deal_id;
@property (nonatomic,copy)NSString * icon;
@property (nonatomic,copy)NSString * ids;
@property (nonatomic,copy)NSString * max;
@property (nonatomic,copy)NSString * name;
@property (nonatomic,copy)NSString * number;
@property (nonatomic,copy)NSString * return_score;
@property (nonatomic,copy)NSString * return_total_score;
@property (nonatomic,copy)NSString * sub_name;
@property (nonatomic,copy)NSString * total_price;
@property (nonatomic,copy)NSString * unit_price;
@end


@interface SupplierModel : NSObject
@property (nonatomic,copy)NSString * preview;
@property (nonatomic,copy)NSString * Id;
@property (nonatomic,copy)NSString * address;
@property (nonatomic,copy)NSString * name;
@property (nonatomic,copy)NSString * tel;

@end

/**
 *  MainTecTC
 *  MainTimeTC
 *  MainHotTC
 */
@interface BestDealModel: NSObject
@property (nonatomic,copy)NSString * Id;
@property (nonatomic,copy)NSString * name;
@property (nonatomic,copy)NSString * buy_count;
@property (nonatomic,copy)NSString * icon;
@property (nonatomic,copy)NSString * current_price;
@property (nonatomic,copy)NSString * origin_price;
@property (nonatomic,copy)NSString * buy_type;
@property (nonatomic,copy)NSString * expiry_date;
@property (nonatomic,copy)NSString * max_bought;
@property (nonatomic,assign)float deal_score;
@end

/**
 *  typeVC model
 */
@interface CateListModel: NSObject
@property (nonatomic,copy)NSString * Id;
@property (nonatomic,copy)NSString * name;
@property (nonatomic,strong)NSArray * bcate_type;
@end

@interface CateGoodTypeModel: NSObject
@property (nonatomic,copy)NSString * Id;
@property (nonatomic,copy)NSString * name;
@property (nonatomic,copy)NSString * cate_id;
@property (nonatomic,copy)NSString * cate_img;
@end
