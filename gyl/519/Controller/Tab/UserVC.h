//
//  User.h
//  519
//
//  Created by 陈 on 16/8/29.
//  Copyright © 2016年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserVC : UIViewController

@property (nonatomic,copy)NSString * userName;
@property (nonatomic,copy)NSString * userPwd;
@property (nonatomic,copy)NSString * userEmail;

@end
