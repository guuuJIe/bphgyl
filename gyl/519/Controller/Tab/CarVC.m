//
//  Car.m
//  519
//
//  Created by 陈 on 16/8/29.
//  Copyright © 2016年 519. All rights reserved.
//

#import "CarVC.h"
#import "CarTC.h"
#import "OrderPayVC.h"
#import "CarModel.h"
#import "GoodInfoVC.h"
#import "GoodListVC.h"
#import "ConfirmOrderVC.h"
#import "NoGoodsView.h"
#import "CarRecommendTC.h"
#import "PurchaseCarAnimationTool.h"
@interface CarVC ()<UITableViewDataSource,UITableViewDelegate,carBorderKeyDelegate,recommandDelegate>
@property(nonatomic,strong)UIButton *backBtn;

@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)CarTC * carCell;
@property(nonatomic,strong)UIView *payView;
@property(nonatomic,strong)UILabel *allPriceLabel;
@property(nonatomic,strong)UIButton *payBtn;
@property (nonatomic,copy)NSString * total_price;
@property (nonatomic,strong)NSMutableArray * dataArr;
@property (nonatomic,assign)NSInteger index;

@property (nonatomic,strong)NSMutableArray * changeGoodNumberMuArr;
@property (nonatomic,assign)double totalPrice;

@property(nonatomic,strong)UIView * showNoDataView;
@property (nonatomic,strong)NoGoodsView * nogoodsView;
@property(nonatomic,strong)NSMutableArray * recommendMuArr;
@end

@implementation CarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"进货单";
    self.totalPrice = 0;
    self.dataArr = [NSMutableArray new];
    self.recommendMuArr = [NSMutableArray new];
    self.view.backgroundColor = APPBGColor;
    [self initView];

    [self requestDataForCar];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshCart:) name:@"refreshCart" object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:APPThreeColor]];
    if(self.totalPrice  > 0.00){
        [self.showNoDataView removeFromSuperview];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(keyboardWillShow:)
                                                name:UIKeyboardWillShowNotification
                                              object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(keyboardDidShow:)
                                                name:UIKeyboardWillHideNotification
                                              object:nil];
}

//刷新cart
-(void)refreshCart:(NSNotification *)obj{
    [self refreshDataOfCart];
}

-(void)refreshDataOfCart{
  
    [self requestDataForCar];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];

}
-(void)requestDataForCar{
  
    _index =1;
    [self.dataArr removeAllObjects];
//    [self.recommendMuArr removeAllObjects];
    NSString * cardataUrl = [NSString stringWithFormat:@"%@ctl=cart&act=index&email=%@&pwd=%@",GYLUrl,EMAIL,USER_PWD];
    [RequestData requestDataOfUrl:cardataUrl success:^(NSDictionary *dic) {
        [MBProgressHUD dissmiss];
        NSMutableArray * firstMuArr = [NSMutableArray new];
        
        if (![dic[@"status"] isEqual:@1]) {
            [self.tabBarController.tabBar hideBadgeOnItemIndex:2];
        }else{
            [self.tabBarController.tabBar showBadgeOnItemIndex:2 value:[NSString stringWithFormat:@"%@",dic[@"total_number"]]];
        }
        
        if (![dic[@"cart_list"] isEqual:[NSNull null]]) {
            for (NSDictionary * subdic in dic[@"cart_list"]) {
                CarModel * model = [[CarModel alloc]init];
                [model setValuesForKeysWithDictionary:subdic];
                [firstMuArr addObject:model];
            }
           
        }else{
//            [self.tabBarController.tabBar hideBadgeOnItemIndex:2];
        }
        
        NSMutableArray * secondMuArr = [NSMutableArray new];
        for (NSDictionary * subdic in dic[@"history_deal"]) {
            BestDealModel * model = [[BestDealModel alloc]init];
            [model setValuesForKeysWithDictionary:subdic];
            [secondMuArr addObject:model];
        }
        [self.dataArr addObject:firstMuArr];
        [self.dataArr addObject:secondMuArr];
        _total_price = [NSString stringWithFormat:@"%.2lf", [dic[@"total_data"][@"total_price"] doubleValue]];
         [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",dic[@"total_number"]] forKey:@"num"];
         [self showData];
    } failure:^(NSError *error) {
        [MBProgressHUD dissmiss];
        NSLog(@"%@",error);
    }];

}
-(void)loadData{
    _index ++ ;
    if(EMAIL){
        NSString * cardataUrl = [NSString stringWithFormat:@"%@ctl=cart&act=index&page=%ld&email=%@&pwd=%@",GYLUrl,(long)_index,EMAIL,USER_PWD];
        [RequestData requestDataOfUrl:cardataUrl success:^(NSDictionary *dic) {
            NSMutableArray * firstMuArr = [NSMutableArray new];
            if (![dic[@"cart_list"] isEqual:[NSNull null]]) {
                for (NSDictionary * subdic in dic[@"cart_list"]) {
                    CarModel * model = [[CarModel alloc]init];
                    [model setValuesForKeysWithDictionary:subdic];
                    [firstMuArr addObject:model];
                }
                NSArray *goodsNum = dic[@"cart_list"];
                [self.tabBarController.tabBar showBadgeOnItemIndex:2 value:[NSString stringWithFormat:@"%lu",(unsigned long)goodsNum.count]];
            }else{
                [self.tabBarController.tabBar hideBadgeOnItemIndex:2];
            }
            NSMutableArray * secondMuArr = [NSMutableArray new];
            for (NSDictionary * subdic in dic[@"history_deal"]) {
                BestDealModel * model = [[BestDealModel alloc]init];
                [model setValuesForKeysWithDictionary:subdic];
                [secondMuArr addObject:model];
            }
            [self.dataArr addObject:firstMuArr];
            [self.dataArr addObject:secondMuArr];
            _total_price = [NSString stringWithFormat:@"%.2lf", [dic[@"total_data"][@"total_price"] doubleValue]];
            
            [self showData];
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }
}

-(void)showData{
    self.payBtn.backgroundColor = APPColor;
    [self.tableView reloadData];

    self.totalPrice = [_total_price doubleValue];
    [self changeTotalPrice:self.totalPrice];
}

-(void)goGoodsList{
    GoodListVC * vc = [GoodListVC new];
    vc.selectstr = @"catJpmpGoodsList";
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark initView
-(void)initView{
    //滑动view
    self.tableView=[[UITableView alloc]init];
    [self.tableView registerClass:[CarTC class] forCellReuseIdentifier:[CarTC getID]];
    [self.tableView registerClass:[CarRecommendTC class] forCellReuseIdentifier:[CarRecommendTC getID]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate=self;
    self.tableView.backgroundColor=[UIColor clearColor];
    self.tableView.dataSource=self;
    self.tableView.showsVerticalScrollIndicator=NO;
    self.tableView.estimatedRowHeight = 0;
    self.tableView.estimatedSectionFooterHeight = 0;
    self.tableView.estimatedSectionHeaderHeight = 0;
    //刷新
//    MJRefreshHeader * hear = [MJRefreshHeader headerWithRefreshingBlock:^{
//        [self refreshDataOfCart];
//    }];
//    self.tableView.mj_header = hear;
    

    self.payView=[[UIView alloc]init];
    self.payView.backgroundColor=[UIColor whiteColor];

    UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 1)];
    lineView.backgroundColor = APPBGColor;
    [self.payView addSubview:lineView];
    UILabel * allLabel = [[UILabel alloc]initWithFrame:CGRectMake(Default_Space, 15, 40, 20)];
    allLabel.text = @"合计:";
    allLabel.textColor = APPFourColor;
    allLabel.textAlignment = NSTextAlignmentLeft;
    allLabel.font = [UIFont systemFontOfSize:17];
    [self.payView addSubview:allLabel];
    
    self.allPriceLabel=[[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(allLabel.frame)+5, 15, (Screen_SelfWidth/3), 20)];
    
    self.payBtn=[[UIButton alloc]initWithFrame:CGRectMake(Screen_SelfWidth-Screen_SelfWidth/3, 1, Screen_SelfWidth/3, 50)];
    [self.payBtn setBackgroundColor:APPColor];
    [self.payBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.payBtn setTitle:@"结算" forState:UIControlStateNormal];
    self.payBtn.titleLabel.font=[UIFont systemFontOfSize:17];
    [self.payBtn addTarget:self action:@selector(orderpayBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.payView addSubview:self.allPriceLabel];
    [self.payView addSubview:self.payBtn];

    [self.view addSubview:self.tableView];
    [self.view addSubview:self.payView];
    
    CGFloat  tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    
//    if (_isCar) {
//        [self.tableView makeConstraints:^(MASConstraintMaker *make) {
//            make.left.top.right.equalTo(self.view).offset(0);
//            make.bottom.equalTo(self.view).offset(-50);
//        }];
//    }else{
//       
//    }
    
    [self.tableView makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self.view).offset(0);
        make.bottom.equalTo(self.payView.mas_top).offset(-1);
    }];
    
   
    
    if(_isCar){
        [self.payView makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view).offset(0);
            make.height.offset(50+BottomAreaHeight);
        }];
    }else{
        [self.payView makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view).offset(0);
            make.bottom.equalTo(self.view);
            make.height.offset(50);
        }];
    }
}
#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        NSArray * firstArr = self.dataArr.firstObject;
        if (firstArr.count>0) {
             return firstArr.count;
        }else{
            return 1;
        }
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        NSArray * firstArr = self.dataArr.firstObject;
        if ([firstArr count]>0) {
            _carCell=[tableView dequeueReusableCellWithIdentifier:[CarTC getID] forIndexPath:indexPath];
            if(_carCell==nil){
                _carCell=[[CarTC alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[CarTC getID]];
            }
            _carCell.selectionStyle=UITableViewCellSelectionStyleNone;
            _carCell.delegate = self;
            CarModel * model = firstArr[indexPath.row];
            _carCell.model = model;
            __weak typeof(self)weself = self;
            _carCell.block = ^(NSString *totalPrice) {
                __strong typeof(weself)self = weself;
                [self changeTotalPrice:[totalPrice doubleValue]];
                [self refreshDataOfCart];
            };
            
            _carCell.deleteBlcok = ^{
                __strong typeof(weself)self = weself;
                UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"确定删除" message:nil preferredStyle:(UIAlertControllerStyleAlert)];
                [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                    [self deleteGoodsWithIndex:indexPath.row withGoodId:[NSString stringWithFormat:@"%@",model.ids]];
                }]];
                [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                    
                }]];
                [self presentViewController:alert animated:YES completion:nil];
            };
            
            return _carCell;
        }else{
            static NSString * defaultIde = @"carDefaultIde";
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:defaultIde];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if (cell == nil) {
                cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:defaultIde];
            }
            [self.nogoodsView removeFromSuperview];
            if (EMAIL) {
                self.nogoodsView = [[NoGoodsView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Width/375*250)];
                __weak typeof(self)weself = self;
                self.nogoodsView.block = ^{
                    __strong typeof(weself)self = weself;
                    [self.navigationController pushViewController:[GoodListVC new] animated:YES];
                };
            }else{
                self.nogoodsView = [[NoGoodsView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Width/375*100)];
                __weak typeof(self)weself = self;
                self.nogoodsView.block = ^{
                    __strong typeof(weself)self = weself;
                    [self.navigationController pushViewController:[LoginViewController new] animated:YES];
                };
            }
            [cell addSubview:self.nogoodsView];
            return cell;
        }
    }else{
        CarRecommendTC * cell = [tableView dequeueReusableCellWithIdentifier:[CarRecommendTC getID]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (cell == nil) {
            cell = [[CarRecommendTC alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:[CarRecommendTC getID]];
        }
        NSArray * secArr = self.dataArr[1];
        cell.dataArray = secArr;
        __weak typeof(self)weself = self;
        cell.delegate = self;
        cell.block = ^(NSString *Ids) {
            __strong typeof(weself)self = weself;
            GoodInfoVC *vc = [[GoodInfoVC alloc]init];
            vc.goodsId = Ids;
            vc.iscar = _isCar;
            [self.navigationController pushViewController:vc animated:YES];
        };
       
        return cell;
    }
}
#pragma mark --delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
//    NSLog(@"%f",[self.tableView contentOffset].y);
}


-(void)addIncar:(BestDealModel *)model withPosition:(CGRect)position withCollectionView:(UICollectionView *)collectionView andCurrentCell:(CarAndMineBottomCell *)cell{
    [self addshopCar:model];
      CGRect cellInSuperview = [collectionView
                            convertRect:position toView:self.view];
    
    NSLog(@"\norginY--%f\norginX--%f\nheight--%f\nwidth--%f",cellInSuperview.origin.y,cellInSuperview.origin.x,cellInSuperview.size.height,cellInSuperview.size.width);
    //系统方法返回处于tableView某坐标处的cell的indexPath
//    NSIndexPath * indexPath = [_tableView indexPathForRowAtPoint:_tableView.contentOffset];
//    NSLog(@"%f---%f---%ld",[self.tableView contentOffset].y,rect.origin.y,(long)indexPath.item);
//    rect.origin.y          = fabs(rect.origin.y - [self.tableView contentOffset].y);
//
//    CGRect imageViewRect   = CGRectMake(0, 0,Screen_Width/2-.5, Screen_Width/2+120);
//    imageViewRect.origin.y = rect.origin.y-imageViewRect.size.height/2;
//    [[PurchaseCarAnimationTool shareTool] startAnimationandView:cell.img
//                                                           rect:cellInSuperview
//                                                    finisnPoint:CGPointMake(ScreenWidth / 4 * 2.5, ScreenHeight - 49)
//                                                    finishBlock:^(BOOL finish) {
//                                                        UIView *tabbarBtn = self.tabBarController.tabBar.subviews[3];
//                                                        [PurchaseCarAnimationTool shakeAnimation:tabbarBtn];
//                                                    }];
//
}

//加入购物车
-(void)addshopCar:(BestDealModel *)model{
    NSString * addShoppingCarUrl = [NSString stringWithFormat:@"%@ctl=cart&act=addcart&id=%@&number=1&email=%@&pwd=%@",GYLUrl,model.Id,EMAIL,USER_PWD];
    [RequestData requestDataOfUrl:addShoppingCarUrl success:^(NSDictionary *dic) {
        
        if([dic[@"status"] isEqual:@1]){
            
            [MBProgressHUD showSuccess:@"加入进货单成功" toView:self.view];
//            [self refreshDataOfCart];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getCarNum" object:nil];
        }else{
            [MBProgressHUD showError:dic[@"info"] toView:self.view];
        }
        
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}


-(void)deleteGoodsWithIndex:(NSInteger)index  withGoodId:(NSString *)Id{
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSString * addShoppingCarUrl = [NSString stringWithFormat:@"%@ctl=cart&act=update&id=%@&num=0&email=%@&pwd=%@",GYLUrl,Id,EMAIL,USER_PWD];
        NSString * removeCartUrl = [NSString stringWithFormat:@"%@ctl=cart&act=del&id=%@&email=%@&pwd=%@",GYLUrl,Id,EMAIL,USER_PWD];
        
//        NSString * price = [NSString stringWithFormat:@"%@",dic[@"cart_total_price"]];
//        [self changeTotalPrice:[price doubleValue]];
        [MBProgressHUD showHUD];
        //删除商品
        [RequestData requestDataOfUrl:removeCartUrl success:^(NSDictionary *dic) {
            if ([dic[@"status"] isEqual:@1]) {
                [_dataArr.firstObject removeObjectAtIndex:index];
//                [_tableView reloadData];
                [self refreshDataOfCart];
            }else{
                [MBProgressHUD showError:dic[@"info"] toView:self.view];
            }
            [MBProgressHUD dissmiss];
            
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
            [MBProgressHUD dissmiss];
        }];
//        //更新总价
//        [RequestData requestDataOfUrl:addShoppingCarUrl success:^(NSDictionary *dic) {
//            if([dic[@"status"] isEqual:@1]){
//
//            }else{
//
//                [MBProgressHUD showError:dic[@"info"] toView:self.view];
//
//            }
//        } failure:^(NSError *error) {
//
//        }];
    });
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if ([_dataArr.firstObject count]>0) {
            return 120;
        }else{
            if (EMAIL) {
                return Screen_Width/375*250;
            }
            return Screen_Width/375*100;
        }
    }else{
        return [_dataArr[1]count]/2*(Screen_Width/2+100)+100;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //        GoodInfoVC * vc = [GoodInfoVC new];
    //        vc.goodsId = self.dealidArray[indexPath.row];
    //        [self.navigationController pushViewController:vc animated:YES];
}


-(void)changeTotalPrice:(double)totalPrice{
    NSMutableAttributedString *priceStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"￥%.2lf",totalPrice]];
    [priceStr addAttribute:NSForegroundColorAttributeName value:APPColor range:NSMakeRange(0,1)];
    [priceStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:FONT_SIZE_X] range:NSMakeRange(0,1)];
    [priceStr addAttribute:NSForegroundColorAttributeName value:APPColor range:NSMakeRange(1,priceStr.length-1)];
    [priceStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:17] range:NSMakeRange(1, priceStr.length-1)];
    self.allPriceLabel.attributedText = priceStr;
//    [self refreshDataOfCart];
}





#pragma mark bar delegate
//购物车结算btn
-(void)orderpayBtnOnClick{
    if (EMAIL) {
        [MBProgressHUD showHUD];
        if (_dataArr.count>0) {
            NSString * checkUrl = [NSString stringWithFormat:@"%@ctl=cart&act=check&email=%@&pwd=%@",GYLUrl,EMAIL,USER_PWD];
            [RequestData requestDataOfUrl:checkUrl success:^(NSDictionary *dic) {
                if ([dic[@"status"] isEqual:@0]) {
                    [MBProgressHUD showError:dic[@"info"] toView:self.view];
                }else{
                    ConfirmOrderVC  * vc =  [[ConfirmOrderVC alloc] init];
                    vc.dic = dic;
                    vc.isbuynow =  _isCar ? YES : NO;
                    [self.navigationController pushViewController:vc animated:YES];
                }
                [MBProgressHUD dissmiss];
            } failure:^(NSError *error) {
                [MBProgressHUD showError:@"网络错误" toView:self.view];
                [MBProgressHUD dissmiss];
            }];
        }
    }else{
        LoginViewController * vc = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(void)addToolBarForTextField:(UITextField *)textField{
    textField.inputAccessoryView = [self addToolbar];
}
-(void)keyboardWillShow:(NSNotificationCenter *)center{
    
}
-(void)keyboardDidShow:(NSNotificationCenter *)center{
    
}
- (UIToolbar *)addToolbar
{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 40)];
    toolbar.tintColor = [UIColor blueColor];
    toolbar.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *bar = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(textFieldDone)];
    bar.tintColor = APPColor;
    toolbar.items = @[ space, bar];
    return toolbar;
}
-(void)textFieldDone{
    [self.view endEditing:YES];

}

@end
