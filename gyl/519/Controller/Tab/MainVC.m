//
//  MainVC.m
//  519
//
//  Created by 陈 on 16/8/2.
//  Copyright (c) 2016年 519. All rights reserved.
//
#import "MainVC.h"
#import "MainViewPageTC.h"
#import "MainNavTC.h"
#import "MainTimeTC.h"
#import "MainWebAdvTC.h"
#import "MainRecTC.h"
#import "MainAdvTC.h"
#import "MainHotTC.h"
#import "MainADVFourTC.h"
#import "MainElseHotTC.h"
#import "HomeSupplierCell.h"
#import "CarModel.h"
#import "GoodInfoVC.h"
#import "SelectVC.h"
#import "AFNetworking.h"
#import "base64.h"
#import "GoodListVC.h"
#import "MainWebView.h"
#import "TimeGoodVC.h"
#import <sys/utsname.h>
#import "MainTabBarController.h"
#import "GYLScanVC.h"
#import "PurchaseCarAnimationTool.h"
@interface MainVC ()<UITableViewDataSource,UITableViewDelegate,SDCycleScrollViewDelegate,MainViewPageDelegate,MainTimeTCDelegate,BMKLocationServiceDelegate,MainWebDelegate,UIAlertViewDelegate, MoreBuyGoodsDelegate,MainNewsDelegate,UIScrollViewDelegate,HomeSupplierCellDelegate,mainRectDelegate,ElseHotDelegate>

{
    NSString * _agmentStr;
    CGFloat _imageHeight;
    NSInteger  _redPackNum;
    CGFloat _alpha;
    NSInteger _rznum;
    BOOL islogin;
}
@property (nonatomic,strong)UIView * navview;
@property (nonatomic,strong)UILabel * titleLabel;
@property (nonatomic,strong)MainTabBarController * maintab;

@property(nonatomic,strong)UITableView *tableView;
@property (strong, nonatomic)CountDown *countDown;
@property (strong,nonatomic)NSArray *countDownArray;

@property (nonatomic,strong)MainViewPageTC * mainRunImagecell;
@property(nonatomic,strong)NSMutableArray *data;
@property (nonatomic,strong)NSMutableArray * imageArray;
@property (nonatomic,strong)NSMutableArray * advIdMuArr;
@property (nonatomic,strong)NSMutableArray * urlMuArr;
@property (nonatomic,strong)NSMutableArray * typeMuArr;


@property (nonatomic,strong)MainNavTC * mainNavCell;
@property (nonatomic,strong)NSMutableArray * imageNavArray;
@property (nonatomic,strong)NSMutableArray * nameNavArray;
@property (nonatomic,strong)NSMutableArray * idNavArray;
@property (nonatomic,strong)NSMutableArray * typeNavArray;
@property (nonatomic,strong)NSMutableArray * titleArray;
@property (nonatomic,strong)NSMutableArray * newsUrlArray;
@property (nonatomic,strong)NSMutableArray * newsTypeArray;
@property (nonatomic,strong)NSMutableArray * actionImageArray;
@property (nonatomic,strong)NSMutableArray * actionTypeArray;
@property (nonatomic,strong)NSMutableArray * actionidArray;
@property (nonatomic,copy)NSString * actionBgUrl;
@property (nonatomic,strong)NSMutableArray * navTextColor;


@property (nonatomic,strong)MainADVFourTC * mainADVFourCell;
@property (nonatomic,strong)NSMutableArray * advFourArray;
@property (nonatomic,strong)NSMutableArray * advFourIDArray;
@property (nonatomic,strong)NSMutableArray * advFourtypeArray;

@property (nonatomic,strong)MainTimeTC * mainTimeTCCell;
@property (nonatomic,strong)NSMutableArray * timerArray;
@property (nonatomic,copy)NSString * endTimer;

@property (nonatomic,strong)MainWebAdvTC * mainWebCell;
@property (nonatomic,copy)NSString * webString;


@property (nonatomic,strong)MainRecTC * mainRectCCell;
@property (nonatomic,strong)NSMutableArray * rectArray;

@property (nonatomic,strong)MainAdvTC * mainADVFiveCell;
@property (nonatomic,strong)NSMutableArray * advFiveImgMuArray;
@property (nonatomic,strong)NSMutableArray * advFiveUrlMuArray;
@property (nonatomic,strong)NSMutableArray * advFiveIdMuArray;
@property (nonatomic,strong)NSMutableArray * advFivetypeMuArray;

@property (nonatomic,strong)MainHotTC * mainHotCell;
@property (nonatomic,strong)MainElseHotTC * mainElseHotCell;
@property (nonatomic,strong)NSMutableArray * hotArray;
@property (nonatomic,strong)NSArray * muarrArr;

//supplier_list
@property (nonatomic,strong)HomeSupplierCell * supplierCell;
@property (nonatomic,strong)NSMutableArray * supplierMuArr;

@property (nonatomic,strong)NSIndexPath * indexPath;

//定位
@property(nonatomic,strong)BMKLocationService * locService;
@property(nonatomic,assign)float x;
@property(nonatomic,assign)float y;
@property(nonatomic,copy)NSString * cityId;
//门店坐标
@property(nonatomic,copy)NSString * xpoint;
@property(nonatomic,copy)NSString * ypoint;

//版本升级url
@property(nonatomic,copy)NSString * versionStr;
@property(nonatomic,strong)NSMutableArray * shopMuArr;
@property(nonatomic,strong)NSMutableArray * shopIdMuArr;


@property(nonatomic,strong)UIView * searchView;
@property(nonatomic,strong)UIButton * searchBtn;

@property(nonatomic,assign)BOOL statuesBarHidden;
@property(nonatomic,assign)BOOL isFirst;
@end

@implementation MainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor redColor];
    _alpha = 0.01;
    _statuesBarHidden = NO;
    _isFirst = NO;
 
    //[self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:APPColor] forBarMetrics:(UIBarMetricsDefault)];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    _imageArray = [[NSMutableArray alloc]init];
    _advIdMuArr = [NSMutableArray new];
    _urlMuArr = [NSMutableArray new];
    _typeMuArr = [NSMutableArray new];
    
    _imageNavArray = [[NSMutableArray alloc]init];
    _nameNavArray = [[NSMutableArray alloc]init];
    _idNavArray = [NSMutableArray new];
    _typeNavArray = [NSMutableArray new];
    _titleArray = [NSMutableArray new];
    _newsUrlArray = [NSMutableArray new];
    _newsTypeArray = [NSMutableArray new];
    _actionImageArray = [NSMutableArray new];
    _actionTypeArray = [NSMutableArray new];
    _actionidArray = [NSMutableArray new];
    _navTextColor = [NSMutableArray new];
    
    _timerArray = [NSMutableArray new];
    _rectArray = [[NSMutableArray alloc]init];
    _hotArray = [NSMutableArray new];
    
    _advFourArray = [NSMutableArray new];
    _advFourIDArray = [NSMutableArray new];
    _advFourtypeArray = [NSMutableArray new];
    
    _advFiveImgMuArray = [NSMutableArray new];
    _advFiveUrlMuArray = [NSMutableArray new];
    _advFiveIdMuArray = [NSMutableArray new];
    _advFivetypeMuArray = [NSMutableArray new];
    
    _supplierMuArr = [NSMutableArray new];
    islogin = NO;
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    [self initView];
    [self requestDataSourceOfMain];
//    [self requestDataOfServerVersion];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onClickNav:) name:@"navNotification" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onClickhot:) name:@"hotNotification" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onRectJump:) name:@"rectJump" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(loginOut) name:@"loginOut" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushToAd) name:@"pushtoad" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newsJump:) name:@"bphNewsJump" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willEnterForeground) name:@"refreshLocation" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshDateforHome) name:@"refreshCart" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginRefresh) name:@"loginRefresh" object:nil];
}



- (void)requestAPPVersionFromServer{
   
    NSString* nowSysVer = [[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleShortVersionString"];
    
    NSString *url = [NSString stringWithFormat:@"http://www.bphgyl.com/cxb_api/index.php?ctl=version&dev_type=ios&version=%@",nowSysVer];
    [RequestData requestDataOfUrl:url success:^(NSDictionary *dic) {
        NSLog(@"%@",dic);
        if (dic && [dic[@"status"] isEqual:@1]) {
            
            //如果手机当前版本小于服务器的版本号 提示更新
            if ([ nowSysVer compare:dic[@"serverVersion"]] == NSOrderedAscending) {
                if ([dic[@"forced_upgrade"] isEqual:@1]) {
                    
                    UIAlertController * alertVC = [UIAlertController alertControllerWithTitle:@"版本升级" message:dic[@"ios_upgrade"]  preferredStyle:(UIAlertControllerStyleAlert)];
                    [alertVC addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                        self.versionStr = [self.versionStr stringByReplacingOccurrencesOfString:@"https://" withString:@"itms-apps://"];
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.versionStr]];
                    }]];
                    [self presentViewController:alertVC animated:YES completion:nil];
                    
                    
                }else{
                    UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"版本升级" message:dic[@"ios_upgrade"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                    [alertview show];
                }
            }
            
        }else{
           
            [MBProgressHUD showSuccess:dic[@"info"] toView:self.view];
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];

}

- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}

-(void)refreshDateforHome{
    islogin = YES;
    [self loadData];
}

-(void)willEnterForeground{
    //[self refreshLocation];
    [self requestDataOfServerVersion];
}

- (void)loginRefresh{
    [self loadData];
}
/**
-(void)isOpenLocation{
    //判断定位是否可用
    if ([CLLocationManager locationServicesEnabled] && ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)) {
        
             //定位功能可用
             _locService = [[BMKLocationService alloc]init];
             _locService.delegate = self;
             [_locService startUserLocationService];
        
    }else if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied) {
        
        //定位不能用
        if (!_cityId) {
            _cityId = @"9999";
        }
        [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"stop_id"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self requestDataForLocation:_cityId];
    }
}

-(void)refreshLocation{
    _x = 0;
    _y = 0;
    [self isOpenLocation];
}
 */
-(void)loginOut{
//    _locService.delegate = self;
//    [_locService startUserLocationService];
//    [self requestDataForLocation:_cityId];
    [self allArrayRemoveObjects];
    [self requestDataSourceOfMain];
}


//rect
-(void)onRectJump:(NSNotification *)noti{
//    NSInteger index = [noti.object integerValue];
//     [self pushWebView:_rectUrlArray[index] withType:_rectIdArray[index]];
}
//nav
-(void)onClickNav:(NSNotification *)noti{
    NSInteger index = [noti.object integerValue];
    [self pushWebView:self.idNavArray[index] withType:_typeNavArray[index]];
}

//爆款
-(void)onClickhot:(NSNotification *)noti{
    NSString * goodId = noti.object;
    [self  barItemBtnOnClick:goodId];
}
//广告页跳转
-(void)pushToAd{
    NSString * runtype = [kUserDefaults objectForKey:@"advImageType"];
    NSString * runImageId = [kUserDefaults objectForKey:@"advImageId"];
    [self pushWebView:runImageId withType:runtype];
}

//头条跳转
-(void)newsJump:(NSNotification *)noti{
    NSArray * arr = noti.object;
    [self pushWebView:arr[0] withType:arr[1]];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

/*
 *  定位
 *  cityid:城市ID
 */
/**
-(void)requestDataForLocation:(NSString *)cityid{
    if (_shopMuArr) {
        [_shopIdMuArr removeAllObjects];
        [_shopMuArr removeAllObjects];
    }else{
        _shopIdMuArr = [NSMutableArray new];
        _shopMuArr = [NSMutableArray new];
    }
    
    if (!_x && !_y) {
        _x = 0;
        _y = 0;
    }
    NSString * locationUrl = [NSString stringWithFormat:@"%@ctl=shop&act=index&city_shop_id=%@&xpoint=%f&ypoint=%f",GYLUrl,cityid,_x,_y];
    
    [RequestData requestDataOfUrl:locationUrl success:^(NSDictionary *dic) {
        if (dic && dic[@"shop_list"]) {
            NSString * tel;
            for (NSDictionary * subdic in dic[@"shop_list"]) {
                [_shopIdMuArr addObject:subdic[@"id"]];
                [_shopMuArr addObject:subdic[@"name"]];
                if (!_ypoint || !_xpoint || !tel) {
                    _xpoint = subdic[@"xpoint"];
                    _ypoint = subdic[@"ypoint"];
                    tel = subdic[@"tel"];
                }
                break;
            }
            NSDictionary * storeDic = @{@"xpoint":_xpoint,@"ypoint":_ypoint,@"shopName":_shopMuArr[0],@"tel":tel};
            [[NSUserDefaults standardUserDefaults]setObject:_shopIdMuArr[0] forKey:@"shop_id"];
            [[NSUserDefaults standardUserDefaults]setObject:storeDic forKey:@"storeInfo"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [self.tableView.mj_header beginRefreshing];
            
            //webview添加.userAgent 方法
            UIWebView * webview = [[UIWebView alloc]initWithFrame:CGRectZero];
            NSString * oldAgent = [webview stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
            NSString * newAgent = [oldAgent stringByAppendingString:[NSString stringWithFormat:@"bOpIhUi90_<shop_id>%@</shop_id>",_shopIdMuArr[0]]];
            [[NSUserDefaults standardUserDefaults] setObject:newAgent forKey:@"UserAgent"];
            
        }
        if (_shopMuArr.count>0) {
            [_mainRunImagecell showShopName:_shopMuArr[0]];
//            _titleLabel.text = _shopMuArr[0];
        }
        
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}
*/


//版本信息数据请求
-(void)requestDataOfServerVersion{
    NSString* nowSysVer = [[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleShortVersionString"];
    NSString * versionUrl  =[NSString stringWithFormat:@"%@ctl=version&dev_type=ios&version=%@",GYLUrl,nowSysVer];
    [RequestData requestDataOfUrl:versionUrl success:^(NSDictionary *dic) {
        if (dic && [dic[@"status"] isEqual: @1]) {
            NSString *serVersion = [NSString stringWithFormat:@"%@",dic[@"serverVersion"]];
            
            if ([nowSysVer compare:serVersion options:NSNumericSearch] == NSOrderedAscending) {
                NSInteger isforced = [dic[@"forced_upgrade"] integerValue];
                if (isforced) {
                    UIAlertController * alertVC = [UIAlertController alertControllerWithTitle:@"版本升级" message:dic[@"ios_upgrade"]  preferredStyle:(UIAlertControllerStyleAlert)];
                    [alertVC addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
//                        _versionStr = [_versionStr stringByReplacingOccurrencesOfString:@"https://" withString:@"itms-apps://"];
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/cn/app/id1331282748?mt=8"]];
                    }]];
                    [self presentViewController:alertVC animated:YES completion:nil];
                    
                }else{
//                    UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"版本升级" message:dic[@"ios_upgrade"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
//                    [alertview show];
                }
            }
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}

//首页数据请求
- (void)requestDataSourceOfMain{
     NSString * mainUrl = [NSString stringWithFormat:@"http://www.bphgyl.com/cxb_api/index.php?cal=index&email=%@&pwd=%@",EMAIL,USER_PWD];
    [RequestData requestDataOfUrl:mainUrl success:^(NSDictionary *dic) {
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            _redPackNum = [[NSString stringWithFormat:@"%@",dic[@"hongbao_count"]]integerValue];
            _rznum = [[NSString stringWithFormat:@"%@",dic[@"rz_status"]]integerValue];
            [kUserDefaults setObject:[NSString stringWithFormat:@"%ld",(long)_rznum] forKey:@"rz_status"];
            [kUserDefaults setObject:[NSString stringWithFormat:@"%@",dic[@"group_id"]] forKey:@"group_id"];
            [kUserDefaults synchronize];
        });
        
        //0
        for (NSDictionary * runImageDic in dic[@"advs"]) {
            [_imageArray addObject:runImageDic[@"img"]];
            [_advIdMuArr addObject:runImageDic[@"id"]];
            [self mutableArrAddStringtypeMuArr:_typeMuArr dataMuArr:_urlMuArr type:runImageDic[@"type"] withData:runImageDic[@"data"]];
        }
        
        //1
        for (NSDictionary * navImageDic in dic[@"indexs"]) {
            [_imageNavArray addObject:navImageDic[@"img"]];
            [_nameNavArray addObject:navImageDic[@"name"]];
            [_navTextColor addObject:navImageDic[@"color"]];
            
            [self mutableArrAddStringtypeMuArr:_typeNavArray dataMuArr:_idNavArray type:navImageDic[@"type"] withData:navImageDic[@"data"]];
        }
        for (NSDictionary * notiDic in dic[@"index_notice"]) {
            [_titleArray addObject:notiDic[@"name"]];
             [self mutableArrAddStringtypeMuArr:_newsTypeArray dataMuArr:_newsUrlArray type:notiDic[@"type"] withData:notiDic[@"data"]];
        }
        for (NSDictionary * actionDic in dic[@"adv_3_pic"]) {
            [_actionImageArray addObject:actionDic[@"img"]];
            [self mutableArrAddStringtypeMuArr:_actionTypeArray dataMuArr:_actionidArray type:actionDic[@"type"] withData:actionDic[@"data"]];
        }
        for (NSDictionary * advBtnDic in dic[@"adv_btn_img"]) {
            if (!_actionBgUrl) {
                _actionBgUrl = advBtnDic[@"img"];
            }
        }
        //2
        for (NSDictionary * timerDic in dic[@"qianggou_deal_list"]) {
            if (self.endTimer.length<1) {
                _endTimer = timerDic[@"end_time_format"];
            }
            BestDealModel * model = [[BestDealModel alloc]init];
            [model setValuesForKeysWithDictionary:timerDic];
            [_timerArray addObject:model];
        }
        
        //3
        for (NSDictionary * advFourDic in dic[@"adv_4"]) {
            [_advFourArray addObject:advFourDic[@"img"]];
            [self mutableArrAddStringtypeMuArr:_advFourtypeArray dataMuArr:_advFourIDArray type:advFourDic[@"type"] withData:advFourDic[@"data"]];
        }

        //4
        for (NSDictionary * supplierDic in dic[@"supplier_list"]) {
            SupplierModel * model = [[SupplierModel alloc]init];
            [model setValuesForKeysWithDictionary:supplierDic];
            [self.supplierMuArr addObject:model];
        }
      
        //5
        _webString = dic[@"zt_html"];
       
        
        //6
        for (NSDictionary * gundongDic in dic[@"history_deal_list"]) {
            BestDealModel * model = [[BestDealModel alloc]init];
            [model setValuesForKeysWithDictionary:gundongDic];
            [_rectArray addObject:model];
        }
    
        
        //7
        for (NSDictionary * advFiveDic in dic[@"adv_5"]) {
            [_advFiveImgMuArray addObject:advFiveDic[@"img"]];
             [self mutableArrAddStringtypeMuArr:_advFivetypeMuArray dataMuArr:_advFiveIdMuArray type:advFiveDic[@"type"] withData:advFiveDic[@"data"]];
        }
        
        //8
        for (NSDictionary * hotDic in dic[@"supplier_deal_list"]) {
            BestDealModel * model = [[BestDealModel alloc]init];
            [model setValuesForKeysWithDictionary:hotDic];
            [_hotArray addObject:model];
        }
        
        
        [self showData];
        
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        
        [MBProgressHUD showError:@"网络错误" toView:self.view];

        NSLog(@"-- %@",error);
        
    }];
}

-(void)loadTap{
    [self allArrayRemoveObjects];
    [self loadData];
}

-(void)mutableArrAddStringtypeMuArr:(NSMutableArray *)typeArr dataMuArr:(NSMutableArray *)dataArr type:(NSString *)type withData:(NSDictionary *)data{
    if ([type isEqualToString:@"0"]) {
        [dataArr addObject: data[@"url"]];
    }else
    if ([type isEqualToString:@"11"]) {
        [dataArr addObject: data[@"tid"]];
    }else
    if ([type isEqualToString:@"12"]) {
        [dataArr addObject: data[@"cate_id"]];
    }else
    if ([type isEqualToString:@"21"]) {
        [dataArr addObject: data[@"item_id"]];
    }else
    if ([type isEqualToString:@"13"]) {
        [dataArr addObject: data[@"data"]];
    }
    [typeArr addObject:type];
}

- (void)scanAction{
    
    NSLog(@"点击了扫描");
    
    GYLScanVC *scanVC = [[GYLScanVC alloc] init];
    [self.navigationController pushViewController:scanVC animated:true];
    
}

#pragma mark 初始化 view
-(void)initView{
    self.view.backgroundColor=APPBGColor;
    
    self.searchView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 40)];
    self.searchView.backgroundColor = [UIColor clearColor];
    
  
    self.navigationItem.titleView = self.searchView;
    
    
    UIButton * leftBtn=[[UIButton alloc]initWithFrame:CGRectMake(6, 3, 35, 35)];
    [leftBtn setImage:[UIImage imageNamed:@"scan"] forState:(UIControlStateNormal)];
    [leftBtn setSelected:NO];
    leftBtn.imageEdgeInsets = UIEdgeInsetsMake(4, 4, 4, 4);
    [leftBtn addTarget:self action:@selector(scanAction) forControlEvents:UIControlEventTouchUpInside];
    [self.searchView addSubview:leftBtn];
    
    self.searchBtn = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(leftBtn.frame)+10, 5, self.searchView.frame.size.width-90, 30)];
    [self.searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
    self.searchBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [self.searchBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.searchBtn setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.7]];
    self.searchBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    [self.searchBtn setImage:[UIImage imageNamed:@"icon_搜索"] forState:(UIControlStateNormal)];
    [self.searchBtn addTarget:self action:@selector(onClickSearchBtn) forControlEvents:UIControlEventTouchUpInside];
    
   
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    
    self.searchBtn.layer.cornerRadius = 5;
    self.searchBtn.layer.masksToBounds = YES;
    [self.searchView addSubview:self.searchBtn];
    self.tableView = [[UITableView alloc] init];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view);
    }];
//    [self.navigationController setNavigationBarHidden:true animated:true];
//    if ([[UIDevice currentDevice].systemVersion floatValue] >= 11.0) {
//        self.tableView=[[UITableView alloc]initWithFrame:CGRectMake(0,-Screen_NavBarHeight-Screen_StatusBarHeight, Screen_SelfWidth, Screen_Height+20)];
//    }else{
//        self.tableView=[[UITableView alloc]initWithFrame:CGRectMake(0,0, Screen_SelfWidth, Screen_Height+20)];
//    }
    
    
    AdjustsScrollViewInsetNever(self, self.tableView);
    
    [self.tableView registerClass:[MainViewPageTC class] forCellReuseIdentifier:[MainViewPageTC getID]];
    [self.tableView registerClass:[MainNavTC class] forCellReuseIdentifier:[MainNavTC getID]];
    [self.tableView registerClass:[MainTimeTC class] forCellReuseIdentifier:[MainTimeTC getID]];
    [self.tableView registerClass:[MainADVFourTC class] forCellReuseIdentifier:[MainADVFourTC getID]];
    [self.tableView registerClass:[MainWebAdvTC class] forCellReuseIdentifier:[MainWebAdvTC getID]];
    [self.tableView registerClass:[MainRecTC class] forCellReuseIdentifier:[MainRecTC getID]];
    [self.tableView registerClass:[MainAdvTC class] forCellReuseIdentifier:[MainAdvTC getID]];
    [self.tableView registerClass:[MainHotTC class] forCellReuseIdentifier:[MainHotTC getID]];
    [self.tableView registerClass:[MainElseHotTC class] forCellReuseIdentifier:[MainElseHotTC getelseId]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.showsVerticalScrollIndicator=NO;
    self.countDown = [[CountDown alloc] init];
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self loadData];

    }];
    header.lastUpdatedTimeLabel.hidden = YES;
    self.tableView.mj_header = header;
   
    
}

-(void)onClickSearchBtn{
    SelectVC * vc = [[SelectVC alloc]init];
    vc.selectStr = @"mainJumpGoods";
    [self.navigationController pushViewController:vc animated:YES];
}



-(void)loadData{
    [self allArrayRemoveObjects];
    [self requestDataSourceOfMain];
}


-(void)allArrayRemoveObjects{
    _endTimer = @"";
    [_imageArray removeAllObjects];
    [_urlMuArr removeAllObjects];
    [_typeMuArr removeAllObjects];
    [_imageNavArray removeAllObjects];
    [_nameNavArray removeAllObjects];
    [_titleArray removeAllObjects];
    [_newsUrlArray removeAllObjects];
    [_newsTypeArray removeAllObjects];
    [_typeNavArray removeAllObjects];
    [_timerArray removeAllObjects];
    [_rectArray removeAllObjects];
    [_actionImageArray removeAllObjects];
    [_actionTypeArray removeAllObjects];
    [_actionidArray removeAllObjects];
    [_navTextColor removeAllObjects];
    _actionBgUrl = nil;
    [_hotArray removeAllObjects];
    
    [_advFourArray removeAllObjects];
    [_advFiveImgMuArray removeAllObjects];
    [_advFiveUrlMuArray removeAllObjects];
    [_idNavArray removeAllObjects];
    [_advFiveIdMuArray removeAllObjects];
    [_advIdMuArr removeAllObjects];
    [_advFourIDArray removeAllObjects];
    
    [_shopIdMuArr removeAllObjects];
    [_shopMuArr removeAllObjects];
    
    [self.supplierMuArr removeAllObjects];
}


-(void)showData{

    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self.tableView reloadData];
     self.isFirst = true;
    ///每秒回调一次
    [self.countDown countDownWithPER_SECBlock:^{
        [self updateTimeInVisibleCells];
    }];
//    if (EMAIL && !islogin) {
//       
//        if (_rznum == 0) {
//            CertificationViewController * certify = [CertificationViewController new];
//            [self.navigationController pushViewController:certify animated:YES];
//        }
//        if (_rznum == 1) {
////            if (_redPackNum > 0) {
////                _redView = [[RedpackView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
////                [self.view addSubview:_redView];
////            }
//        }
//        if (_rznum == 2) {
//            RunCertificationViewController * certify = [RunCertificationViewController new];
//            certify.rz_status = 2;
//            [self.navigationController pushViewController:certify animated:YES];
//        }
//        if (_rznum == 3) {
//            RunCertificationViewController * certify = [RunCertificationViewController new];
//            certify.rz_status = 3;
//            [self.navigationController pushViewController:certify animated:YES];
//        }
//    }
//    islogin = NO;
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return 8+self.hotArray.count/2;
    return 8+(self.hotArray.count - 1);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    _indexPath = indexPath;
    if(indexPath.row==0){
        _mainRunImagecell = [tableView dequeueReusableCellWithIdentifier:[MainViewPageTC getID] forIndexPath:indexPath];
        if(_mainRunImagecell==nil){
            _mainRunImagecell=[[MainViewPageTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainViewPageTC getID]];
        }
        _mainRunImagecell.tag = 100;
        _mainRunImagecell.delegate=self;
        _mainRunImagecell.mainViewPageDelagate=self;
        [_mainRunImagecell showImg:_imageArray withTag:100];
        return _mainRunImagecell;
    }else if(indexPath.row==1){
        _mainNavCell =[tableView dequeueReusableCellWithIdentifier:[MainNavTC getID] forIndexPath:indexPath];
        if(_mainNavCell==nil){
            _mainNavCell=[[MainNavTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainNavTC getID]];
        }
        [_mainNavCell showData:[_imageNavArray copy] withName:[_nameNavArray copy] newsNameArr:_titleArray newsUrl:_newsUrlArray withNewsType:_newsTypeArray actionType:_actionTypeArray actionId:_actionidArray actionImage:_actionImageArray actionBgImage:_actionBgUrl navTextColor:_navTextColor];
        _mainNavCell.delegate = self;
        return _mainNavCell;
    }else if(indexPath.row==2){
        _mainTimeTCCell =[tableView dequeueReusableCellWithIdentifier:[MainTimeTC getID] forIndexPath:indexPath];
        if(_mainTimeTCCell==nil){
            _mainTimeTCCell=[[MainTimeTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainTimeTC getID]];
        }
        
        if(_timerArray.count){
            _mainTimeTCCell.delagete=self;
            [_mainTimeTCCell showTime:[self getNowTimeWithString:_endTimer] ];
            [_mainTimeTCCell showData:_timerArray];
            _mainTimeTCCell.tag=indexPath.row;
        }else{
            _mainTimeTCCell.hidden = YES;
        }
        return _mainTimeTCCell;
    }else if (indexPath.row == 3){
        
        _mainADVFourCell = [tableView dequeueReusableCellWithIdentifier:[MainADVFourTC getID] forIndexPath:indexPath];
        if (_mainADVFourCell == nil) {
            _mainADVFourCell = [[MainADVFourTC alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:[MainADVFourTC getID]];
        }
        _mainADVFourCell.delegate = self;
        [_mainADVFourCell showImgOfAdvFour:_advFourArray withTag:200] ;
        return _mainADVFourCell;
        
    }else if(indexPath.row==4){
        _mainRectCCell =[tableView dequeueReusableCellWithIdentifier:[MainRecTC getID] forIndexPath:indexPath];
        if(_mainRectCCell==nil){
            _mainRectCCell=[[MainRecTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainRecTC getID]];
        }
        _mainRectCCell.delegate = self;
        [_mainRectCCell showData:_rectArray];
        return _mainRectCCell;
    }else if(indexPath.row==5){
        _mainWebCell =[tableView dequeueReusableCellWithIdentifier:[MainWebAdvTC getID] forIndexPath:indexPath];
        if(_mainWebCell==nil){
            _mainWebCell=[[MainWebAdvTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainWebAdvTC getID]];
        }
        _mainWebCell.delegate = self;
        [_mainWebCell showData:_webString];

        return _mainWebCell;
    }else if(indexPath.row==6){
        self.supplierCell = [tableView dequeueReusableCellWithIdentifier:[HomeSupplierCell getIDE]];
        if (self.supplierCell == nil) {
            self.supplierCell = [[HomeSupplierCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:[HomeSupplierCell getIDE]];
        }
        self.supplierCell.deleate = self;
        [self.supplierCell showDataForSupplier:self.supplierMuArr];
        
        return self.supplierCell;
        
        return _mainHotCell;
    }else if(indexPath.row==7){
        _mainADVFiveCell = [tableView dequeueReusableCellWithIdentifier:[MainAdvTC getID] forIndexPath:indexPath];
        if (_mainADVFiveCell == nil) {
            _mainADVFiveCell = [[MainAdvTC alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:[MainAdvTC getID]];
        }
        _mainADVFiveCell.delegate = self;
        [_mainADVFiveCell showImgOfAdvFour:_advFiveImgMuArray withTag:300];
        return _mainADVFiveCell;
    }else if(indexPath.row==8){
        if(_hotArray.count){
            _mainHotCell=[tableView dequeueReusableCellWithIdentifier:[MainHotTC getID] forIndexPath:indexPath];
            if(_mainHotCell==nil){
                _mainHotCell=[[MainHotTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainHotTC getID]];
            }
            _mainHotCell.delegate = self;
            [_mainHotCell showHotTcData:@[_hotArray[0],_hotArray[1]]];
            WS(weself);
            _mainHotCell.clikAction = ^(BestDealModel *model, NSInteger index, UIImageView *imageView) {
              
                [weself addshopCar:model andSuccessBlock:^{
//                    [weself clickAniwithCurrentIndexpath:indexPath ImageView:imageView Index:index];
                }];
    
                
               
            
            };
           
        }else{
            _mainHotCell.hidden = YES;
        }
        return _mainHotCell;
    }else{
        NSInteger index = indexPath.row-8;
        if(_hotArray.count){
            _mainElseHotCell=[tableView dequeueReusableCellWithIdentifier:[MainElseHotTC getelseId] forIndexPath:indexPath];
            if(_mainElseHotCell==nil){
                _mainElseHotCell=[[MainElseHotTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[MainElseHotTC getelseId]];
            }
//            if (_hotArray.count < index*2+1) {
//                [_hotArray addObject:_hotArray.lastObject];
//            }
            _mainElseHotCell.delegate = self;
//            [_mainElseHotCell showHotTcData:@[_hotArray[index*2],_hotArray[index*2+1]]];
            [_mainElseHotCell showHotTcData:@[_hotArray[index+1]]];
        }else{
            _mainElseHotCell.hidden = YES;
        }
        WS(weself);
        _mainElseHotCell.addAction = ^(BestDealModel *modal,UIImageView *imageView) {
            [weself addshopCar:modal andSuccessBlock:^{
//                [weself clickAniwithCurrentIndexpath:indexPath ImageView:imageView Index:index];
            }];
        };
        return _mainElseHotCell;
    }
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if(indexPath.row==0){
        return Screen_Width/640*360;
    }else if(indexPath.row==1){
        CGFloat oneLine = Screen_Width/5;
        if (_actionImageArray.count>0) {
            NSInteger countIn = _actionImageArray.count/5;
            
            CGFloat coutF = countIn + (_actionImageArray.count%5);
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:_actionImageArray[0]]];
            UIImage *image = [UIImage imageWithData:data];
            //san'mu
            return _nameNavArray.count>0 ? oneLine * coutF+40+10+(Screen_Width/(image.size.width*3)*image.size.height)-1 : 40+10+(Screen_Width/(image.size.width*3)*image.size.height)-1;
        }else{
            return _nameNavArray.count>0 ? oneLine * ((self.nameNavArray.count+4)/5)+40+20-1 : 40+20-1;
        }
        
        
//        CGFloat oneLine = Screen_Width/750*320/2;
//        return oneLine * ((_nameNavArray.count+4)/5);
    }else if(indexPath.row==2){
        if (_timerArray.count) {
            return Default_Space+10+0.1+(60+Screen_Width/3.3);
        }else{
            return 0;
        }
    } else if (indexPath.row == 3){
        if(_advFourArray.count){
            return Default_Space+Screen_Width / 750 * 180+10;
        }
        return 0;
        
    }else if(indexPath.row==4){
        if (_rectArray.count>0){
            return 110*2+40+20;
        }
        return 0;
    }else if(indexPath.row==5){
        return  Screen_Width/750*600+60;
    }else if(indexPath.row==6){
        if (self.supplierMuArr.count) {
            int lineNumber=ceil(self.supplierMuArr.count/2);
            return  (Screen_Width/750*200)*lineNumber+40;
        }else{
            _supplierCell.hidden = true;
            return 0;
        }
    }else if(indexPath.row==7){
        return Default_Space+Screen_Width / 640 * 180;
    }else if(indexPath.row==8){
        if (_hotArray.count) {
            return 40+10.5+120*2;
        }else{
            return 0;
        }
    }else{
        if (_hotArray.count) {
//            return (Screen_Width-5*3)/2+120;
            return 120;
        }else{
            return 0;
        }
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(void)mainActiontype:(NSInteger)inter{
    [self pushWebView:self.actionidArray[inter] withType:self.actionTypeArray[inter]];
}


#pragma mark 轮播 delegate
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    NSString * jumpId = @"";
    NSString * typeId = @"";
    
    if (cycleScrollView.tag == 100) {
        jumpId = self.urlMuArr[index];
        typeId = self.typeMuArr[index];
        
    }
    if (cycleScrollView.tag == 200) {
        jumpId = self.advFourIDArray[index];
        typeId = self.advFourtypeArray[index];
    }
    if (cycleScrollView.tag == 300) {
        jumpId = self.advFiveIdMuArray[index];
        typeId = self.advFivetypeMuArray[index];
    }
    
    [self pushWebView:jumpId withType:typeId];
}


#pragma mark 倒计时相关方法
//更新cell 可见 cell 数据
-(void)updateTimeInVisibleCells{
    NSArray  *cells = self.tableView.visibleCells; //取出屏幕可见ceLl
    for (UITableViewCell *cell in cells) {
        if([cell class]==[MainTimeTC class]){
            MainTimeTC *mainTimeTC=(MainTimeTC *)cell;
            [mainTimeTC showTime:[self getNowTimeWithString:_endTimer]];
        }else{
            continue;
        }
        
    }
}

//获取 index cell 数据
-(NSString *)getNowTimeWithString:(NSString *)aTimeString{
    NSDateFormatter* formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // 截止时间date格式
    NSDate  *expireDate = [formater dateFromString:aTimeString];
    NSDate  *nowDate = [NSDate date];
    // 当前时间字符串格式
    NSString *nowDateStr = [formater stringFromDate:nowDate];
    // 当前时间date格式
    nowDate = [formater dateFromString:nowDateStr];
    
    NSTimeInterval timeInterval =[expireDate timeIntervalSinceDate:nowDate];
    
    int days = (int)(timeInterval/(3600*24));
    int hours = (int)((timeInterval-days*24*3600)/3600);
    int minutes = (int)(timeInterval-days*24*3600-hours*3600)/60;
    int seconds = timeInterval-days*24*3600-hours*3600-minutes*60;
    
    NSString *dayStr;NSString *hoursStr;NSString *minutesStr;NSString *secondsStr;
    //天
    dayStr = [NSString stringWithFormat:@"%d",days];
    //小时
    hoursStr = [NSString stringWithFormat:@"%d",hours];
    //分钟
    if(minutes<Default_Space)
        minutesStr = [NSString stringWithFormat:@"0%d",minutes];
    else
        minutesStr = [NSString stringWithFormat:@"%d",minutes];
    //秒
    if(seconds < Default_Space)
        secondsStr = [NSString stringWithFormat:@"0%d", seconds];
    else
        secondsStr = [NSString stringWithFormat:@"%d",seconds];
    if (hours<=0&&minutes<=0&&seconds<=0) {
        return @"活动已经结束！";
    }
    if (days) {
        return [NSString stringWithFormat:@"%@天 %@小时 %@分 %@秒", dayStr,hoursStr, minutesStr,secondsStr];
    }
    return [NSString stringWithFormat:@"%@小时 %@分 %@秒",hoursStr , minutesStr,secondsStr];
}



#pragma  mark cell delegate

-(void)selectViewOnClick{
    SelectVC *vc=[[SelectVC alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

//mainRectdelegate
-(void)clickMainTectCell:(NSString *)rectId{
    GoodInfoVC * vc = [GoodInfoVC new];
    vc.goodsId = rectId;
     [self.navigationController pushViewController:vc animated:YES];
}
//maintimeDelegate
-(void)allTimeGood{
    TimeGoodVC * vc =[[TimeGoodVC alloc] init];
    vc.endTimer = _endTimer;
     [self.navigationController pushViewController:vc animated:YES];
}
-(void)clickTimerCell:(NSString *)timerID{
    GoodInfoVC * vc = [GoodInfoVC new];
    vc.goodsId = timerID;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)clickHotCell:(NSString *)hotId{
    GoodInfoVC * vc = [GoodInfoVC new];
    vc.goodsId = hotId;
     [self.navigationController pushViewController:vc animated:YES];
}

-(void)barItemBtnOnClick:(NSString *)goodsId{

    GoodInfoVC *vc=[[GoodInfoVC alloc]init];
    vc.goodsId = goodsId;
     [self.navigationController pushViewController:vc animated:YES];
}

- (void)addIncar:(BestDealModel *)model withImageView:(UIImageView *)imageView{
//    [self addshopCar:model];
   
}

-(void)hotGoodsaddIncar:(BestDealModel *)model andNSIndexPath:(NSIndexPath *)indexpath{
    NSLog(@"%ld",(long)indexpath.item);
//    [self addshopCar:model];
}

- (void)clickAniwithCurrentIndexpath:(NSIndexPath *)indexPath ImageView:(UIImageView *)imageView Index:(NSInteger)index{
    CGRect rect = [_tableView rectForRowAtIndexPath:indexPath];
    /// 获取当前cell 相对于self.view 当前的坐标
  
    rect.origin.y          = rect.origin.y - [_tableView contentOffset].y;
    CGRect imageViewRect   = imageView.frame;
    if (indexPath.row == 8) {
        if (index == 0){
            imageViewRect   = imageView.frame;
        }else{
            imageViewRect = CGRectMake(10, 180, 60, 60);
        }
        
    }
    imageViewRect.origin.y = rect.origin.y + imageViewRect.origin.y;
    //
    [[PurchaseCarAnimationTool shareTool] startAnimationandView:imageView
                                                           rect:imageViewRect
                                                    finisnPoint:CGPointMake(ScreenWidth / 4 * 2.5, ScreenHeight - 49)
                                                    finishBlock:^(BOOL finish) {
                                                        UIView *tabbarBtn = self.tabBarController.tabBar.subviews[3];
                                                        [PurchaseCarAnimationTool shakeAnimation:tabbarBtn];
                                                    }];
}


-(void)pushWebView:(NSString *)weburl withType:(NSString *)typeId{
    GoodListVC * goods = [[GoodListVC alloc]init];
    MainWebView * webView = [[MainWebView alloc]init];
    //网页
    if ([typeId isEqualToString:@"0"]) {
        webView.webUrl = weburl;
        if(weburl.length>2){
            [self.navigationController pushViewController:webView animated:YES];
        }
    }
    //团购
    if ([typeId isEqualToString:@"11"]) {
        goods.idStr = [NSString stringWithFormat:@"%@",weburl];
        goods.selectstr = @"mainJumpGoods";
        
        [self.navigationController pushViewController:goods animated:YES];
    }
    //商品列表
    if ([typeId isEqualToString:@"12"]) {
        
        goods.idStr = [NSString stringWithFormat:@"%@",weburl];
        goods.selectstr = @"mainJumpGoods";
        [self.navigationController pushViewController:goods animated:YES];
    }
    //商品详情
    if ([typeId isEqualToString:@"21"]) {
        GoodInfoVC * vc = [GoodInfoVC new];
        vc.goodsId = weburl;
       [self.navigationController pushViewController:vc animated:YES];
    }
    //积分
    if ([typeId isEqualToString:@"13"]) {
        goods.idStr = [NSString stringWithFormat:@"%@",weburl];
        goods.typeId = typeId;
        goods.selectstr = @"mainJumpGoods";
         [self.navigationController pushViewController:goods animated:YES];
    }
}


//加入购物车
-(void)addshopCar:(BestDealModel *)model andSuccessBlock:(void(^)(void))SuccessBlock{
    NSString * addShoppingCarUrl = [NSString stringWithFormat:@"%@ctl=cart&act=addcart&id=%@&number=1&email=%@&pwd=%@",GYLUrl,model.Id,EMAIL,USER_PWD];
    [RequestData requestDataOfUrl:addShoppingCarUrl success:^(NSDictionary *dic) {
        
        if([dic[@"status"] isEqual:@1]){
          
            [MBProgressHUD showSuccess:@"加入进货单成功" toView:self.view];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getCarNum" object:nil];
            SuccessBlock();
//            [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshCart" object:nil];
        }else{
            [MBProgressHUD showError:dic[@"info"] toView:self.view];
        }
        
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}


#pragma moreBuyGoodsDelegate
-(void)moregoodsJump{
    GoodListVC * goods = [[GoodListVC alloc]init];
    goods.idStr = @"nil&type_mz=1";
    goods.selectstr = @"mainJumpGoods";
    [self.navigationController pushViewController:goods animated:YES];
}

-(void)clickSupplierCell:(NSString *)supplierId{
    
}
-(void)clickmoreBtn{
    
}

/**
#pragma MAKLocationDelegate
-(void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation{
    if (!_x || !_y || _x == 0 || _y == 0) {
        _y = userLocation.location.coordinate.latitude;
        _x = userLocation.location.coordinate.longitude;

        CLGeocoder * geocoder = [CLGeocoder new];
        [geocoder reverseGeocodeLocation:userLocation.location completionHandler:^(NSArray * array, NSError * _Nullable error) {
            if (array.count > 0){
                CLPlacemark *placemark = [array objectAtIndex:0];
                //将获得的所有信息显示到label上
                
                //获取城市
                NSString *city = placemark.locality;
                if (!city) {
                    //四大直辖市的城市信息无法通过locality获得，只能通过获取省份的方法来获得（如果city为空，则可知为直辖市）
                    city = placemark.administrativeArea;
                    if (!city) {
                        city = @"温州";
                    }
                }
                [self requestDataForLocationCity:city];
            }else{
                [self requestDataForLocationCity:@"温州"];
            }
        }];
        _locService.delegate = nil;
    }
}

-(void)didUpdateUserHeading:(BMKUserLocation *)userLocation{

}

-(void)requestDataForLocationCity:(NSString *)cityStr{
    NSString * cityUrl = [NSString stringWithFormat:@"%@ctl=city&act=get_city&city_name=%@",GYLUrl,cityStr];
    [RequestData requestDataOfUrl:cityUrl success:^(NSDictionary *dic) {
        _cityId = dic[@"current_city"][@"id"];
        [self requestDataForLocation:_cityId];
    } failure:^(NSError *error) {
        if (error.code == -1009) {
            [MBProgressHUD showError:@"网络错误" toView:self.view];
        }else{
            [MBProgressHUD showError:@"定位出错" toView:self.view];
        }
    }];
}
 */
#pragma mark mainwebdeleagete
-(void)webPush:(NSString *)pushUrl withUrlId:(NSString *)urlId{
    if ([pushUrl isEqualToString:@"item"]) {
        GoodInfoVC * vc = [GoodInfoVC new];
        vc.goodsId = urlId;
        [self.navigationController pushViewController:vc animated:YES];
    }
    if ([pushUrl isEqualToString:@"list"]) {
        GoodListVC * vc = [GoodListVC new];
        vc.idStr = urlId;
        [self.navigationController pushViewController:vc animated:YES];
    }
    if ([pushUrl isEqualToString:@"web"]) {
        MainWebView * vc = [MainWebView new];
        vc.webUrl = urlId;
        [self.navigationController pushViewController:vc animated:YES];
    }
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    if (self.tableView.contentOffset.y > 100) {
        [self setNavigationAlpha:0.95];
    }else{
        [self setNavigationAlpha:_alpha];
    }
//     [self.navigationController setNavigationBarHidden:true animated:true];
    [self setStatusBarBackgroundColor:[UIColor clearColor]];
    [self requestDataOfServerVersion];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //[self setNavigationAlpha:0.01];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:(UIBarMetricsDefault)];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:APPThreeColor]];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat f = scrollView.contentOffset.y;
    NSLog(@"%f",f);
//    self.statuesBarHidden = NO;
//    if (f <= 0 && self.isFirst) {
//        if (scrollView.contentOffset.y <= -10.0) {
//
//            self.statuesBarHidden = true;
//            [UIView animateWithDuration:0.5 animations:^{
//                [self setNeedsStatusBarAppearanceUpdate];
//            }];
//        }
//    }else{
//        [UIView animateWithDuration:0.5 animations:^{
//            [self setNeedsStatusBarAppearanceUpdate];
//        }];
//
//    }
    
    
        if (scrollView.contentOffset.y <= 0) {
            _alpha = 0;
            [self.searchBtn setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.7]];
        }else{
            CGFloat minAlphaOffset = - 74;
            
            CGFloat maxAlphaOffset = 200;
            
            CGFloat offset = scrollView.contentOffset.y;
            
            CGFloat alpha = (offset - minAlphaOffset) / (maxAlphaOffset - minAlphaOffset);
            _alpha = alpha;
            [self.searchBtn setBackgroundColor:[UIColor colorWithWhite:1 alpha:1]];
        }
    
    
        
        [self setNavigationAlpha:_alpha];
    
    
//    if ([[UIDevice currentDevice].systemVersion floatValue] >= 11.0) {
//        if (scrollView.contentOffset.y < -Screen_NavBarHeight-Screen_NavBarHeight) {
//            [self setNavigationAlpha:0.01];
//            self.searchView.hidden = YES;
//        }else{
//            self.searchView.hidden = NO;
//            if (f>=0) {
//                if (f < 100) {
//                    _alpha = (f) / 100.f;
//                    if (_alpha == 0) {
//                        _alpha = 0.01;
//                    }
//                    [self setNavigationAlpha:_alpha];
//                    [self.searchBtn setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.7]];
//                }else{
//                    if (_alpha != 1) {
//                        _alpha = 1;
//                        [self setNavigationAlpha:1];
//                        [self.searchBtn setBackgroundColor:[UIColor colorWithWhite:1 alpha:1]];
//                    }
//                }
//            }else{
//                _alpha = 0.01;
//                [self setNavigationAlpha:_alpha];
//                [self.searchBtn setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.7]];
//            }
//        }
//    }else{
//        if (f<0) {
//            [self setNavigationAlpha:0.01];
//            self.searchView.hidden = YES;
//        }else{
//            self.searchView.hidden = NO;
//            if (f < 100) {
//                _alpha = (f) / 100.f;
//                if (_alpha == 0) {
//                    _alpha = 0.01;
//                }
//                [self setNavigationAlpha:_alpha];
//                [self.searchBtn setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.7]];
//            }else{
//                if (_alpha != 1) {
//                    _alpha = 1;
//                    [self setNavigationAlpha:1];
//                   [self.searchBtn setBackgroundColor:[UIColor colorWithWhite:1 alpha:1]];
//                }
//
//            }
//        }
//    }
    
}
-(void)setNavigationAlpha:(CGFloat)alpha{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"0xFF5511" andAlpha:alpha]] forBarMetrics:(UIBarMetricsDefault)];
}


-(void)setNavviewt{
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 30)];
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.center = CGPointMake(Screen_Width/2, _navview.frame.size.height-20);
    [_navview addSubview:_titleLabel];
    
}



-(BOOL)prefersStatusBarHidden {
    NSLog(@"--------[ViewController prefersStatusBarHidden]--------");
    return self.statuesBarHidden;
    //return YES;
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    
    NSLog(@"--------[RootNavigationController preferredStatusBarStyle]--------");
    return UIStatusBarStyleLightContent;
}


- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation{
    return UIStatusBarAnimationSlide;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
