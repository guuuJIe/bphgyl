//
//  NoGoodsView.m
//  519
//
//  Created by Macmini on 2018/1/10.
//  Copyright © 2018年 519. All rights reserved.
//

#import "NoGoodsView.h"

@interface NoGoodsView()

@property (nonatomic,strong)UIView * loginView;

@end

@implementation NoGoodsView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        if(EMAIL){
            [self createUI];
        }else{
            [self isNOLogin];
        }
        
    }
    return self;
}

-(void)createUI{
    
    UIImageView * imageV = [[UIImageView alloc]init];
    imageV.contentMode = UIViewContentModeScaleAspectFit;
    imageV.image = [UIImage imageNamed:@"icon_购物车"];
    [self addSubview:imageV];
    [imageV makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(40);
        make.centerX.equalTo(self.mas_centerX);
        make.width.offset(100);
        make.height.offset(75);
    }];
    
    UILabel * label = [[UILabel alloc]init];
    label.text = @"这里空空如也，想喝来逛啊~";
    label.font = [UIFont systemFontOfSize:13];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = APPThreeColor;
    [self addSubview:label];
    [label makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imageV.mas_bottom).with.offset(15);
        make.height.offset(20);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
    UIButton * goBtn  = [[UIButton alloc]init];
    [goBtn setTitle:@"去逛逛~" forState:(UIControlStateNormal)];
    [goBtn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
    [goBtn addTarget:self action:@selector(btnClick) forControlEvents:(UIControlEventTouchUpInside)];
    goBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    goBtn.layer.borderColor = APPFourColor.CGColor;
    goBtn.layer.borderWidth = .5;
    goBtn.layer.cornerRadius = 5;
    goBtn.layer.masksToBounds = YES;
    [self addSubview:goBtn];
    [goBtn makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label.mas_bottom).with.offset(10);
        make.centerX.equalTo(self.mas_centerX);
        make.width.offset(100);
        make.height.offset(25);
    }];
}

-(void)isNOLogin{
    UILabel * label = [[UILabel alloc]init];
    label.text = @"您还未登录，请先登录~";
    label.font = [UIFont systemFontOfSize:13];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = APPThreeColor;
    [self addSubview:label];
    [label makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(20);
        make.height.offset(20);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
    UIButton * goBtn  = [[UIButton alloc]init];
    [goBtn setTitle:@"去登陆" forState:(UIControlStateNormal)];
    [goBtn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
    [goBtn addTarget:self action:@selector(btnClick) forControlEvents:(UIControlEventTouchUpInside)];
    goBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    goBtn.layer.borderColor = APPFourColor.CGColor;
    goBtn.layer.borderWidth = .5;
    goBtn.layer.cornerRadius = 5;
    goBtn.layer.masksToBounds = YES;
    [self addSubview:goBtn];
    [goBtn makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label.mas_bottom).with.offset(10);
        make.centerX.equalTo(self.mas_centerX);
        make.width.offset(100);
        make.height.offset(25);
    }];
}

-(void)btnClick{
    if (self.block) {
        self.block();
    }
}
@end
