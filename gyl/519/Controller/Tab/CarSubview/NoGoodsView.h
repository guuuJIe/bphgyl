//
//  NoGoodsView.h
//  519
//
//  Created by Macmini on 2018/1/10.
//  Copyright © 2018年 519. All rights reserved.
//
typedef void(^golistBlock)(void);
#import <UIKit/UIKit.h>

@interface NoGoodsView : UIView
@property (nonatomic,copy)golistBlock block;
@end
