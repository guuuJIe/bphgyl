//
//  User.m
//  519
//
//  Created by 陈 on 16/8/29.
//  Copyright © 2016年 519. All rights reserved.
//
#import "UserVC.h"
#import "LoginViewController.h"
#import "UserInfoView.h"
#import "DetailTypeView.h"
#import "ElseChooseView.h"
#import "CollectVC.h"
#import "AddressVC.h"
#import "UserInfoVC.h"
#import "OrderListVC.h"
#import "CarRecommendTC.h"
#import "CarModel.h"
#import "GoodListVC.h"
#import "GoodInfoVC.h"
#import "QYSDK.h"
#import "QYSessionViewController.h"
#import "CertificationViewController.h"
#import "MainWebView.h"
#import "AboutUsVC.h"
#import "ConnactView.h"
#import "PurchaseCarAnimationTool.h"
@interface UserVC ()<UITableViewDelegate,UITableViewDataSource,recommandDelegate>
{
    CGFloat _alpha;
}
@property (nonatomic,strong)UserInfoView * infoView;
@property (nonatomic,strong)DetailTypeView * detailView;
@property (nonatomic,strong)ElseChooseView * elseView;
@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,strong)CAGradientLayer *gradientLayer;
@property (nonatomic,strong)NSMutableArray * dataMuArr;

@property (nonatomic,strong)UIImageView *navImageView;

@property (nonatomic,strong)UILabel *titleText;
@property (nonnull,nonatomic)UIView *navView;
@property(nonatomic,strong)ConnactView *conView;
@end

@implementation UserVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _dataMuArr = [NSMutableArray new];
    _alpha = 0;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.view.backgroundColor = APPBGColor;
    [self createUI];
    [self requestData];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshData) name:@"refreshCart" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshData) name:@"refreshUserInfo" object:nil];
    
    AdjustsScrollViewInsetNever(self, self.tableView);
   [self setrightitem];
    
    //获取导航栏的View
    self.navImageView = self.navigationController.navigationBar.subviews.firstObject;
}
-(void)refreshData{
    [self requestData];
}



#pragma mark ----lifeCycle-----
-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:(UIBarMetricsDefault)];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    if (self.tableView.contentOffset.y > 60) {
        _alpha = 0.95;
        
        [self setNavigationAlpha:0.95];
    }else{
        _alpha = 0;
        [self setNavigationAlpha:_alpha];
       
    }
//    if (self.titleText.alpha == 0.0) {
//        self.titleText.hidden = true;
//    }else{
//        self.titleText.hidden = false;
//    }
    
    NSLog(@"%f---%f",self.titleText.alpha,self.navigationItem.titleView.alpha);
    
    
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:(UIBarMetricsDefault)];
    [self.navigationController.navigationBar setShadowImage:[UIImage imageWithColor:APPThreeColor]];
    //[self.gradientLayer removeFromSuperlayer];
}

- (void)viewDidDisappear:(BOOL)animated{
     [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:(UIBarMetricsDefault)];
    [self setNav];
}

- (void)setNav{
//    if (self.tableView.contentOffset.y > 60) {
//        _alpha = 0.95;
//
//        [self setNavigationAlpha:0.95];
//    }else{
//        _alpha = 0;
//        [self setNavigationAlpha:_alpha];
//
//    }
}


#pragma mark --------setUI-------
-(void)setrightitem{
    UIBarButtonItem * rightItem  = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"button_设置"] style:(UIBarButtonItemStyleDone) target:self action:@selector(rightBtnClick)];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    self.navView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 150, 40)];
    self.navView.backgroundColor = [UIColor clearColor];
    
    
    self.navigationItem.titleView = self.navView;
    
    [self.navView addSubview:self.titleText];
    
  
}
-(void)rightBtnClick{
    if (EMAIL) {
        UserInfoVC * vc = [[UserInfoVC alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [self.navigationController pushViewController:[LoginViewController new] animated:YES];
    }
}
-(void)createUI{
     CGFloat height = Screen_NavBarHeight+Screen_StatusBarHeight+66;
    

    [self.view addSubview:self.tableView];
    UIView * headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Height, Screen_Width/375*height+90+100)];
    headView.backgroundColor = APPBGColor;
    self.tableView.tableHeaderView = headView;
    
    __weak typeof(self)weself = self;
    //导航栏都不隐藏，难怪点击事件一直触发不了，都是坑
    self.infoView = [[UserInfoView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Width/375*height+20)];
    self.infoView.userInteractionEnabled = true;
    [self.infoView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(click)]];
//    self.infoView.block = ^{
//        __strong typeof(weself)self = weself;
//        if (EMAIL) {
//            [self.navigationController pushViewController:[UserInfoVC new] animated:YES];
//        }else{
//            [self.navigationController pushViewController:[LoginViewController new] animated:YES];
//        }
//    };
    
    [headView addSubview:self.infoView];
    self.detailView = [[DetailTypeView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.infoView.frame), Screen_Width, 90)];
    self.detailView.block = ^(NSInteger index) {
        __strong typeof(weself)self = weself;
        if (EMAIL) {
            [self allOrderVIewOnClick:index];
        }else{
            [self.navigationController pushViewController:[LoginViewController new] animated:YES];
        }
    };
    [headView addSubview:_detailView];
    self.elseView = [[ElseChooseView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_detailView.frame)+10, Screen_Width, 90)];
    self.elseView.block = ^(NSInteger index) {
        __strong typeof(weself)self = weself;
        if (index ==4) {
            AboutUsVC *us = [AboutUsVC new];
            us.ID = @"6";
            [self.navigationController pushViewController:us animated:true];
            return ;
        }
        if (EMAIL) {
            switch (index) {
                case 0:
                    [self.navigationController pushViewController:[CollectVC new] animated:YES];
                    break;
                case 1:
                {
                    GoodListVC * vc = [GoodListVC new];
                    vc.selectstr = @"mainJumpGoods";
                    vc.idStr = @"nil&type_mz=history";
                    [self.navigationController pushViewController:vc animated:YES];
                }
                    break;
                case 2:
                    [self.navigationController pushViewController:[AddressVC new] animated:YES];
                    break;
                case 3:{
                    
//                    MainWebView * sessionViewController = [[MainWebView alloc]init];
//                    NSString* nowSysVer = [[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleShortVersionString"];
//                    sessionViewController.webUrl = [NSString stringWithFormat:@"http://www.bphapp.com/wap/index.php?ctl=onlinekf&userId=%@&os=ios&appVersion=%@&device=%@&deal_id=0&order_sn=0",EMAIL,nowSysVer,[[UIDevice currentDevice] model]];
//                    QYSessionViewController *sessionViewController = [[QYSDK sharedSDK] sessionViewController];
//                    if (EMAIL && SALES_OID) {
//                        sessionViewController.staffId = [SALES_OID intValue];
//                    }else{
//                        sessionViewController.groupId = 880528;
//                    }
//                    BaseNC *nav = [[BaseNC alloc] initWithRootViewController:sessionViewController];
//                    sessionViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:self action:@selector(onBack)];
//                    nav.navigationBar.tintColor = APPFourColor;
//                    [nav.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:19], NSForegroundColorAttributeName:APPFourColor}];
//
//                    [self presentViewController:nav animated:YES completion:^{
//
//                    }];
                    
//                    [self.navigationController pushViewController:sessionViewController animated:true];
                    
                    NSString * kefuInterface = [NSString stringWithFormat:@"%@ctl=user&act=kf&email=%@&pwd=%@",GYLUrl,EMAIL,USER_PWD];
                    [RequestData requestDataOfUrl:kefuInterface success:^(NSDictionary *dic) {
                        NSLog(@"%@",dic);
                        if([dic[@"user_login_status"] integerValue] == 0){//未登入
                            MainWebView * vc = [[MainWebView alloc]init];
                            NSString* nowSysVer = [[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleShortVersionString"];
                            vc.webUrl = [NSString stringWithFormat:@"http://www.bphapp.com/wap/index.php?ctl=onlinekf&userId=%@&os=ios&appVersion=%@&device=%@&deal_id=0&order_sn=0",EMAIL,nowSysVer,[[UIDevice currentDevice] model]];
                            [weself.navigationController pushViewController:vc animated:true];
                            //                if (EMAIL) {
                            //
                            //                }else{
                            //                    [weself.navigationController pushViewController:[LoginViewController new] animated:YES];
                            //                }
                        }else{
                            _conView = [[ConnactView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
                            [weself.conView setupData:dic];
                            [weself.view addSubview:weself.conView];
                        }
                    } failure:^(NSError *error) {
                        
                    }];
                    
                }
                    break;
                case 4:
                {
//                    if (EMAIL ) {
//                        if ([RZ_STATUS integerValue] == 0) {
//                            CertificationViewController * certify = [CertificationViewController new];
//                            [self.navigationController pushViewController:certify animated:YES];
//                        }
//                        if ([RZ_STATUS integerValue] == 1) {
//                            [MBProgressHUD showSuccess:@"已通过审核" toView:self.view];
//                        }
//                        if ([RZ_STATUS integerValue] == 2) {
//                            RunCertificationViewController * certify = [RunCertificationViewController new];
//                            certify.rz_status = 2;
//                            [self.navigationController pushViewController:certify animated:YES];
//                        }
//                        if ([RZ_STATUS integerValue] == 3) {
//                            RunCertificationViewController * certify = [RunCertificationViewController new];
//                            certify.rz_status = 3;
//                            [self.navigationController pushViewController:certify animated:YES];
//                        }
//                    }
                  
                }
                    break;
                default:
                    break;
            }
           
        }else{
            [self.navigationController pushViewController:[LoginViewController new] animated:YES];
        }
    };
    [headView addSubview:self.elseView];
    
    
}
-(void)onBack{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)click{
    if (EMAIL) {
        [self.navigationController pushViewController:[UserInfoVC new] animated:YES];
    }else{
        [self.navigationController pushViewController:[LoginViewController new] animated:YES];
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (EMAIL) {
        CarRecommendTC * cell = [tableView dequeueReusableCellWithIdentifier:[CarRecommendTC getID]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (cell == nil) {
            cell = [[CarRecommendTC alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:[CarRecommendTC getID]];
        }
        cell.dataArray = _dataMuArr;
        
        cell.delegate = self;
        cell.block = ^(NSString *Ids) {
            GoodInfoVC * vc = [GoodInfoVC new];
            vc.goodsId = Ids;
            [self.navigationController pushViewController:vc animated:YES];
        };
        
        return cell;
    }else{
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"tableViewIde"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (cell == nil) {
            cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:@"tableViewIde"];
        }
        return cell;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(EMAIL){
        return (_dataMuArr.count+1)/2*(Screen_Width/2+100)+100;
    }else{
        return 1;
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(void)addIncar:(BestDealModel *)model withPosition:(CGRect)position withCollectionView:(UICollectionView *)collectionView andCurrentCell:(CarAndMineBottomCell *)cell{
    NSString * addShoppingCarUrl = [NSString stringWithFormat:@"%@ctl=cart&act=addcart&id=%@&number=1&email=%@&pwd=%@",GYLUrl,model.Id,EMAIL,USER_PWD];
    CGRect cellInSuperview = [collectionView
                              convertRect:position toView:self.view];
    [RequestData requestDataOfUrl:addShoppingCarUrl success:^(NSDictionary *dic) {
        
        if([dic[@"status"] isEqual:@1]){
            
            [MBProgressHUD showSuccess:@"加入进货单成功" toView:self.view];
//            [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshCart" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"getCarNum" object:nil];
//            [[PurchaseCarAnimationTool shareTool] startAnimationandView:cell.img
//                                                                   rect:cellInSuperview
//                                                            finisnPoint:CGPointMake(ScreenWidth / 4 * 2.5, ScreenHeight - 49)
//                                                            finishBlock:^(BOOL finish) {
//                                                                UIView *tabbarBtn = self.tabBarController.tabBar.subviews[3];
//                                                                [PurchaseCarAnimationTool shakeAnimation:tabbarBtn];
//                                                            }];
        }else{
            [MBProgressHUD showError:dic[@"info"] toView:self.view];
        }
        
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}

-(void)requestData{
    if (EMAIL) {
        [_dataMuArr removeAllObjects];
        self.infoView.userNameLabel.text = @"登陆中...";
         NSString * userUrl = [NSString stringWithFormat:@"%@ctl=user_center&email=%@&pwd=%@",GYLUrl,EMAIL,USER_PWD];
        WS(weself);
        [RequestData requestDataOfUrl:userUrl success:^(NSDictionary *dic) {
            if ([dic[@"status"] isEqual:@1]) {
                [self.infoView.userImgView sd_setImageWithURL:[NSURL URLWithString:dic[@"user_avatar"]]];
                self.infoView.userNameLabel.text = dic[@"user_name"];
               
                
//                CGSize sizeName = [NSString sizeWithText:self.infoView.userNameLabel.text font:[UIFont systemFontOfSize:14] maxSize:Max_Size];
//                if (sizeName.width > 100) {
//                    [self.infoView.userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//                        make.width.mas_equalTo(@150);
//                    }];
//                }else{
//                    [self.infoView.userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//                        make.width.mas_equalTo(sizeName.width+10);
//                    }];
//                }
                self.infoView.elseMoneyLabel.text = [NSString stringWithFormat:@"余额:%@     积分:%@",dic[@"user_money_format"],dic[@"user_score_format"]];
                
                self.infoView.levelLabel.text = [NSString stringWithFormat:@"会员级别:%@",dic[@"user_group_name"]];
                [self.infoView.elseMoneyLabel updateConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_equalTo(self.infoView.userImgView.mas_right).with.offset(20);
                    make.bottom.mas_equalTo(self.infoView.mas_bottom).with.offset(-20);
                    make.height.offset(15);
                }];
                self.infoView.RZStatus.hidden = false;
                self.infoView.levelLabel.hidden = false;
                if ([RZ_STATUS isEqualToString:@"0"]) {
                    [self.infoView.RZStatus setTitle:@"未认证" forState:0];
                }else if ([RZ_STATUS isEqualToString:@"1"]){
                    [self.infoView.RZStatus setTitle:@"已认证" forState:0];
                }else if ([RZ_STATUS isEqualToString:@"2"]){
                    [self.infoView.RZStatus setTitle:@"认证中" forState:0];
                }else{
                    [self.infoView.RZStatus setTitle:@"认证失败" forState:0];
                }
                [self.infoView.RZStatus mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.infoView.userNameLabel.mas_right).offset(8);
                    make.centerY.equalTo(self.infoView.userNameLabel);
                    make.size.mas_equalTo(CGSizeMake(54, 18));
                }];
                
                [self.infoView.levelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.infoView.RZStatus.mas_right).offset(5);
                    make.centerY.equalTo(self.infoView.RZStatus);
                }];
                
                self.infoView.RZblock = ^{
                    if ([RZ_STATUS isEqualToString:@"0"]) {
                        CertificationViewController * certify = [CertificationViewController new];
                        [weself.navigationController pushViewController:certify animated:YES];
                    }
                    if ([RZ_STATUS isEqualToString:@"2"]) {
                        //            [MBProgressHUD showError:@"企业认证审核中，请耐心等待" toView:weself.view];
                        RunCertificationViewController * certify = [RunCertificationViewController new];
                        certify.rz_status = 2;
                        [weself.navigationController pushViewController:certify animated:YES];
                    }
                    if ([RZ_STATUS isEqualToString:@"3"]) {
                        RunCertificationViewController * certify = [RunCertificationViewController new];
                        certify.rz_status = 3;
                        [weself.navigationController pushViewController:certify animated:YES];
                    }
                };
                
                for (NSDictionary * subdic in dic[@"history_deal"]) {
                    BestDealModel * model = [[BestDealModel alloc]init];
                    [model setValuesForKeysWithDictionary:subdic];
                    [_dataMuArr addObject:model];
                }
                [_tableView reloadData];
            }
             [self.tableView.mj_header endRefreshing];
        } failure:^(NSError *error) {
            [self.tableView.mj_header endRefreshing];
        }];
    }else{
        self.infoView.RZStatus.hidden = true;
        self.infoView.levelLabel.hidden = true;
        self.infoView.userImgView.image = [UIImage imageNamed:@"user_white"];
        self.infoView.userNameLabel.text = @"登陆/注册";
        self.infoView.elseMoneyLabel.text = @"";
        [self.infoView.elseMoneyLabel updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.infoView.userImgView.mas_right).with.offset(20);
            make.bottom.mas_equalTo(self.infoView.mas_bottom).with.offset(-20);
            make.height.offset(15);
        }];
        [_tableView reloadData];
        [self.tableView.mj_header endRefreshing];
    }
}



/**
 *  index  all:0  1,2,3,4
 */
-(void)allOrderVIewOnClick:(NSInteger)index{
    NSArray *allOrderTitleArray=[[NSArray alloc]initWithObjects:@"全部",@"待付款",@"待配送",@"待评价",@"售后", nil];
    NSMutableArray *itemTitleWidthArray=[[NSMutableArray alloc]init];
    NSMutableArray *vcArray=[[NSMutableArray alloc]init];
    for (int i=0; i<allOrderTitleArray.count;i++){
        Class class=[OrderListVC class];
        [vcArray addObject:class];
        
        NSNumber *itemWidth=[NSNumber numberWithFloat:[NSString sizeWithText:[allOrderTitleArray objectAtIndex:i] font:[UIFont systemFontOfSize:FONT_SIZE_M] maxSize:Max_Size].width+Default_Space];
        [itemTitleWidthArray addObject:itemWidth];
    }
    
    WMPageController *pageController=[[WMPageController alloc] initWithViewControllerClasses:vcArray andTheirTitles:allOrderTitleArray];
    pageController.postNotification = YES;
    pageController.bounces = NO;
    pageController.menuBGColor=[UIColor whiteColor];
    pageController.menuHeight=40;
    pageController.menuViewStyle = WMMenuViewStyleLine;
    pageController.progressHeight = 2;
    pageController.itemsWidths=itemTitleWidthArray;
    pageController.progressViewWidths = itemTitleWidthArray;
    pageController.titleSizeSelected = FONT_SIZE_M;
    pageController.titleSizeNormal=FONT_SIZE_M;
//    pageController.titleFontName=@"Helvetica-Bold";
    pageController.titleColorSelected=APPColor;
    pageController.titleColorNormal=APPFourColor;
    pageController.selectIndex = (int)index;
    [pageController initBar];
    
    [self.navigationController pushViewController:pageController animated:YES];

    
}




//登出
-(void)barItemBtnOnClick{
    [MBProgressHUD showHUD];
    NSString * loginOutUrl = [NSString stringWithFormat:@"%@&ctl=user&act=loginout",GYLUrl];
    
    [RequestData requestDataOfUrl:loginOutUrl success:^(NSDictionary *dic) {
        
        NSString * appDomain =[ [NSBundle mainBundle]bundleIdentifier];
        [[NSUserDefaults standardUserDefaults]removePersistentDomainForName: appDomain];
        
        //七鱼注销
        [[QYSDK sharedSDK] logout:^(){}];
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"loginOut" object:nil];
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"]);
        [MBProgressHUD dissmiss];
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];

}


#pragma mark ---UIScrollViewDelegate-------

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y <= 0) {
        _alpha = 0;
    }else{
        CGFloat minAlphaOffset = - 74;
        
        CGFloat maxAlphaOffset = 200;
        
        CGFloat offset = scrollView.contentOffset.y;
        
        CGFloat alpha = (offset - minAlphaOffset) / (maxAlphaOffset - minAlphaOffset);
        _alpha = alpha;
    }
   
    
    [self setNavigationAlpha:_alpha];
}

-(void)setNavigationAlpha:(CGFloat)alpha{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"0xFF5511" andAlpha:alpha]] forBarMetrics:(UIBarMetricsDefault)];
    self.navView.alpha = alpha;
    self.titleText.alpha = alpha;
}



#pragma mark -------Get------

-(UITableView *)tableView{
    if (!_tableView) {
//        if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
//            _tableView=[[UITableView alloc]initWithFrame:CGRectMake(0,-Screen_NavBarHeight-Screen_StatusBarHeight, Screen_SelfWidth, Screen_Height+20)];
//        }else{
            _tableView=[[UITableView alloc]initWithFrame:CGRectMake(0,0, Screen_SelfWidth, Screen_Height-Screen_NavBarHeight)];
//        }
//        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
        [_tableView registerClass:[CarRecommendTC class] forCellReuseIdentifier:@"userCarRecommendIde"];
        _tableView.estimatedRowHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
        _tableView.estimatedSectionHeaderHeight = 0;
        MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self refreshData];
            
        }];
        header.lastUpdatedTimeLabel.hidden = YES;
        self.tableView.mj_header = header;
    }
    return _tableView;
}

- (CAGradientLayer *)gradientLayerview {
    self.gradientLayer = [[CAGradientLayer alloc]init];
    // CGColor是无法放入数组中的，必须要转型。
    self.gradientLayer.colors = @[
                             (__bridge id)[UIColor colorFromHexRGB:@"0xFF4B1F"].CGColor,
                             (__bridge id)[UIColor colorFromHexRGB:@"0xFF9068"].CGColor,
                             ];
    // 颜色分割线
    self.gradientLayer.locations = @[@0,@1];
    // 颜色渐变的起点和终点，范围为 (0~1.0, 0~1.0)
    self.gradientLayer.startPoint = CGPointMake(0, 0);
    self.gradientLayer.endPoint = CGPointMake(1.0, 0);
    self.gradientLayer.frame = CGRectMake(0, -Screen_StatusBarHeight, self.navigationController.navigationBar.bounds.size.width, Screen_StatusBarHeight + self.navigationController.navigationBar.bounds.size.height);
    return self.gradientLayer;
}

- (UILabel *)titleText{
    if (!_titleText) {
        _titleText = [[UILabel alloc] initWithFrame:CGRectMake(0, 8, 150, 25)];
        _titleText.text = @"个人中心";
        _titleText.textColor = [UIColor whiteColor];
        _titleText.font = [UIFont systemFontOfSize:17];
        _titleText.textAlignment = NSTextAlignmentCenter;
        
       
    }
    return _titleText;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
