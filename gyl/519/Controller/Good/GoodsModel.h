//
//  GoodsModel.h
//  gylPT
//
//  Created by Macmini on 2017/9/5.
//  Copyright © 2017年 wzBPH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GoodsModel : NSObject

@property (nonatomic,copy)NSString * name;
@property (nonatomic,copy)NSString * sub_name;
@property (nonatomic,copy)NSString * brief;
@property (nonatomic,copy)NSString * current_price;
@property (nonatomic,copy)NSString * origin_price;
@property (nonatomic,copy)NSArray * images;
@property (nonatomic,copy)NSString * begin_time;
@property (nonatomic,copy)NSString * end_time;
@property (nonatomic,copy)NSString * time_status;
@property (nonatomic,copy)NSString * now_time;
@property (nonatomic,copy)NSString * buy_count;
@property (nonatomic,copy)NSString * max_bought;
@property (nonatomic,copy)NSString * last_time;
@property (nonatomic,copy)NSString * share_url;
@property (nonatomic,copy)NSArray * price_list;
@property (nonatomic,copy)NSArray * supplier_info;


@end


@interface GuessModel : NSObject
@property (nonatomic,copy)NSString * icon;
@property (nonatomic,copy)NSString * name;
@property (nonatomic,copy)NSString * Id;
@property (nonatomic,copy)NSString * current_price;
@end


@interface CommentModel : NSObject
@property (nonatomic,copy)NSString * Id;
@property (nonatomic,copy)NSString * create_time;
@property (nonatomic,copy)NSString * content;
@property (nonatomic,copy)NSString * point;
@property (nonatomic,copy)NSString * user_name;
@property (nonatomic,copy)NSString * user_avatar;
@end

@interface EvaulateModel : NSObject
@property (nonatomic,copy)NSString * Id;
@property (nonatomic,copy)NSString * create_time;
@property (nonatomic,copy)NSString * content;
@property (nonatomic,copy)NSString * point;
@property (nonatomic,copy)NSString * user_name;
@property (nonatomic,copy)NSString * avatar;
@end

//@interface GoodsListModel : NSObject
//@property (nonatomic,copy)NSString * Id;
//@property (nonatomic,copy)NSString * icon;
//@property (nonatomic,copy)NSString * name;
//@property (nonatomic,copy)NSString * current_price;
//@property (nonatomic,copy)NSString * origin_price;
//@property (nonatomic,copy)NSString * buy_count;
//@end

