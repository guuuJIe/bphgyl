//
//  TimeGoodModel.h
//  519
//
//  Created by Macmini on 16/12/27.
//  Copyright © 2016年 519. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeGoodModel : NSObject

@property(nonatomic,copy)NSString * icon;
@property(nonatomic,copy)NSString * ids;
@property(nonatomic,copy)NSString * end_time_format;
@property(nonatomic,copy)NSString * name;
@property(nonatomic,copy)NSString * sub_name;
@property(nonatomic,copy)NSString * begin_time;
@property(nonatomic,copy)NSString * origin_price;
@property(nonatomic,copy)NSString * current_price;


@end
