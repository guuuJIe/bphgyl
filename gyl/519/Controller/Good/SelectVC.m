//
//  SelectVC.m
//  519
//
//  Created by 陈 on 16/9/9.
//  Copyright © 2016年 519. All rights reserved.
//

#import "SelectVC.h"
#import "GoodListVC.h"
@interface SelectVC()<UITextFieldDelegate>
@property(nonatomic,strong)UIView *customBarView;
@property(nonatomic,strong)UIButton *backBtn;
//@property(nonatomic,strong)UITextField *selectText;
@property(nonatomic,strong)UIButton *selectBtn;

@property(nonatomic,strong)TPKeyboardAvoidingScrollView *scrollView;

@property(nonatomic,strong)UIView *historyView;
@property(nonatomic,strong)UIImageView *historyImg;
@property(nonatomic,strong)UILabel *historyLabel;
@property(nonatomic,strong)UIImageView *historDelImg;
@property(nonatomic,strong)UIView *historyTagView;

@property(nonatomic,strong)UIView *hotView;
@property(nonatomic,strong)UIImageView *hotImg;
@property(nonatomic,strong)UILabel *hotLabel;
@property(nonatomic,strong)UIView *hotTagView;



@property(nonatomic,strong)NSMutableArray * selectMuArr;
@property(nonatomic,strong)NSArray * historyStrArray;
@property(nonatomic,strong)UITextField * textField;
@end

@implementation SelectVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _selectMuArr  = [NSMutableArray new];
    _historyStrArray = [[NSUserDefaults standardUserDefaults]objectForKey:@"arrayOfSelect"];
    
    [self initBar];
    [self initView];
}
-(void)initBar{
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"取消" style:(UIBarButtonItemStyleDone) target:self action:@selector(backClick)];
    
    UIView *selectView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-80, 30)];
    selectView.backgroundColor=[UIColor whiteColor];
    selectView.layer.cornerRadius=4;
    selectView.layer.masksToBounds=YES;
    
    UIImageView *selectIcon=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, 5, selectView.frame.size.height-5*2, selectView.frame.size.height-5*2)];
    selectIcon.image=[UIImage imageNamed:@"select_black_icon"];
    
    self.textField=[[UITextField alloc]initWithFrame:CGRectMake(selectIcon.frame.size.width+selectIcon.frame.origin.x+Default_Space, 0, selectView.frame.size.width-selectIcon.frame.size.width, selectView.frame.size.height)];
    self.textField.textColor=APPFourColor;
    self.textField.placeholder=@"输入关键字";
    self.textField.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.textField.returnKeyType = UIReturnKeySearch;
    self.textField.delegate = self;
    [selectView addSubview:selectIcon];
    [selectView addSubview:self.textField];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:selectView];
}
-(void)backClick{
    [_textField resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)initView{
    self.view.backgroundColor = APPBGColor;
    //滑动view
    self.scrollView=[[TPKeyboardAvoidingScrollView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-Screen_NavBarHeight-Screen_StatusBarHeight)];
    self.scrollView.bounces=NO;
    self.scrollView.showsVerticalScrollIndicator=NO;
    self.scrollView.backgroundColor=APPBGColor;
    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide)];
    viewTap.cancelsTouchesInView = NO;
    [self.scrollView addGestureRecognizer:viewTap];
    
    self.historyView=[[UIView alloc]init];
    self.historyView.backgroundColor=[UIColor whiteColor];
    self.historyImg=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, 20, 20)];
    self.historyImg.image=[UIImage imageNamed:@"select_black_icon"];
    self.historyLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.historyImg.frame.size.width+self.historyImg.frame.origin.x+Default_Space, Default_Space, Screen_Width-Default_Space-20-Default_Space-Default_Space-20-Default_Space, 20)];
    self.historyLabel.text=@"历史搜索";
    self.historyLabel.textColor=APPFourColor;
    self.historyLabel.font=[UIFont systemFontOfSize:FONT_SIZE_L];
    self.historDelImg=[[UIImageView alloc]initWithFrame:CGRectMake(self.historyLabel.frame.size.width+self.historyLabel.frame.origin.x+Default_Space  , Default_Space, 20, 20)];
    self.historDelImg.image=[UIImage imageNamed:@"delect_icon"];
    UITapGestureRecognizer * imgTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imgTap)];
    [self.historDelImg addGestureRecognizer:imgTap];
    
    self.historyTagView=[[UIView alloc]init];
    
    [self.historyView addSubview:self.historyImg];
    [self.historyView addSubview:self.historyLabel];
    [self.historyView addSubview:self.historDelImg];
    [self.historyView addSubview:self.historyTagView];


    [self.scrollView addSubview:self.historyView];
    [self.view addSubview:self.scrollView];
    
    [self loadData];
}

-(void)imgTap{
    _historyStrArray = @[];
    
    [_selectMuArr removeAllObjects];
    [[NSUserDefaults standardUserDefaults]setObject:_selectMuArr forKey:@"arrayOfSelect"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self showData];
}
-(void)loadData{
    
    [self showData];
}

-(void)showData{
    self.historyTagView.frame=CGRectMake(0, 40, Screen_Width, [self getTagViewHeight:self.historyStrArray]);
    self.historyView.frame=CGRectMake(0, 0, Screen_Width, self.historyTagView.frame.size.height+40);
    
    [self showTagData:self.historyTagView array:self.historyStrArray];
    
    self.scrollView.contentSize=CGSizeMake(Screen_Width, self.historyView.frame.size.height+self.historyView.frame.origin.y+Default_Space);
}


-(void)showTagData:(UIView *)tagView array:(NSArray *)array{
    
    for (UIView *view in tagView.subviews){
        [view removeFromSuperview];
    }
    
    CGFloat tagNowX=0;
    CGFloat tagItemHeight=36;
    int tagNowLine=0;
    for(int i=0;i<array.count;i++){
        UIFont *titleFont=[UIFont systemFontOfSize:FONT_SIZE_M];
        NSString *titleStr=[array objectAtIndex:i];
        CGFloat itemWidth=Default_Space+[NSString sizeWithText:titleStr font:titleFont maxSize:Max_Size].width+20+Default_Space;
        
        if(tagNowX+itemWidth>Screen_Width){
            tagNowLine+=1;
            tagNowX=0;
        }
        
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(tagNowX, tagNowLine*tagItemHeight, itemWidth, tagItemHeight)];
        view.backgroundColor=[UIColor clearColor];
        
        UILabel *title=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, 5, itemWidth-Default_Space*2, tagItemHeight-Default_Space)];
        title.textColor=APPFourColor;
        title.textAlignment=NSTextAlignmentCenter;
        title.text=titleStr;
        title.font=titleFont;
        title.layer.cornerRadius=5;
        title.layer.borderColor=APPFourColor.CGColor;
        title.layer.borderWidth=Default_Line;
        title.layer.masksToBounds=YES;
        title.backgroundColor=[UIColor clearColor];
        
        [view addSubview:title];
        
        view.tag=i;
        UITapGestureRecognizer *itemTap;
        if(array==self.historyStrArray){
            itemTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hisItemClick:)];
        }
        [view addGestureRecognizer:itemTap];
        
        [tagView addSubview:view];
        
        tagNowX+=itemWidth;
    }
}


-(void)hisItemClick:(id)sender{
    UITapGestureRecognizer *tap = (UITapGestureRecognizer *)sender;
    self.textField.text=[self.historyStrArray objectAtIndex:[tap view].tag];
    GoodListVC * vc = [GoodListVC new];
    vc.selectstr = self.selectStr;
    vc.searchStr = self.textField.text;
    [self.navigationController pushViewController:vc animated:YES];
}


-(void)selectOnClick{
    
    if (self.textField.text.length>0) {
        for (NSInteger i = 0; i<_historyStrArray.count; i++) {
            [_selectMuArr insertObject:_historyStrArray[i] atIndex:0];
        }
        if ([_selectMuArr containsObject:self.textField.text]) {
            [_selectMuArr removeObject:self.textField.text];
        }
        [_selectMuArr insertObject:self.textField.text atIndex:0];
        [[NSUserDefaults standardUserDefaults]setObject:_selectMuArr forKey:@"arrayOfSelect"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        GoodListVC * vc = [GoodListVC new];
        vc.selectstr = self.textField.text;
        vc.idStr = @"";
        vc.searchStr = self.textField.text;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(CGFloat)getTagViewHeight:(NSArray *)array{
    CGFloat tagNowX=0;
    CGFloat tagItemHeight=36;
    int tagNowLine=0;
    for(int i=0;i<array.count;i++){
        UIFont *titleFont=[UIFont systemFontOfSize:FONT_SIZE_M];
        NSString *titleStr=[array objectAtIndex:i];
        CGFloat itemWidth=Default_Space+[NSString sizeWithText:titleStr font:titleFont maxSize:Max_Size].width+20+Default_Space;
        
        if(tagNowX+itemWidth>Screen_Width){
            tagNowLine+=1;
            tagNowX=0;
        }
        
        tagNowX+=itemWidth;
    }
    
    return (tagNowLine+1)*tagItemHeight+Default_Space;
}

-(void)keyboardHide{
    [self.textField resignFirstResponder];
}

-(UITextField *)textField{
    if (!_textField) {
        _textField = [[UITextField alloc]initWithFrame:CGRectMake(0, 5, Screen_Width, 30)];
        [_textField becomeFirstResponder];
        _textField.tintColor = APPThreeColor;
        _textField.backgroundColor = [UIColor colorWithWhite:0.95 alpha:1];
        _textField.placeholder = @"请输入关键字";
        _textField.borderStyle = UITextBorderStyleRoundedRect;
        _textField.textColor = APPFourColor;
        _textField.textAlignment = NSTextAlignmentLeft;
        _textField.returnKeyType = UIReturnKeySearch;
        _textField.font = [UIFont systemFontOfSize:13];
        _textField.leftViewMode = UITextFieldViewModeAlways;
        _textField.enablesReturnKeyAutomatically = YES;
        _textField.delegate = self;
        _textField.leftView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Search"]];
    }
    return _textField;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    [self selectOnClick];
    return YES;
}


@end
