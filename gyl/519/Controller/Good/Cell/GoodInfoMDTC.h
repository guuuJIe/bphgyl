//
//  GoodInfoMDTC.h
//  519
//
//  Created by 陈 on 16/9/6.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"

@interface GoodInfoMDTC : BaseTC
-(void)showDataForAddress:(NSString *)address withPhone:(NSString *)phone;

+(NSString *)getID;
@end
