//
//  TimeGoodItemTC.h
//  519
//
//  Created by 陈 on 16/9/16.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"
#import "TimerGoodModel.h"

@protocol TimerGoodsNowRobDelegate <NSObject>

-(void)nowRobWithIds:(NSString *)ids;

@end

@interface TimeGoodItemTC : BaseTC
@property(nonatomic,strong)TimerGoodModel * model;
@property(nonatomic,assign)id<TimerGoodsNowRobDelegate>delegate;
+(NSString *)getID;
@end
