//
//  GoodInfoEvaTC.m
//  519
//
//  Created by 陈 on 16/9/6.
//  Copyright © 2016年 519. All rights reserved.
//

#import "GoodInfoEvaTC.h"

@interface GoodInfoEvaTC()

@property(nonatomic,strong)UIImageView *img;
@property(nonatomic,strong)UILabel *name;
@property(nonatomic,strong)UILabel *content;
@property(nonatomic,strong)UILabel *info;
@property(nonatomic,strong)RatingBar *ratingBar;
@property(nonatomic,strong)UIView *line;
@end

@implementation GoodInfoEvaTC

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initView];
    }
    
    return  self;
}

-(void)initView{
        self.backgroundColor=[UIColor whiteColor];
    self.selectionStyle=UITableViewCellSelectionStyleNone;

    self.img=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, 40, 40)];
    self.img.layer.cornerRadius=self.img.frame.size.width/2;
    self.img.layer.masksToBounds=YES;

    self.name=[[UILabel alloc]init];
    self.name.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.name.textColor=APPFourColor;

    self.content=[[UILabel alloc]init];
    self.content.numberOfLines=0;
    self.content.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.content.textColor=APPFourColor;

    self.info=[[UILabel alloc]init];
    self.info.numberOfLines=0;
    self.info.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.info.textColor=APPThreeColor;

    self.ratingBar=[[RatingBar alloc]init];
    self.ratingBar.isIndicator=YES;
    [self.ratingBar setImageDeselected:@"rating_icon_0" halfSelected:@"rating_icon_0.5" fullSelected:@"rating_icon_1" andDelegate:nil];
    
    self.line=[[UIView alloc]init];
    self.line.backgroundColor=APPBGColor;

    [self addSubview: self.img];
    [self addSubview: self.name];
    [self addSubview: self.content];
    [self addSubview: self.info];
    [self addSubview: self.ratingBar];
    [self addSubview:self.line];
}

-(void)showData:( CommentModel*)entity{
    [self.img sd_setImageWithURL:[[NSURL alloc] initWithString:entity.user_avatar]];

    self.name.frame=CGRectMake(self.img.frame.size.width+self.img.frame.origin.x+Default_Space, Default_Space, [NSString sizeWithText:@"1826****363" font:[UIFont systemFontOfSize:FONT_SIZE_M] maxSize:Max_Size].width+30, 40);
    
    self.ratingBar.frame=CGRectMake(Screen_Width-15*5-Default_Space, Default_Space+25/2, 15, 15*5);
    [self.ratingBar displayRating:[entity.point integerValue]];
    
    
    if (entity.user_name.length == 11 && [entity.user_name hasPrefix:@"1"]) {
        entity.user_name = [entity.user_name  stringByReplacingCharactersInRange:NSMakeRange(3, 4)  withString:@"****"];
    }
    self.name.text=entity.user_name;
    
    self.content.frame=CGRectMake(Default_Space, self.img.frame.size.height+Default_Space+Default_Space, Screen_Width-Default_Space*2, [entity.content textSize:self.content.font withWidth:Screen_Width-Default_Space*2].height);
    self.content.text=entity.content;
    
    self.info.frame=CGRectMake(Default_Space, self.content.frame.size.height+self.content.frame.origin.y+Default_Space, Screen_Width-Default_Space*2, [entity.create_time textSize:[UIFont systemFontOfSize:FONT_SIZE_M] withWidth:Screen_Width-Default_Space*2].height);
    self.info.text=entity.create_time;
    
    self.line.frame=CGRectMake(0, self.info.frame.origin.y+self.info.frame.size.height+Default_Space-Default_Line, Screen_Width, Default_Line);
}

-(void)showItemData:( EvaulateModel*)entity{
    [self.img sd_setImageWithURL:[[NSURL alloc] initWithString:entity.avatar]];
    
    self.name.frame=CGRectMake(self.img.frame.size.width+self.img.frame.origin.x+Default_Space, Default_Space, [NSString sizeWithText:@"1826****363" font:[UIFont systemFontOfSize:FONT_SIZE_M] maxSize:Max_Size].width+30, 40);
    
    self.ratingBar.frame=CGRectMake(Screen_Width-15*5-Default_Space, Default_Space+25/2, 15, 15*5);
    [self.ratingBar displayRating:[entity.point integerValue]];
    
    
    if (entity.user_name.length == 11 && [entity.user_name hasPrefix:@"1"]) {
        entity.user_name = [entity.user_name  stringByReplacingCharactersInRange:NSMakeRange(3, 4)  withString:@"****"];
    }
    self.name.text=entity.user_name;
    
    self.content.frame=CGRectMake(Default_Space, self.img.frame.size.height+Default_Space+Default_Space, Screen_Width-Default_Space*2, [entity.content textSize:self.content.font withWidth:Screen_Width-Default_Space*2].height);
    self.content.text=entity.content;
    
    self.info.frame=CGRectMake(Default_Space, self.content.frame.size.height+self.content.frame.origin.y+Default_Space, Screen_Width-Default_Space*2, [entity.create_time textSize:[UIFont systemFontOfSize:FONT_SIZE_M] withWidth:Screen_Width-Default_Space*2].height);
    self.info.text=entity.create_time;
    
    self.line.frame=CGRectMake(0, self.info.frame.origin.y+self.info.frame.size.height+Default_Space-Default_Line, Screen_Width, Default_Line);
}


+(CGFloat)getEvaItemHeight:(CommentModel *)entity{
    return Default_Space+40+Default_Space+[entity.content textSize:[UIFont systemFontOfSize:FONT_SIZE_M] withWidth:Screen_Width-Default_Space*2].height+Default_Space+[entity.create_time textSize:[UIFont systemFontOfSize:FONT_SIZE_M] withWidth:Screen_Width-Default_Space*2].height+Default_Space;
}

+(CGFloat)getItemHeight:(EvaulateModel *)entity{
    return Default_Space+40+Default_Space+[entity.content textSize:[UIFont systemFontOfSize:FONT_SIZE_M] withWidth:Screen_Width-Default_Space*2].height+Default_Space+[entity.create_time textSize:[UIFont systemFontOfSize:FONT_SIZE_M] withWidth:Screen_Width-Default_Space*2].height+Default_Space;
}


+(NSString *)getID{
    return @"GoodInfoEvaTC";
}
@end
