//
//  GoodListTableViewCell.m
//  519
//
//  Created by Macmini on 2017/12/30.
//  Copyright © 2017年 519. All rights reserved.
//

#import "GoodListTableViewCell.h"

@interface GoodListTableViewCell()

@property(nonatomic,strong)UIImageView * iconImgV;
@property (nonatomic,strong)UILabel * nameLabel;
@property (nonatomic,strong)UILabel * nowPriceLabel;
@property (nonatomic,strong)UILabel * oldPriceLabel;
@property (nonatomic,strong)UILabel * buyCountLabel;

@end

@implementation GoodListTableViewCell


-(instancetype)initWithFrame:(CGRect)frame{
    if (self == [super initWithFrame:frame]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self createUI];
    }
    return self;
}

-(void)createUI{
    _iconImgV = [[UIImageView alloc]init];
    [self addSubview:_iconImgV];
    [_iconImgV makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.offset(10);
        make.width.and.height.equalTo(80);
    }];
    
    _nameLabel = [[UILabel alloc]init];
    _nameLabel.textColor = APPFourColor;
    _nameLabel.textAlignment = NSTextAlignmentLeft;
    _nameLabel.font = [UIFont systemFontOfSize:13];
    _nameLabel.numberOfLines=2;
    [self addSubview:_nameLabel];
    [_nameLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_iconImgV.mas_left).width.offset(10);
        make.top.offset(10);
        make.width.offset(self.frame.size.width-110);
    }];
    
    _nowPriceLabel = [[UILabel alloc]init];
    _nowPriceLabel.textColor = [UIColor redColor];
    _nowPriceLabel.textAlignment = NSTextAlignmentLeft;
    _nowPriceLabel.font = [UIFont systemFontOfSize:13];
    [self addSubview:_nameLabel];
    [_nowPriceLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_iconImgV.mas_right).with.offset(10);
        make.bottom.offset(-10);
        make.height.offset(20);
    }];
    
    _oldPriceLabel = [[UILabel alloc]init];
    _oldPriceLabel.textColor = APPThreeColor;
    _oldPriceLabel.textAlignment = NSTextAlignmentLeft;
    _oldPriceLabel.font = [UIFont systemFontOfSize:11];
    [self addSubview:_oldPriceLabel];
    [_oldPriceLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_nowPriceLabel.mas_right).with.offset(10);
        make.bottom.offset(-10);
        make.height.offset(20);
    }];
    
    _buyCountLabel = [[UILabel alloc]init];
    _buyCountLabel.textColor = APPThreeColor;
    _buyCountLabel.textAlignment = NSTextAlignmentRight;
    _buyCountLabel.font = [UIFont systemFontOfSize:11];
    [self addSubview:_buyCountLabel];
    [_buyCountLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.bottom.offset(-10);
        make.height.offset(20);
    }];
    
}
//-(void)setModel:(GoodsListModel *)model{
//    _model = model;
//    [_iconImgV sd_setImageWithURL:[NSURL URLWithString:model.icon]];
//    _nameLabel.text = model.name;
//    _nowPriceLabel.text = [NSString stringWithFormat:@"%@",model.current_price];
//    _oldPriceLabel.text = [NSString stringWithFormat:@"%@",model.origin_price];
//    _buyCountLabel.text = [NSString stringWithFormat:@"%@",model.buy_count];
//}


@end
