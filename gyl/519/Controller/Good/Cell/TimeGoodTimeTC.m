//
//  TimeGoodTimeTC.m
//  519
//
//  Created by 陈 on 16/9/16.
//  Copyright © 2016年 519. All rights reserved.
//

#import "TimeGoodTimeTC.h"

@interface TimeGoodTimeTC()
@property (nonatomic,strong)UIView *redLine;
@property(nonatomic,copy)NSString *xsqgStr;
@property(nonatomic,strong)UIFont *xsqgFont;
@property(nonatomic,strong)UILabel *xsqgLabel;
@property(nonatomic,copy)NSString *timeStr;
@property(nonatomic,strong)UILabel *timeLabel;

@end

@implementation TimeGoodTimeTC
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initView];
    }
    
    return  self;
}

-(void)initView{
    self.backgroundColor=[UIColor whiteColor];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    self.redLine=[[UIView alloc]initWithFrame:CGRectMake(Default_Space, 25, 3, 20)];
    self.redLine.backgroundColor=APPColor;

    self.xsqgStr=@"限时抢购";
    self.xsqgFont=[UIFont systemFontOfSize:FONT_SIZE_L];
    self.xsqgLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.redLine.frame.origin.x+self.redLine.frame.size.width+Default_Space, 25, [NSString sizeWithText:self.xsqgStr font:self.xsqgFont maxSize:Max_Size].width, 20)];
    self.xsqgLabel.text=self.xsqgStr;
    self.xsqgLabel.textAlignment=NSTextAlignmentCenter;
    self.xsqgLabel.font=self.xsqgFont;
    self.xsqgLabel.textColor=APPFourColor;
    
    self.timeStr=@"距结束 00:00:00";
    self.timeLabel=[[UILabel alloc]init];
    self.timeLabel.textColor=[UIColor whiteColor];
    self.timeLabel.textAlignment=NSTextAlignmentCenter;
    self.timeLabel.backgroundColor=APPThreeColor;
    self.timeLabel.layer.cornerRadius=5;
    self.timeLabel.layer.masksToBounds=YES;
    self.timeLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    
    UIView *line=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Default_Space)];
    line.backgroundColor=APPBGColor;
    UIView *line1=[[UIView alloc]initWithFrame:CGRectMake(0, 60, Screen_Width, Default_Space)];
    line1.backgroundColor=APPBGColor;

    [self addSubview:self.xsqgLabel];
    [self addSubview:self.redLine];
    [self addSubview:self.timeLabel];
    [self addSubview:line];
    [self addSubview:line1];
}

-(void)showData:(NSString *)str{
    self.timeStr=str;
    self.timeLabel.frame=CGRectMake(Screen_Width-[NSString sizeWithText:self.timeStr font:self.timeLabel.font maxSize:Max_Size].width-Default_Space*2, 25, [NSString sizeWithText:self.timeStr font:self.timeLabel.font maxSize:Max_Size].width+Default_Space,  20);
    self.timeLabel.text=self.timeStr;
}

+(NSString *)getID{
    return @"TimeGoodTimeTC";
}
@end
