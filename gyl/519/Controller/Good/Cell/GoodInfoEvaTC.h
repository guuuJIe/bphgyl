//
//  GoodInfoEvaTC.h
//  519
//
//  Created by 陈 on 16/9/6.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"
#import "GoodsModel.h"
@interface GoodInfoEvaTC : BaseTC


-(void)showData:(CommentModel *)entity;
-(void)showItemData:( EvaulateModel*)entity;
+(CGFloat)getEvaItemHeight:(CommentModel *)entity;
+(CGFloat)getItemHeight:(EvaulateModel *)entity;
+(NSString *)getID;

@end
