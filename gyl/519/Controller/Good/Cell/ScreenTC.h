//
//  ScreenTC.h
//  519
//
//  Created by Macmini on 16/11/25.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"
#import "GoodsRepleaseModel.h"

@interface ScreenTC : BaseTC

@property (nonatomic,strong)UILabel * titleLabel;
@property (nonatomic,strong)UIButton * subCellBtn;
@property (nonatomic,strong)NSArray * dataArray;
@property (nonatomic,strong)UIView * mlView;

+(NSString *)getID;
@end
