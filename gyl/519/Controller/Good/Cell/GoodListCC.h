//
//  GoodListCC.h
//  519
//
//  Created by 陈 on 16/9/17.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseCC.h"
#import "GoodInfoModel.h"
#import "GoodsModel.h"
@class BestDealModel;

typedef void(^AddIncarcarBlock)(NSString *totalPrice);

@protocol RemoveCollectGoodsDelegate <NSObject>

@optional

-(void)removeCollectGoodsIds:(NSInteger)index;
-(void)addInCar:(NSString *)model;

@end


@interface GoodListCC : BaseCC
@property (nonatomic,copy)NSString * goodsId;
@property (nonatomic,assign)id<RemoveCollectGoodsDelegate>delegate;
@property (nonatomic,strong)GuessModel * guesssModel; //猜喜欢

@property (nonatomic,strong)BestDealModel * homeModel;

-(void)showDataForCollectVCName:(NSString *)name icon:(NSString *)icon withPrice:(NSString *)price  oldprice:(NSString *)oldPrice withCound:(NSString *)count withcount:(NSInteger)index;

-(void)showDataOfTitle:(NSString *)title image:(NSString *)image withPrice:(NSString *)price  oldprice:(NSString *)oldPrice withCound:(NSString *)count withspec:(NSString *)spec withid:(NSString *)ID withExpiredata:(NSString *)data;
//buy_type:(NSString *)buy_type withDeal_score:(NSString *)score;



+(NSString *)getID;
@end
