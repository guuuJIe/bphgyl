//
//  TimeGoodViewPageTC.h
//  519
//
//  Created by 陈 on 16/9/16.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"

@interface TimeGoodViewPageTC : BaseTC
@property(nonatomic,strong)id<SDCycleScrollViewDelegate> delegate;

-(void)showImg:(NSArray *)imgs;

+(NSString *)getID;
@end
