//
//  GoodInfoMDTC.m
//  519
//
//  Created by 陈 on 16/9/6.
//  Copyright © 2016年 519. All rights reserved.
//

#import "GoodInfoMDTC.h"

@interface GoodInfoMDTC()
@property(nonatomic,strong)UILabel *title;
@property(nonatomic,strong)UILabel *phone;
@property(nonatomic,strong)UIImageView *call;
@property(nonatomic,strong)UIView *line;
@end

@implementation GoodInfoMDTC

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initView];
    }
    
    return  self;
}

-(void)initView{
    self.backgroundColor=[UIColor whiteColor];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    self.title=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, Screen_Width-Default_Space*2, 20)];
    self.title.textColor=APPFourColor;
    self.title.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.phone=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, self.title.frame.size.height+Default_Space+Default_Space, Screen_Width-Default_Space*2, 20)];
    self.phone.textColor=APPFourColor;
    self.phone.font=[UIFont systemFontOfSize:FONT_SIZE_M];

    self.call=[[UIImageView alloc]initWithFrame:CGRectMake(Screen_Width-Default_Space-40, 15, 40, 40)];
    self.call.image=[UIImage imageNamed:@"phone_black_icon"];
    UITapGestureRecognizer * cellTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cellTap)];
    [self.call addGestureRecognizer:cellTap];
    
    self.line=[[UIView alloc]initWithFrame:CGRectMake(0, 70-Default_Line, Screen_Width, Default_Line)];
    self.line.backgroundColor=APPBGColor;
    
    [self addSubview:self.title];
    [self addSubview:self.phone];
    [self addSubview:self.call];
    [self addSubview:self.line];
}

-(void)showDataForAddress:(NSString *)address withPhone:(NSString *)phone{
    self.title.text=address;
    self.phone.text=phone;
}

-(void)cellTap{
    NSLog(@"call");
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:self.phone.text delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alertView show];
    
}


+(NSString *)getID{
    return @"GoodInfoMDTC";
}

@end
