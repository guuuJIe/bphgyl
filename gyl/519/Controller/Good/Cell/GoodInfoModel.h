//
//  GoodInfoModel.h
//  519
//
//  Created by Macmini on 2017/8/4.
//  Copyright © 2017年 519. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GoodInfoModel : NSObject
@property(nonatomic,copy)NSString * name;
@property(nonatomic,copy)NSString * buy_count;
@property(nonatomic,copy)NSString * icon;
@property(nonatomic,copy)NSString * ids;
@end
