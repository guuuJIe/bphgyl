//
//  ScreenTC.m
//  519
//
//  Created by Macmini on 16/11/25.
//  Copyright © 2016年 519. All rights reserved.
//
#import "ScreenTC.h"
@interface ScreenTC()




@property (nonatomic,strong)NSMutableArray * subCellTitleArr;
@end

@implementation ScreenTC

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.subCellTitleArr = [NSMutableArray new];
        [self initView];
    }
    
    return  self;
}

-(void)initView{
    
    
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 10, 100, 30)];
    _titleLabel.textColor = [UIColor colorWithRed:134/250.0 green:134/250.0 blue:134/250.0 alpha:1];
    _titleLabel.font = [UIFont systemFontOfSize:15];
    _titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:_titleLabel];
    
    _mlView = [[UIView alloc]initWithFrame:CGRectMake(20, 40, Screen_Width-40, 60)];
    [self.contentView addSubview:_mlView];
}


+(NSString *)getID{
    return @"GoodsScreenCell";
}


@end
