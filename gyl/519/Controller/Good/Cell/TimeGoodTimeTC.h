//
//  TimeGoodTimeTC.h
//  519
//
//  Created by 陈 on 16/9/16.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"
#import "TimerGoodModel.h"
@interface TimeGoodTimeTC : BaseTC
@property(nonatomic,strong)TimerGoodModel * model;


-(void)showData:(NSString *)str;

+(NSString *)getID;
@end
