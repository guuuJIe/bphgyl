//
//  EvaluateVC.m
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//

#import "EvaluateVC.h"

#import "GoodInfoEvaTC.h"

@interface EvaluateVC()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)UIButton *backBtn;

@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *evaData;


@end

@implementation EvaluateVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"评价";
    [self initView];
    [self requestDataForEvaluation];
}

-(void)requestDataForEvaluation{
    if(self.evaData==nil){
        self.evaData=[[NSMutableArray alloc] init];
    }

    NSString * email = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    NSString * pwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
    NSString * evaluateUrl = [NSString stringWithFormat:@"%@ctl=dp&type=deal&data_id=%@&email=%@&pwd=%@",GYLUrl,_evaluateId,email,pwd];
    
    [RequestData requestDataOfUrl:evaluateUrl success:^(NSDictionary *dic) {
        
        for (NSDictionary * sbdic  in dic[@"item"]) {
            EvaulateModel * eva = [EvaulateModel new];
            [eva setValuesForKeysWithDictionary:sbdic];
            [self.evaData addObject:eva];
        }
        [self showData];
    } failure:^(NSError *error) {
        
    }];
}


-(void)initView{
    self.tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-Screen_NavBarHeight-Screen_StatusBarHeight)];
    [self.tableView registerClass:[GoodInfoEvaTC class] forCellReuseIdentifier:[GoodInfoEvaTC getID]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.showsVerticalScrollIndicator=NO;
    [self.view addSubview:self.tableView];
    self.view.backgroundColor = [UIColor whiteColor];
}


-(void)showData{
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    
    [self.tableView reloadData];
}


#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.evaData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

        EvaulateModel *entity=[self.evaData objectAtIndex:indexPath.row];
        GoodInfoEvaTC *cell=[tableView dequeueReusableCellWithIdentifier:[GoodInfoEvaTC getID] forIndexPath:indexPath];
        if(cell==nil){
            cell=[[GoodInfoEvaTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[GoodInfoEvaTC getID]];
        }
        [cell showItemData:entity];
        
        return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
        EvaulateModel *entity=[self.evaData objectAtIndex:indexPath.row];
        return [GoodInfoEvaTC getItemHeight:entity];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

@end
