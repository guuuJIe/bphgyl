//
//  GoodInfoVC.m
//  519
//
//  Created by 陈 on 16/9/5.
//  Copyright © 2016年 519. All rights reserved.
//

#import "GoodInfoVC.h"
#import "OneScrollView.h"
#import "TwoScrollView.h"
#import "GoodsModel.h"
#import "EndView.h"
#import "CarVC.h"
#import "AddCarView.h"
#import "ConfirmOrderVC.h"
#import "EvaluateVC.h"
#import "LoginViewController.h"
#import "MainWebView.h"
#import "ConnactView.h"
@interface GoodInfoVC ()<UIScrollViewDelegate,SDCycleScrollViewDelegate,UIWebViewDelegate>

{
    CGFloat contentHeight;
    NSString * _shareSubTitle;
    NSString * _shareIcon;
    NSString * _shareUrl;
    NSInteger  _goodsnum;
    
    
    NSString * _buy_type;//是否积分商品 1：1，0：0
    
    NSString * _timeStantus; //秒杀状态
    NSString * _beginTime;  //秒杀开始时间
    int _nowTime;
    
    BOOL isfirst;
}

@property (nonatomic,strong)UIView * parentView;
@property (nonatomic,strong)OneScrollView * oneScrollView;
@property (nonatomic,strong)TwoScrollView * twoscrollView;
@property (nonatomic,strong)EndView * endView;
@property (nonatomic,strong)AddCarView * addview;
@property (nonatomic,strong)NSMutableArray * dataArr;
@property (nonatomic,copy)NSString * shareTitile;
@property (nonatomic,copy)NSString * phoneNum;
@property (nonatomic,copy)NSString * icon;
@property (nonatomic,copy)NSString * onePrice;//单价
@property (nonatomic,copy)NSString * endGoodNum;//库存
@property(nonatomic,strong)CountDown *countDown;
@property(nonatomic,strong)NSMutableArray * guessMuArray;//猜喜欢
@property(nonatomic,strong)NSMutableArray * commentMuArray;//评论
@property(nonatomic,strong)ConnactView *conView;

@property(nonatomic,strong)UIWebView *webview;

@end

@implementation GoodInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"商品详情";
    self.dataArr = [NSMutableArray new];
    self.guessMuArray = [NSMutableArray new];
    self.commentMuArray = [NSMutableArray new];
    [self initView];
    [self requestGoodInfoData];
    
    isfirst = true;
}

//- (void)viewWillAppear:(BOOL)animated{
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
//}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [_countDown destoryTimer];
}
-(void)initView{
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    UIView * shareView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton * shareBtn = [[UIButton alloc]initWithFrame:shareView.bounds];
    [shareBtn setImage:[UIImage imageNamed:@"分享-1"] forState:(UIControlStateNormal)];
    [shareBtn addTarget:self action:@selector(shareItem) forControlEvents:(UIControlEventTouchUpInside)];
    [shareView addSubview:shareBtn];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:shareView];;
    
    _parentView = [[UIView alloc]initWithFrame:CGRectMake(0,0, Screen_Width, Screen_Height*2)];
    _parentView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_parentView];
    [_parentView makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.offset(0);
        make.width.offset(Screen_Width);
        make.height.offset(Screen_Height*2);
    }];
    
    [_parentView addSubview:self.oneScrollView];
    [self.oneScrollView makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.left.equalTo(self.view).offset(0);
        make.bottom.equalTo(self.view.mas_bottom).offset(-50-BottomAreaHeight);
    }];
    
    [self.parentView layoutIfNeeded];
    
    self.webview = [[UIWebView alloc]initWithFrame:CGRectMake(0, Screen_Height, Screen_Width, Screen_Height-Screen_NavBarHeight-50-BottomAreaHeight)];
    self.webview.backgroundColor = [UIColor whiteColor];
//    self.webview.scrollView.scrollEnabled = true;
    self.webview.scrollView.delegate = self;
    self.webview.delegate = self;
    [self.webview sizeToFit];
//    self.webview.scrollView.bounces=NO;
    self.webview.scrollView.showsVerticalScrollIndicator=NO;
    self.webview.scrollView.showsHorizontalScrollIndicator = NO;
   
    [_parentView addSubview:self.webview];
    
    _endView = [[EndView alloc]init];
    [self.view addSubview:_endView];
    [_endView makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(0);
        make.width.offset(Screen_Width);
        make.bottom.equalTo(self.view.bottom).offset(0);
        make.height.offset(50+BottomAreaHeight);
    }];
    __weak typeof(self)weself = self;

    //底部按钮调用 endthingNum 0:收藏  1:跳转进货单  2:加入进货单  3:购买
    _endView.connact = ^(NSInteger endThingNum, NSString *things) {
        NSString * kefuInterface = [NSString stringWithFormat:@"%@ctl=user&act=kf&email=%@&pwd=%@",GYLUrl,EMAIL,USER_PWD];
        [RequestData requestDataOfUrl:kefuInterface success:^(NSDictionary *dic) {
            NSLog(@"%@",dic);
            if([dic[@"user_login_status"] integerValue] == 0){//未登入
                MainWebView * vc = [[MainWebView alloc]init];
                NSString* nowSysVer = [[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleShortVersionString"];
                vc.webUrl = [NSString stringWithFormat:@"http://www.bphapp.com/wap/index.php?ctl=onlinekf&userId=%@&os=ios&appVersion=%@&device=%@&deal_id=0&order_sn=0",EMAIL,nowSysVer,[[UIDevice currentDevice] model]];
                [weself.navigationController pushViewController:vc animated:true];
//                if (EMAIL) {
//                  
//                }else{
//                    [weself.navigationController pushViewController:[LoginViewController new] animated:YES];
//                }
            }else{
                _conView = [[ConnactView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
                [weself.conView setupData:dic];
                [weself.view addSubview:weself.conView];
            }
        } failure:^(NSError *error) {
            
        }];
    };
    
    _endView.verify = ^{
        
        NSLog(@"%@",EMAIL);
        if (EMAIL == NULL) {
            [weself.navigationController pushViewController:[LoginViewController new] animated:YES];
            return;
        }
        
        if ([RZ_STATUS isEqualToString:@"0"]) {
            CertificationViewController * certify = [CertificationViewController new];
            [weself.navigationController pushViewController:certify animated:YES];
        }
        if ([RZ_STATUS isEqualToString:@"2"]) {
//            [MBProgressHUD showError:@"企业认证审核中，请耐心等待" toView:weself.view];
                        RunCertificationViewController * certify = [RunCertificationViewController new];
                        certify.rz_status = 2;
                        [weself.navigationController pushViewController:certify animated:YES];
        }
        if ([RZ_STATUS isEqualToString:@"3"]) {
            RunCertificationViewController * certify = [RunCertificationViewController new];
            certify.rz_status = 3;
            [weself.navigationController pushViewController:certify animated:YES];
        }
        
        if ([RZ_STATUS isEqualToString:@"1"]) {
//            RunCertificationViewController * certify = [RunCertificationViewController new];
//            certify.rz_status = 1;
//            [weself.navigationController pushViewController:certify animated:YES];
//            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",weself.phoneNum]]];
            NSString * kefuInterface = [NSString stringWithFormat:@"%@ctl=user&act=kf&email=%@&pwd=%@",GYLUrl,EMAIL,USER_PWD];
            [RequestData requestDataOfUrl:kefuInterface success:^(NSDictionary *dic) {
                NSLog(@"%@",dic);
                if([dic[@"user_login_status"] integerValue] == 0){//未登入
                    MainWebView * vc = [[MainWebView alloc]init];
                    NSString* nowSysVer = [[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleShortVersionString"];
                    vc.webUrl = [NSString stringWithFormat:@"http://www.bphapp.com/wap/index.php?ctl=onlinekf&userId=%@&os=ios&appVersion=%@&device=%@&deal_id=0&order_sn=0",EMAIL,nowSysVer,[[UIDevice currentDevice] model]];
                    [weself.navigationController pushViewController:vc animated:true];
                    //                if (EMAIL) {
                    //
                    //                }else{
                    //                    [weself.navigationController pushViewController:[LoginViewController new] animated:YES];
                    //                }
                }else{
                    _conView = [[ConnactView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
                    [weself.conView setupData:dic];
                    [weself.view addSubview:weself.conView];
                }
            } failure:^(NSError *error) {
                
            }];
        }
    };
    
    _endView.block = ^(NSInteger endThingNum, NSString *things) {
        
        
        if (EMAIL == NULL) {
            [weself.navigationController pushViewController:[LoginViewController new] animated:YES];
            return;
        }
        
        if ([RZ_STATUS isEqualToString:@"1"]) {
            if (endThingNum == 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD showSuccess:things toView:weself.view];
                });
            }
            if (endThingNum == 1) {
                CarVC * car = [CarVC new];
                car.isCar = YES;
                [weself.navigationController pushViewController:car animated:YES];
            }
            if (endThingNum == 2 || endThingNum == 3) {
                [weself addcarView:endThingNum withgoodNum:1];
            }if (endThingNum == 4) {
                [weself.navigationController pushViewController:[LoginViewController new] animated:YES];
            }
            if (endThingNum == 5) {
                MainWebView * vc = [[MainWebView alloc]init];
                vc.webUrl = [NSString stringWithFormat:@"http://www.bphgyl.com/wap/index.php?ctl=onlinekf&userId=%@&os=ios&appVersion=%@&device=xiaomi&deal_id=%@&order_sn=%@",EMAIL,[[UIDevice currentDevice] model],@"",@""];
                [weself.navigationController pushViewController:vc animated:true];
            }
        }
        if ([RZ_STATUS isEqualToString:@"0"]) {
            CertificationViewController * certify = [CertificationViewController new];
            [weself.navigationController pushViewController:certify animated:YES];
        }
        if ([RZ_STATUS isEqualToString:@"2"]) {
//            [MBProgressHUD showError:@"企业认证审核中，请耐心等待" toView:weself.view];
                        RunCertificationViewController * certify = [RunCertificationViewController new];
                        certify.rz_status = 2;
                        [weself.navigationController pushViewController:certify animated:YES];
        }
        if ([RZ_STATUS isEqualToString:@"3"]) {
            RunCertificationViewController * certify = [RunCertificationViewController new];
            certify.rz_status = 3;
            [weself.navigationController pushViewController:certify animated:YES];
        }
    };
    
    self.oneScrollView.priceScroll.block = ^(NSInteger number) {
        __strong typeof(weself)self = weself;
        [self addcarView:3 withgoodNum:number];
    };
    
    _endView.exchangeGoods = ^{
        __strong typeof(weself)self = weself;
        if (EMAIL) {
            
        }else{
            [MBProgressHUD showError:@"请先登录" toView:self.view];
            return ;
        }
        
         [self addcarView:1 withgoodNum:1];
    };
    
    //更多评论跳转
    self.oneScrollView.commentView.block = ^{
         __strong typeof(weself)self = weself;
        EvaluateVC *vc = [[EvaluateVC alloc] init];
        vc.evaluateId = self.goodsId;
        [weself.navigationController pushViewController:vc animated:YES];
    };
    
    //onescrollview回来时调用
    self.oneScrollView.block = ^{
        __strong typeof(weself)self = weself;
        [UIView animateWithDuration:0.5 animations:^{
            self.parentView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -Screen_Height);
        }];
    };
    //调用电话
    self.oneScrollView.thingBlock = ^(NSInteger thingNum) {
        __strong typeof(weself)self = weself;
        if (thingNum == 0) {
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",self.phoneNum]]];
        }
    };
    //出现twoscrollview调用
    self.twoscrollView.block = ^{
        __strong typeof(weself)self = weself;
        [UIView animateWithDuration:0.5 animations:^{
            self.parentView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, 0);
        }];
    };
    
}
-(void)addcarView:(NSInteger)endThingNum withgoodNum:(NSInteger)goodnum{
    self.addview = [[AddCarView alloc]init];
    self.addview.Id = self.goodsId;
    [self.addview.iconImageView sd_setImageWithURL:[NSURL URLWithString:self.icon]];
    self.addview.nameLabel.text = self.shareTitile;
    self.addview.onePrice = self.onePrice;
    self.addview.buytype = [_buy_type integerValue];
    if ([_buy_type integerValue] == 1) {
        self.addview.onePriceLael.text = [NSString stringWithFormat:@"%@积分",self.onePrice];
        self.addview.totalPriceLabel.text = [NSString stringWithFormat:@"%@积分",self.onePrice];
    }else{
       
        self.addview.onePriceLael.text = [NSString stringWithFormat:@"￥%@",self.onePrice];
        self.addview.totalPriceLabel.text = [NSString stringWithFormat:@"￥%@",self.onePrice];
        
    }
    
    
  
    self.addview.goodCountLabel.text = [NSString stringWithFormat:@"库存:%@件",self.endGoodNum];
    self.addview.textField.text = [NSString stringWithFormat:@"%ld",(long)goodnum];
    
     __weak typeof(self)weself = self;
    if (endThingNum == 2) {
        [self.addview.buyGoodsBtn setTitle:@"加入进货单" forState:(UIControlStateNormal)];
        self.addview.block = ^(NSInteger number) {
            __strong typeof(weself)self = weself;
            _goodsnum = number;
            [self addCar];
        };
    }else{
        
      
        
        [self.addview.buyGoodsBtn setTitle:@"立即购买" forState:(UIControlStateNormal)];
        self.addview.block = ^(NSInteger number) {
            __strong typeof(weself)self = weself;
            _goodsnum = number;
            [self buyNow];
        };
    }
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [self.addview calculateAllValue:goodnum];
    });
    self.addview.tfblock = ^(UITextField *textfield) {
        __strong typeof(weself)self = weself;
        textfield.inputAccessoryView = [self addToolbar];
    };
    [self.view addSubview:weself.addview];
    [self.addview makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.offset(0);
        make.width.offset(Screen_Width);
    }];
}
-(void)requestGoodInfoData{
    
    NSString * urlstring = [NSString stringWithFormat:@"%@ctl=deal&id=%@&email=%@&pwd=%@",GYLUrl,_goodsId,EMAIL,USER_PWD];
    [RequestData requestDataOfUrl:urlstring success:^(NSDictionary *dic) {
        if (![dic[@"status"] isEqual:@1]) {
            _oneScrollView.commentView.hidden = YES;
            _oneScrollView.guessView.hidden = YES;
            [MBProgressHUD showError:dic[@"info"] toView:self.view];
            return ;
        }
        _endView.goodsId = dic[@"id"];
        //fenxiang
        _shareTitile = dic[@"name"];
        _shareSubTitle = dic[@"sub_name"];
        _shareIcon = dic[@"icon"];
        _shareUrl = dic[@"share_url"];
        
        _icon = dic[@"icon"];
        _buy_type = dic[@"buy_type"];
         _endView.max_bought = dic[@"max_bought"];
        _endView.buytype = [dic[@"buy_type"] integerValue];
       
        _oneScrollView.buytype = [dic[@"buy_type"] integerValue];
        _oneScrollView.cyc.imageURLStringsGroup = dic[@"images"];
        _oneScrollView.nameLabel.text = dic[@"name"];
        _oneScrollView.briefLabel.text = dic[@"brief"];
        
        CGFloat h = [self getHeightBytitle:dic[@"brief"]];
        
        _rz_status = [[NSString stringWithFormat:@"%@",dic[@"rz_status"]]integerValue];
        [kUserDefaults setObject:[NSString stringWithFormat:@"%ld",(long)_rz_status] forKey:@"rz_status"];
        
      
        /**---------**/
        if ([_buy_type integerValue] == 1) {
            _onePrice = [NSString stringWithFormat:@"%@",dic[@"return_score_show"]];
            _oneScrollView.beforPrice.hidden = true;
            _oneScrollView.afterPrice.hidden = false;
            _oneScrollView.afterPrice.text = [NSString stringWithFormat:@"%@积分",self.onePrice];
//            _oneScrollView.priceScroll.hidden = true;
        }else{
            _oneScrollView.beforPrice.hidden = true;
            _oneScrollView.afterPrice.hidden = true;
            //活动价格
            NSArray * arr = dic[@"price_list"];
            _oneScrollView.priceScroll.priceData = arr;
            _oneScrollView.priceScroll.beforPrice = [NSString stringWithFormat:@"￥%@",dic[@"origin_price"]];
            _oneScrollView.priceScroll.contentSize = CGSizeMake(Screen_Width/3.4*arr.count, 70);
            _oneScrollView.priceScroll.collectionview.frame = CGRectMake(0, 1, Screen_Width/3.4*arr.count, 70);
            [_oneScrollView.priceScroll.collectionview reloadData];
            
            _onePrice = [NSString stringWithFormat:@"%@",dic[@"current_price"]];
//            _oneScrollView.priceScroll.hidden = false;
        }
        
        _endGoodNum = [NSString stringWithFormat:@"%@",dic[@"max_bought"]];
        _oneScrollView.standardLabel.text = [NSString stringWithFormat:@"保质期:%@",dic[@"expiry_date"]];
        _oneScrollView.buyCountLabel.text = [NSString stringWithFormat:@"%@人已购",dic[@"buy_count"]];
        _oneScrollView.stocklabel.text = [NSString stringWithFormat:@"库存%@件", _endGoodNum];
        
        
        //商品优惠类型 1:未结束 2:已结束  3:未开始
        _timeStantus = [NSString stringWithFormat:@"%@",dic[@"time_status"]];
        _beginTime = [NSString stringWithFormat:@"%@",dic[@"begin_time"]];
        _nowTime = [_beginTime intValue] - [dic[@"now_time"]intValue];
        
        /**
         *  group_id  门店类型: 1:加盟店  2:经销
         */
        if ([GROUP_ID isEqualToString:@"2"]) {
            _oneScrollView.priceScroll.hidden = YES;
            //当前状态是经销店且是积分商品
            if ([_buy_type integerValue] == 1) {
                _oneScrollView.afterPrice.text = [NSString stringWithFormat:@"%@积分",dic[@"return_score_show"]];
                _oneScrollView.beforPrice.hidden = true;
                
               
            }else{
                
                //当前状态是经销店且不是积分商品
                _oneScrollView.beforPrice.hidden = false;
                _oneScrollView.afterPrice.hidden = false;
                _oneScrollView.afterPrice.text = [NSString stringWithFormat:@"￥%@",dic[@"current_price"]];
                _oneScrollView.beforPrice.attributedText = [self addLineFromeLable:[NSString stringWithFormat:@"￥%@",dic[@"origin_price"]]];
            }
           
            

            [_oneScrollView.miaoshaView updateConstraints:^(MASConstraintMaker *make) {
                make.left.right.offset(0);
                make.top.equalTo(_oneScrollView.nameLabel.mas_bottom).offset(1);
                make.height.offset(30);
            }];
            [_oneScrollView.standardLabel updateConstraints:^(MASConstraintMaker *make) {
                make.left.offset(10);
                make.top.equalTo(_oneScrollView.nameLabel.mas_bottom).offset(32);
                make.height.offset(30);
            }];
            [_oneScrollView.buyCountLabel updateConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(_oneScrollView.mas_centerX);
                make.top.equalTo(_oneScrollView.nameLabel.mas_bottom).offset(32);
                make.height.offset(30);
            }];
            [_oneScrollView.stocklabel updateConstraints:^(MASConstraintMaker *make) {
                make.right.offset(-10);
                make.top.equalTo(_oneScrollView.nameLabel.mas_bottom).offset(32);
                make.height.offset(30);
            }];
            _oneScrollView.contentView.frame = CGRectMake(0, Screen_Width/375*300+.5,Screen_Width, CGRectGetMaxY(_oneScrollView.briefLabel.frame)+h-40);
            [_oneScrollView updateConstraintsIfNeeded];
            [_oneScrollView layoutIfNeeded];
            
            
        }else{
            
            //当前状态是加盟店且是积分商品
            if ([_buy_type integerValue] == 1) {
                _oneScrollView.miaoshaView.hidden = false;
                
                [_oneScrollView.miaoshaView updateConstraints:^(MASConstraintMaker *make) {
                    make.left.right.offset(0);
                    make.top.equalTo(_oneScrollView.nameLabel.mas_bottom).offset(1);
                    make.height.offset(30);
                }];
                [_oneScrollView.standardLabel updateConstraints:^(MASConstraintMaker *make) {
                    make.left.offset(10);
                    make.top.equalTo(_oneScrollView.nameLabel.mas_bottom).offset(32);
                    make.height.offset(30);
                }];
                [_oneScrollView.buyCountLabel updateConstraints:^(MASConstraintMaker *make) {
                    make.centerX.mas_equalTo(_oneScrollView.mas_centerX);
                    make.top.equalTo(_oneScrollView.nameLabel.mas_bottom).offset(32);
                    make.height.offset(30);
                }];
                [_oneScrollView.stocklabel updateConstraints:^(MASConstraintMaker *make) {
                    make.right.offset(-10);
                    make.top.equalTo(_oneScrollView.nameLabel.mas_bottom).offset(32);
                    make.height.offset(30);
                }];
                
               
                
                [_oneScrollView updateConstraintsIfNeeded];
                [_oneScrollView layoutIfNeeded];
                
                
            }else{
                //当前状态是加盟店且不是积分商品
                _oneScrollView.miaoshaView.hidden = true;
                _oneScrollView.priceScroll.hidden = false;
               
            }
            
           
            
            _oneScrollView.contentView.frame = CGRectMake(0, Screen_Width/375*300+.5,Screen_Width, CGRectGetMaxY(_oneScrollView.briefLabel.frame)+h);
        }
        
        //门店
        if (![dic[@"supplier_info"][@"name"] isEqual:[NSNull null]]) {
            _oneScrollView.storenameLabel.text = dic[@"supplier_info"][@"name"];
            _oneScrollView.addressLabel.text = dic[@"supplier_info"][@"address"];
            self.phoneNum = dic[@"supplier_info"][@"tel"];
            _endView.phoneNum = self.phoneNum;
            _oneScrollView.storeView.frame = CGRectMake(0, CGRectGetMaxY(_oneScrollView.contentView.frame)+10, Screen_Width, 80);
        }else{
            _oneScrollView.storeView.frame = CGRectMake(0, CGRectGetMaxY(_oneScrollView.contentView.frame), Screen_Width, 0.1);
            _oneScrollView.storeView.hidden = YES;
        }
        
        
        //评价
        NSArray * evaArr = dic[@"dp_list"];
        if (evaArr.count>0) {
            for (NSDictionary * subdic in dic[@"dp_list"]) {
                CommentModel * model = [[CommentModel alloc]init];
                [model setValuesForKeysWithDictionary:subdic];
                [_commentMuArray addObject:model];
            }
            CGFloat commentViewHight = 0;
            for (int i = 0; i<_commentMuArray.count; i++) {
                commentViewHight += [self getEvaItemHeight:i];
            }
            
            _oneScrollView.commentView.frame = CGRectMake(0, CGRectGetMaxY(_oneScrollView.storeView.frame)+10, Screen_Width, commentViewHight+90);
            _oneScrollView.commentDataArr = _commentMuArray;
        }else{
            _oneScrollView.commentView.frame = CGRectMake(0, CGRectGetMaxY(_oneScrollView.storeView.frame)+10, Screen_Width, 210);
        }
        
        
        //猜喜欢
        if ([dic[@"guess_goods"] isKindOfClass:[NSDictionary class]]) {
            for (NSDictionary * subdic in dic[@"guess_goods"]) {
                GuessModel * model = [[GuessModel alloc]init];
                [model setValuesForKeysWithDictionary:subdic];
                [_guessMuArray addObject:model];
            }
            _oneScrollView.guessView.guessData = _guessMuArray;
            _oneScrollView.guessView.frame = CGRectMake(0, CGRectGetMaxY(_oneScrollView.commentView.frame)+10, Screen_Width, ((Screen_Width-5*4)/3+60)*2+60);
        }else{
            _oneScrollView.guessView.frame = CGRectMake(0, CGRectGetMaxY(_oneScrollView.commentView.frame)+10, Screen_Width,0.1);
            _oneScrollView.guessView.hidden = YES;
        }
        
        //收藏view
        NSString * iscollect = [NSString stringWithFormat:@"%@",dic[@"is_collect"]];
        NSLog(@"%@",iscollect);
        if([dic[@"is_collect"] isEqual:@1]){
            [_endView.collectionBtn setImage:[UIImage imageNamed:@"button_收藏_高亮"] forState:(UIControlStateNormal)];
            
        }
        
        //提醒label
        _oneScrollView.dispalyLabel.frame = CGRectMake(0, CGRectGetMaxY(_oneScrollView.guessView.frame), Screen_Width, 40);
        _oneScrollView.contentSize = CGSizeMake(Screen_Width, CGRectGetMaxY(_oneScrollView.dispalyLabel.frame));
        
        
//         self.webview.frame = CGRectMake(0, CGRectGetMaxY(_oneScrollView.frame)+10, Screen_Width, Screen_Height-Screen_NavBarHeight-50);
        
//        twoscrollview
//        self.twoscrollView.frame = CGRectMake(0, CGRectGetMaxY(_oneScrollView.dispalyLabel.frame), Screen_Width, Screen_Height-64-40);
        NSString * htmlstr = dic[@"description"];
        [self.webview loadHTMLString:[NSString stringWithFormat:@"<html><head><meta content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no;\" name=\"viewport\"><style>img{max-width: 100%%}</style></head><body>%@</body></html>",htmlstr] baseURL:nil];
       
        
        
    } failure:^(NSError *error) {
        
    }];
}


-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (scrollView == self.webview.scrollView) {
        if (decelerate) {
            CGFloat offset_y = scrollView.contentOffset.y;
            if (offset_y<-50) {
                [UIView animateWithDuration:0.5 animations:^{
                    self.parentView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, 0);
                }];
            }
        }
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    CGFloat webViewHeight = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"] floatValue];
    
    NSLog(@"web加载完成%f",webViewHeight);
//    self.webview.frame = CGRectMake(0, CGRectGetMaxY(_oneScrollView.dispalyLabel.frame), Screen_Width, webViewHeight);
    
}

//加入购物车
-(void)addCar{
    NSString * addShoppingCarUrl = [NSString stringWithFormat:@"%@ctl=cart&act=addcart&id=%@&number=%ld&email=%@&pwd=%@",GYLUrl,_goodsId,(long)_goodsnum,EMAIL,USER_PWD];
    [RequestData requestDataOfUrl:addShoppingCarUrl success:^(NSDictionary *dic) {
        
        if([dic[@"status"] isEqual:@1]){
            if (_addview) {
                [_addview removeFromSuperview];
            }
            [MBProgressHUD showSuccess:@"加入进货单成功" toView:self.view];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshCart" object:nil];
        }else{
            [MBProgressHUD showError:dic[@"info"] toView:self.view];
        }
        
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
//立即购买
-(void)buyNow{
    NSString * buyNowUrl = [NSString stringWithFormat:@"%@ctl=cart&act=buynow&buynow_id=%@&buynow_number=%ld&email=%@&pwd=%@&buy_type=%@",GYLUrl,_goodsId,(long)_goodsnum,EMAIL,USER_PWD,_buy_type];
    [RequestData requestDataOfUrl:buyNowUrl success:^(NSDictionary *dic) {
        NSLog(@"%@",dic);
        NSString * str = [NSString stringWithFormat:@"%@",dic[@"status"]];
        if ([str isEqualToString:@"0"]) {
            [MBProgressHUD showError:dic[@"info"] toView:self.view];
        }else{
            ConfirmOrderVC *vc=[[ConfirmOrderVC alloc] init];
            vc.dic =dic;
            vc.isbuynow = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
}

-(NSMutableAttributedString *)addLineFromeLable:(NSString *)str{
    if (@available(iOS 11.0, *)){
        //中划线
        NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
        NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:str attributes:attribtDic];
        
        return attribtStr;
    }else{
        NSMutableAttributedString *showAttrStr = [[NSMutableAttributedString alloc] initWithString:str];
        NSRange range3 = [str rangeOfString:str options:NSBackwardsSearch];
        NSDictionary *attr = @{
                               NSFontAttributeName               : [UIFont systemFontOfSize:12],
                               NSStrikethroughStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid),
                               NSBaselineOffsetAttributeName     : @(NSUnderlineStyleSingle),
                               NSStrikethroughColorAttributeName : APPThreeColor};
        [showAttrStr addAttributes:attr range:range3];
        return showAttrStr;
    }
    
}

//计算字体
-(CGSize)sizeWithText:(NSString *)text font:(UIFont *)font maxSize:(CGSize)maxSize
{
    NSDictionary *attrs = @{NSFontAttributeName : font};
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
}


-(OneScrollView *)oneScrollView{
    if (!_oneScrollView) {
        _oneScrollView = [[OneScrollView alloc]init];
        _oneScrollView.contentSize = CGSizeMake(Screen_Width, Screen_Height*2);
        _oneScrollView.backgroundColor = APPBGColor;
        _countDown=[[CountDown alloc]init];
    }
    return _oneScrollView;
}


//-(TwoScrollView *)twoscrollView{
//    if (!_twoscrollView) {
//        _twoscrollView = [[TwoScrollView alloc]initWithFrame:CGRectMake(0, Screen_Height, Screen_Width, Screen_Height-Screen_NavBarHeight-50)];
//        _twoscrollView.contentSize = CGSizeMake(Screen_Width, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight);
////        _twoscrollView.scrollEnabled = true;
//        _twoscrollView.backgroundColor = APPBGColor;
//    }
//    return _twoscrollView;
//}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSLog(@"%lf",scrollView.contentOffset.y);
}



#pragma mark bar
-(void)shareItem{
    //    分享
    [UMUtil shareUM:self delegate:self title:_shareTitile body:_shareSubTitle url:_shareUrl img:nil urlImg:_shareUrl];
}

#pragma mark onclick
//计算label高度
- (CGFloat)getHeightBytitle:(NSString *)title
{
    CGRect r = [title boundingRectWithSize:CGSizeMake(Screen_Width-40,10000) options:(NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]} context:nil];
    
    CGFloat height = r.size.height;
    return height;
}

//此方法用两个时间戳做参数进行倒计时
-(void)startLongLongStartStamp:(int)strtLL withBeginTime:(int)beginTime{
    [self.countDown countDownWithStratTimeStamp:strtLL  completeBlock:^(NSInteger day, NSInteger hour, NSInteger minute, NSInteger second) {
        [self refreshUIDay:day hour:hour minute:minute second:second];
    }];
}
#pragma mark 倒计时方法
-(void)refreshUIDay:(NSInteger)day hour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second{
    if ([_timeStantus isEqualToString:@"1"]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            _oneScrollView.timerLabel.text=[NSString stringWithFormat:@"%li天%li时%li分%li秒后结束",(long)day,(long)hour,(long)minute,(long)second];
        });
    }
    if ([_timeStantus isEqualToString:@"2"]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            _oneScrollView.timerLabel.text = @"活动已结束";
        });
    }
    if ([_timeStantus isEqualToString:@"3"]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            _oneScrollView.timerLabel.text=[NSString stringWithFormat:@"%li天%li时%li分%li秒后开始",(long)day,(long)hour,(long)minute,(long)second];
        });
    }
}

//获取评价 item 高度
-(CGFloat)getEvaItemHeight:(int)i{
    CommentModel *entity=[self.commentMuArray objectAtIndex:i];
    return Default_Space+40+Default_Space +
    [entity.content textSize:[UIFont systemFontOfSize:FONT_SIZE_M] withWidth:Screen_Width-Default_Space*2].height+
    Default_Space+
    [entity.create_time textSize:[UIFont systemFontOfSize:FONT_SIZE_M] withWidth:Screen_Width-Default_Space*2].height+
    Default_Space;
}

- (UIToolbar *)addToolbar
{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 40)];
    toolbar.tintColor = [UIColor blueColor];
    toolbar.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *bar = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(textFieldDone)];
    bar.tintColor = APPColor;
    toolbar.items = @[ space, bar];
    return toolbar;
}
-(void)textFieldDone{
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    NSLog(@"商品详情销毁");
}
@end
