//
//  ConnactView.h
//  519
//
//  Created by Macmini on 2019/4/10.
//  Copyright © 2019 519. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ConnactView : UIView
@property (nonatomic,strong)NSDictionary *dic;
- (void)setupData:(NSDictionary *)dic;
@end

NS_ASSUME_NONNULL_END
