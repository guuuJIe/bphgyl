//
//  OneScrollView.h
//  gylPT
//
//  Created by Macmini on 2017/9/4.
//  Copyright © 2017年 wzBPH. All rights reserved.
//

typedef void(^oneScrollviewBlock) (void);
typedef void(^oneScrollThingBlock)(NSInteger thingNum);
#import <UIKit/UIKit.h>
#import "PriceScrollView.h"
#import "CommentAndGuessView.h"
@interface OneScrollView : UIScrollView

@property (nonatomic,copy)oneScrollviewBlock block;
@property (nonatomic,copy)oneScrollThingBlock thingBlock;

@property (nonatomic,strong)UIView * runImgView;
@property (nonatomic,strong)UIView * contentView;
@property (nonatomic,strong)UILabel * dispalyLabel;
@property (nonatomic,strong)SDCycleScrollView * cyc;
@property (nonatomic,strong)UILabel * nameLabel;
@property (nonatomic,strong)PriceScrollView * priceScroll;
@property (nonatomic,strong)UILabel * standardLabel;//规格
@property (nonatomic,strong)UILabel * buyCountLabel;
@property (nonatomic,strong)UILabel * stocklabel;
@property (nonatomic,strong)UILabel * briefLabel;
@property (nonatomic,strong)UILabel * miaoshaLabel;

@property (nonatomic,strong)UIView * miaoshaView;
@property (nonatomic,strong)UILabel * beforPrice;
@property (nonatomic,strong)UILabel * afterPrice;
@property (nonatomic,strong)UILabel * timerLabel;

@property (nonatomic,strong)UIView * storeView;
@property (nonatomic,strong)UILabel * storenameLabel;
@property (nonatomic,strong)UILabel * addressLabel;

@property (nonatomic,strong)CommentAndGuessView * commentView;
@property (nonatomic,strong)CommentAndGuessView * guessView;

@property (nonatomic,strong)NSArray * commentDataArr;

@property (nonatomic,assign)NSInteger buytype;
@end
