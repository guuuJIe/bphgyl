//
//  CommentAndGuessView.h
//  519
//
//  Created by Macmini on 2017/12/7.
//  Copyright © 2017年 519. All rights reserved.
//

typedef void(^MoreCommentBlock)(void);
#import <UIKit/UIKit.h>

@interface CommentAndGuessView : UIView
@property (nonatomic,copy)MoreCommentBlock block;
@property(nonatomic,strong)NSArray * commentArray;
@property(nonatomic,strong)UITableView * commentTableView;
-(instancetype)initCommentViewWithFrame:(CGRect)frame;

@property(nonatomic,strong)NSArray * guessData;
-(instancetype)initGuessViewWithFrame:(CGRect)frame;
@end
