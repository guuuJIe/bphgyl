//
//  AddCarView.m
//  519
//
//  Created by Macmini on 2017/12/4.
//  Copyright © 2017年 519. All rights reserved.
//

#import "AddCarView.h"
@interface AddCarView()<UITextFieldDelegate>
@property(nonatomic,strong)UIView * whiteView;

@end


@implementation AddCarView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.5];
        _number = 1;
        [self configUI];
    }
    return self;
}

-(void)configUI{
    _whiteView = [[UIView alloc]init];
    _whiteView.backgroundColor = [UIColor whiteColor];
    _whiteView.userInteractionEnabled = YES;
    [self addSubview:_whiteView];
    
    [_whiteView makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.mas_equalTo(self).with.mas_offset(0);
        make.bottom.mas_equalTo(self.mas_bottom).with.mas_offset(0);
        make.height.equalTo(250);
    }];
    
    _iconImageView = [[UIImageView alloc]init];
    [_whiteView addSubview:_iconImageView];

    [_iconImageView makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_whiteView.mas_left).with.mas_offset(10);
        make.top.mas_equalTo(_whiteView.mas_top).with.mas_offset(-20);
        make.width.and.height.equalTo(80);
    }];
    
    _nameLabel = [[UILabel alloc]init];
    _nameLabel.text = @"";
    _nameLabel.textColor = APPFourColor;
    _nameLabel.textAlignment = NSTextAlignmentLeft;
    _nameLabel.font =[UIFont systemFontOfSize:15];
    _nameLabel.numberOfLines = 0;
    [_whiteView addSubview:_nameLabel];
    [_nameLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_iconImageView.mas_right).with.mas_offset(10);
        make.top.mas_equalTo(_whiteView.mas_top).with.mas_offset(5);
        make.height.equalTo(30);
        make.width.equalTo(Screen_Width-120);
    }];
    
    _onePriceLael = [[UILabel alloc]init];
    _onePriceLael.textColor = APPColor;
    _onePriceLael.text = @"100.11";
    _onePriceLael.textAlignment = NSTextAlignmentLeft;
    _onePriceLael.font = [UIFont systemFontOfSize:14];
    [_whiteView addSubview:_onePriceLael];
    [_onePriceLael makeConstraints:^(MASConstraintMaker *make) {
       make.left.mas_equalTo(_iconImageView.mas_right).with.mas_offset(10);
        make.top.mas_equalTo(_nameLabel.mas_bottom).with.mas_offset(0);
        make.height.equalTo(20);
    }];
    
    _goodCountLabel = [[UILabel alloc]init];
    _goodCountLabel.textColor = APPThreeColor;
    _goodCountLabel.text = @"100.11";
    _goodCountLabel.textAlignment = NSTextAlignmentLeft;
    _goodCountLabel.font = [UIFont systemFontOfSize:12];
    [_whiteView addSubview:_goodCountLabel];
    [_goodCountLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_iconImageView.mas_right).with.mas_offset(10);
        make.top.mas_equalTo(_onePriceLael.mas_bottom).with.mas_offset(0);
        make.height.equalTo(15);
    }];
    
    UIButton * backBtn = [[UIButton alloc]init];
    [backBtn setImage:[UIImage imageNamed:@"button_退出"] forState:(UIControlStateNormal)];
    [backBtn addTarget:self action:@selector(clickBackBtn) forControlEvents:(UIControlEventTouchUpInside)];
    [_whiteView addSubview:backBtn];
    [backBtn makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_whiteView.mas_top).with.mas_offset(10);
        make.right.mas_equalTo(_whiteView.mas_right).with.mas_offset(-10);
        make.width.and.height.equalTo(20);
    }];
    
    UIView * lineView = [[UIView alloc]init];
    lineView.backgroundColor = APPBGColor;
    [_whiteView addSubview:lineView];
    [lineView makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.mas_equalTo(self).with.mas_offset(0);
        make.top.mas_equalTo(_iconImageView.mas_bottom).with.mas_offset(10);
        make.height.equalTo(1);
    }];
    
    UILabel * buyLabel = [[UILabel alloc]init];
    buyLabel.text = @"购买数量";
    buyLabel.font = [UIFont systemFontOfSize:14];
    buyLabel.textColor = APPFourColor;
    buyLabel.textAlignment = NSTextAlignmentLeft;
    [_whiteView addSubview:buyLabel];
    [buyLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_whiteView.left).with.mas_offset(10);
        make.top.mas_equalTo(lineView.mas_bottom).with.mas_offset(20);
        make.height.equalTo(30);
    }];
    //+
    UIButton * addBtn = [[UIButton alloc]init];
    [addBtn setImage:[UIImage imageNamed:@"加_高亮"] forState:(UIControlStateNormal)];
    [addBtn addTarget:self action:@selector(clickAddBtn) forControlEvents:(UIControlEventTouchUpInside)];
    [_whiteView addSubview:addBtn];
    [addBtn makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_whiteView.mas_right).with.mas_offset(-10);
        make.centerY.mas_equalTo(buyLabel.mas_centerY);
        make.width.and.height.equalTo(30);
    }];

    _textField = [[UITextField alloc]init];
    _textField.textColor = APPFourColor;
    _textField.text =[NSString stringWithFormat:@"%ld",(long)self.number];
    _textField.textAlignment =NSTextAlignmentCenter;
    _textField.delegate = self;
    _textField.font = [UIFont systemFontOfSize:12];
    _textField.layer.borderWidth = 1;
    _textField.layer.borderColor = APPBGColor.CGColor;
    _textField.keyboardType = UIKeyboardTypeNumberPad;
    [_whiteView addSubview:_textField];
    [_textField makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(addBtn.mas_left).with.mas_offset(1);
        make.centerY.mas_equalTo(addBtn.mas_centerY);
        make.height.offset(30);
        make.width.offset(50);
    }];
    
    
    //-
    UIButton * subBtn = [[UIButton alloc]init];
    [subBtn setImage:[UIImage imageNamed:@"减_高亮"] forState:(UIControlStateNormal)];
    [subBtn addTarget:self action:@selector(clickSubBtn) forControlEvents:(UIControlEventTouchUpInside)];
    [_whiteView addSubview:subBtn];
    
    [subBtn makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_textField.mas_left).with.mas_offset(1);
        make.centerY.mas_equalTo(_textField.mas_centerY);
        make.width.and.height.equalTo(30);
    }];
    
    //合计价格
    UILabel * totalPrice = [[UILabel alloc]init];
    totalPrice.text = @"商品总价";
    totalPrice.textColor = APPFourColor;
    totalPrice.textAlignment = NSTextAlignmentLeft;
    totalPrice.font = [UIFont systemFontOfSize:14];
    [_whiteView addSubview:totalPrice];
    [totalPrice makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_whiteView.mas_left).with.mas_offset(10);
        make.top.mas_equalTo(buyLabel.mas_bottom).with.mas_offset(20);
        make.height.equalTo(30);
    }];
    
    //总价
    _totalPriceLabel = [[UILabel alloc]init];
    _totalPriceLabel.text = @"10000:00";
    _totalPriceLabel.textColor = APPColor;
    _totalPriceLabel.textAlignment = NSTextAlignmentRight;
    _totalPriceLabel.font = [UIFont boldSystemFontOfSize:14];
    [_whiteView addSubview:_totalPriceLabel];
    [_totalPriceLabel makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_whiteView.mas_right).with.mas_offset(-10);
        make.centerY.equalTo(totalPrice.mas_centerY);
        make.height.equalTo(20);
    }];
    
    //提交按钮
    _buyGoodsBtn = [[UIButton alloc]init];
    [_buyGoodsBtn setTitle:_addstring forState:(UIControlStateNormal)];
    [_buyGoodsBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [_buyGoodsBtn setBackgroundColor:APPColor];
    _buyGoodsBtn.layer.cornerRadius = 5;
    _buyGoodsBtn.layer.masksToBounds = YES;
    [_buyGoodsBtn addTarget:self action:@selector(buyGoodsBtn:) forControlEvents:(UIControlEventTouchUpInside)];
    [_whiteView addSubview:_buyGoodsBtn];
    
    [_buyGoodsBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_whiteView.mas_left).with.mas_offset(10);
        make.right.mas_equalTo(_whiteView.mas_right).with.mas_offset(-10);
        make.top.mas_equalTo(totalPrice.mas_bottom).with.mas_offset(20);
        make.height.equalTo(40);
    }];

    
}

-(void)clickSubBtn{
    _number--;
    if (_number < 1) {
        _number = 1;
    }else{
        _textField.text = [NSString stringWithFormat:@"%ld",(long)_number];
        [self updateAllPrice:_number];
    }

}
-(void)clickAddBtn{
    _number++;
//    if (_number > 99) {
//        _number = 99;
//    }else{
        _textField.text = [NSString stringWithFormat:@"%ld",(long)_number];
        [self updateAllPrice:_number];
//    }
    
}

-(void)updateAllPrice:(NSInteger)number{
    NSString * addShoppingCarUrl = [NSString stringWithFormat:@"%@ctl=cart&act=update&id=%@&num=%ld&chk=1&email=%@&pwd=%@",GYLUrl,_Id,(long)number,EMAIL,USER_PWD];
    [MBProgressHUD showHUD];
    [RequestData requestDataOfUrl:addShoppingCarUrl success:^(NSDictionary *dic) {
        if([dic[@"status"] isEqual:@1]){
            dispatch_async(dispatch_get_main_queue(), ^{
               
                if (self.buytype == 1) {
                    float data = [dic[@"return_total_score"] floatValue];
                    _totalPriceLabel.text = [NSString stringWithFormat:@"%.2f积分",fabsf(data)];
                }else{
                   _totalPriceLabel.text = [NSString stringWithFormat:@"￥%@",dic[@"total_price"]];
                }
                
                
                
            });
        }
        [MBProgressHUD dissmiss];
    } failure:^(NSError *error) {
        [MBProgressHUD dissmiss];
    }];
}
-(void)calculateAllValue:(NSInteger)number{
    [self updateAllPrice:number];
}

-(void)buyGoodsBtn:(UIButton *)btn{
    if (self.block) {
        self.block(_number);
    }
}

-(void)clickBackBtn{
    [self removeFromSuperview];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    NSSet *allTouches = [event allTouches];    //返回与当前接收者有关的所有的触摸对象
    UITouch *touch = [allTouches anyObject];   //视图中的所有对象
    CGPoint point = [touch locationInView:self]; //返回触摸点在视图中的当前坐标
    CGFloat y = point.y;

    if (y < CGRectGetMinY(_whiteView.frame)) {
        [UIView animateWithDuration:0.5 animations:^{
            [_textField resignFirstResponder];
            [self removeFromSuperview];
        }];
    }
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [UIView animateWithDuration:0.5 animations:^{
        self.whiteView.frame = CGRectMake(self.whiteView.frame.origin.x, self.whiteView.frame.origin.y-200, self.whiteView.frame.size.width, self.whiteView.frame.size.height);
    }];
    
    if (self.tfblock) {
        self.tfblock(textField);
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSInteger num = [textField.text integerValue];
    if (num<=1) {
        num = 1;
        textField.text = @"1";
    }
//    if (num >=999) {
//        num = 999;
//        textField.text = @"999";
//    }
    [UIView animateWithDuration:0.5 animations:^{
        self.whiteView.frame = CGRectMake(self.whiteView.frame.origin.x, self.whiteView.frame.origin.y+200, self.whiteView.frame.size.width, self.whiteView.frame.size.height);
    }];
    _number = num;
    [self updateAllPrice:num];
}

@end







