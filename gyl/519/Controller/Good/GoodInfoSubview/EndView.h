//
//  EndView.h
//  gylPT
//
//  Created by Macmini on 2017/9/6.
//  Copyright © 2017年 wzBPH. All rights reserved.
//

typedef void(^endViewThingBlock)(NSInteger endThingNum, NSString * things);
typedef void(^kefuBlock)(NSInteger endThingNum, NSString * things);
typedef void(^isNeedVerifyBlock)(void);
typedef void(^exchangeScoreGoods)(void);
#import <UIKit/UIKit.h>

@interface EndView : UIView

@property(nonatomic,copy)NSString * goodsId;
@property(nonatomic,copy)endViewThingBlock block;
@property(nonatomic,copy)kefuBlock connact;
@property(nonatomic,copy)isNeedVerifyBlock verify;
@property(nonatomic,copy)exchangeScoreGoods exchangeGoods;
@property(nonatomic,strong)UIButton * collectionBtn;
@property(nonatomic,strong)UIButton * serviceBtn;
@property(nonatomic,assign)NSInteger buytype;
@property(nonatomic,strong)NSString *phoneNum;
@property(nonatomic,strong)NSString *max_bought;
@end
