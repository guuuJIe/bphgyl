//
//  AddCarView.h
//  519
//
//  Created by Macmini on 2017/12/4.
//  Copyright © 2017年 519. All rights reserved.
//


typedef void(^addGoodsNumberBlock)(NSInteger number);
typedef void(^clickTextFieldBlock)(UITextField * textfield);
#import <UIKit/UIKit.h>

@interface AddCarView : UIView
@property (nonatomic,copy)addGoodsNumberBlock block;
@property (nonatomic,copy)clickTextFieldBlock tfblock;
@property (nonatomic,strong)UIImageView * iconImageView;
@property (nonatomic,strong)UILabel * nameLabel;
@property (nonatomic,strong)UILabel * onePriceLael;
@property (nonatomic,strong)UILabel * goodCountLabel;
@property (nonatomic,assign)NSInteger number;
@property (nonatomic,strong)UILabel * numberLabel;
@property (nonatomic,strong)UILabel * totalPriceLabel;
@property (nonatomic,strong)UITextField * textField;

@property (nonatomic,strong)UIButton * buyGoodsBtn;
@property (nonatomic,copy)NSString * addstring;
@property (nonatomic,copy)NSString * onePrice;
@property (nonatomic,copy)NSString * Id;
@property (nonatomic,assign) NSInteger buytype;
-(void)calculateAllValue:(NSInteger)number;
@end
