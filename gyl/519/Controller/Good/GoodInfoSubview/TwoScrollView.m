//
//  TwoScrollView.m
//  gylPT
//
//  Created by Macmini on 2017/9/4.
//  Copyright © 2017年 wzBPH. All rights reserved.
//

#define WIDTH self.frame.size.width
#define HEIGHT self.frame.size.height
#import "TwoScrollView.h"

@interface TwoScrollView()<UIScrollViewDelegate,UIWebViewDelegate>
@property (nonatomic,strong)UILabel * dispalyLabel;
@property (nonatomic,strong)UIView * contentView;
@property (nonatomic,strong)UILabel * endLabel;

@end
@implementation TwoScrollView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.delegate = self;
        self.showsVerticalScrollIndicator = NO;
        [self addSubview:self.dispalyLabel];
        [self initView];
        [self addSubview:self.endLabel];
    }
    return self;
}


-(void)initView{
    self.webview = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, self.frame.size.height)];
    self.webview.backgroundColor = [UIColor whiteColor];
    self.webview.scrollView.scrollEnabled = true;
    self.webview.delegate = self;
    [self.webview sizeToFit];
    self.webview.scrollView.bounces=NO;
    self.webview.scrollView.showsVerticalScrollIndicator=NO;
    self.webview.scrollView.showsHorizontalScrollIndicator = NO;
    [self addSubview:self.webview];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
 
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y <-50) {
        _dispalyLabel.text = @"释放查看商品";
    }else{
        _dispalyLabel.text = @"下拉查看商品";
    }
}



-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (decelerate) {
        CGFloat offset_y = scrollView.contentOffset.y;
        if (offset_y<-50) {
            self.block();
        }
    }
}

-(UILabel *)dispalyLabel{
    if (!_dispalyLabel) {
        _dispalyLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, -40, WIDTH, 40)];
        _dispalyLabel.textAlignment = NSTextAlignmentCenter;
        _dispalyLabel.text = @"下拉查看商品";
        _dispalyLabel.textColor = APPFourColor;
        _dispalyLabel.font = [UIFont systemFontOfSize:14];
    }
    return _dispalyLabel;
}

-(UIView *)contentView{
    if (!_contentView) {
        _contentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
        _contentView.backgroundColor = [UIColor colorWithRed:arc4random()%256/256.f green:arc4random()%256/256.f blue:arc4random()%256/256.f alpha:1];
        
    }
    return _contentView;
}

-(UILabel *)endLabel{
    if (!_endLabel) {
        _endLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_webview.frame), WIDTH, 40)];
        _endLabel.textAlignment = NSTextAlignmentCenter;
        _endLabel.text = @"已经到底了";
        _endLabel.textColor = APPFourColor;
        _endLabel.font = [UIFont systemFontOfSize:14];
    }
    return _endLabel;
}
@end
