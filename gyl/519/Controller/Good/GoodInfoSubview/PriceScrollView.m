//
//  PriceScrollView.m
//  gylPT
//
//  Created by Macmini on 2017/9/5.
//  Copyright © 2017年 wzBPH. All rights reserved.
//

#import "PriceScrollView.h"

@interface PriceScrollView()<UICollectionViewDelegate,UICollectionViewDataSource>

@end


@implementation PriceScrollView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
        [self initView];
    }
    return self;
}

-(void)initView{

    UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(Screen_Width/3.4, 70) ;
    layout.minimumLineSpacing = 0  ;
    layout.minimumInteritemSpacing = 10;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.collectionview = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 1, CGRectGetMaxX(self.frame), 70) collectionViewLayout:layout];
    [self.collectionview registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"collectionPriceIde"];
    self.collectionview.delegate = self;
    self.collectionview.dataSource = self;
    self.collectionview.scrollEnabled = NO;
    self.collectionview.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.collectionview];
}

- (void)setBuytype:(NSInteger)buytype{
    _buytype = buytype;
//    self.priceScroll.buytype = buytype;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return  1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.priceData.count;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    CGSize itemSize=[self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:indexPath];
    
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collectionPriceIde" forIndexPath:indexPath];
    
    if(cell==nil){
        cell = [[UICollectionViewCell alloc]initWithFrame:CGRectMake(0, 0, itemSize.width, itemSize.height)];
    }
    cell.layer.borderColor = APPBGColor.CGColor;
    cell.layer.borderWidth = 1;
    cell.layer.cornerRadius = 3;
    cell.layer.masksToBounds = YES;
   //现价
    UILabel * priceLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, itemSize.width, 25)];
    priceLabel.textColor = APPColor;
    priceLabel.font = [UIFont fontWithName:@"GillSans-Bold" size:18];
    priceLabel.textAlignment = NSTextAlignmentCenter;
    [cell addSubview:priceLabel];
    
//    //原价
    UILabel * beforLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 30, itemSize.width, 15)];
    beforLabel.attributedText = [self addLineFromeLable:_beforPrice];
    beforLabel.textColor = APPThreeColor;
    beforLabel.textAlignment = NSTextAlignmentCenter;
    beforLabel.font = [UIFont systemFontOfSize:11];
    [cell addSubview:beforLabel];
    
    //件数
    UILabel * countLabel = [[UILabel alloc]initWithFrame:CGRectMake(0,50, itemSize.width, 15)];
    countLabel.textColor = APPFourColor;
    countLabel.textAlignment = NSTextAlignmentCenter;
    countLabel.font = [UIFont systemFontOfSize:13];
    [cell addSubview:countLabel];
    
    
    NSDictionary * arr = self.priceData[indexPath.row];
    NSString * priceStr =[NSString stringWithFormat:@"￥%@",arr[@"price"]];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:priceStr];
    [attrStr addAttribute:NSFontAttributeName
                    value:[UIFont fontWithName:@"GillSans-Bold" size:13]
                    range:NSMakeRange(0, 1)];
    priceLabel.attributedText = attrStr;
    countLabel.text = [NSString stringWithFormat:@"%@件起",arr[@"num"]];
   
    
    return  cell;
}
//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(Screen_Width/3.4-10, 70);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * arr = self.priceData[indexPath.row];
    
    if (self.buytype == 2) {
        
    }else{
        if (self.block) {
            self.block([arr[@"num"] integerValue]);
        }
    }
    
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


+(NSString *)getIde{
    return @"priceide";
}


-(NSMutableAttributedString *)addLineFromeLable:(NSString *)str{
    if (@available(iOS 11.0, *)){
        //中划线
        NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
        NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:str attributes:attribtDic];
        
        return attribtStr;
    }else{
        NSMutableAttributedString *showAttrStr = [[NSMutableAttributedString alloc] initWithString:str];
        NSRange range3 = [str rangeOfString:str options:NSBackwardsSearch];
        NSDictionary *attr = @{
                               NSFontAttributeName               : [UIFont systemFontOfSize:12],
                               NSStrikethroughStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid),
                               NSBaselineOffsetAttributeName     : @(NSUnderlineStyleSingle),
                               NSStrikethroughColorAttributeName : APPThreeColor};
        [showAttrStr addAttributes:attr range:range3];
        return showAttrStr;
    }
    
}

@end
