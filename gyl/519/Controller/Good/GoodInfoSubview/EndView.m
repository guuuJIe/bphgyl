//
//  EndView.m
//  gylPT
//
//  Created by Macmini on 2017/9/6.
//  Copyright © 2017年 wzBPH. All rights reserved.
//
#define WIDTH self.frame.size.width
#define HEIGHT self.frame.size.height

#import "EndView.h"
@interface EndView()
@property (nonatomic,strong)UIButton *carButton;
@property (nonatomic,strong)UIButton *stockView;
@property (nonatomic,strong)UIButton *scoreBtn;
@end

@implementation EndView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self initview];
    }
    return  self;
}

-(void)initview{
    
    _serviceBtn = [[UIButton alloc]init];
    _serviceBtn.backgroundColor = [UIColor whiteColor];
    [_serviceBtn setImage:[UIImage imageNamed:@"icon_service"] forState:(UIControlStateNormal)];
    [_serviceBtn setTitle:@"客服" forState:(UIControlStateNormal)];
    [_serviceBtn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
    _serviceBtn.titleLabel.font = [UIFont systemFontOfSize:9];
    _serviceBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 19, 17, 0);
    _serviceBtn.titleEdgeInsets = UIEdgeInsetsMake(17, 0, 0, 14);
    [_serviceBtn addTarget:self action:@selector(kefu) forControlEvents:(UIControlEventTouchUpInside)];
    _serviceBtn.tag = 2005;
    [self addSubview:_serviceBtn];
    [_serviceBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(0);
        make.top.offset(1);
        make.height.offset(49);
        make.width.offset(Screen_Width/6);
    }];
    
    
    _collectionBtn = [[UIButton alloc]init];
    _collectionBtn.backgroundColor = [UIColor whiteColor];
    [_collectionBtn setImage:[UIImage imageNamed:@"button_收藏_默认"] forState:(UIControlStateNormal)];
    [_collectionBtn setTitle:@"收藏" forState:(UIControlStateNormal)];
    [_collectionBtn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
    _collectionBtn.titleLabel.font = [UIFont systemFontOfSize:9];
    _collectionBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 18, 17, 0);
    _collectionBtn.titleEdgeInsets = UIEdgeInsetsMake(17, 0, 0, 14);
    [_collectionBtn addTarget:self action:@selector(collectionClick:) forControlEvents:(UIControlEventTouchUpInside)];
    _collectionBtn.tag = 2000;
    [self addSubview:_collectionBtn];
    [_collectionBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_serviceBtn.mas_right).offset(0);
        make.top.offset(1);
        make.width.offset(Screen_Width/6);
        make.height.offset(49);
    }];
    
    
    UIView *carView = [[UIView alloc] init];
    carView.backgroundColor = [UIColor whiteColor];
    [self addSubview:carView];
    [carView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.collectionBtn.mas_right).offset(0);
        make.top.offset(1);
        make.width.offset(Screen_Width/6);
        make.height.offset(49);
    }];
    
    UIImageView *ima = [[UIImageView alloc] init];
    ima.image = [UIImage imageNamed:@"icon_进货单_默认"];
    
    UILabel *lbl = [[UILabel alloc] init];
    lbl.text = @"进货单";
    lbl.font= [UIFont systemFontOfSize:9];
    lbl.textColor = APPFourColor;
    
   
    [carView addSubview:ima];
    [carView addSubview:lbl];
    
    
    [ima mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(carView);
        make.top.mas_equalTo(8);
    }];
    
    [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(carView);
        make.top.mas_equalTo(ima.mas_bottom).offset(3);
    }];
    
//    [carBtn setImage:[UIImage imageNamed:@"icon_进货单_默认"] forState:(UIControlStateNormal)];
//    [carBtn setTitle:@"进货单" forState:(UIControlStateNormal)];
//    [carBtn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
//    carBtn.titleLabel.font = [UIFont systemFontOfSize:9];
//    carBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 15, 17, 0);
//    carBtn.titleEdgeInsets = UIEdgeInsetsMake(17, 0, 0, 14);
//    carBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    UIButton * carBtn = [[UIButton alloc]init];
    carBtn.backgroundColor = [UIColor clearColor];
    [carBtn addTarget:self action:@selector(collectionClick:) forControlEvents:(UIControlEventTouchUpInside)];
    carBtn.tag = 2001;
    [self addSubview:carBtn];
    [carBtn makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(carView);
    }];
    
    self.carButton = carBtn;
    
}

- (void)kefu{
    if (self.connact) {
        self.connact(5, @"");
    }
}

- (void)setBuytype:(NSInteger)buytype{
    //1是积分商品，2是立即询价商品，其它就正常
    if (buytype == 0) {
        UIButton * addCarBtn = [[UIButton alloc]init];
        [addCarBtn setTitle:@"加入进货单" forState:(UIControlStateNormal)];
        [addCarBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        addCarBtn.backgroundColor = [UIColor colorWithHexString:@"0xFFBB55"];
        [addCarBtn addTarget:self action:@selector(collectionClick:) forControlEvents:(UIControlEventTouchUpInside)];
        addCarBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        addCarBtn.tag = 2002;
        [self addSubview:addCarBtn];
        [addCarBtn makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.carButton.mas_right).offset(0);
            make.top.offset(1);
            make.width.offset(Screen_Width/4);
            make.height.offset(49);
        }];
        
        UIButton * nowBuyBtn = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMaxX(addCarBtn.frame), 1, self.width/4, HEIGHT-1)];
        [nowBuyBtn setTitle:@"立即购买" forState:(UIControlStateNormal)];
        [nowBuyBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        nowBuyBtn.backgroundColor = APPColor;
        nowBuyBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [nowBuyBtn addTarget:self action:@selector(collectionClick:) forControlEvents:(UIControlEventTouchUpInside)];
        nowBuyBtn.tag = 2003;
        [self addSubview:nowBuyBtn];
        [nowBuyBtn makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(addCarBtn.mas_right).offset(0);
            make.top.equalTo(1);
            make.width.offset(Screen_Width/4);
            make.height.offset(49);
        }];
        
        if ([self.max_bought integerValue]<=0) {
            self.stockView.hidden = false;
            [self addSubview:self.stockView];
            self.stockView.hidden = false;
//            self.stockView.enabled = false;
            [self.stockView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.top.mas_equalTo(addCarBtn);
                make.right.equalTo(nowBuyBtn);
                make.height.mas_equalTo(nowBuyBtn);
            }];
        }
       
    }else if(buytype == 2){
        UIButton * addCarBtn = [[UIButton alloc]init];
        [addCarBtn setTitle:@"立即询价" forState:(UIControlStateNormal)];
        [addCarBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        addCarBtn.backgroundColor = APPColor;
        [addCarBtn addTarget:self action:@selector(searchPriceClick) forControlEvents:(UIControlEventTouchUpInside)];
        addCarBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        addCarBtn.tag = 2002;
        [self addSubview:addCarBtn];
        [addCarBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.carButton.mas_right).offset(0);
            make.top.offset(1);
            make.width.offset(Screen_Width/2);
            make.height.offset(49);
        }];
        if ([self.max_bought integerValue]<=0) {
             self.stockView.hidden = false;
            [self addSubview:self.stockView];
            self.stockView.hidden = false;
//            self.stockView.enabled = false;
            [self.stockView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.mas_equalTo(addCarBtn);
            }];
        }
    }else if(buytype == 1){
        UIButton * addCarBtn = [[UIButton alloc]init];
        [addCarBtn setTitle:@"立即兑换" forState:(UIControlStateNormal)];
        [addCarBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        addCarBtn.backgroundColor = APPColor;
        [addCarBtn addTarget:self action:@selector(exchangeClick) forControlEvents:(UIControlEventTouchUpInside)];
        addCarBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        addCarBtn.tag = 2003;
        [self addSubview:addCarBtn];
        [addCarBtn makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.carButton.mas_right).offset(0);
            make.top.offset(1);
            make.width.offset(Screen_Width/2);
            make.height.offset(49);
        }];
        
    }
}

//立即兑换
- (void)exchangeClick{
    if (self.exchangeGoods) {
        self.exchangeGoods();
    }
    
}

//暂无库存
- (void)stockLimit{
    
}

- (void)setPhoneNum:(NSString *)phoneNum{
    _phoneNum = phoneNum;
    
}

- (void)searchPriceClick{
    if (self.verify) {
        self.verify();
    }
//    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",self.phoneNum]]];
}

-(void)collectionClick:(UIButton *)btn{
    if (EMAIL) {
        switch (btn.tag) {
            case 2000:
                //收藏
                [self scGoods:btn];
                break;
            case 2001:
                //购物车
                self.block(1, @"");
                break;
            case 2002:
                //加入购物车
                self.block(2, @"");
                break;
            case 2003:
                //立即购买
                self.block(3, @"");
                break;
            case 2005:
                //客服
                self.block(5, @"");
                break;
            default:
                break;
        }
    }else{
        self.block(4, @"未登录");
    }
    
}

-(void)scGoods:(UIButton *)btn{
    
    NSString * email = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    NSString * pwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
    NSString * scUrl = [NSString stringWithFormat:@"%@ctl=deal&act=add_collect&id=%@&email=%@&pwd=%@",GYLUrl,self.goodsId,email,pwd];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [RequestData requestDataOfUrl:scUrl success:^(NSDictionary *dic) {
            [MBProgressHUD dissmiss];
            self.block(0, dic[@"info"]);
            if ([dic[@"status"] isEqual:@1]) {
                if ([dic[@"is_collect"] isEqual:@0]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [btn setImage:[UIImage imageNamed:@"button_收藏_默认"] forState:(UIControlStateNormal)];
                    });
                }else if ([dic[@"is_collect"] isEqual:@1]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [btn setImage:[UIImage imageNamed:@"button_收藏_高亮"] forState:(UIControlStateNormal)];
                    });
                }
            }
        } failure:^(NSError *error) {
            
        }];
    });
}

- (UIButton *)stockView{
    if (!_stockView) {
        _stockView = [[UIButton alloc] init];
        [_stockView setTitle:@"暂无库存" forState:0];
        _stockView.titleLabel.font = [UIFont systemFontOfSize:14];
        [_stockView setBackgroundImage:[UIImage imageWithColor:DisableColor] forState:UIControlStateNormal];
        [_stockView setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        _stockView.hidden = true;
    }
    return _stockView;
}

- (UIButton *)scoreBtn{
    if (!_scoreBtn) {
        _scoreBtn = [[UIButton alloc] init];
        [_scoreBtn setTitle:@"立即兑换" forState:0];
        _scoreBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_scoreBtn setBackgroundImage:[UIImage imageWithColor:APPColor] forState:UIControlStateNormal];
        [_scoreBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        
    }
    return _scoreBtn;
}

@end
