//
//  PriceScrollView.h
//  gylPT
//
//  Created by Macmini on 2017/9/5.
//  Copyright © 2017年 wzBPH. All rights reserved.
//

typedef void(^priceValueBlock)(NSInteger number);
#import <UIKit/UIKit.h>

@interface PriceScrollView : UIScrollView
@property (nonatomic,copy)priceValueBlock block;
@property (nonatomic,strong)NSArray * priceData;
@property (nonatomic,strong)UICollectionView * collectionview;
@property (nonatomic,copy)NSString * beforPrice;
@property (nonatomic,assign)NSInteger buytype;
@end
