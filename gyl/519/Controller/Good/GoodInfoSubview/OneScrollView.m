//
//  OneScrollView.m
//  gylPT
//
//  Created by Macmini on 2017/9/4.
//  Copyright © 2017年 wzBPH. All rights reserved.
//

#define WIDTH self.frame.size.width
#define HEIGHT self.frame.size.height
#define ScreenHeight [UIScreen mainScreen].bounds.size.height
#import "OneScrollView.h"

@interface OneScrollView()<UIScrollViewDelegate,SDCycleScrollViewDelegate>


@end


@implementation OneScrollView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.delegate = self;
        self.showsVerticalScrollIndicator = NO;
        [self addSubview:self.cyc];
        [self.cyc makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.offset(0);
            make.width.offset(Screen_Width);
            make.height.offset(Screen_Width/375*300);
        }];
        [self addSubview:self.contentView];
        [self addSubview:self.storeView];
        [self addSubview:self.commentView];
        [self addSubview:self.guessView];
        [self addSubview:self.dispalyLabel];
        [self initView];
    }
    return self;
}

-(void)initView{
    
    _nameLabel = [[UILabel alloc]init];
    _nameLabel.textAlignment = NSTextAlignmentLeft;
    _nameLabel.font = [UIFont systemFontOfSize:16];
    _nameLabel.textColor = APPFourColor;
    _nameLabel.numberOfLines = 0;
    [_contentView addSubview:_nameLabel];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).with.offset(0);
        make.left.equalTo(self.contentView).with.offset(10);
        make.width.offset(Screen_Width-20);
        make.height.offset(50);
    }];
    
    
//    UIView * firstline = [[UIView alloc]init];
//    firstline.backgroundColor = APPBGColor;
//    [_contentView addSubview:firstline];
//    [firstline mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.offset(0);
//        make.top.equalTo(_nameLabel.mas_bottom).with.offset(0);
//        make.width.offset(Screen_Width);
//        make.height.offset(1);
//    }];
    

    //经销价格
    _miaoshaView = [[UIView alloc]init];
    _miaoshaView.backgroundColor = [UIColor whiteColor];
    [_contentView addSubview:_miaoshaView];
    [_miaoshaView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(0);
        make.top.mas_equalTo(_nameLabel.mas_bottom).with.mas_offset(0);
        make.height.equalTo(60);
    }];
    
    //现价
    _afterPrice = [[UILabel alloc]init];
    _afterPrice.textAlignment = NSTextAlignmentLeft;
    _afterPrice.font = [UIFont fontWithName:@"GillSans-Bold" size:20];
    _afterPrice.textColor = APPColor;
    [_miaoshaView addSubview:_afterPrice];
    [_afterPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(10);
        make.top.equalTo(0);
        make.height.equalTo(30);
    }];
    //原价
    _beforPrice = [[UILabel alloc]init];
    _beforPrice.textAlignment = NSTextAlignmentLeft;
    _beforPrice.font = [UIFont systemFontOfSize:13];
    _beforPrice.textColor = APPThreeColor;
    [_miaoshaView addSubview:_beforPrice];
    [_beforPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_afterPrice.mas_right).with.offset(5);
        make.top.equalTo(2);
        make.height.equalTo(30);
    }];
    

    //加盟价格
    _priceScroll = [[PriceScrollView alloc]init];
    _priceScroll.contentSize = CGSizeMake(WIDTH*1.2, 70) ;
//    _priceScroll.buytype = self.buytype;
    [_contentView addSubview:_priceScroll];
    [_priceScroll mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(10);
        make.right.offset(-10);
        make.top.mas_equalTo(_nameLabel.mas_bottom).with.mas_offset(0);
        make.height.equalTo(71);
    }];

    
    
    
    
//    _timerLabel = [UILabel new];
//    _timerLabel.textAlignment = NSTextAlignmentLeft;
//    _timerLabel.font = [UIFont systemFontOfSize:13];
//    _timerLabel.textColor = APPThreeColor;
//    [_miaoshaView addSubview:_timerLabel];
//    [_timerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(10);
//        make.right.equalTo(-10);
//        make.top.mas_equalTo(_afterPrice.mas_bottom).with.offset(0);
//        make.height.equalTo(30);
//    }];
    
    _standardLabel = [[UILabel alloc]init];
    _standardLabel.text = @"规格：";
    _standardLabel.textAlignment = NSTextAlignmentLeft;
    _standardLabel.font = [UIFont systemFontOfSize:13];
    _standardLabel.textColor = APPThreeColor;
    [_contentView addSubview:_standardLabel];
    [_standardLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(10);
        make.top.mas_equalTo(_nameLabel.mas_bottom).with.offset(71);
        make.height.equalTo(30);
    }];
    
    _buyCountLabel = [[UILabel alloc]init];
    _buyCountLabel.textAlignment = NSTextAlignmentCenter;
    _buyCountLabel.font = [UIFont systemFontOfSize:13];
    _buyCountLabel.textColor = APPThreeColor;
    [_contentView addSubview:_buyCountLabel];
    [_buyCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(10);
        make.left.equalTo(_standardLabel.mas_right).offset(5);
        make.top.mas_equalTo(_nameLabel.mas_bottom).with.offset(71);
        make.height.equalTo(30);
    }];
    
    _stocklabel = [[UILabel alloc]init];
    _stocklabel.textAlignment = NSTextAlignmentRight;
    _stocklabel.font = [UIFont systemFontOfSize:13];
    _stocklabel.textColor = APPThreeColor;
    [_contentView addSubview:_stocklabel];
    [_stocklabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-10);
        make.top.mas_equalTo(_nameLabel.mas_bottom).with.offset(71);
        make.height.equalTo(30);
    }];

    
    
    UIView * lineview =[[ UIView alloc]init];
    lineview.backgroundColor = APPBGColor;
    [_contentView addSubview:lineview];
    [lineview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(0);
        make.top.mas_equalTo(self.buyCountLabel.mas_bottom).with.offset(0);
        make.height.equalTo(1);
    }];
    
    _briefLabel = [[UILabel alloc]init];
    _briefLabel.textAlignment = NSTextAlignmentLeft;
    _briefLabel.font = [UIFont systemFontOfSize:13];
    _briefLabel.numberOfLines = 0;
    _briefLabel.textColor = APPThreeColor;
    [_contentView addSubview:_briefLabel];
    [_briefLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(10);
        make.top.mas_equalTo(lineview.mas_bottom).with.offset(5);
        make.right.equalTo(-10);
    }];
    
    
   /**
    *  门店信息
    */
    _storenameLabel = [[UILabel alloc]init];
    _storenameLabel.textAlignment = NSTextAlignmentLeft;
    _storenameLabel.font = [UIFont systemFontOfSize:16];
    _storenameLabel.textColor = APPFourColor;
    [_storeView addSubview:_storenameLabel];
    
    _addressLabel = [[UILabel alloc]init];
    _addressLabel.textAlignment = NSTextAlignmentLeft;
    _addressLabel.font = [UIFont systemFontOfSize:13];
    _addressLabel.textColor = APPThreeColor;
    [_storeView addSubview:_addressLabel];
    
    UIButton * phone  = [[UIButton alloc]init];
    [phone setBackgroundImage:[UIImage imageNamed:@"商家热线"] forState:(UIControlStateNormal)];
    [phone addTarget:self action:@selector(callPhone) forControlEvents:(UIControlEventTouchUpInside)];
    [_storeView addSubview:phone];

    [_storenameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(10);
        make.left.equalTo(10);
        make.height.equalTo(30);
    }];
    [_addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_storenameLabel.mas_bottom).with.offset(0);
        make.left.equalTo(10);
        make.height.equalTo(30);
    }];
    [phone mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_storeView.mas_centerY);
        make.right.offset(-10);
        make.width.height.offset(40);
    }];
}



-(void)callPhone{
    if (self.thingBlock) {
        self.thingBlock(0);
    }
}

- (void)setBuytype:(NSInteger)buytype{
    _buytype = buytype;
    self.priceScroll.buytype = buytype;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (decelerate) {
        CGFloat offset_y = scrollView.contentOffset.y;
        CGFloat onescrollOffset = (self.contentSize.height - HEIGHT)+40;
        if (offset_y>onescrollOffset) {
            if (self.block) {
                self.block();
            }
        }
    }
}


-(void)setCommentDataArr:(NSArray *)commentDataArr{
    _commentDataArr = commentDataArr;
    _commentView.commentArray = commentDataArr;
}

-(UIView *)contentView{
    if (!_contentView) {
        _contentView = [[UIView alloc]initWithFrame:CGRectMake(0, WIDTH/375*300+1, WIDTH, 380)];
        _contentView.backgroundColor = [UIColor whiteColor];
    }
    return _contentView;
}

-(UILabel *)dispalyLabel{
    if (!_dispalyLabel) {
        _dispalyLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_commentView.frame), WIDTH, 40)];
        _dispalyLabel.text = @"上拉查看详情";
        _dispalyLabel.textColor = APPFourColor;
        _dispalyLabel.textAlignment = NSTextAlignmentCenter;
        _dispalyLabel.font = [UIFont boldSystemFontOfSize:13];
    }
    return _dispalyLabel;
}

-(SDCycleScrollView *)cyc{
    if (!_cyc) {
        _cyc = [[SDCycleScrollView alloc]init];
        _cyc.delegate = self;
        _cyc.backgroundColor = [UIColor whiteColor];
        _cyc.autoScrollTimeInterval = 5;
        _cyc.autoScroll = YES;
        _cyc.infiniteLoop = YES;
        
    }
    return _cyc;
}

-(UIView *)storeView{
    if (!_storeView) {
        _storeView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_contentView.frame)+10, WIDTH, 80)];
        _storeView.backgroundColor = [UIColor whiteColor];
    }
    return _storeView;
}

-(CommentAndGuessView*)commentView{
    if (!_commentView) {
        _commentView = [[CommentAndGuessView alloc]initCommentViewWithFrame:CGRectMake(0, CGRectGetMaxY(_storeView.frame)+10, self.width, 210)];
        _commentView.backgroundColor = [UIColor whiteColor];
    }
    return _commentView;
}

-(CommentAndGuessView *)guessView{
    if (!_guessView) {
        _guessView = [[CommentAndGuessView alloc]initGuessViewWithFrame:CGRectMake(0, CGRectGetMaxY(_commentView.frame)+20, self.width, ((self.width-5*4)/3+60)*2+60)];
        _guessView.backgroundColor = [UIColor whiteColor];
    }
    return _guessView;
}
@end
