//
//  CommentAndGuessView.m
//  519
//
//  Created by Macmini on 2017/12/7.
//  Copyright © 2017年 519. All rights reserved.
//

#import "CommentAndGuessView.h"
#import "GoodListCC.h"
#import "GoodInfoEvaTC.h"
@interface CommentAndGuessView()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource>
{
    CGFloat _collectionWidth;
    CGFloat _collectionHeight;
}
@property(nonatomic,strong)UIView * neverCommentView;
@property(nonatomic,strong)UIView * commentView;
@property(nonatomic,strong)UICollectionView * collectionView;
@end
@implementation CommentAndGuessView


/**
 *  评论
 */
-(instancetype)initCommentViewWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self commentViewUI];
    }
    return self;
}
-(void)commentViewUI{
    UILabel * commentTitle = [[UILabel alloc]init];
    commentTitle.text = @"用户评价";
    commentTitle.textColor = APPFourColor;
    commentTitle.textAlignment = NSTextAlignmentLeft;
    commentTitle.font = [UIFont systemFontOfSize:14];
    commentTitle.backgroundColor = [UIColor whiteColor];
    [self addSubview:commentTitle];
    [commentTitle makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(10);
        make.top.offset(0);
        make.height.offset(40);
    }];
    
    UIView * lineView = [[UIView alloc]init];
    lineView.backgroundColor = APPBGColor;
    [self addSubview:lineView];
    [lineView makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(0);
        make.right.offset(0);
        make.top.equalTo(commentTitle.mas_bottom).offset(0);
        make.height.offset(1);
    }];
    //没有评论
   self.neverCommentView = [[UIView alloc]init];
    self.neverCommentView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.neverCommentView];
    [self.neverCommentView makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineView.mas_bottom).offset(0);
        make.left.right.offset(0);
        make.height.offset(Screen_Width/375*170);
    }];
    
    UIImageView * neverImageView = [[UIImageView alloc]init];
    neverImageView.image = [UIImage imageNamed:@"icon_暂无评价"];
    [self.neverCommentView addSubview:neverImageView];
    [neverImageView makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.neverCommentView.mas_centerX);
        make.centerY.equalTo(self.neverCommentView.mas_centerY);
        make.width.height.offset(60);
    }];
    
    UILabel * neverTitle = [[UILabel alloc]init];
    neverTitle.textAlignment = NSTextAlignmentCenter;
    neverTitle.textColor = APPThreeColor;
    neverTitle.text = @"还没有人评论";
    neverTitle.font = [UIFont systemFontOfSize:13];
    [self.neverCommentView addSubview:neverTitle];
    [neverTitle makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.neverCommentView.mas_centerX);
        make.top.equalTo(neverImageView.mas_bottom).offset(20);
        make.height.offset(15);
    }];
    
//    self.commentView.hidden = YES;
}

-(void)setCommentArray:(NSArray *)commentArray{
    _commentArray = commentArray;
    if (commentArray.count>0) {
        
        self.neverCommentView.hidden = YES;
        //评论
        self.commentView = [[UIView alloc]initWithFrame:CGRectMake(0, 41, self.width, self.height-50)];
        self.commentView.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.commentView];
        
        [self.commentView addSubview:self.commentTableView];
        UIButton * moreBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.width/3, 30)];
        moreBtn.center = CGPointMake(self.width/2, CGRectGetMaxY(self.commentTableView.frame)+20);
        [moreBtn setTitle:@"更多评论 >>" forState:(UIControlStateNormal)];
        moreBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [moreBtn setTitleColor:APPColor forState:(UIControlStateNormal)];
        [moreBtn addTarget:self action:@selector(moreBtn) forControlEvents:(UIControlEventTouchUpInside)];
        moreBtn.layer.cornerRadius = 5;
        moreBtn.layer.masksToBounds = YES;
        moreBtn.layer.borderWidth = 1;
        moreBtn.layer.borderColor = APPColor.CGColor;
        [self.commentView addSubview:moreBtn];
        
        [_commentTableView reloadData];
    }
}
-(void)moreBtn{
    if(self.block){
        self.block();
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _commentArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CommentModel *entity=[self.commentArray objectAtIndex:indexPath.row];
    GoodInfoEvaTC *cell=[tableView dequeueReusableCellWithIdentifier:[GoodInfoEvaTC getID] forIndexPath:indexPath];
    if(cell==nil){
        cell=[[GoodInfoEvaTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[GoodInfoEvaTC getID]];
    }
    [cell showData:entity];
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self getEvaItemHeight:(int)indexPath.row];
}


-(UITableView *)commentTableView{
    if (!_commentTableView) {
        _commentTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.width, self.height-90) style:(UITableViewStylePlain)];
        _commentTableView.delegate = self;
        _commentTableView.dataSource = self;
        _commentTableView.showsVerticalScrollIndicator = NO;
        _commentTableView.scrollEnabled = NO;
        _commentTableView.showsHorizontalScrollIndicator = NO;
        _commentTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_commentTableView registerClass:[GoodInfoEvaTC class] forCellReuseIdentifier:[GoodInfoEvaTC getID]];
    }
    return _commentTableView;
}
//获取评价 item 高度
-(CGFloat)getEvaItemHeight:(int)i{
    CommentModel *entity=[self.commentArray objectAtIndex:i];
    return Default_Space+40+Default_Space+[entity.content textSize:[UIFont systemFontOfSize:FONT_SIZE_M] withWidth:Screen_Width-Default_Space*2].height+Default_Space+[entity.create_time textSize:[UIFont systemFontOfSize:FONT_SIZE_M] withWidth:Screen_Width-Default_Space*2].height+Default_Space;
}

/**
 *  猜你喜欢
 */
-(instancetype)initGuessViewWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        [self createGuessViewUI];
    }
    return self;
}

-(void)createGuessViewUI{
    UILabel * guessLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 39)];
    guessLabel.text = @"猜你喜欢";
    guessLabel.textColor = APPFourColor;
    guessLabel.backgroundColor = [UIColor whiteColor];
    guessLabel.font = [UIFont boldSystemFontOfSize:14];
    [self addSubview:guessLabel];
    
    _collectionWidth=(self.width-5*4)/3;
    _collectionHeight=_collectionWidth+60;
    UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(_collectionWidth,_collectionHeight);
    layout.minimumInteritemSpacing =0;
    layout.minimumLineSpacing = 5;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0,40,Screen_Width,400) collectionViewLayout:layout];
    self.collectionView.backgroundColor=[UIColor whiteColor];
    [self.collectionView registerClass:[GoodListCC class] forCellWithReuseIdentifier:[GoodListCC getID]];
    self.collectionView.delegate=self;
    self.collectionView.showsVerticalScrollIndicator=NO;
    self.collectionView.dataSource=self;
    [self addSubview:self.collectionView];
}

-(void)setGuessData:(NSArray *)guessData{
    _guessData = guessData;
    
    [_collectionView reloadData];
}

#pragma collection delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.guessData.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize itemSize=[self collectionView:collectionView layout:collectionView.collectionViewLayout sizeForItemAtIndexPath:indexPath];
    
    GoodListCC *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[GoodListCC getID] forIndexPath:indexPath];
    if(cell==nil){
        cell=[[GoodListCC alloc] initWithFrame:CGRectMake(0, 0,itemSize.width,itemSize.height)];
    }
    
    return  cell;
}

//定义每个UICollectionView 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(_collectionWidth,_collectionHeight);
}

//定义每个UICollectionView 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0,0,0,0);
}

//UICollectionView被选中时调用的方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    GoodInfoVC * goodsInfo = [GoodInfoVC new];
//    goodsInfo.goodsId = [NSString stringWithFormat:@"%@",self.guessIdMuArr[indexPath.row]];
//    [self tabHidePushVC:goodsInfo];
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

@end
