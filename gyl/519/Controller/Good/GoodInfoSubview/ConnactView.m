//
//  ConnactView.m
//  519
//
//  Created by Macmini on 2019/4/10.
//  Copyright © 2019 519. All rights reserved.
//

#import "ConnactView.h"

@interface ConnactView()
@property(nonatomic,strong)UIView * whiteView;
@property (nonatomic,strong)UILabel * nameLabel;
@property (nonatomic,strong)UILabel * telLbl;
@property (nonatomic,strong)UIButton *dail;
@end

@implementation ConnactView


- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.5];
        self.userInteractionEnabled = true;
        [self createUI];
    }
    return self;
}

- (void)createUI{
    _whiteView = [[UIView alloc]init];
    _whiteView.backgroundColor = [UIColor whiteColor];
    _whiteView.userInteractionEnabled = true;
    _whiteView.layer.cornerRadius = 8;
    _whiteView.layer.masksToBounds = true;
    [self addSubview:_whiteView];
    
    
    
    
    
    [_whiteView makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(25);
        make.right.mas_equalTo(-25);
        make.height.equalTo(120);
        make.centerY.equalTo(self);
        make.centerY.equalTo(self).offset(-60);
    }];
    
    _nameLabel = [[UILabel alloc]init];
    _nameLabel.text = @"你的专属业务员:";
    _nameLabel.textColor = APPFourColor;
    _nameLabel.textAlignment = NSTextAlignmentLeft;
    _nameLabel.font =[UIFont systemFontOfSize:15];
    _nameLabel.numberOfLines = 0;
    [_whiteView addSubview:_nameLabel];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(23);
        make.left.mas_equalTo(23);
    }];
    
    
  
    
//    _dail = [[UIButton alloc] init];
//    [_dail setImage:[UIImage imageNamed:@"button_拨号"] forState:0];
//    [self addSubview:_dail];
//    [_dail addTarget:self action:@selector(dailTel) forControlEvents:UIControlEventTouchUpInside];
//    [_dail mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(_nameLabel.mas_bottom).offset(23);
//        make.right.equalTo(_whiteView.mas_right).offset(-10);
//    }];
    
    _telLbl = [[UILabel alloc]init];
    _telLbl.text = @"手机号:";
    _telLbl.textColor = APPFourColor;
    _telLbl.textAlignment = NSTextAlignmentLeft;
    _telLbl.font =[UIFont systemFontOfSize:15];
    _telLbl.numberOfLines = 0;
    _telLbl.userInteractionEnabled = true;
    [_telLbl addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dailTel)]];
    [_whiteView addSubview:_telLbl];
    [_telLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_nameLabel.mas_bottom).offset(23);
        make.left.mas_equalTo(23);
        make.right.equalTo(_whiteView).offset(-5);
    }];
    
    UIButton *xBtn = [[UIButton alloc] init];
    [xBtn setImage:[UIImage imageNamed:@"Exit"] forState:0];
    [self addSubview:xBtn];
    [xBtn addTarget:self action:@selector(disapperfromSuperView) forControlEvents:UIControlEventTouchUpInside];
    [xBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_whiteView);
        make.top.equalTo(_whiteView.mas_bottom).offset(15);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];

}

- (void)dailTel{
     [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",self.dic[@"mobile"]]]];
}

- (void)disapperfromSuperView{
    [self removeFromSuperview];
}

- (void)setupData:(NSDictionary *)dic{
    _dic = dic;
    self.nameLabel.text = [NSString stringWithFormat:@"您的专属业务经理:%@",dic[@"name"]];
    self.telLbl.text = [NSString stringWithFormat:@"手机号:%@",dic[@"mobile"]];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
