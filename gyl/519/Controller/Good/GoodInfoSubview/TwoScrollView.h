//
//  TwoScrollView.h
//  gylPT
//
//  Created by Macmini on 2017/9/4.
//  Copyright © 2017年 wzBPH. All rights reserved.
//

typedef void(^TwoScrollviewBlock)();

#import <UIKit/UIKit.h>

@interface TwoScrollView : UIScrollView

@property (nonatomic,copy)TwoScrollviewBlock block;
@property (nonatomic,strong)UIWebView * webview;
@end
