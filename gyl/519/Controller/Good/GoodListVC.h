//
//  GoodListVC.h
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodListVC : UIViewController
@property (nonatomic,copy)NSString * idStr;
@property(nonatomic,copy)NSString * selectstr;
@property (nonatomic,copy)NSString * searchStr;
@property (nonatomic,copy)NSString * typeId;
@end
