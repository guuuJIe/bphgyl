//
//  OrderListTC.h
//  519
//
//  Created by 陈 on 16/9/13.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"
#import "OrderListModel.h"

@protocol orderBtnOnClickDelegate <NSObject>

-(void)nowPay:(NSString *)orderId;
-(void)deleteOrderId:(NSString *)orderid withindex:(NSInteger )index;
-(void)orderDetail:(NSString *)detail withTitle:(NSString *)title withCellIndex:(NSInteger)inter;
-(void)refundFor:(NSString *)orderId withrefund:(NSArray *)refundArr;
-(void)goodsEvaluationwithGoodsData:(NSArray *)evaluationArr withEvaluationId:(NSString *)ids;
-(void)buyOne:(NSString *)ids;
@end


@interface OrderListTC : BaseTC
@property(nonatomic,assign)id<orderBtnOnClickDelegate>delegate;

@property(nonatomic,strong)OrderListModel * model;
@property(nonatomic,assign)NSInteger index;


+(NSString *)getID;

@end
