//
//  OrderListTCTC.m
//  519
//
//  Created by 陈 on 16/9/13.
//  Copyright © 2016年 519. All rights reserved.
//

#import "OrderListTCTC.h"

@interface OrderListTCTC()
@property(nonatomic,strong)UIImageView *img;
@property(nonatomic,strong)UILabel *name;
@property(nonatomic,strong)UILabel *price;
@property(nonatomic,strong)UILabel *info;
@property(nonatomic,strong)UILabel *count;
@end

@implementation OrderListTCTC
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initView];
    }
    
    return  self;
}

-(void)initView{
    self.backgroundColor=[UIColor colorWithWhite:0.98 alpha:1];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    self.img=[[UIImageView alloc]init];
    self.img.contentMode = UIViewContentModeScaleAspectFit;
    self.name=[[UILabel alloc]init];
    self.name.textColor=APPFourColor;
    self.name.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.name.numberOfLines=2;
    self.name.lineBreakMode=NSLineBreakByTruncatingTail;
    
    self.price=[[UILabel alloc]init];
    self.price.textColor=APPFourColor;
    self.price.textAlignment = NSTextAlignmentRight;
    self.price.font=[UIFont systemFontOfSize:FONT_SIZE_M];

    self.count=[[UILabel alloc]init];
    self.count.textColor=APPThreeColor;
    self.count.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    [self addSubview:self.img];
    [self addSubview:self.name];
    [self addSubview:self.price];
    [self addSubview:self.info];
    [self addSubview:self.count];
    [_img makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset(10);
        make.top.equalTo(self).with.offset(10);
        make.width.and.height.offset(80);
    }];
    [_name makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_img.mas_right).with.offset(10);
        make.top.equalTo(self.contentView).with.offset(10);
        make.width.offset(Screen_Width-190);
    }];
    [_price makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_name.mas_right).with.offset(Default_Space);
        make.right.equalTo(self.mas_right).with.offset(-5);
        make.centerY.equalTo(_name.mas_centerY);
        make.height.offset(20);
    }];
    [_count makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_img.mas_centerY);
        make.right.equalTo(self).with.offset(-5);
        make.height.offset(40);
    }];


    
}

-(void)setModel:(DealOrderModel *)model{
    _model = model;
    [_img sd_setImageWithURL:[NSURL URLWithString:model.deal_icon]];
    self.name.text=model.name;
    
   
    NSString * price;
    if ([model.return_score floatValue] >0.0) {
        price = [NSString stringWithFormat:@"%@积分",model.return_score];
    }else{
        price = [NSString stringWithFormat:@"%@",model.total_price];
        if (price.length>5) {
            price =[NSString stringWithFormat:@"￥%.2lf",[model.total_price doubleValue]];
        }else{
            price = [NSString stringWithFormat:@"￥%@",model.total_price];
        }
    }
    
    
    self.price.text=price;
    
    self.info.text=model.attr_str;
    self.count.text=[NSString stringWithFormat:@"x%@",model.number];
    
}


+(NSString *)getID{
    return @"OrderListTCTC";
}
@end
