//
//  AddressLocationTC.m
//  519
//
//  Created by 陈 on 16/9/19.
//  Copyright © 2016年 519. All rights reserved.
//

#import "AddressLocationTC.h"

@interface AddressLocationTC()
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *infoLabel;
@property(nonatomic,strong)UIView *line;

@end

@implementation AddressLocationTC
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initView];
    }
    
    return  self;
}

-(void)initView{
    self.backgroundColor=[UIColor whiteColor];
    self.titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space, Default_Space, Screen_Width-Default_Space*2, 20)];
    self.titleLabel.textColor=APPFourColor;
    self.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_L];
    
    self.infoLabel=[[UILabel alloc] initWithFrame:CGRectMake(Default_Space,self.titleLabel.frame.size.height+self.titleLabel.frame.origin.y+Default_Space, Screen_Width-Default_Space*2, 20)];
    self.infoLabel.textColor=APPFourColor;
    self.infoLabel.font=[UIFont systemFontOfSize:FONT_SIZE_L];
    
    self.line=[[UIView alloc]initWithFrame:CGRectMake(0, [AddressLocationTC getHeight]-Default_Line, Screen_Width, Default_Line)];
    self.line.backgroundColor=APPBGColor;
    
    [self addSubview:self.titleLabel];
    [self addSubview:self.infoLabel];
    [self addSubview:self.line];
}

-(void)showData:(BMKPoiInfo *)poi index:(int)index{
    if(index==0){
        self.titleLabel.textColor=APPColor;
        self.titleLabel.text=[NSString stringWithFormat:@"[当前]%@",poi.name];
    }else{
        self.titleLabel.textColor=APPFourColor;
        self.titleLabel.text=poi.name;
    }
    
    self.infoLabel.text=poi.address;
}

+(CGFloat)getHeight{
    return Default_Space*3+20+20;
}

+(NSString *)getID{
    return @"AddressLocationTC";
}
@end
