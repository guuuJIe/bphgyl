//
//  AddressLocationTC.h
//  519
//
//  Created by 陈 on 16/9/19.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"

@interface AddressLocationTC : BaseTC
-(void)showData:(BMKPoiInfo *)poi index:(int)index;

+(CGFloat)getHeight;

+(NSString *)getID;
@end
