//
//  EvaluationTC.h
//  519
//
//  Created by Macmini on 16/12/21.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"
#import "DealOrderModel.h"

@protocol PassDataForVCDelegate <NSObject>
/**
 *  传递数据
 */
-(void)passDataForIds:(NSString *)ids point:(NSString *)pointString withContent:(NSString *)contents;


//打开键盘
-(void)keyUp:(UITextView *)textview with:(NSInteger)index;

//收起键盘
-(void)keydown;

@end

@interface EvaluationTC : BaseTC

@property(nonatomic,strong)DealOrderModel * model;
@property(nonatomic,assign)id<PassDataForVCDelegate>delegate;
@property(nonatomic,assign)BOOL isbtnClick;
@property(nonatomic,assign)BOOL isTextView;
@property(nonatomic,copy)NSString * pointStr;
@property(nonatomic,copy)NSString * ids;
@property(nonatomic,copy)NSString * contents;
@property(nonatomic,assign)NSInteger  index;
+(NSString *)getID;
@end
