//
//  RefundTC.h
//  519
//
//  Created by Macmini on 16/12/16.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"
#import "DealOrderModel.h"

@class DealOrderModel;

typedef void(^refundGoodIdBlock)(NSString * ids,BOOL isYes);
typedef void(^refundcenterBlock)(NSString * content, NSString * ids);

@interface RefundTC : BaseTC
@property(nonatomic,strong)DealOrderModel * model;
@property(nonatomic,copy)refundcenterBlock block;
@property(nonatomic,copy)refundGoodIdBlock idBlock;
@property(nonatomic,assign)NSInteger index;
@property(nonatomic,copy)NSString * refundId;

+(NSString *)getID;



@end
