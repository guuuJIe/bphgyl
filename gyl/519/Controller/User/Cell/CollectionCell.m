//
//  CollectionCell.m
//  519
//
//  Created by Macmini on 2019/5/2.
//  Copyright © 2019 519. All rights reserved.
//

#import "CollectionCell.h"
@interface CollectionCell()
@property(nonatomic,strong)UIImageView *img;
@property(nonatomic,strong)UIView *line;
@property(nonatomic,strong)UILabel *title;
@property(nonatomic,strong)UILabel *price;
@property(nonatomic,strong)UILabel *specificationLabel;//规格
@property(nonatomic,strong)UIImageView *car;
@property(nonatomic,strong)UILabel *oldprice;
@property(nonatomic,strong)UILabel *buycountLabel;
@property(nonatomic,strong)UIView * bgview;
@property(nonatomic,strong)UIImageView * deleteimg;
@property(nonatomic,strong)UIView * deleteview;
@property(nonatomic,assign)NSInteger  index;

@property(nonatomic,strong)UIImageView *delImage;
@end
@implementation CollectionCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        //[self initView];
        [self createUI];
    }
    return self;
}
-(void)createUI{
    
    
    _img = [[UIImageView alloc]init];
    [self addSubview:_img];
    [_img makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.offset(10);
        make.width.and.height.equalTo(80);
    }];
    
    _title = [[UILabel alloc]init];
    _title.textColor = APPFourColor;
    _title.textAlignment = NSTextAlignmentLeft;
    _title.font = [UIFont systemFontOfSize:14];
    _title.numberOfLines=2;
    [self addSubview:_title];
    [_title makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_img.mas_right).with.offset(10);
        make.top.offset(15);
        make.width.offset(self.frame.size.width-110);
    }];
    
    _specificationLabel = [[UILabel alloc]init];
    _specificationLabel.textColor = APPThreeColor;
    _specificationLabel.textAlignment = NSTextAlignmentLeft;
    _specificationLabel.font = [UIFont systemFontOfSize:12];
    _specificationLabel.text = @"";
    [self addSubview:_specificationLabel];
    [_specificationLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_img.mas_right).offset(10);
        make.top.equalTo(_title.mas_bottom).offset(5);
        make.height.offset(20);
    }];
    
    _price = [[UILabel alloc]init];
    _price.textColor = [UIColor redColor];
    _price.textAlignment = NSTextAlignmentLeft;
    _price.font = [UIFont systemFontOfSize:13];
    [self addSubview:_price];
    [_price makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_img.mas_right).with.offset(10);
        make.bottom.offset(-10);
        make.height.offset(20);
    }];
    
    _oldprice = [[UILabel alloc]init];
    _oldprice.textColor = APPThreeColor;
    _oldprice.textAlignment = NSTextAlignmentLeft;
    _oldprice.font = [UIFont systemFontOfSize:11];
    [self addSubview:_oldprice];
    [_oldprice makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_price.mas_right).with.offset(10);
        make.bottom.offset(-10);
        make.height.offset(20);
    }];
    
    _buycountLabel = [[UILabel alloc]init];
    _buycountLabel.textColor = APPThreeColor;
    _buycountLabel.textAlignment = NSTextAlignmentRight;
    _buycountLabel.font = [UIFont systemFontOfSize:11];
    [self addSubview:_buycountLabel];
    [_buycountLabel makeConstraints:^(MASConstraintMaker *make) {
        make.right.and.bottom.offset(-10);
        make.height.offset(20);
    }];
    
    UIView * lineView = [[UIView alloc]init];
    lineView.backgroundColor = APPBGColor;
    [self addSubview:lineView];
    [lineView makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.offset(-1);
        make.left.and.right.offset(0);
        make.height.offset(1);
    }];
    
    [self addSubview:self.delImage];
    [self.delImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.price);
        make.right.mas_equalTo(-15);
    }];
    
}

-(void)deleteCollectGoods{
    [self.delegate removeCollectGoodsIds:_index];
}

-(void)showDataOfTitle:(NSString *)title image:(NSString *)image withPrice:(NSString *)price  oldprice:(NSString *)oldPrice withCound:(NSString *)count withspec:(NSString *)spec{
    //buy_type:(NSString *)buy_type withDeal_score:(NSString *)score{
    //    self.deleteimg.hidden = YES;
    [self.img sd_setImageWithURL:[NSURL URLWithString:image]];
    self.title.text=title;
    
    self.price.text=[NSString stringWithFormat:@"￥%@",price];
    //中划线
    NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
    NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"￥%@",oldPrice] attributes:attribtDic];
    
    self.oldprice.attributedText = attribtStr;
    //    self.specificationLabel.text = [NSString stringWithFormat:@"规格:%@",spec];
    self.buycountLabel.text = [NSString stringWithFormat:@"%@人已购",count];
    
}

-(void)showDataForCollectVCName:(NSString *)name icon:(NSString *)icon withPrice:(NSString *)price oldprice:(NSString *)oldPrice withCound:(NSString *)count withcount:(NSInteger)index withbuytype:(nonnull NSString *)buytype withscore:(nonnull NSString *)score{
    _index = index;
    self.buycountLabel.hidden = YES;
    [self.img sd_setImageWithURL:[NSURL URLWithString:icon]];
    self.title.text=name;
    if ([buytype integerValue] == 1) {
        self.oldprice.hidden = true;
        self.price.text=[NSString stringWithFormat:@"%.2lf积分",[score doubleValue]];
    }else{
        self.oldprice.hidden = false;
       self.price.text=[NSString stringWithFormat:@"￥%.0lf",[price doubleValue]];
        NSDictionary *attribtDic = @{NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
        NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"￥%.0lf",[oldPrice doubleValue]] attributes:attribtDic];
        self.oldprice.attributedText = attribtStr;
    }
    
    
    
}

//guessgoods
-(void)setGoodsModel:(GuessModel *)guessModel{
    _guesssModel = guessModel;
    [self.img sd_setImageWithURL:[NSURL URLWithString:guessModel.icon]];
    self.title.text =  guessModel.name;
    self.price.frame = CGRectMake(Default_Space,self.title.frame.origin.y+self.title.frame.size.height, self.frame.size.width, 20);
    self.price.text = [NSString stringWithFormat:@"￥%.02f",[guessModel.current_price doubleValue]];
    self.bgview.hidden = YES;
    self.deleteimg.hidden = YES;
}

- (void)delGoods{
    [self.delegate removeCollectGoodsIds:_index];
}


- (UIImageView *)delImage{
    if (!_deleteimg) {
        _deleteimg = [[UIImageView alloc] init];
        _deleteimg.image = [UIImage imageNamed:@"icon_del"];
        [_deleteimg addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(delGoods)]];

    }
    return _deleteimg;
}


+(NSString *)getID{
    return @"CollectionCell";
}
@end
