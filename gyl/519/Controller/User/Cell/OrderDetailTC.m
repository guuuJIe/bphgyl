//
//  OrderDetailTC.m
//  519
//
//  Created by Macmini on 16/12/13.
//  Copyright © 2016年 519. All rights reserved.
//

#import "OrderDetailTC.h"

@interface OrderDetailTC()
@property(nonatomic,strong)UIImageView * goodsIconImg;
@property(nonatomic,strong)UILabel * nameLabel;
@property(nonatomic,strong)UILabel * goodsInfoLabel;
@property(nonatomic,strong)UILabel * onepriceLabel;
@property(nonatomic,strong)UILabel * numberLabel;

@end

@implementation OrderDetailTC

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initView];
    }
    return self;
}

-(void)initView{
    UIView * lineView = [[UIView alloc]init];
    lineView.backgroundColor = APPBGColor;
    [self addSubview:lineView];
    [lineView makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.offset(0);
        make.height.offset(1);
    }];
    
    _goodsIconImg = [[UIImageView alloc]init];
    _goodsIconImg.contentMode = UIViewContentModeScaleAspectFit;
    
    _nameLabel = [[UILabel alloc]init];
    _nameLabel.text = @"titlename";
    _nameLabel.textAlignment = NSTextAlignmentLeft;
    _nameLabel.textColor = APPFourColor;
    _nameLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    _nameLabel.numberOfLines = 0;
    
    _goodsInfoLabel = [[UILabel alloc]init];
    _goodsInfoLabel.text = @"sssss";
    _goodsInfoLabel.textColor = APPThreeColor;
    _goodsInfoLabel.textAlignment = NSTextAlignmentLeft;
    _goodsInfoLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    
    _onepriceLabel = [[UILabel alloc]init];
    _onepriceLabel.text = @"x1111";
    _onepriceLabel.textColor = APPFourColor;
    _onepriceLabel.textAlignment = NSTextAlignmentRight;
    _onepriceLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];
    
    _numberLabel = [[UILabel alloc]init];
    _numberLabel.text = @"x111";
    _numberLabel.textColor = APPThreeColor;
    _numberLabel.textAlignment = NSTextAlignmentRight;
    _numberLabel.font = [UIFont systemFontOfSize:FONT_SIZE_M];


    
    [self addSubview:_nameLabel];
    [self addSubview:_goodsInfoLabel];
    [self addSubview:_onepriceLabel];
    [self addSubview:_numberLabel];
    [self addSubview:_goodsIconImg];
    
    [_goodsIconImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.equalTo(self.contentView).with.offset(Default_Space);
        make.width.and.height.mas_equalTo(80);
    }];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_goodsIconImg.mas_right).with.offset(Default_Space);
        make.top.equalTo(self).with.offset(Default_Space);
        make.height.mas_equalTo(40);
        make.width.mas_equalTo(Screen_Width-150);
    }];
    [_onepriceLabel makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).with.offset(-Default_Space);
        make.centerY.equalTo(_nameLabel.mas_centerY);
        make.height.mas_equalTo(20);
    }];
    [_goodsInfoLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_goodsIconImg.mas_right).with.offset(Default_Space);
        make.top.equalTo(_nameLabel.mas_bottom).with.offset(5);
        make.height.mas_equalTo(40);
    }];
    [_numberLabel makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).with.offset(-Default_Space);
        make.centerY.equalTo(_goodsInfoLabel.mas_centerY);
        make.height.mas_equalTo(20);
    }];
    
    
}

-(void)setModel:(GoodsDetailModel *)model{
    
    _model = model;
    [_goodsIconImg sd_setImageWithURL:[NSURL URLWithString:model.deal_icon]];
    _nameLabel.text = model.sub_name;
    _goodsInfoLabel.text = model.attr_str;
    _numberLabel.text = [NSString stringWithFormat:@"x%@",model.number];
    if ([model.return_score floatValue] > 0.0) {
        _onepriceLabel.text = [NSString stringWithFormat:@"%@积分",model.return_score];
    }else{
        _onepriceLabel.text = [NSString stringWithFormat:@"￥%@",model.unit_price];
    }
    
}



+(NSString *)getID{
    return @"OrderDetailTC";
}

@end
