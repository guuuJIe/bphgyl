//
//  OrderListTC.m
//  519
//
//  Created by 陈 on 16/9/13.
//  Copyright © 2016年 519. All rights reserved.
//

#import "OrderListTC.h"
#import "OrderListTCTC.h"
#import "DealOrderModel.h"
@interface OrderListTC()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
@property(nonatomic,strong)UILabel *state;
@property(nonatomic,strong)UILabel *time;
@property(nonatomic,strong)UIView *line;
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)UIView *line1;
@property(nonatomic,strong)UILabel *price;
@property(nonatomic,strong)UIView *btnView;

@property(nonatomic,strong)UIView *marLine;
@property(nonatomic,strong)NSMutableArray * dealOrderItem;
@property(nonatomic,strong)NSMutableArray * allDealOrderMuArr;
@property(nonatomic,copy)NSString * orderId;
@property(nonatomic,copy)NSString * goodID;
@property(nonatomic,copy)NSString * orderIds;
@property(nonatomic,copy)NSString * status;
@property(nonatomic,strong)NSArray * dealArr;
@end

@implementation OrderListTC
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        _allDealOrderMuArr = [NSMutableArray new];
        if (_dealOrderItem) {
            [_dealOrderItem removeAllObjects];
        }else{
            _dealOrderItem = [NSMutableArray new];
        }
        
        [self initView];
    }
    
    return  self;
}



-(void)initView{
    self.backgroundColor=[UIColor whiteColor];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    
    self.marLine=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Default_Space)];
    self.marLine.backgroundColor=APPBGColor;
    
    self.state=[[UILabel alloc]init];
    self.state.font=[UIFont systemFontOfSize:FONT_SIZE_M];
        self.state.textColor=APPFourColor;
    self.time=[[UILabel alloc]init];
    self.time.textColor=APPThreeColor;
    self.time.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    self.time.textAlignment=NSTextAlignmentRight;
    
    self.line=[[UIView alloc]init];
    self.line.backgroundColor=APPBGColor;
    
    self.tableView=[[UITableView alloc]init];
    [self.tableView registerClass:[OrderListTCTC class] forCellReuseIdentifier:[OrderListTCTC getID]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.scrollEnabled=NO;
    self.tableView.showsVerticalScrollIndicator=NO;
    
    self.line1=[[UIView alloc]init];
    self.line1.backgroundColor=APPBGColor;

    self.price=[[UILabel alloc] init];
    self.price.textColor = APPFourColor;
    self.price.textAlignment = NSTextAlignmentRight;
    self.price.font=[UIFont systemFontOfSize:14];
    
    UILabel * allPrice = [[UILabel alloc]init];
    allPrice.text = @"合计总价:";
    allPrice.textAlignment = NSTextAlignmentRight;
    allPrice.textColor = APPThreeColor;
    allPrice.font = [UIFont systemFontOfSize:14];
    self.btnView=[[UIView alloc]init];
    
    
    [self addSubview:self.marLine];
    [self addSubview:self.state];
    [self addSubview:self.time];
    [self addSubview:self.line];
    [self addSubview:self.tableView];
    [self addSubview:self.line1];
    [self addSubview:self.price];
    [self addSubview:allPrice];
    [self addSubview:self.btnView];
    
    [self.state makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).with.offset(Default_Space) ;
        make.top.equalTo(_marLine.mas_bottom).with.offset(Default_Space);
        make.height.mas_equalTo(20);
    }];
    [_time makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_marLine.mas_bottom).with.offset(Default_Space);
        make.right.equalTo(self.contentView).with.offset(-Default_Space);
        make.height.mas_equalTo(20);
    }];
    [_line makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_state.mas_bottom).with.offset(5);
        make.left.and.right.equalTo(self.contentView).with.offset(0);
        make.height.mas_equalTo(1);
    }];
    [_tableView makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_line.mas_bottom).with.offset(0);
        make.left.equalTo(self.contentView).with.offset(0);
        make.right.equalTo(self.contentView).with.offset(0);
        make.height.mas_equalTo(120);
    }];
    [_line1 makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_tableView.mas_bottom).with.offset(0);
        make.left.and.right.equalTo(self.contentView).with.offset(0);
        make.height.mas_equalTo(Default_Line);
    }];
    [_price makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_line1.mas_bottom).with.offset(Default_Space);
        make.right.offset(-Default_Space);
        make.height.mas_equalTo(20);
    }];
    [allPrice makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_line1.mas_bottom).with.offset(10);
        make.right.equalTo(_price.mas_left).with.offset(-10);
        make.height.offset(20);
    }];
    [_btnView makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_price.mas_bottom).with.offset(Default_Space);
        make.left.right.offset(0);
        make.height.mas_equalTo(30);
        
    }];
    [self.contentView makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
}



-(void)setModel:(OrderListModel *)model{
    _model = model;
    _orderId = model.ids;
    _status = model.status;
    _dealArr = model.deal_order_item;
    self.state.text=model.status;
    self.time.text=model.create_time;
    NSString * price;
    if ([model.return_total_score floatValue] > 0.0) {
        price = [NSString stringWithFormat:@"%@积分 %@",model.return_total_score,[model.total_price floatValue] > 0.0 ? [NSString stringWithFormat:@"%@元",model.total_price] : @""];
    }else{
        price = [NSString stringWithFormat:@"%@",model.total_price];
        if (price.length>5) {
            price =[NSString stringWithFormat:@"￥%.2lf",[model.total_price doubleValue]];
        }else{
            price = [NSString stringWithFormat:@"￥%@",model.total_price];
        }
        
        
    }
    
   
    self.price.text =price;
    for (UIView *itemView in self.btnView.subviews) {
        [itemView removeFromSuperview];
    }
   
    if (_dealOrderItem) {
        [_dealOrderItem removeAllObjects];
    }else{
        _dealOrderItem = [NSMutableArray new];
    }
    for (NSDictionary * subdic in model.deal_order_item) {
        DealOrderModel * model = [DealOrderModel new];
        [model setValuesForKeysWithDictionary:subdic];
        [_dealOrderItem addObject:model];
        
    }
    
    [_allDealOrderMuArr addObject:_dealOrderItem];
   
    NSString * pay_status = [NSString stringWithFormat:@"%@",model.pay_status];
    NSString * items_refund_status = [NSString stringWithFormat:@"%@",model.items_refund_status];
    NSString * order_status = [NSString stringWithFormat:@"%@",model.order_status];
    NSString * keyi_dianpin = [NSString stringWithFormat:@"%@",model.keyi_dianpin];
    
    
    NSArray * btnTitleArray;
    if ([pay_status isEqualToString:@"0"]) {
        btnTitleArray=[[NSArray alloc] initWithObjects:@"立即支付",@"删除订单", nil];
    }
    if([pay_status isEqualToString:@"2"] && [items_refund_status isEqualToString:@"1"] && [order_status isEqualToString:@"0"] ){
        btnTitleArray=[[NSArray alloc] initWithObjects:@"我要退款", nil];
    }
    if([keyi_dianpin isEqualToString:@"1"] && [order_status isEqualToString:@"0"]){
        btnTitleArray=[[NSArray alloc] initWithObjects:@"我要评价", nil];
    }
    
    CGFloat allWidth=Screen_Width/3*2-Default_Space*2;
    for (int i=0;i<btnTitleArray.count;i++) {
        NSString *itemTitle=[btnTitleArray objectAtIndex:i];
        UIFont *itemFont=[UIFont systemFontOfSize:14];
        CGFloat itemWidth=[NSString sizeWithText:itemTitle font:itemFont maxSize:Max_Size].width+Default_Space*2;
        allWidth=allWidth-Default_Space-itemWidth;
        if (allWidth<0) {
            allWidth = allWidth*(-1)+itemWidth+Default_Space;
        }
        UIButton *btn=[[UIButton alloc]initWithFrame:CGRectMake(Screen_Width-((itemWidth+10)*(i+1)), 0,itemWidth, 30)];
        [btn setTitle:itemTitle forState:UIControlStateNormal];
        
        btn.layer.cornerRadius=3;
        btn.layer.masksToBounds=YES;
        if(i==0){
            btn.layer.borderColor = APPColor.CGColor;
            [btn setTitleColor:APPColor forState:UIControlStateNormal];
        }else{
            btn.layer.borderColor = APPFourColor.CGColor;
            [btn setTitleColor:APPFourColor forState:UIControlStateNormal];
        }
        btn.layer.borderWidth = 1;
        btn.titleLabel.font=itemFont;
    
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.btnView addSubview:btn];
    }
    
    
    [_tableView updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_line.mas_bottom).with.offset(0);
        make.left.equalTo(self).with.offset(0);
        make.right.equalTo(self).with.offset(0);
        make.height.mas_equalTo(_dealOrderItem.count*100);
    }];
    
    
    [self.tableView reloadData];
    
}

-(void)btnClick:(UIButton *)btn{
    if ([btn.titleLabel.text isEqualToString:@"立即支付"]) {
        [self.delegate nowPay:_orderId];
    }else if([btn.titleLabel.text isEqualToString:@"删除订单"]){
        
        UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"" message:@"是否删除订单" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alertview show];
        
    }else if([btn.titleLabel.text isEqualToString:@"我要退款"]){
        [self.delegate refundFor:_orderId withrefund:_dealArr];
    }else if ([btn.titleLabel.text isEqualToString:@"我要评价"]){
        [self.delegate goodsEvaluationwithGoodsData:_dealArr withEvaluationId:_orderId];
    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex == 1){
        [self.delegate deleteOrderId:_orderId withindex:_index ];
    }
}



#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return _model.deal_order_item.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderListTCTC *cell=[tableView dequeueReusableCellWithIdentifier:[OrderListTCTC getID] forIndexPath:indexPath];
    if(cell==nil){
        cell=[[OrderListTCTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[OrderListTCTC getID]];
    }
    DealOrderModel * model = _dealOrderItem[indexPath.row];
    cell.model = model;
    return  cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.delegate orderDetail:_orderId withTitle:_status withCellIndex:indexPath.row];
}

+(NSString *)getID{
    return @"OrderListTC";
}
@end
