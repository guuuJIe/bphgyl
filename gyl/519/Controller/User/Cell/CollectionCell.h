//
//  CollectionCell.h
//  519
//
//  Created by Macmini on 2019/5/2.
//  Copyright © 2019 519. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseCC.h"
#import "GoodInfoModel.h"
#import "GoodsModel.h"
@protocol RemoveGoodsDelegate <NSObject>

@optional

-(void)removeCollectGoodsIds:(NSInteger)index;

@end
NS_ASSUME_NONNULL_BEGIN

@interface CollectionCell : BaseCC
@property (nonatomic,copy)NSString * goodsId;
@property(nonatomic,assign)id<RemoveGoodsDelegate>delegate;
@property (nonatomic,strong)GuessModel * guesssModel; //猜喜欢

-(void)showDataForCollectVCName:(NSString *)name icon:(NSString *)icon withPrice:(NSString *)price  oldprice:(NSString *)oldPrice withCound:(NSString *)count withcount:(NSInteger)index withbuytype:(NSString *)buytype withscore:(NSString *)score;

-(void)showDataOfTitle:(NSString *)title image:(NSString *)image withPrice:(NSString *)price oldprice:(NSString *)oldPrice withCound:(NSString *)count withspec:(NSString *)spec;
//buy_type:(NSString *)buy_type withDeal_score:(NSString *)score;



+(NSString *)getID;
@end

NS_ASSUME_NONNULL_END
