//
//  ReturnPriceTableViewCell.m
//  519
//
//  Created by Macmini on 2018/1/10.
//  Copyright © 2018年 519. All rights reserved.
//

#import "ReturnPriceTableViewCell.h"

@interface ReturnPriceTableViewCell()<UITextViewDelegate>

@property(nonatomic,strong)UIButton * clickBtn;
@property(nonatomic,strong)UIImageView * iconImgV;
@property(nonatomic,strong)UILabel * nameLabel;
@property(nonatomic,strong)UILabel * priceLabel;
@property(nonatomic,strong)UILabel * numberLabel;
@property(nonatomic,strong)UILabel * reasonLabel;
@property(nonatomic,strong)UITextView * textView;

@end

@implementation ReturnPriceTableViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

-(void)createReturnPriceUI{
    _clickBtn = [[UIButton alloc]init];
    [_clickBtn setImage:[UIImage imageNamed:@""] forState:(UIControlStateNormal)];
    [_clickBtn addTarget:self action:@selector(isChooseBtn) forControlEvents:(UIControlEventTouchUpInside)];
    [self addSubview:_clickBtn];
    [_clickBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(10);
        make.top.offset(10);
        make.width.height.offset(15);
    }];
    
    _iconImgV = [[UIImageView alloc]init];
    _iconImgV.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:_iconImgV];
    [_iconImgV makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_clickBtn.mas_right).with.offset(10);
        make.centerY.equalTo(_clickBtn.centerY);
        make.width.height.offset(60);
    }];
    
    _nameLabel  = [[UILabel alloc]init];
    _nameLabel.textColor = APPFourColor;
    _nameLabel.textAlignment = NSTextAlignmentLeft;
    _nameLabel.font = [UIFont systemFontOfSize:13];
    _nameLabel.numberOfLines = 2;
    [self addSubview:_nameLabel];
    [_nameLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_iconImgV.mas_right).with.offset(10);
        make.top.offset(10);
        make.width.offset(self.frame.size.width-100);
    }];
    
    UILabel * returnTitle = [[UILabel alloc]init];
    returnTitle.textAlignment = NSTextAlignmentLeft;
    returnTitle.text = @"退款金额:";
    returnTitle.textColor = APPThreeColor;
    returnTitle.font = [UIFont systemFontOfSize:13];
    [self addSubview:returnTitle];
    [returnTitle makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_iconImgV.mas_right).with.offset(10);
        make.bottom.equalTo(_iconImgV.mas_bottom).width.offset(0);
        make.height.offset(20);
    }];
    
    _priceLabel = [[UILabel alloc]init];
    _priceLabel.textColor = APPColor;
    _priceLabel.textAlignment = NSTextAlignmentLeft;
    _priceLabel.font = [UIFont systemFontOfSize:13];
    [self addSubview:_priceLabel];
    [_priceLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(returnTitle.mas_right).with.offset(0);
        make.centerY.equalTo(returnTitle.centerY);
        make.height.offset(20);
    }];
    
    _numberLabel = [[UILabel alloc]init];
    _numberLabel.textColor = APPThreeColor;
    _numberLabel.textAlignment = NSTextAlignmentRight;
    _numberLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:_numberLabel];
    [_numberLabel makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-10);
        make.centerY.equalTo(_priceLabel.centerY);
    }];
    
    UIView * lineView = [[UIView alloc]init];
    lineView.backgroundColor = APPBGColor;
    [self addSubview:lineView];
    [lineView makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_iconImgV.mas_bottom).with.offset(10);
        make.left.right.offset(0);
        make.height.offset(1);
    }];
    
    UILabel * reasonTitle = [[UILabel alloc]init];
    reasonTitle.textAlignment = NSTextAlignmentLeft;
    reasonTitle.text = @"退款说明";
    reasonTitle.textColor = APPFourColor;
    reasonTitle.font = [UIFont systemFontOfSize:13];
    [self addSubview:reasonTitle];
    [reasonTitle makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(10);
        make.top.equalTo(lineView.mas_bottom).with.offset(10);
        make.height.offset(20);
    }];
    
    _textView = [[UITextView alloc]init];
    _textView.textColor = APPFourColor;
    _textView.textAlignment = NSTextAlignmentLeft;
    _textView.returnKeyType = UIReturnKeyDone;
    _textView.font = [UIFont systemFontOfSize:13];
    _textView.backgroundColor = APPBGColor;
    _textView.delegate = self;
    [self addSubview:_textView];
    [_textView makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(10);
        make.top.equalTo(reasonTitle.mas_bottom).with.offset(10);
        make.width.offset(self.frame.size.width-20);
        make.height.offset(70);
    }];
    
    _reasonLabel = [[UILabel alloc]init];
    _reasonLabel.textAlignment = NSTextAlignmentLeft;
    _reasonLabel.text = @"请输入退款原因";
    _reasonLabel.textColor = APPFourColor;
    _reasonLabel.font = [UIFont systemFontOfSize:13];
    [_textView addSubview:_reasonLabel];
    [_reasonLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(10);
        make.top.offset(10);
        make.height.offset(20);
    }];
    
}
-(void)isChooseBtn{
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]){ //判断输入的字是否是回车，即按下return
        //在这里做你响应return键的代码
        [textView resignFirstResponder];
        return NO; //这里返回NO，就代表return键值失效，即页面上按下return，不会出现换行，如果为yes，则输入页面会换行
        
    }
    
    return YES;
}
@end
