//
//  OrderListTCTC.h
//  519
//
//  Created by 陈 on 16/9/13.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"
#import "DealOrderModel.h"
@interface OrderListTCTC : BaseTC

@property(nonatomic,strong)DealOrderModel * model;


+(NSString *)getID;
@end
