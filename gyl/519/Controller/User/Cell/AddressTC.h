//
//  AddressTC.h
//  519
//
//  Created by 陈 on 16/9/19.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseTC.h"
#import "AddressModel.h"
@protocol AddressTCDelegate

-(void)editImgOnClicksWithIndex:(NSInteger)cellIndex Ids:(NSString *)ids name:(NSString *)name phone:(NSString *)phone address:(NSString *)address region:(NSString *)region isdefault:(NSString *)isdefault;

-(void)delectOnClickWithIndex:(NSInteger)cellIndex withIds:(NSString *)ids;

-(void)setDefaultIndex:(NSInteger)cellIndex withIds:(NSString *)ids;

@end

@interface AddressTC : BaseTC

@property(nonatomic,strong)id<AddressTCDelegate> delegate;
@property(nonatomic,strong)AddressModel * model;
@property(nonatomic,assign)NSInteger cellIndex;
@property(nonatomic,copy)NSString * ids;
@property(nonatomic,strong)UIButton * defueltBtn;
@property(nonatomic,strong)UIButton * renewBtn;


+(CGFloat)getHeight;

+(NSString *)getID;
@end
