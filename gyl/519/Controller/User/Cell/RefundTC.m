//
//  RefundTC.m
//  519
//
//  Created by Macmini on 16/12/16.
//  Copyright © 2016年 519. All rights reserved.
//

#import "RefundTC.h"

@interface RefundTC()<UITextViewDelegate>
@property(nonatomic,strong)UIView *refundView;
@property(nonatomic,strong)UIButton * isYesBtn;
@property(nonatomic,strong)UIImageView * refundImg;
@property(nonatomic,strong)UILabel * refundNameLabel;
@property(nonatomic,strong)UILabel * refundPriceLabel;
@property(nonatomic,strong)UILabel * refundNumberLabel;
@property(nonatomic,strong)UILabel * refundAttrLabel;
@property(nonatomic,strong)UITextView * whyTextView;
@property(nonatomic,strong)UILabel * norefundLabel;
@property(nonatomic,strong)UIView * lineView;
@property(nonatomic,strong)UIView * lineView2;
@property(nonatomic,copy)NSString * placeholder;
@property(nonatomic,copy)NSString * refundContent;

@property(nonatomic,assign)BOOL iskeyboard;
@property(nonatomic,assign)BOOL isyes;
@property(nonnull,nonatomic,strong) UILabel *status;
@end

@implementation RefundTC

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _iskeyboard = NO;
        for (UIView * subview in self.subviews) {
            if ([subview isKindOfClass:[UIButton class]]) {
                [subview removeFromSuperview];
            }
        }
        [self newInitView];
        self.backgroundColor = APPBGColor;
        
    }
    return self;
}

-(void)newInitView{
    _refundView = [[UIView alloc]init];
    _refundView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_refundView];
    [_refundView makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.offset(0);
        make.height.offset(100);
    }];
    
    _isYesBtn =[[ UIButton alloc]init];
    _isyes = NO;
    [_isYesBtn setImage:[UIImage imageNamed:@"未勾选"] forState:(UIControlStateNormal)];
     [_isYesBtn setImage:[UIImage imageNamed:@"icon_勾选"] forState:(UIControlStateSelected)];
    [_isYesBtn addTarget:self action:@selector(isYesBtnClick:) forControlEvents:(UIControlEventTouchUpInside)];
    [_refundView addSubview:_isYesBtn];
    [_isYesBtn makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(10);
        make.centerY.equalTo(_refundView.mas_centerY);
        make.width.height.offset(20);
    }];
    
    _refundImg = [[UIImageView alloc]init];
    _refundImg.contentMode = UIViewContentModeScaleAspectFit;
    [_refundView addSubview:_refundImg];
    [_refundImg makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_isYesBtn.mas_right).with.offset(10);
        make.top.offset(10);
        make.width.height.offset(80);
    }];
    
    _refundNameLabel = [[UILabel alloc]init];
    _refundNameLabel.textColor = APPFourColor;
    _refundNameLabel.textAlignment = NSTextAlignmentLeft;
    _refundNameLabel.font = [UIFont systemFontOfSize:13];
    _refundNameLabel.numberOfLines = 2;
    [_refundView addSubview:_refundNameLabel];
    [_refundNameLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_refundImg.mas_right).with.offset(10);
        make.top.offset(10);
        make.width.offset(Screen_Width-150);
    }];
    UILabel * refundText = [[UILabel alloc]init];
    refundText.textColor = APPThreeColor;
    refundText.textAlignment = NSTextAlignmentLeft;
    refundText.font = [UIFont systemFontOfSize:13];
    refundText.text = @"退款金额:";
    [_refundView addSubview:refundText];
    [refundText makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_refundImg.mas_right).with.offset(10);
        make.bottom.equalTo(_refundView.mas_bottom).with.offset(-10);
        make.height.offset(20);
    }];
    _refundPriceLabel = [[UILabel alloc]init];
    _refundPriceLabel.textColor = APPColor;
    _refundPriceLabel.textAlignment = NSTextAlignmentLeft;
    _refundPriceLabel.font = [UIFont systemFontOfSize:13];
    [_refundView addSubview:_refundPriceLabel];
    [_refundPriceLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(refundText.mas_right).with.offset(5);
        make.centerY.equalTo(refundText.mas_centerY);
        make.height.offset(20);
    }];
    
    _refundNumberLabel =[[ UILabel alloc]init];
    _refundNumberLabel.textColor = APPThreeColor;
    _refundNumberLabel.textAlignment = NSTextAlignmentLeft;
    _refundNumberLabel.font = [UIFont systemFontOfSize:13];
    [_refundView addSubview:_refundNumberLabel];
    [_refundNumberLabel makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-10);
        make.centerY.equalTo(_refundPriceLabel.mas_centerY);
        make.height.offset(20);
    }];
    UILabel * refundNumTitle = [[UILabel alloc]init];
    refundNumTitle.textColor = APPThreeColor;
    refundNumTitle.textAlignment = NSTextAlignmentLeft;
    refundNumTitle.font = [UIFont systemFontOfSize:13];
    refundNumTitle.text = @"数量:";
    [_refundView addSubview:refundNumTitle];
    [refundNumTitle makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_refundNumberLabel.mas_left).with.offset(-5);
        make.centerY.equalTo(_refundNumberLabel.mas_centerY);
        make.height.offset(20);
    }];
    
    UIView * cententView = [[UIView alloc]init];
    cententView.backgroundColor = [UIColor whiteColor];
    [self addSubview:cententView];
    [cententView makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.top.equalTo(_refundView.mas_bottom).offset(1);
        make.height.offset(110);
    }];
    UILabel* cententTitle = [[UILabel alloc]init];
    cententTitle.textColor = APPFourColor;
    cententTitle.textAlignment = NSTextAlignmentLeft;
    cententTitle.font = [UIFont systemFontOfSize:13];
    cententTitle.text = @"退款原因";
    [cententView addSubview:cententTitle];
    [cententTitle makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(10);
        make.top.offset(8);
        make.height.offset(20);
    }];
    
    _status = cententTitle;
    _whyTextView = [[UITextView alloc]init];
    _whyTextView.editable = YES;
    _whyTextView.delegate = self;
    _whyTextView.textColor = APPFourColor;
    _whyTextView.backgroundColor = APPBGColor;
    _whyTextView.returnKeyType = UIReturnKeyDone;
    _whyTextView.font = [UIFont systemFontOfSize:13];
    _whyTextView.scrollEnabled = NO;
    [cententView addSubview:_whyTextView];
    [_whyTextView makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(10);
        make.top.equalTo(cententTitle.mas_bottom).offset(8);
        make.height.offset(60);
        make.width.offset(Screen_Width-20);
    }];
    
    _norefundLabel = [[UILabel alloc]init];
    _norefundLabel.textColor = APPThreeColor;
    _norefundLabel.text = @"请输入退款原因";
    _norefundLabel.font = [UIFont systemFontOfSize:13];
    _norefundLabel.textAlignment = NSTextAlignmentLeft;
    _norefundLabel.hidden = NO;
    [_whyTextView addSubview:_norefundLabel];
    [_norefundLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(7);
        make.top.equalTo(cententTitle.mas_bottom).offset(10);
        make.height.offset(20);
    }];
}
-(void)isYesBtnClick:(UIButton *)sender{
    [_whyTextView resignFirstResponder];
    sender.selected = !sender.selected;
//    if (sender.selected) {
//
//        _isyes = YES;
        if (self.idBlock) {
            self.idBlock(_refundId,sender.selected);
        }
////      [_isYesBtn setImage:[UIImage imageNamed:@"icon_勾选"] forState:(UIControlStateSelected)];
//    }else{
//        _isyes = NO;
//
//        if (self.idBlock) {
//            self.idBlock(_refundId,NO,self.model);
//
//        }
//
//
//    }
}

-(void)setModel:(DealOrderModel *)model{
    _model = model;
    _refundId = model.ids;
    if (model.isSel) {

        self.isYesBtn.selected = true;
        
    }else{
        self.isYesBtn.selected = false;

    }
    
    
    if (model.refundReson.length > 0) {
        _norefundLabel.hidden = true;
        self.whyTextView.text = model.refundReson;
    }else{
        _norefundLabel.hidden = false;
    }
    
    
    if(model.name.length>0){
        _refundNameLabel.text = model.name;
    }else{
        _refundNameLabel.text = model.sub_name;
    }
    _refundAttrLabel.text = model.attr_str;
    [_refundImg sd_setImageWithURL:[NSURL URLWithString:model.deal_icon]];
    _refundPriceLabel.text = [NSString stringWithFormat:@"￥%@",model.unit_price];
    _refundNumberLabel.text = [NSString stringWithFormat:@"%@",model.number];
    
    NSString * keyi_refund = [NSString stringWithFormat:@"%@",model.keyi_refund];
    
    if ([keyi_refund isEqualToString:@"1"]) {
        _whyTextView.hidden = NO;
        _isYesBtn.hidden = NO;
        _norefundLabel.hidden = NO;
        _norefundLabel.textColor = APPThreeColor;
        _norefundLabel.text = @"请输入退款原因";
        _whyTextView.userInteractionEnabled = true;
        if (model.refundReson&&model.refundReson.length >= 0) {
            _norefundLabel.hidden = YES;
            _whyTextView.text = model.refundReson;
        }else{
            _norefundLabel.hidden = NO;
        }
//        _norefundLabel.hidden = NO;
    }
    if ([keyi_refund isEqualToString:@"0"]) {
        _norefundLabel.hidden = NO;
        _norefundLabel.text = model.refund_status_tip;
        _norefundLabel.textColor = APPFourColor;
        _whyTextView.hidden = YES;

        _isYesBtn.hidden = YES;
        _whyTextView.userInteractionEnabled = false;
    }
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    _iskeyboard = YES;
    _norefundLabel.hidden = YES;
    return YES;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    _iskeyboard = NO;
    
    if ([textView.text isEqualToString:@""]) {
        _norefundLabel.hidden = NO;
    }
    if (textView.text.length>0) {
        _refundContent = textView.text;
        self.model.refundReson = _refundContent;
    }
   
//        if (self.block) {
//            self.block(_model.refundReson,_refundId);
//        }
    return YES;
}
-(void)textViewDidChangeSelection:(UITextView *)textView{
    NSLog(@"%f",textView.frame.origin.y);
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{

    [_whyTextView resignFirstResponder];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    _model.refundReson = textView.text;
    if ([text isEqualToString:@"\n"]){ //判断输入的字是否是回车，即按下return
        //在这里做你响应return键的代码
        [textView resignFirstResponder];
        
//        if (self.block) {
//            self.block(textView.text,_refundId);
//        }
        return NO; //这里返回NO，就代表return键值失效，即页面上按下return，不会出现换行，如果为yes，则输入页面会换行
    }
    return YES;
}




+(NSString *)getID{
    return @"RefundTC";
}

@end
