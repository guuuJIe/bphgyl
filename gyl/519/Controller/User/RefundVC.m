//
//  RefundVC.m
//  519
//
//  Created by Macmini on 16/12/13.
//  Copyright © 2016年 519. All rights reserved.
//

#import "RefundVC.h"
#import "RefundTC.h"
#import "DealOrderModel.h"
@interface RefundVC ()<UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
@property(nonatomic,strong)UIButton * backBtn;
@property(nonatomic,strong)UITableView * tableView;
@property(nonatomic,strong)UIView * refundView;

@property(nonatomic,strong)NSMutableArray * dataMuArr;
@property(nonatomic,copy)NSString * item_ids;
@property(nonatomic,copy)NSString * itemidsstr;
@property(nonatomic,copy)NSString * contents;
@property(nonatomic,strong)NSMutableArray * idsArr;
@property(nonatomic,strong)NSMutableArray * contentArr;

@end

@implementation RefundVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"退款申请";
    _dataMuArr = [NSMutableArray new];
    self.idsArr = [NSMutableArray new];
    self.contentArr = [NSMutableArray new];
    self.view.backgroundColor = APPBGColor;
    [self.navigationController setNavigationBarHidden:false animated:true];
  
    [self initView];
    [self addData];
}

-(void)addData{
    NSString * refundUrl = [NSString stringWithFormat:@"%@&ctl=uc_order&act=refund&order_id=%@&email=%@&pwd=%@",GYLUrl,_orderId,EMAIL,USER_PWD];
    [RequestData requestDataOfUrl:refundUrl success:^(NSDictionary *dic) {

        _refundArr = dic[@"items"];
        for (NSDictionary * subdic in dic[@"items"]) {
            DealOrderModel * model = [DealOrderModel new];
            [model setValuesForKeysWithDictionary:subdic];
            [_dataMuArr addObject:model];
        }
      
        [_tableView reloadData];
        
    } failure:^(NSError *error) {
        
    }];
}


-(void)initView{
    self.edgesForExtendedLayout = UIRectEdgeNone;
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-Screen_NavBarHeight-Screen_StatusBarHeight-50)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_tableView registerClass:[RefundTC class] forCellReuseIdentifier:@"RefundTC"];
    self.tableView.backgroundColor=APPBGColor;
    [self.view addSubview:_tableView];
    
    _refundView = [[UIView alloc]initWithFrame:CGRectMake(0, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight-50-BottomAreaHeight, Screen_Width, 50+BottomAreaHeight)];

    _refundView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_refundView];
    
    UIView * lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 0.5)];
    lineView.backgroundColor = APPBGColor;
    [_refundView addSubview:lineView];
    
    UIButton *sureBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 50)];
    [sureBtn setBackgroundColor:APPColor];
    [sureBtn setTitle:@"提交" forState:(UIControlStateNormal)];
    [sureBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    sureBtn.titleLabel.font = [UIFont systemFontOfSize:FONT_SIZE_L];
    [sureBtn addTarget:self action:@selector(runUp) forControlEvents:(UIControlEventTouchUpInside)];
    [_refundView addSubview:sureBtn];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 210;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataMuArr.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *Identifier =@"RefundTC";
    
    RefundTC * cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (cell == nil) {
        cell = [[RefundTC alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:Identifier];
    }
    
    DealOrderModel * model = self.dataMuArr[indexPath.row];
    cell.model = model;
    cell.index = indexPath.row;
    
    [self reloadCurrentCell:cell andModel:model withNSindexPath:indexPath];
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
  
    return cell;
}

- (void)reloadCurrentCell:(RefundTC *)cell andModel:(DealOrderModel *)model withNSindexPath:(NSIndexPath *)indexPath{
    __weak typeof(self)weself = self;
    cell.idBlock = ^(NSString *ids, BOOL isYes) {
        __strong typeof(weself)self = weself;
        model.isSel = !model.isSel;
        if (isYes) {
            [self.idsArr addObject:ids];
        }else{
//            for (NSString * ids in self.idsArr) {//还边遍历边删除
                if ([self.idsArr containsObject:ids]) {
                     [self.idsArr removeObject:ids];
                }

//            }
        }
        [self.tableView reloadData];
//        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:indexPath.row inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        
    };
    cell.block = ^(NSString *content, NSString *ids) {
        __strong typeof(weself)self = weself;
        if (self.contentArr.count < 1) {
            if (content.length == 0) {
//                [MBProgressHUD showMessag:@"" toView:<#(UIView *)#>]
            }else{
                [self.contentArr addObject:@{ids:content}];
                [self.contentArr addObject:@{ids:content}];
            }
          
            NSLog(@"%@",_contentArr);
        }else{
            for (NSInteger i =0; i<self.contentArr.count; i++) {
                NSDictionary * dic = self.contentArr[i];
                if ([dic.allKeys containsObject:ids]) {
                    [self.contentArr removeObject:dic];
                }
            }

            //            [self.contentArr addObject:@{ids:content}];
        }
    };
}

-(void)runUp{

    [self changeArrayToJson];
    NSString * refundUrl = [NSString stringWithFormat:@"%@ctl=Uc_order&act=do_refund&item_ids=%@&contents=%@&email=%@&pwd=%@",GYLUrl,_itemidsstr,_contents,EMAIL,USER_PWD];

    if(_itemidsstr){
        if ([_contents rangeOfString:@"退款原因"].location != NSNotFound || _contents.length<5) {
            [MBProgressHUD showError:@"请输入退款理由" toView:self.view];
        }else{
            [RequestData requestDataOfUrl:refundUrl success:^(NSDictionary *dic) {
                if ([dic[@"status"] isEqual:@1]) {
                    [MBProgressHUD showSuccess:dic[@"info"] toView:self.view];
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"refundAction" object:nil];
                    double delayInSeconds = 1.0;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                        [self.navigationController popViewControllerAnimated:YES];
                    });
                }else{
                    [MBProgressHUD showSuccess:dic[@"info"] toView:self.view];
                }
            } failure:^(NSError *error) {
                [MBProgressHUD showError:@"网络错误" toView:self.view];
            }];
        }
    }else{
        [MBProgressHUD showError:@"请选择商品" toView:self.view];
    }
}


-(void)changeArrayToJson{
    
//    [self.idsArr removeAllObjects];
    [self.contentArr removeAllObjects];
    
    for (int i = 0; i<self.dataMuArr.count; i++) {
        DealOrderModel *model = self.dataMuArr[i];
//        [self.contentArr addObject:];
        if (model.isSel) {
            [self.contentArr addObject:@{model.ids:model.refundReson.length > 0?model.refundReson:@""}];
        }
        
    }
    
    
    NSData *data=[NSJSONSerialization dataWithJSONObject:_idsArr options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStr=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    for ( NSDictionary * subdic in _contentArr) {
        [dic addEntriesFromDictionary:subdic];
    }
    NSArray * valuesArr = dic.allValues;
    NSData *datas=[NSJSONSerialization dataWithJSONObject:valuesArr options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonStrs=[[NSString alloc]initWithData:datas encoding:NSUTF8StringEncoding];
    
    _itemidsstr = jsonStr;
    _contents = jsonStrs;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



@end
