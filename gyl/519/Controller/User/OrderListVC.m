//
//  OrderListVC.m
//  519
//
//  Created by 陈 on 16/9/13.
//  Copyright © 2016年 519. All rights reserved.
//

#import "OrderListVC.h"
#import "OrderListTC.h"
#import "OrderListModel.h"
#import "OrderPayVC.h"
#import "OrderDetailVC.h"
#import "RefundVC.h"
#import "GoodsEvaluationVC.h"
#import "GoodInfoVC.h"
#import "MKPAlertView.h"
@interface OrderListVC()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,orderBtnOnClickDelegate>
@property(nonatomic,strong)UIButton *backBtn;

@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,copy)NSString * email;
@property(nonatomic,copy)NSString * pwd;
@property(nonatomic,strong)NSMutableArray * orderMuArr;
@property(nonatomic,assign)NSInteger  inter;
@property(nonatomic,assign)NSInteger  page;
@property(nonatomic,copy)NSString * status;
@end

@implementation OrderListVC
- (void)viewDidLoad {
    [super viewDidLoad];
    [MBProgressHUD showHUD];
    _page = 1;
    _email = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    _pwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
    _orderMuArr = [NSMutableArray new];
    
    [self initView];
    [self requestDataForAllOrder];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(deleteOrder:) name:@"deleteOrder" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refundOrder:) name:@"refundAction" object:nil];
}



-(void)listpopVC{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (BOOL)navigationShouldPopOnBackButton {
    MKPAlertView *alertView = [[MKPAlertView alloc]initWithTitle:@"订单已生成,确定取消支付" message:nil sureBtn:@"确认" cancleBtn:@"取消"];
    alertView.resultIndex = ^(NSInteger index)
    {
        
    };
    [alertView showMKPAlertView];
    return NO;
}

-(void)refundOrder:(NSNotification *)noti{
    [self refreshDataForOrder];
    
}

-(void)deleteOrder:(NSNotification *)noti{
    NSInteger index = [noti.object integerValue];
    [_orderMuArr removeObjectAtIndex:index];
    [_tableView reloadData];
//    [self refreshDataForOrder];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"deleteOrder" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"refundAction" object:nil];
}


-(void)requestDataForAllOrder{
    if ([self.typeTitle isEqualToString:@"全部"]) {
        _status = @"0";
    }else if ([self.typeTitle isEqualToString:@"待付款"]) {
        _status = @"1";
    }else if ([self.typeTitle isEqualToString:@"待配送"]) {
        _status = @"2";
    }else if ([self.typeTitle isEqualToString:@"待评价"]) {
        _status = @"3";
    }else if ([self.typeTitle isEqualToString:@"售后"]){
        _status = @"4";
    }else{
        _status = @"0";
    }
    
    NSString * orderUrl = [NSString stringWithFormat:@"%@ctl=uc_order&status=%@&page=%ld&email=%@&pwd=%@",GYLUrl,_status,(long)_page,_email,_pwd];
    [RequestData requestDataOfUrl:orderUrl success:^(NSDictionary *dic) {
       
        NSArray * itemArr= dic[@"item"];
        if (itemArr.count>0) {
            for (NSDictionary * subdic in itemArr) {
                OrderListModel * model = [OrderListModel new];
                [model setValuesForKeysWithDictionary:subdic];
                [_orderMuArr addObject:model];
            }
            [self showData];
        }else{
            if (_page == 1) {
                [self showData];
            }else{
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
                
            }
            
        }
        [MBProgressHUD dissmiss];
    } failure:^(NSError *error) {
        [MBProgressHUD dissmiss];
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}

-(void)initView{
    self.view.backgroundColor=[UIColor whiteColor];
    
    self.tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-Screen_NavBarHeight-Screen_StatusBarHeight-40)];
    [self.tableView registerClass:[OrderListTC class] forCellReuseIdentifier:[OrderListTC getID]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.backgroundColor=APPBGColor;

    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshDataForOrder)];
    header.lastUpdatedTimeLabel.hidden = YES;
    self.tableView.mj_header = header;
    
    MJRefreshAutoNormalFooter *footer= [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        _page++;
        [self loadData];
    }];
    self.tableView.mj_footer = footer;

    
    [self.view addSubview:self.tableView];
}

-(void)refreshDataForOrder{
    if (_orderMuArr) {
        [_orderMuArr removeAllObjects];
    }else{
        _orderMuArr = [NSMutableArray new];
    }
    
    _page = 1;
    [self requestDataForAllOrder];
}

-(void)loadData{
    
    [self requestDataForAllOrder];
    [self showData];
}

-(void)showData{
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    [self.tableView reloadData];
}


#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
        return self.orderMuArr.count;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderListModel * model = _orderMuArr[indexPath.row];
    
    
    NSString *cellIDE = [NSString stringWithFormat:@"%@%ld",[OrderListTC getID],(unsigned long)model.deal_order_item.count];
    OrderListTC *cell=[tableView dequeueReusableCellWithIdentifier:cellIDE];
    
    if(cell==nil){
        cell=[[OrderListTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIDE];
    }
    
    cell.delegate = self;
    cell.model = _orderMuArr[indexPath.row];
    cell.index = indexPath.row;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderListModel * model = _orderMuArr[indexPath.row];
    _inter = model.deal_order_item.count;
    return Default_Space+35+80+100*_inter +Default_Line*2;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderListModel * model = _orderMuArr[indexPath.row];
    [self orderDetail:model.ids withTitle:model.status withCellIndex:indexPath.row];
}

#pragma mark orderOnclickDelegate
-(void)nowPay:(NSString *)orderId{
    OrderPayVC * vc = [OrderPayVC new];
    vc.paytype = @"nowPay";
    vc.orderId = orderId;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)deleteOrderId:(NSString *)orderid withindex:(NSInteger )index{
    [_orderMuArr removeObjectAtIndex:index];
    [self.tableView reloadData];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSString * deleteUrl = [NSString stringWithFormat:@"%@ctl=uc_order&act=cancel&id=%@&email=%@&pwd=%@",GYLUrl,orderid,_email,_pwd];
        [RequestData requestDataOfUrl:deleteUrl success:^(NSDictionary *dic) {
            
            if (dic[@"info"]) {
                [MBProgressHUD showSuccess:dic[@"info"] toView:self.view];
            }
        } failure:^(NSError *error) {
            NSLog(@"error");
        }];
    });
    
}

-(void)orderDetail:(NSString *)detail withTitle:(NSString *)title withCellIndex:(NSInteger)inter{
    OrderDetailVC * vc = [OrderDetailVC new];
//    OrderListModel * model = _orderMuArr[inter];
    vc.orderDetailId = detail;
    vc.vcName = title;
    vc.index = inter;
//    vc.detailArr = model.deal_order_item;
    [self.navigationController pushViewController:vc animated:YES];
}


-(void)refundFor:(NSString *)orderId withrefund:(NSArray *)refundArr{
    RefundVC * vc = [RefundVC new];
    vc.orderId = orderId;
    vc.refundArr = refundArr;
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)goodsEvaluationwithGoodsData:(NSArray *)evaluationArr withEvaluationId:(NSString *)ids{
    NSLog(@"evaluationArr -- %@",evaluationArr);
    GoodsEvaluationVC * vc = [GoodsEvaluationVC new];
    vc.goodsEvaluationDataArr = evaluationArr;
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)buyOne:(NSString *)ids{
    GoodInfoVC * vc = [GoodInfoVC new];
    vc.goodsId = ids;
    [self.navigationController pushViewController:vc animated:YES];
}


@end
