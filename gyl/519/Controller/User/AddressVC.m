//
//  AddressVC.m
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//

#import "AddressVC.h"
#import "AddressTC.h"
#import "AddressEditVC.h"
#import "AddressModel.h"

@interface AddressVC()<UITableViewDataSource,UITableViewDelegate,AddressTCDelegate,UIAlertViewDelegate>
@property(nonatomic,strong)UIButton *backBtn;
@property(nonatomic,strong)UIButton *barRightBtn;
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)UILabel * neverLabel;
@property(nonatomic,strong)NSMutableArray * addressMuArray;
@property(nonatomic,strong)NSMutableArray * btnMuarr;

@property(nonatomic,assign)NSInteger removeinter;
@property(nonatomic,strong)NSString * removeIds;
@end

@implementation AddressVC

-(void)viewDidLoad{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    _addressMuArray = [NSMutableArray new];
    _btnMuarr = [NSMutableArray new];
    
    [self initBar];
    [self initView];
    [self requestDataOfAddress];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(editUserInfo) name:@"editImageOnClick" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshAddress) name:@"saveAddress" object:nil];
}

-(void)editUserInfo{
    AddressEditVC *vc=[[AddressEditVC alloc] init];
    vc.typeStr=@"编辑地址";
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

-(void)initBar{
    self.title = @"地址管理";
    UIView * addView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 70, 30)];
    UIButton * addBtn = [[UIButton alloc]initWithFrame:addView.bounds];
    [addBtn setTitle:@"新增地址" forState:(UIControlStateNormal)];
    addBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [addBtn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
    [addBtn addTarget:self action:@selector(barItemBtnOnClick) forControlEvents:(UIControlEventTouchUpInside)];
    [addView addSubview:addBtn];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:addView];;
}

//请求地址
-(void)requestDataOfAddress{

    NSString * addressUrl = [NSString stringWithFormat:@"%@ctl=uc_address&email=%@&pwd=%@",GYLUrl,EMAIL,USER_PWD];
    [RequestData requestDataOfUrl:addressUrl success:^(NSDictionary *dic) {
        if (dic[@"consignee_list"] != nil) {
            for (NSDictionary * subdic in dic[@"consignee_list"]) {
                AddressModel * model = [AddressModel new];
                [model setValuesForKeysWithDictionary:subdic];
                [self.addressMuArray addObject:model];
            }
        }
        [self showData];
        
    } failure:^(NSError *error) {
        
    }];
}

-(void)initView{
    self.tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-Screen_NavBarHeight-Screen_StatusBarHeight)];
    [self.tableView registerClass:[AddressTC class] forCellReuseIdentifier:[AddressTC getID]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.showsVerticalScrollIndicator=NO;
    self.tableView.backgroundColor=APPBGColor;
    
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshAddress)];
    header.lastUpdatedTimeLabel.hidden = YES;
    self.tableView.mj_header = header;
    
    [self.view addSubview:self.tableView];
    
    
}

-(void)refreshAddress{
    [self.addressMuArray removeAllObjects];
    [self.btnMuarr removeAllObjects];
    [self requestDataOfAddress];
}

-(void)showData{
    if (self.addressMuArray.count>0) {
        if (_neverLabel) {
            [_neverLabel removeFromSuperview];
        }
        [self.tableView.mj_header endRefreshing];
        [self.tableView reloadData];
    }else{
        [self setNeverLabel];
    }
}
-(void)setNeverLabel{
    self.tableView.hidden = YES;
    _neverLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 150, 30)];
    _neverLabel.text = @"还没有添加地址";
    _neverLabel.center = self.view.center;
    _neverLabel.font = [UIFont systemFontOfSize:14];
    _neverLabel.textColor = APPFourColor;
    _neverLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:_neverLabel];
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.addressMuArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AddressTC *cell=[tableView dequeueReusableCellWithIdentifier:[AddressTC getID] forIndexPath:indexPath];
    
    if(cell==nil){
        cell=[[AddressTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[AddressTC getID]];
    }
    cell.delegate=self;
    AddressModel * model = _addressMuArray[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = model;
    [_btnMuarr addObject:cell.defueltBtn];
    
    cell.cellIndex = indexPath.row;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [AddressTC getHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.pushStr isEqualToString:@"0"]) {
        
        AddressModel * model = _addressMuArray[indexPath.row];
        NSString * address = [NSString stringWithFormat:@"%@ %@ %@ %@",model.region_lv2_name,model.region_lv3_name,model.region_lv4_name,model.address];
        self.pushBlock(model.consignee,model.mobile,address,model.ids);
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [self setDefaultIndex:indexPath.row withIds:model.ids];
        });
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
//        AddressModel * model = _addressMuArray[indexPath.row];
//        AddressEditVC *vc=[[AddressEditVC alloc] init];
//        vc.typeStr=@"编辑地址";
//        vc.nameStr = model.consignee;
//        vc.phoneStr = model.mobile;
//        vc.isdefualt = model.is_default;
//        vc.regoinStr = [NSString stringWithFormat:@"%@ %@ %@",model.region_lv2_name,model.region_lv3_name,model.region_lv4_name];
//        vc.addressStr = model.address;
//        vc.ids = model.ids;
//        vc.isedit = @"1";
//        [self tabHidePushVC:vc];
    }
    
}


#pragma mark bar delegate
-(void)barItemBtnOnClick{
    AddressEditVC *vc=[[AddressEditVC alloc] init];
    vc.typeStr=@"添加地址";
    vc.isedit = @"0";
   [self.navigationController pushViewController:vc animated:YES];
}



-(void)editImgOnClicksWithIndex:(NSInteger)cellIndex Ids:(NSString *)ids name:(NSString *)name phone:(NSString *)phone address:(NSString *)address region:(NSString *)region isdefault:(NSString *)isdefault{
    AddressEditVC *vc=[[AddressEditVC alloc] init];
    vc.typeStr=@"编辑地址";
    
    vc.nameStr = name;
    vc.phoneStr = phone;
    vc.isdefualt = isdefault;
    vc.regoinStr = region;
    vc.addressStr = address;
    vc.ids = ids;
    vc.isedit = @"1";
    
   [self.navigationController pushViewController:vc animated:YES];
}
//删除地址
-(void)delectOnClickWithIndex:(NSInteger)cellIndex withIds:(NSString *)ids{
    _removeinter = cellIndex;
    _removeIds = ids;
    UIAlertController * alertVC = [UIAlertController alertControllerWithTitle:@"提示" message:@"是否确定删除"  preferredStyle:(UIAlertControllerStyleAlert)];
    [alertVC addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        [self deleteAddress];
    }]];
    [alertVC addAction:[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
       
    }]];
    [self presentViewController:alertVC animated:YES completion:nil];

}

-(void)deleteAddress{
    [_addressMuArray removeObjectAtIndex:_removeinter];
    [self.tableView reloadData];
    
    NSString * deleteUrl = [NSString stringWithFormat:@"%@ctl=uc_address&act=del&id=%@&email=%@&pwd=%@",GYLUrl,_removeIds,EMAIL,USER_PWD];
    [RequestData requestDataOfUrl:deleteUrl success:^(NSDictionary *dic) {
        
        if ([dic[@"status"] isEqual:@1]) {
            [MBProgressHUD showSuccess:@"删除成功" toView:self.view];
            if (_addressMuArray.count == 0) {
                [self setNeverLabel];
            }
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}


-(void)setDefaultIndex:(NSInteger)cellIndex withIds:(NSString *)ids{
    for (NSInteger i = 0; i<_btnMuarr.count; i++) {
        UIButton * btn = _btnMuarr[i];
        if (i != cellIndex) {
            btn.selected = NO;
            btn.layer.borderColor = APPFourColor.CGColor;
            [btn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
            btn.size = CGSizeMake(70, 20);
        }else{
            btn.selected = YES;
            btn.layer.borderColor = APPColor.CGColor;
            [btn setTitleColor:APPColor forState:(UIControlStateNormal)];
            btn.size = CGSizeMake(50, 20);
        }
    }
    NSString * defUrl = [NSString stringWithFormat:@"%@ctl=uc_address&act=set_default&id=%@&email=%@&pwd=%@",GYLUrl,ids,EMAIL,USER_PWD];
    [RequestData requestDataOfUrl:defUrl success:^(NSDictionary *dic) {
        
    } failure:^(NSError *error) {
        
    }];
}

@end
