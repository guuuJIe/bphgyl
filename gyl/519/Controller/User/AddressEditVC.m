//
//  AddressEditVC.m
//  519
//
//  Created by 陈 on 16/9/19.
//  Copyright © 2016年 519. All rights reserved.
//

#import "AddressEditVC.h"
#import "AddressLocationVC.h"
#import "DQAreasView.h"
#import "DQAreasModel.h"

@interface AddressEditVC()<UITextFieldDelegate,DQAreasViewDelegate,UITextViewDelegate>
{
    CGFloat _locationX;
    CGFloat _locationY;
    CGFloat _infoAddress_x;
}
@property(nonatomic,strong)UIButton *backBtn;
@property(nonatomic,strong)UIButton *barRightBtn;
@property (nonatomic, strong) UIView *DQbackgroundView;
@property (nonatomic, strong) DQAreasView *areasView;//所在地

@property(nonatomic,strong)TPKeyboardAvoidingScrollView *scrollView;

@property(nonatomic,strong)UILabel *nameLabel;
@property(nonatomic,strong)UITextField *nameText;
@property(nonatomic,strong)UIView *nameLine;
@property(nonatomic,strong)UILabel *phoneLabel;
@property(nonatomic,strong)UITextField *phoneText;
@property(nonatomic,strong)UIView *phoneLine;
@property(nonatomic,strong)UILabel *addressLabel;
@property(nonatomic,strong)UIImageView *addressImg;
@property(nonatomic,strong)UITextField *addressText;
@property(nonatomic,strong)UIImageView *addressRightImg;
@property(nonatomic,strong)UIView *addressLine;
@property(nonatomic,strong)UILabel *infoAddressLabel;
/**
@property(nonatomic,strong)UIView * infoView;
@property(nonatomic,strong)UILabel * infoLable;
@property(nonatomic,strong)UIView *infoAddressLine;
*/
@property(nonatomic,strong)UITextField * doorNumberText;
@property(nonatomic,strong)UIView *doorNumberLine;

@property(nonatomic,strong)UILabel *defaultLabel;
@property(nonatomic,strong)UIButton *defaultBtn;
@property(nonatomic,strong)UIView *defaultLine;

@property(nonatomic,strong)UIButton *deleteBtn;

@property(nonatomic,copy)NSString * email;
@property(nonatomic,copy)NSString * pwd;

@property(nonatomic,copy)NSString * region_lv2_name;
@property(nonatomic,copy)NSString * region_lv3_name;
@property(nonatomic,copy)NSString * region_lv4_name;
@property(nonatomic,copy)NSString * isdef;

@end

@implementation AddressEditVC
-(void)viewDidLoad{
    [super viewDidLoad];
    
    self.pwd = [[NSUserDefaults standardUserDefaults]objectForKey:@"user_pwd"];
    self.email = [[NSUserDefaults standardUserDefaults]objectForKey:@"email"];
    [self initBar];
    [self initView];
}

-(void)initBar{
    self.title = self.typeStr;
    self.areasView = [DQAreasView new];
    self.areasView.delegate = self;
}

-(void)initView{
    self.scrollView=[[TPKeyboardAvoidingScrollView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-Screen_StatusBarHeight-Screen_NavBarHeight)];
    self.scrollView.showsVerticalScrollIndicator=NO;
    self.scrollView.backgroundColor=[UIColor whiteColor];
    self.scrollView.bounces=NO;
    self.view.backgroundColor = APPBGColor;
    UIFont *nameFont=[UIFont systemFontOfSize:FONT_SIZE_M];
    UIColor *nameColor=APPFourColor;
    UIColor *nameTintColor=APPColor;
    CGFloat nameHeight=20;
    
    NSString *nameStr=@"收货人:";
    self.nameLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, Default_Space, [NSString sizeWithText:nameStr font:nameFont maxSize:Max_Size].width, nameHeight)];
    self.nameLabel.font=nameFont;
    self.nameLabel.textColor=nameColor;
    self.nameLabel.text=nameStr;
    self.nameText=[[UITextField alloc]initWithFrame:CGRectMake(self.nameLabel.frame.size.width+self.nameLabel.frame.origin.x+Default_Space, self.nameLabel.frame.origin.y, Screen_Width-self.nameLabel.frame.size.width-Default_Space*3, nameHeight)];
    self.nameText.textColor=nameColor;
    self.nameText.tintColor=nameTintColor;
    self.nameText.placeholder=@"您的姓名";
    self.nameText.font=nameFont;
    self.nameLine=[[UIView alloc] initWithFrame:CGRectMake(0, self.nameText.frame.size.height+self.nameText.frame.origin.y+Default_Space, Screen_Width, Default_Line)];
    self.nameLine.backgroundColor=APPBGColor;
    
    NSString *phoneStr=@"手机:";
    self.phoneLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, self.nameLine.frame.size.height+self.nameLine.frame.origin.y+Default_Space, [NSString sizeWithText:phoneStr font:nameFont maxSize:Max_Size].width, nameHeight)];
    self.phoneLabel.font=nameFont;
    self.phoneLabel.textColor=nameColor;
    self.phoneLabel.text=phoneStr;
    self.phoneText=[[UITextField alloc]initWithFrame:CGRectMake(self.phoneLabel.frame.size.width+self.phoneLabel.frame.origin.x+Default_Space, self.phoneLabel.frame.origin.y, Screen_Width-self.phoneLabel.frame.size.width-Default_Space*3, nameHeight)];
    self.phoneText.textColor=nameColor;
    self.phoneText.tintColor=nameTintColor;
    self.phoneText.placeholder=@"配送员联系您的方式";
    self.phoneText.font=nameFont;
    self.phoneLine=[[UIView alloc] initWithFrame:CGRectMake(0, self.phoneText.frame.size.height+self.phoneText.frame.origin.y+Default_Space, Screen_Width, Default_Line)];
    self.phoneLine.backgroundColor=APPBGColor;
    
    NSString *addressStr=@"选择区域:";
    self.addressLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, self.phoneLine.frame.size.height+self.phoneLine.frame.origin.y+Default_Space, [NSString sizeWithText:addressStr font:nameFont maxSize:Max_Size].width, nameHeight)];
    self.addressLabel.font=nameFont;
    self.addressLabel.textColor=nameColor;
    self.addressLabel.text=addressStr;

    self.addressText=[[UITextField alloc]initWithFrame:CGRectMake(self.addressLabel.frame.size.width+self.addressLabel.frame.origin.x+Default_Space, self.addressLabel.frame.origin.y, Screen_Width-self.addressLabel.frame.size.width-Default_Space*5-20, nameHeight)];
    self.addressText.textColor=nameColor;
    self.addressText.tintColor=nameTintColor;
    self.addressText.placeholder=@"选择收货区域";
    self.addressText.font=nameFont;
    self.addressText.delegate=self;
    
    self.addressRightImg=[[UIImageView alloc]initWithFrame:CGRectMake(Screen_Width-40, self.addressText.frame.origin.y, nameHeight, nameHeight)];
    self.addressRightImg.image=[UIImage imageNamed:@"right_black_icon"];
    self.addressLine=[[UIView alloc] initWithFrame:CGRectMake(0, self.addressText.frame.size.height+self.addressText.frame.origin.y+Default_Space, Screen_Width, Default_Line)];
    self.addressLine.backgroundColor=APPBGColor;
    
    /**定位UI
    self.infoView = [[UIView alloc]initWithFrame:CGRectMake(0, self.addressLine.frame.origin.y+self.addressLine.frame.size.height, Screen_Width-self.addressText.frame.origin.x-20, 30)];
    [self.scrollView addSubview:self.infoView];
    
    NSString * sendStr = @"选择定位:";
    UILabel * sendLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, [NSString sizeWithText:sendStr font:nameFont maxSize:Max_Size].width, nameHeight)];
    sendLabel.font=nameFont;
    sendLabel.textColor=nameColor;
    sendLabel.text=sendStr;
    
    _infoAddress_x = sendLabel.frame.size.width+sendLabel.frame.origin.x + Default_Space;
    self.infoLable = [[UILabel alloc]initWithFrame:CGRectMake(_infoAddress_x,10,100,nameHeight)];
    self.infoLable.text = @"选择定位";
    self.infoLable.textColor = APPThreeColor;
    self.infoLable.textAlignment = NSTextAlignmentLeft;
    self.infoLable.font = nameFont;
    self.infoLable.lineBreakMode = NSLineBreakByTruncatingTail;
    
    UIImageView * imgV = [[UIImageView alloc]initWithFrame:CGRectMake(Screen_Width-40, 10, nameHeight, nameHeight)];
    imgV.image=[UIImage imageNamed:@"right_black_icon"];
    
    
    [self.infoView addSubview:sendLabel];
    [self.infoView addSubview:self.infoLable];
    [self.infoView addSubview:imgV];
    UITapGestureRecognizer * maptap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(jumpMapTap)];
    [self.infoView addGestureRecognizer:maptap];
    
    
    self.infoAddressLine =[[UIView alloc] initWithFrame:CGRectMake(0, self.infoView.frame.size.height+self.infoView.frame.origin.y+Default_Space, Screen_Width, Default_Line)];
    self.infoAddressLine.backgroundColor=APPBGColor;
    [self.scrollView addSubview:self.infoAddressLine];
    */
    
    NSString *infoAddressStr=@"详细地址:";
    self.infoAddressLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, self.addressLine.frame.size.height+self.addressLine.frame.origin.y+Default_Space, [NSString sizeWithText:infoAddressStr font:nameFont maxSize:Max_Size].width, nameHeight)];
    self.infoAddressLabel.font=nameFont;
    self.infoAddressLabel.textColor=nameColor;
    self.infoAddressLabel.text=infoAddressStr;
    self.doorNumberText=[[UITextField alloc]initWithFrame:CGRectMake(self.infoAddressLabel.frame.size.width+self.infoAddressLabel.frame.origin.x+Default_Space, self.infoAddressLabel.frame.origin.y, Screen_Width-self.infoAddressLabel.frame.size.width-Default_Space*3, nameHeight)];
    self.doorNumberText.textColor=nameColor;
    self.doorNumberText.tintColor=nameTintColor;
    self.doorNumberText.placeholder=@"补充地址详细信息";
    self.doorNumberText.font=nameFont;
    self.doorNumberLine=[[UIView alloc] initWithFrame:CGRectMake(0, self.doorNumberText.frame.size.height+self.doorNumberText.frame.origin.y+Default_Space, Screen_Width, Default_Line)];
    self.doorNumberLine.backgroundColor=APPBGColor;
    
    NSString *dafaultStr=@"设置为默认地址:";
    self.defaultLabel=[[UILabel alloc]initWithFrame:CGRectMake(Default_Space, self.doorNumberLine.frame.size.height+self.doorNumberLine.frame.origin.y+Default_Space, [NSString sizeWithText:dafaultStr font:nameFont maxSize:Max_Size].width, nameHeight)];
    self.defaultLabel.font=nameFont;
    self.defaultLabel.textColor=nameColor;
    self.defaultLabel.text=dafaultStr;
    self.defaultBtn=[[UIButton alloc]initWithFrame:CGRectMake(self.defaultLabel.frame.size.width+self.defaultLabel.frame.origin.x+Default_Space, self.defaultLabel.frame.origin.y-5, 30, 30)];
    [self.defaultBtn setBackgroundImage:[UIImage imageNamed:@"close_icon"] forState:UIControlStateNormal];
    [self.defaultBtn setBackgroundImage:[UIImage imageNamed:@"open_icon"] forState:UIControlStateSelected];
    [self.defaultBtn setSelected:NO];
    _isdef = @"0";
    [self.defaultBtn addTarget:self action:@selector(defaultBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.defaultLine=[[UIView alloc] initWithFrame:CGRectMake(0, self.defaultLabel.frame.size.height+self.defaultLabel.frame.origin.y+Default_Space, Screen_Width, Default_Line)];
    self.defaultLine.backgroundColor=APPBGColor;
    
    self.deleteBtn=[[UIButton alloc]initWithFrame:CGRectMake(Default_Space, self.defaultLine.frame.size.height+self.defaultLine.frame.origin.y+Default_Space,Screen_Width-Default_Space*2, 35)];
    self.deleteBtn.backgroundColor=APPColor;
    [self.deleteBtn setTitle:@"保存" forState:UIControlStateNormal];
    self.deleteBtn.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    [self.deleteBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.deleteBtn addTarget:self action:@selector(saveBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:self.nameLabel];
    [self.scrollView addSubview:self.nameText];
    [self.scrollView addSubview:self.nameLine];
    [self.scrollView addSubview:self.phoneLabel];
    [self.scrollView addSubview:self.phoneText];
    [self.scrollView addSubview:self.phoneLine];
    [self.scrollView addSubview:self.addressLabel];
    [self.scrollView addSubview:self.addressText];
    [self.scrollView addSubview:self.addressRightImg];
    [self.scrollView addSubview:self.addressLine];
    [self.scrollView addSubview:self.infoAddressLabel];
    [self.scrollView addSubview:self.doorNumberText];
    [self.scrollView addSubview:self.doorNumberLine];
    [self.scrollView addSubview:self.defaultLabel];
    [self.scrollView addSubview:self.defaultBtn];
    [self.scrollView addSubview:self.defaultLine];
    [self.scrollView addSubview:self.deleteBtn];
    
    [self.view addSubview:self.scrollView];
    
    [self loadData];
}

-(void)loadData{

    [self showData];
}

-(void)showData{
    if ([_isedit isEqualToString:@"1"]) {
        _nameText.text = _nameStr;
        _phoneText.text = _phoneStr;
        _addressText.text = _regoinStr;
        NSArray * addressArr = [_addressStr componentsSeparatedByString:@" "];
        if (addressArr.count>1) {
            NSString * add = [NSString stringWithFormat:@"%@",addressArr[1]];
            _doorNumberText.text = add;
            //_infoLable.text = addressArr[0];
            //_infoLable.textColor = APPFourColor;
        }else{
            _doorNumberText.text = addressArr[0];
        }
        
        
        if ([_isdefualt isEqualToString:@"1"]) {
            [self.defaultBtn setSelected:YES];
        }else{
            [self.defaultBtn setSelected:NO];
        }
    }
}

#pragma mark bar delegate

-(void)saveBtnOnClick{
  
    if ([_isedit isEqualToString:@"0"]) {
        
        [self saveAddress];
    }else if([_isedit isEqualToString:@"1"]){

        [self editAddress];
    }
}
//添加地址
-(void)saveAddress{
    
    
    //NSString * infoAddress = [NSString stringWithFormat:@"%@ %@",self.infoLable.text,_doorNumberText.text];
    NSString * infoAddress = [NSString stringWithFormat:@"%@",_doorNumberText.text];
    NSString * addAddressUrl = [NSString stringWithFormat:@"%@ctl=uc_address&act=save&region_lv2_str=%@&region_lv3_str=%@&region_lv4_str=%@&address=%@&mobile=%@&consignee=%@&is_default=%@&xpoint=%f&ypoint=%f&email=%@&pwd=%@",GYLUrl,_region_lv2_name,_region_lv3_name,_region_lv4_name,infoAddress,_phoneText.text,_nameText.text,_isdef,_locationY,_locationX,_email,_pwd] ;
    
            [RequestData requestDataOfUrl:addAddressUrl success:^(NSDictionary *dic) {
               
                if ([dic[@"add_status"] isEqual:@1]) {
                    if (dic[@"info"]) {
                        [MBProgressHUD showError:dic[@"info"] toView:self.view];
                    }
                    
                    //购买界面未添加地址跳转，
                    if ([_isBuy isEqualToString:@"1"]) {
                        NSString * address = [NSString stringWithFormat:@"%@ %@ %@ %@",_region_lv2_name,_region_lv3_name,_region_lv4_name,infoAddress];
                        self.pushBlock(_nameText.text,_phoneText.text,address);
                    }
                    
                    
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"saveAddress" object:nil];
                    double delayInSeconds = 1.0;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                        [self.navigationController popViewControllerAnimated:YES];
                    });
                }else{
                    if (dic[@"info"]) {
                        [MBProgressHUD showError:dic[@"info"] toView:self.view];
                    }
                }
            } failure:^(NSError *error) {
                [MBProgressHUD showError:@"网络错误" toView:self.view];
            }];
}
//编辑地址
-(void)editAddress{
    
    _isdef = self.defaultBtn.selected ? @"1" : @"0";
    
//    NSString * infoAddress = [NSString stringWithFormat:@"%@ %@",self.infoLable.text,_doorNumberText.text];
     NSString * infoAddress = [NSString stringWithFormat:@"%@",_doorNumberText.text];
    NSArray * array = [_addressText.text componentsSeparatedByString:@" "];
    NSString * region1 = array[0];
    NSString * region2 = array[1];
    NSString * region3 = array[2];
    
    NSString * editAddressUrl = [NSString stringWithFormat:@"%@ctl=uc_address&act=save&id=%@&region_lv2_str=%@&region_lv3_str=%@&region_lv4_str=%@&address=%@&mobile=%@&consignee=%@&is_default=%@&xpoint=%f&ypoint=%f&email=%@&pwd=%@",GYLUrl,_ids,region1,region2,region3,infoAddress,_phoneText.text,_nameText.text,_isdef,_locationY,_locationX,_email,_pwd];
    
    [RequestData requestDataOfUrl:editAddressUrl success:^(NSDictionary *dic) {
        
        if ([dic[@"add_status"] isEqual:@1]) {
            if (dic[@"info"]) {
                [MBProgressHUD showError:dic[@"info"] toView:self.view];
            }
            [[NSNotificationCenter defaultCenter]postNotificationName:@"saveAddress" object:nil];
            double delayInSeconds = 1.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else{
            if (dic[@"info"]) {
                [MBProgressHUD showError:dic[@"info"] toView:self.view];
            }
            
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];


}



-(void)defaultBtnOnClick{
    if(self.defaultBtn.selected){
        _isdef=@"0";
        [self.defaultBtn setSelected:NO];
    }else{
        _isdef = @"1";
        [self.defaultBtn setSelected:YES];
    }
}


#pragma mark addressText delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    [_nameText resignFirstResponder];
    [_phoneText resignFirstResponder];
    [_doorNumberText resignFirstResponder];
    [self.areasView startAnimationFunction];
    return NO;
}

#pragma mark uitextviewdelegate



//点击选中哪一行 的代理方法
- (void)clickAreasViewEnsureBtnActionAreasDate:(DQAreasModel *)model{
    _region_lv2_name = model.Province;
    _region_lv3_name = model.city;
    _region_lv4_name = model.county;
    self.addressText.text = [NSString stringWithFormat:@"%@ %@ %@",model.Province,model.city,model.county];
}


/**
-(void)jumpMapTap{
    [_nameText resignFirstResponder];
    [_phoneText resignFirstResponder];
    [_doorNumberText resignFirstResponder];
//    NSString * locationStr = @"";
//    if (![_infoLable.text isEqualToString:@"选择定位"]) {
//        locationStr = _infoLable.text;
//    }
    AddressLocationVC * locationVC = [[AddressLocationVC alloc]init];
    if ([_isedit isEqualToString:@"0"]) {
        NSString * region = [NSString stringWithFormat:@"%@",_region_lv3_name];
        locationVC.cityStr = region;
    }else if([_isedit isEqualToString:@"1"]){
        
        NSArray * array = [_addressText.text componentsSeparatedByString:@" "];
        NSString * region = [NSString stringWithFormat:@"%@",array[1]];
        locationVC.cityStr = region;
    }
    
    
    __weak typeof (self)weself = self;
    locationVC.block = ^(BMKPoiInfo *poi){
        weself.infoLable.text = [NSString stringWithFormat:@"%@",poi.name];
        //CGSize maximumLabelSize = CGSizeMake(100, 9999);//labelsize的最大值
        //关键语句
        //CGSize expectSize = [self.infoAddressLabel sizeThatFits:maximumLabelSize];
        //别忘了把frame给回label，如果用xib加了约束的话可以只改一个约束的值
        self.infoLable.frame = CGRectMake(_infoAddress_x, 10, Screen_Width-_infoAddress_x-30, 20);
        self.infoLable.textColor = APPFourColor;
        _locationX = poi.pt.latitude;
        _locationY = poi.pt.longitude;
        NSLog(@"%f,%f",_locationX,_locationY);
    };
    [self.navigationController pushViewController:locationVC animated:YES];
}

*/


@end
