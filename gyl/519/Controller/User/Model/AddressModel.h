//
//  AddressModel.h
//  519
//
//  Created by Macmini on 16/12/9.
//  Copyright © 2016年 519. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressModel : NSObject

@property(nonatomic,copy)NSString * address;
@property(nonatomic,copy)NSString * consignee;
@property(nonatomic,copy)NSString * ids;
@property(nonatomic,copy)NSString * is_default;
@property(nonatomic,copy)NSString * mobile;
@property(nonatomic,copy)NSString * region_lv1_name;
@property(nonatomic,copy)NSString * region_lv2_name;
@property(nonatomic,copy)NSString * region_lv3_name;
@property(nonatomic,copy)NSString * region_lv4_name;
@property(nonatomic,copy)NSString * user_id;
@property(nonatomic,copy)NSString * address_id;
@property(nonatomic,copy)NSString * xpoint;
@end
