//
//  GoodsDetailModel.h
//  519
//
//  Created by Macmini on 16/12/13.
//  Copyright © 2016年 519. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GoodsDetailModel : NSObject
@property(nonatomic,copy)NSString * sub_name;
@property(nonatomic,copy)NSString * attr_str;
@property(nonatomic,copy)NSString * number;
@property(nonatomic,copy)NSString * unit_price;
@property(nonatomic,copy)NSString * deal_icon;
@property(nonatomic,copy)NSString * name;
@property(nonatomic,copy)NSString * ids;
@property(nonatomic,copy)NSString * dp_id;
@property(nonatomic,copy)NSString * return_score;
@property(nonatomic,copy)NSString * return_total_score;
@end
