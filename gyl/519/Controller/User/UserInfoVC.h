//
//  UserInfoVC.h
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//
typedef void(^exitBlock)(void);
#import <UIKit/UIKit.h>

@interface UserInfoVC : UIViewController

@property (nonatomic,copy)NSString * infousername;
@property (nonatomic,copy)NSString * infouserPwd;
@property (nonatomic,strong)UIImage * infoImage;
@property (nonatomic,copy)exitBlock block;
@end
