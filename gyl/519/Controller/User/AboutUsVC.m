//
//  AboutUsVC.m
//  519
//
//  Created by Macmini on 16/12/19.
//  Copyright © 2016年 519. All rights reserved.
//

#import "AboutUsVC.h"

@interface AboutUsVC ()
@property (nonatomic,strong)UIButton * backBtn;

@property (nonatomic ,strong)UILabel * phoneLabel;
@property(nonatomic,strong)UILabel * emailLabel;
@property(nonatomic,strong)UILabel * wxLabel;
@property(nonatomic,strong)UIWebView * webview;
@property(nonatomic,copy)NSString * webStr;
@end

@implementation AboutUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"关于我们";
    [self initview];
    [self requestDataOfAboutUs];
    
}

-(void)requestDataOfAboutUs{
    NSString * aboutUrl = [NSString stringWithFormat:@"%@ctl=notice&act=detail&id=%@",GYLUrl,_ID];
    [RequestData requestDataOfUrl:aboutUrl success:^(NSDictionary *dic) {
        
        if (dic[@"result"]) {
            _webStr = dic[@"result"][@"content"];
             [_webview loadHTMLString:_webStr baseURL:nil];
        }
        [self showdata];
    } failure:^(NSError *error) {
        
    }];
}


-(void)initview{
    
    _webview = [[UIWebView alloc]init];
    _webview.backgroundColor = APPBGColor;
    self.view.backgroundColor = APPBGColor;
    self.webview.scrollView.scrollEnabled=YES;
    [self.view addSubview:_webview];
}


-(void)showdata{
    _webview.frame = CGRectMake(0,0, Screen_Width, Screen_Height-64);
}







- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
  
}



@end
