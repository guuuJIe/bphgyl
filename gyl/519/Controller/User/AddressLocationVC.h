//
//  AddressLocationVC.h
//  519
//
//  Created by 陈 on 16/9/19.
//  Copyright © 2016年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^LocationBlock)(BMKPoiInfo *poi);

@interface AddressLocationVC : UIViewController

@property(nonatomic,copy)LocationBlock block;

@property(nonatomic,copy)NSString * cityStr;

@end
