//
//  GoodsEvaluationVC.h
//  519
//
//  Created by Macmini on 16/12/17.
//  Copyright © 2016年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodsEvaluationVC : UIViewController

@property (nonatomic,strong)NSArray * goodsEvaluationDataArr;
@property(nonatomic,copy)NSString * evaluationId;
@end
