//
//  UserInfoVC.m
//  519
//
//  Created by 陈 on 16/9/14.
//  Copyright © 2016年 519. All rights reserved.
//
static NSString * cellIde = @"infoVCIde";
#import "UserInfoVC.h"
#import "ChangeCrameView.h"
#import "base64.h"
#import "AddUserInfoViewController.h"
#import "AboutUsVC.h"
#import "RegisterViewController.h"
#import "MainWebView.h"
@interface UserInfoVC()<UIActionSheetDelegate,UIImagePickerControllerDelegate,UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate,UITextFieldDelegate>

@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,strong)UIImageView * img;
@property (nonatomic,strong)NSArray * titleArr;
@property (nonatomic,strong)NSArray * infomationArr;
@property (nonatomic,strong)UITextField * nameLabel;
@property (nonatomic,strong)UILabel * phoneLabel;
@property (nonatomic,strong)ChangeCrameView * crameView;
@property (nonatomic,strong)UILabel * sdLabel;
@property (nonatomic,strong)UILabel * verLabel;

@property (nonatomic,strong)UIButton *RZStatus;
@property (nonatomic,strong)NSString *StatusStr;
@property (nonatomic,assign)NSInteger rz_status;
@property (nonatomic,assign)BOOL isreload;
@property (nonatomic,strong)NSDictionary *dataDic;
@end

@implementation UserInfoVC

-(void)viewDidLoad{
    [super viewDidLoad];
    self.title = @"个人资料";
    [self initView];
    [self requestDataForUserInfo];
//    [self requestDataForCertifivation];
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:(UIBarMetricsDefault)];
}

-(void)requestDataForUserInfo{
    NSString * userInfoUrl = [NSString stringWithFormat:@"%@ctl=uc_account&email=%@&pwd=%@",GYLUrl,EMAIL,USER_PWD];
    
    [RequestData requestDataOfUrl:userInfoUrl success:^(NSDictionary *dic) {
        if ([dic[@"status"] isEqual:@1]) {
            _dataDic = dic;
//            _nameLabel.text = [NSString stringWithFormat:@"%@",dic[@"user_info"][@"user_name"]];
            self.phoneLabel.text = [NSString stringWithFormat:@"%@",dic[@"user_info"][@"mobile"]];
            [self.img sd_setImageWithURL:[NSURL URLWithString:dic[@"user_info"][@"avatar"]]];
            [self.tableView reloadData];
        }
    } failure:^(NSError *error) {
        NSLog(@"userinfo %@",error);
    }];
}


-(void)initView{
    _titleArr = @[@[@"头像",@"昵称",@"修改密码",@"绑定手机"],@[@"清除缓存",@"分享给朋友",@"当前版本",@"关于泊啤汇供应链"]];
    [self.view addSubview:self.tableView];
    
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _titleArr.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_titleArr[section] count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIde];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:cellIde];
    }
    cell.textLabel.text = _titleArr[indexPath.section][indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:13];
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            _img = [[UIImageView alloc]init];
            _img.layer.cornerRadius = 20;
            _img.layer.masksToBounds = YES;
            [cell addSubview:_img];
            [_img makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(cell.mas_centerY);
                make.right.offset(-50);
                make.width.height.offset(40);
            }];
        }if (indexPath.row == 1) {
           
//            if (_isreload) {
//                _nameLabel = nil;
//            }

            
            _nameLabel = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, 215, 20)];
            _nameLabel.textColor = APPThreeColor;
            _nameLabel.textAlignment = NSTextAlignmentRight;
            _nameLabel.font = [UIFont systemFontOfSize:13];
            _nameLabel.returnKeyType = UIReturnKeyDone;
            _nameLabel.clearsOnBeginEditing = YES;
            _nameLabel.delegate = self;
            _nameLabel.text = @"";
            _nameLabel.text = [NSString stringWithFormat:@"%@",_dataDic[@"user_info"][@"user_name"]];

//            [cell.contentView addSubview:_nameLabel];

            cell.accessoryView = _nameLabel;
          
            //            [_nameLabel makeConstraints:^(MASConstraintMaker *make) {
            //                make.centerY.equalTo(cell.mas_centerY);
            //                make.right.offset(-50);
            //                make.height.offset(40);
            //                make.width.equalTo(80);
            //            }];
            //            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            
            
        }if (indexPath.row == 3) {
            _phoneLabel = [[UILabel alloc]init];
            _phoneLabel.textColor = APPThreeColor;
            _phoneLabel.textAlignment = NSTextAlignmentRight;
            _phoneLabel.font = [UIFont systemFontOfSize:13];
            [cell addSubview:_phoneLabel];
            [_phoneLabel makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(cell.mas_centerY);
                make.right.offset(-50);
                make.height.offset(40);
            }];
        }
    }if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            self.sdLabel.text = [NSString stringWithFormat:@"%.1fMB",[[SDImageCache sharedImageCache] checkTmpSize]];
            [cell addSubview:self.sdLabel];
            [self.sdLabel makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(cell.mas_centerY);
                make.right.offset(-50);
                make.height.offset(40);
            }];
        }else if (indexPath.row == 2){
           
            self.verLabel.text = [NSString stringWithFormat:@"v%@",currentAppVersion];
            [cell addSubview:self.verLabel];
            [self.verLabel makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(cell.mas_centerY);
                make.right.offset(-50);
                make.height.offset(40);
            }];
            
        }
        
        
    }
    
    return cell;
}



-(UILabel*)sdLabel{
    if (!_sdLabel) {
        _sdLabel = [[UILabel alloc]init];
        _sdLabel.textColor = APPThreeColor;
        _sdLabel.textAlignment = NSTextAlignmentRight;
        _sdLabel.font = [UIFont systemFontOfSize:13];
    }
    return _sdLabel;
}

-(UILabel*)verLabel{
    if (!_verLabel) {
        _verLabel = [[UILabel alloc]init];
        _verLabel.textColor = APPThreeColor;
        _verLabel.textAlignment = NSTextAlignmentRight;
        _verLabel.font = [UIFont systemFontOfSize:13];
    }
    return _verLabel;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0) {
        return 60;
    }
    return 40;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 10)];
        view.backgroundColor = [UIColor clearColor];
        return view;
    }if (section == 1) {
        UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width, 60)];
        view.backgroundColor = [UIColor clearColor];
        
        UIButton * exitBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 20, Screen_Width-20, 40)];
        exitBtn.layer.cornerRadius = 3;
        exitBtn.layer.masksToBounds = YES;
        exitBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [exitBtn setTitle:@"退出登陆" forState:(UIControlStateNormal)];
        [exitBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [exitBtn setBackgroundColor:APPColor];
        [exitBtn addTarget:self action:@selector(exitBtn) forControlEvents:(UIControlEventTouchUpInside)];
        [view addSubview:exitBtn];
        return view;
    }
    return [[UIView alloc]init];
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 10;
    }if (section == 1) {
        return 60;
    }
    return 1;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            _crameView = [[ChangeCrameView alloc]initWithFrame:self.view.bounds];
            __weak typeof(self)weself = self;
            _crameView.cameraBlock = ^(NSInteger num) {
                if (num == 0) {
                    [weself addCamera];
                }
                if (num == 1) {
                    [weself libraryAction];
                }
            };
            [self.view addSubview:_crameView];
        }
        
        if (indexPath.row == 1) {
            
        }
        
        if (indexPath.row == 2) {
            RegisterViewController * vc =[RegisterViewController new];
            vc.isRegister = 1;
            [self.navigationController pushViewController:vc animated:YES];
        }
        if (indexPath.row == 3) {
            [self.navigationController pushViewController:[AddUserInfoViewController new] animated:YES];
        }
    }
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            [[SDImageCache sharedImageCache]clearDisk];
            [_sdLabel removeFromSuperview];
            [self.tableView reloadData];
        }
        
        if (indexPath.row == 1) {
            
            [self shareItem];
//            MainWebView *VC = [[MainWebView alloc] init];
//            VC.webUrl = @"http://www.bphgyl.com/download/";
//            [self.navigationController pushViewController:VC animated:true];
        }
        
        if (indexPath.row == 3) {
            AboutUsVC *us = [AboutUsVC new];
            us.ID = @"2";
            [self.navigationController pushViewController:us animated:YES];
        }
    }
}




-(void)shareItem{
    //    分享
    [UMUtil shareUM:self delegate:self title:@"泊啤汇供应链" body:@"上泊啤汇供应链，享酒水批发最低价！" url:@"http://www.bphgyl.com/download/" img:nil urlImg:@""];
}



-(void)exitBtn{
    NSString * loginOutUrl = [NSString stringWithFormat:@"%@&ctl=user&act=loginout&email=%@&pwd=%@",GYLUrl,EMAIL,USER_PWD];
    
    [RequestData requestDataOfUrl:loginOutUrl success:^(NSDictionary *dic) {
        if([dic[@"status"] isEqual:@1]){
//            NSString * appDomain =[ [NSBundle mainBundle]bundleIdentifier];
//            [[NSUserDefaults standardUserDefaults]removePersistentDomainForName: appDomain];
            NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
            NSDictionary * dict = [defs dictionaryRepresentation];
            for (id key in dict) {
                [defs removeObjectForKey:key];
            }
            [defs synchronize];
            //七鱼注销
            [[QYSDK sharedSDK] logout:^(){}];
            [self onchat];
            [MBProgressHUD dissmiss];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshCart" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"loginOut" object:nil];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"getCarNum" object:nil];
            [self.tabBarController.tabBar hideBadgeOnItemIndex:2];
            [self.navigationController popViewControllerAnimated:YES];
            
        }
    } failure:^(NSError *error) {
        [MBProgressHUD showError:@"网络错误" toView:self.view];
    }];
}
-(void)onchat{
    QYSource *source = [[QYSource alloc] init];
    source.title =  @"泊啤汇";
    source.urlString = @"https://8.163.com/";
    QYSessionViewController *sessionViewController = [[QYSDK sharedSDK] sessionViewController];
    //sessionViewController.delegate = self;
    sessionViewController.sessionTitle = @"供应链";
    sessionViewController.source = source;
    sessionViewController.groupId = 880528;
    [[QYSDK sharedSDK] customUIConfig].bottomMargin = 2;
}

-(void)changePwd{
    //[self tabHidePushVC:[[PasswordVC alloc]init]];
}

- (void)RenZhen:(UIButton *)sender{
    NSLog(@"%@",sender.currentTitle);
    if ([_StatusStr equals:@"未认证"] || [_StatusStr equals:@"认证失败"]) {
        CertificationViewController * certify = [CertificationViewController new];
        [self.navigationController pushViewController:certify animated:YES];
    }else if([_StatusStr equals:@"认证中"]){
        RunCertificationViewController *VC = [[RunCertificationViewController alloc] init];
        
        [self.navigationController pushViewController:VC animated:true];
    }
}

-(void)requestDataForCertifivation{
    NSString * urlstring = [NSString stringWithFormat:@"http://www.bphgyl.com/cxb_api/index.php?ctl=user&act=renzheng&email=%@&pwd=%@",EMAIL,USER_PWD];
    [RequestData requestDataOfUrl:urlstring success:^(NSDictionary *dic) {
        NSLog(@"%@",dic);
        [MBProgressHUD dissmiss];
        if ([dic[@"status"] isEqual: @1]) {
            _rz_status = [[NSString stringWithFormat:@"%@",dic[@"data"][@"rz_status"]]integerValue];
            [kUserDefaults setObject:[NSString stringWithFormat:@"%ld",(long)_rz_status] forKey:@"rz_status"];
            if ([[NSString stringWithFormat:@"%@",dic[@"data"][@"rz_status"]] isEqualToString:@"3"]) {
                [_RZStatus setTitle:@"认证失败" forState:UIControlStateNormal];
                _StatusStr = @"认证失败";
            }else if ([[NSString stringWithFormat:@"%@",dic[@"data"][@"rz_status"]] isEqualToString:@"2"]){
               [_RZStatus setTitle:@"认证中" forState:UIControlStateNormal];
                 _StatusStr = @"认证中";
            }else if ([[NSString stringWithFormat:@"%@",dic[@"data"][@"rz_status"]] isEqualToString:@"1"]){
                [_RZStatus setTitle:@"已认证" forState:UIControlStateNormal];
                 _StatusStr = @"已认证";
            }else if ([[NSString stringWithFormat:@"%@",dic[@"data"][@"rz_status"]] isEqualToString:@"0"]){
                [_RZStatus setTitle:@"未认证" forState:UIControlStateNormal];
                 _StatusStr = @"未认证";
            }
            
        }
    } failure:^(NSError *error) {
        
    }];
}


-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width,Screen_Height) style:(UITableViewStylePlain)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = APPBGColor;
        _tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    }
    return _tableView;
}

#pragma mark ----- UITextFiledDelegate--------

//实现UITextField代理方法
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField.text.length < 0) {
        [MBProgressHUD showMessag:@"请输入昵称" toView:self.view];
        return  false;
    }
   
    //取消第一响应者
    NSLog(@"%@",textField.text);
    self.nameLabel.text = textField.text;
    [textField resignFirstResponder];
    [self changeUsername];
    return YES;
}



-(void)textFieldDidBeginEditing:(UITextField *)textField{
   

//    _isreload = true;
//    self.nameLabel.text = @"";
//    textField.text = @"";
//    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:1 inSection:0];
//    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
    [textField becomeFirstResponder];
//    [self addToolBarForTextField:textField];
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    textField.text = @"";
//    return true;
//}

//退出编辑模式
-(BOOL)textViewShouldEndEditing:(UITextField *)texrField{
    if (texrField.text.length <=0) {
        [MBProgressHUD showMessag:@"请输入昵称" toView:self.view];
    }

    NSLog(@"%@",texrField.text);
    return true;
}

-(void)addToolBarForTextField:(UITextField *)textField{
    textField.inputAccessoryView = [self addToolbar];
    
}

- (UIToolbar *)addToolbar
{
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 40)];
    toolbar.tintColor = [UIColor blueColor];
    toolbar.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *bar = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(textFieldDone)];
    bar.tintColor = APPColor;
    toolbar.items = @[ space, bar];
    return toolbar;
}

- (void)textFieldDone{
    [self changeUsername];
    [self.view endEditing:YES];
}

- (void)changeUsername{
    
    NSString *username = self.nameLabel.text;

//    NSStringEncoding gbkEncoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
//    
//    username = [username stringByAddingPercentEscapesUsingEncoding:gbkEncoding];//转字符串
   
    NSString *requestUrl = [NSString stringWithFormat:@"http://www.bphgyl.com/cxb_api/index.php?ctl=uc_account&act=set_username&new_name=%@",username];
    
    NSLog(@"参数%@",requestUrl);
    
    [RequestData requestDataOfUrl:requestUrl success:^(NSDictionary *dic) {

        if ([dic[@"status"] isEqual:@1]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshUserInfo" object:nil];
            [self.navigationController popViewControllerAnimated:true];
        }else{

        }


        [MBProgressHUD showSuccess:dic[@"info"] toView:self.view];

    } failure:^(NSError *error) {
        [MBProgressHUD showMessag:@"网络错误" toView:self.view];
    }];
}


#pragma mark --------ImagePicker---------
-(void)addCamera{
    //拍照
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *picker = [UIImagePickerController new];
        picker.delegate = self;
        picker.allowsEditing = YES;
        //摄像头
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:nil];
    }
}
-(void)libraryAction{
    //进入图库
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        UIImagePickerController *picker = [UIImagePickerController new];
        picker.delegate = self;
        picker.allowsEditing = YES;
        //打开相册
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:nil];
        
    }
}
//点击保存时调用的方法
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString* ,id>* )info{
    [picker dismissViewControllerAnimated:YES completion:^{
    }];
    [_crameView removeFromSuperview];
    UIImage * image = [info objectForKey:UIImagePickerControllerEditedImage];
    [self performSelector:@selector(saveImage:) withObject:image afterDelay:0.5];
}
//点击取消是调用的方法
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:^{
    }];
}

-(void)saveImage:(UIImage *)image{
    self.img.image = image;
    
    NSString * uploadStr = [NSString stringWithFormat:@"%@ctl=uc_account&act=upload_avatar&email=%@&pwd=%@",GYLUrl,EMAIL,USER_PWD];
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager POST:uploadStr parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSData * imageData;
        if (UIImagePNGRepresentation(self.img.image) == nil) {
            imageData = UIImageJPEGRepresentation(self.img.image, 1);
        }else{
            imageData = UIImagePNGRepresentation(self.img.image);
        }
        NSDateFormatter * formattter = [NSDateFormatter new];
        formattter.dateFormat = @"yyyyMMddHHmmss";
        NSString * str = [formattter stringFromDate:[NSDate date]];
        NSString * fileName = [NSString stringWithFormat:@"%@.jpg",str];
        
        [formData appendPartWithFileData:imageData name:@"file" fileName:fileName mimeType:@"image/jpg"];
        
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSString *base64Decoded = [[NSString alloc]
                                   initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        NSData * jsondatastring = [Base64 decodeString:base64Decoded];
        
        NSDictionary * dic = [NSJSONSerialization JSONObjectWithData:jsondatastring options:NSJSONReadingMutableLeaves error:nil];
        NSLog(@"%@",dic);
        [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshCart" object:nil];
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"图片上传出错" message:[NSString stringWithFormat:@"%@",error] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        
        [alert show];
        
        NSLog(@"no -- %@",error);
        
    }];
}
@end
