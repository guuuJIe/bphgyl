//
//  OrderDetailVC.m
//  519
//
//  Created by Macmini on 16/12/12.
//  Copyright © 2016年 519. All rights reserved.
//

#import "OrderDetailVC.h"
#import "GoodsDetailModel.h"
#import "OrderDetailTC.h"
#import "AddressVC.h"
#import "OrderPayVC.h"
#import "RefundVC.h"
#import "GoodsEvaluationVC.h"
#import "GoodInfoVC.h"

#import "OrderInfoView.h"
#import "OrderGoodsView.h"
#import "OrderElseInfoView.h"
#import "OrderEndView.h"
#import "MainWebView.h"
#import "ConnactView.h"
@interface OrderDetailVC ()<UITableViewDelegate ,UITableViewDataSource,UIAlertViewDelegate>
@property(nonatomic,strong)UIScrollView * scrollView;
@property(nonatomic,strong)OrderInfoView * infoView;
@property(nonatomic,strong)OrderGoodsView * goodsView;
@property(nonatomic,strong)OrderElseInfoView * elseView;
@property(nonatomic,strong)OrderEndView * endView;
@property(nonatomic,copy)NSString * ids;

@property(nonatomic,strong)UIView * detailTypeView;
@property(nonatomic,strong)UILabel * typeLabel;
@property(nonatomic,strong)NSMutableArray * goodsDetailMuArr;

@property(nonatomic,strong)UITableView * tableView;
@property(nonatomic,copy)NSString * orderId;

@property(nonatomic,copy)NSString * storePhone;
//二维码
@property(nonatomic,strong)UIView * erwmView;
@property(nonatomic,strong)UIImageView * imageView;
@property(nonatomic,strong)ConnactView *conView;
@end

@implementation OrderDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];

   // [MBProgressHUD showHUD];
    
    _goodsDetailMuArr = [NSMutableArray new];
    
    self.title = @"订单详情";
    self.view.backgroundColor = APPBGColor;
    [self createNewView];
    [self requestdataForOrderDetail];
}

-(void)createNewView{
    [self.view addSubview:self.scrollView];
    self.infoView = [[OrderInfoView alloc]init];
    __weak typeof(self)weself = self;
    self.infoView.block = ^(CGFloat height) {
        __strong typeof(weself)self = weself;
        self.infoView.frame = CGRectMake(0, 0, Screen_Width, 200+height);
    };
    [self.scrollView addSubview:self.infoView];
    
    self.goodsView = [[OrderGoodsView alloc]init];
    self.goodsView.block = ^(NSInteger indes) {
        __strong typeof(weself)self = weself;
        self.goodsView.frame = CGRectMake(0, CGRectGetMaxY(self.infoView.frame)+10, Screen_Width, 100*indes+40);
    };
    [self.scrollView addSubview:self.goodsView];
    
    self.elseView = [[OrderElseInfoView alloc]init];
    self.elseView.block = ^(CGFloat height) {
        __strong typeof(weself)self = weself;
        self.elseView.frame = CGRectMake(0, CGRectGetMaxY(self.goodsView.frame)+10, Screen_Width, 210) ;
        self.scrollView.contentSize = CGSizeMake(Screen_Width, CGRectGetMaxY(self.elseView.frame)+150);
    };
    [self.scrollView addSubview:self.elseView];
    
//    self.endView = [[OrderEndView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-50-Screen_NavBarHeight-Screen_StatusBarHeight, Screen_Width, 50+BottomAreaHeight)];
    self.endView = [[OrderEndView alloc] init];
    [self.view addSubview:self.endView];
    [self.endView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.view);
        make.height.mas_equalTo(50+BottomAreaHeight);
    }];
    
    self.endView.block = ^(NSString *str) {
        __strong typeof(weself)self = weself;
        [self btnClick:str];
    };
    self.endView.tellblock = ^(NSString *phoneNum) {
        __strong typeof(weself)self = weself;
        
        
        NSString * kefuInterface = [NSString stringWithFormat:@"%@ctl=user&act=kf&email=%@&pwd=%@",GYLUrl,EMAIL,USER_PWD];
        
        [RequestData requestDataOfUrl:kefuInterface success:^(NSDictionary *dic) {
            
            if([dic[@"user_login_status"] integerValue] == 0){
                MainWebView * vc = [[MainWebView alloc]init];
                NSString* nowSysVer = [[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleShortVersionString"];
                vc.webUrl = [NSString stringWithFormat:@"http://www.bphapp.com/wap/index.php?ctl=onlinekf&userId=%@&os=ios&appVersion=%@&device=%@&deal_id=%@&order_sn=%@",EMAIL,nowSysVer,[[UIDevice currentDevice] model],@"",@""];
                [weself.navigationController pushViewController:vc animated:true];
//                if (EMAIL) {
//
//                }else{
//                  [weself.navigationController pushViewController:[LoginViewController new] animated:YES];
//                }
               
            }else{
                _conView = [[ConnactView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
                [weself.conView setupData:dic];
                [weself.view addSubview:weself.conView];
            }
        } failure:^(NSError *error) {
            
        }];
        
//        MainWebView * vc = [[MainWebView alloc]init];
//        
//        
//        vc.webUrl = [NSString stringWithFormat:@"http://www.bphgyl.com/wap/index.php?ctl=onlinekf&userId=%@&os=ios&appVersion=%@&device=xiaomi&deal_id=%@&order_sn=%@",EMAIL,[[UIDevice currentDevice] model],@"",@""];
//        [self.navigationController pushViewController:vc animated:true];
//        UIAlertController * alert = [UIAlertController alertControllerWithTitle:nil message:phoneNum preferredStyle:(UIAlertControllerStyleAlert)];
//        [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
//            [self tellPhoneNum:phoneNum];
//        }]];
//        [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
//
//        }]];
//        [self presentViewController:alert animated:YES completion:nil];
    };
//    [self.view addSubview:self.endView];
}
-(void)tellPhoneNum:(NSString *)phoneNum{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phoneNum]]];
}
-(UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc]initWithFrame:self.view.bounds];
        _scrollView.contentSize = CGSizeMake(Screen_Width, Screen_Height+50);
        _scrollView.backgroundColor = APPBGColor;
    }
    return _scrollView;
}

-(void)requestdataForOrderDetail{

    
    NSString * orderDetail = [NSString stringWithFormat:@"%@ctl=uc_order&act=view&id=%@&email=%@&pwd=%@",GYLUrl,_orderDetailId,EMAIL,USER_PWD];
    [RequestData requestDataOfUrl:orderDetail success:^(NSDictionary *dic) {
        NSLog(@"%@",dic);
        if([dic[@"status"] isEqual:@1]){
            if (![dic[@"shop_info"] isEqual:@0]) {
                self.storePhone = dic[@"shop_info"][@"tel"];
            }else{
                self.storePhone = @"86191919";
            }
            
            [self.infoView showOrderInfoData:dic];
            [self.goodsView showdataOfGoodData:dic];
            [self.elseView showdataOfOrderElseInfo:dic];
            _detailArr = dic[@"deal_order_item"];
            [self.endView showDataOfOrderEndView:dic];
        }

    } failure:^(NSError *error) {
        
    }];
}

-(void)btnClick:(NSString *)btnTitle{
    if ([btnTitle isEqualToString:@"立即支付"]) {
        OrderPayVC * vc = [OrderPayVC new];
        vc.paytype = @"nowPay";
        vc.orderId = _orderDetailId;
        [self.navigationController pushViewController:vc animated:YES];
    }
    if ([btnTitle isEqualToString:@"删除订单"]) {
        UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"是否删除订单" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alertView show];
        
    }
    if ([btnTitle isEqualToString:@"我要退款"]) {
        RefundVC * vc = [RefundVC new];
        vc.orderId = _orderDetailId;
        vc.refundArr = _detailArr;
        [self.navigationController pushViewController:vc animated:YES];
    }
    if ([btnTitle isEqualToString:@"我要评价"]) {
        NSLog(@"_detailArr -- %@",_detailArr);
        GoodsEvaluationVC * vc = [GoodsEvaluationVC new];
        vc.goodsEvaluationDataArr = _detailArr;
        [self.navigationController pushViewController:vc animated:YES];
    }

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _goodsDetailMuArr.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderDetailTC * cell = [tableView dequeueReusableCellWithIdentifier:[OrderDetailTC getID]];
    if (cell == nil) {
        cell = [[OrderDetailTC alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:[OrderDetailTC getID]];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    GoodsDetailModel * model = _goodsDetailMuArr[indexPath.row];
    cell.model = model;
    
    return cell;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) {
        if (alertView.tag == 4100) {
            NSString *allString = [NSString stringWithFormat:@"tel:%@",self.storePhone];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:allString]];
        }else{
             NSString * indexStr = [NSString stringWithFormat:@"%ld",(long)_index];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"deleteOrder" object:indexStr];
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                [self deleteOrder];
            });
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}
-(void)deleteOrder{
    NSString * deleteUrl = [NSString stringWithFormat:@"%@ctl=uc_order&act=cancel&id=%@&email=%@&pwd=%@",GYLUrl,_orderId,EMAIL,USER_PWD];
    [RequestData requestDataOfUrl:deleteUrl success:^(NSDictionary *dic) {

        if (dic[@"info"]) {
            [MBProgressHUD showSuccess:dic[@"info"] toView:self.view];
        }
    } failure:^(NSError *error) {
        NSLog(@"error");
    }];
}

-(void)storePhoneClick{
    UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"是否拨打电话" message:self.storePhone delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertview.tag = 4100;
    [alertview show];
   
}



//二维码生成
-(void)boringErwm:(NSString *)orderNumber{
    if (orderNumber) {
        
        // 1.创建滤镜
        CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
        
        // 2.还原滤镜默认属性
        [filter setDefaults];
        
        // 3.设置需要生成二维码的数据到滤镜中
        // OC中要求设置的是一个二进制数据
        NSData *data = [orderNumber dataUsingEncoding:NSUTF8StringEncoding];
        [filter setValue:data forKeyPath:@"InputMessage"];
        
        // 4.从滤镜从取出生成好的二维码图片
        CIImage *ciImage = [filter outputImage];
        
        _imageView.layer.shadowOffset = CGSizeMake(0, 0.5);  // 设置阴影的偏移量
        _imageView.layer.shadowRadius = 1;  // 设置阴影的半径
        _imageView.layer.shadowColor = [UIColor blackColor].CGColor; // 设置阴影的颜色为黑色
        _imageView.layer.shadowOpacity = 0.3; // 设置阴影的不透明度
        
        _imageView.image = [self createNonInterpolatedUIImageFormCIImage:ciImage size: 500];
    }
}


- (UIImage *)createNonInterpolatedUIImageFormCIImage:(CIImage *)ciImage size:(CGFloat)widthAndHeight
{
    CGRect extentRect = CGRectIntegral(ciImage.extent);
    CGFloat scale = MIN(widthAndHeight / CGRectGetWidth(extentRect), widthAndHeight / CGRectGetHeight(extentRect));
    
    // 1.创建bitmap;
    size_t width = CGRectGetWidth(extentRect) * scale;
    size_t height = CGRectGetHeight(extentRect) * scale;
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    
    CIContext *context = [CIContext contextWithOptions:nil];
    
    CGImageRef bitmapImage = [context createCGImage:ciImage fromRect:extentRect];
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    CGContextScaleCTM(bitmapRef, scale, scale);
    CGContextDrawImage(bitmapRef, extentRect, bitmapImage);
    
    // 保存bitmap到图片
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    CGContextRelease(bitmapRef);
    CGColorSpaceRelease(cs);
    CGImageRelease(bitmapImage);
    
    UIImage *newImage = [UIImage imageWithCGImage:scaledImage];
    CGImageRelease(scaledImage);
    return [self imageBlackToTransparent:newImage withRed:0.0 andGreen:0.0 andBlue:0.0];
}

#pragma mark - 图片透明度
void ProviderReleaseData (void *info, const void *data, size_t size){
    free((void*)data);
}
- (UIImage*)imageBlackToTransparent:(UIImage*)image withRed:(CGFloat)red andGreen:(CGFloat)green andBlue:(CGFloat)blue{
    const int imageWidth = image.size.width;
    const int imageHeight = image.size.height;
    size_t      bytesPerRow = imageWidth * 4;
    uint32_t* rgbImageBuf = (uint32_t*)malloc(bytesPerRow * imageHeight);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(rgbImageBuf, imageWidth, imageHeight, 8, bytesPerRow, colorSpace,
                                                 kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipLast);
    CGContextDrawImage(context, CGRectMake(0, 0, imageWidth, imageHeight), image.CGImage);
    // 遍历像素
    int pixelNum = imageWidth * imageHeight;
    uint32_t* pCurPtr = rgbImageBuf;
    for (int i = 0; i < pixelNum; i++, pCurPtr++){
        if ((*pCurPtr & 0xFFFFFF00) < 0x99999900)    // 将白色变成透明
        {
            // 改成下面的代码，会将图片转成想要的颜色
            uint8_t* ptr = (uint8_t*)pCurPtr;
            ptr[3] = red; //0~255
            ptr[2] = green;
            ptr[1] = blue;
        }
        else
        {
            uint8_t* ptr = (uint8_t*)pCurPtr;
            ptr[0] = 0;
        }
    }
    // 输出图片
    CGDataProviderRef dataProvider = CGDataProviderCreateWithData(NULL, rgbImageBuf, bytesPerRow * imageHeight, ProviderReleaseData);
    CGImageRef imageRef = CGImageCreate(imageWidth, imageHeight, 8, 32, bytesPerRow, colorSpace,
                                        kCGImageAlphaLast | kCGBitmapByteOrder32Little, dataProvider,
                                        NULL, true, kCGRenderingIntentDefault);
    CGDataProviderRelease(dataProvider);
    UIImage* resultUIImage = [UIImage imageWithCGImage:imageRef];
    // 清理空间
    CGImageRelease(imageRef);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    return resultUIImage;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



@end
