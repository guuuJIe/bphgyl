//
//  AddressLocationVC.m
//  519
//
//  Created by 陈 on 16/9/19.
//  Copyright © 2016年 519. All rights reserved.

#import "AddressLocationVC.h"
#import "AddressLocationTC.h"

@interface AddressLocationVC()<UITableViewDataSource,UITableViewDelegate,BMKMapViewDelegate,BMKLocationServiceDelegate,BMKGeoCodeSearchDelegate,UITextFieldDelegate>

{
    CLGeocoder * _geocoder;
}

@property(nonatomic,strong)UIView *customBarView;
@property(nonatomic,strong)UIButton *backBtn;
@property(nonatomic,strong)UITextField *selectText;
@property(nonatomic,strong)UIButton *selectBtn;

@property(nonatomic,strong)BMKLocationService *locService;
@property(nonatomic,strong)BMKUserLocation *userLocation;
@property(nonatomic,strong)BMKMapView *mapView;
@property(nonatomic,strong)BMKGeoCodeSearch *codeSearch;
@property(nonatomic,strong)UIView *mapViewView;
@property(nonatomic,strong)UIImageView *mapViewMidImg;
@property(nonatomic,strong)UIButton *locationBtn;

@property(nonatomic,strong)NSArray *poiArray;
@property(nonatomic,strong)UITableView *tableView;

@end

@implementation AddressLocationVC
-(void)viewDidLoad{
    [super viewDidLoad];
    
    NSLog(@"_city -- %@",_cityStr);
    
    [self initBar];
    [self initView];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.mapView viewWillAppear];
    self.mapView.delegate = self;
    self.locService.delegate = self;
    self.codeSearch.delegate=self;
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.mapView viewWillDisappear];
    self.mapView.delegate = nil;
    self.locService.delegate = nil;
    self.codeSearch.delegate=nil;
}

-(void)initBar{
    self.navigationItem.leftBarButtonItem = nil;
    UIView *selectView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, Screen_Width-80, 30)];
    selectView.backgroundColor=[UIColor whiteColor];
    selectView.layer.cornerRadius=4;
    selectView.layer.masksToBounds=YES;
    
    UIImageView *selectIcon=[[UIImageView alloc]initWithFrame:CGRectMake(Default_Space, 5, selectView.frame.size.height-5*2, selectView.frame.size.height-5*2)];
    selectIcon.image=[UIImage imageNamed:@"select_black_icon"];
    
    self.selectText=[[UITextField alloc]initWithFrame:CGRectMake(selectIcon.frame.size.width+selectIcon.frame.origin.x+Default_Space, 0, selectView.frame.size.width-selectIcon.frame.size.width, selectView.frame.size.height)];
    self.selectText.textColor=APPFourColor;
    self.selectText.placeholder=@"输入关键字";
    self.selectText.returnKeyType = UIReturnKeySearch;
    self.selectText.delegate = self;
    self.selectText.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    
    [selectView addSubview:selectIcon];
    [selectView addSubview:self.selectText];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:selectView];
    
    self.selectBtn=[[UIButton alloc]initWithFrame:CGRectMake(selectView.frame.size.width+selectView.frame.origin.x, 0, self.customBarView.frame.size.height, self.customBarView.frame.size.height)];
    [self.selectBtn setTitle:@"取消" forState:UIControlStateNormal];
    self.selectBtn.titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_M];
    [self.selectBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.selectBtn addTarget:self action:@selector(popVC) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:_selectBtn];
}

-(void)initView{
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide)];
    viewTap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:viewTap];
    
    
    self.mapView = [[BMKMapView alloc]init];
    self.mapView.showsUserLocation = YES;//显示定位图层
    self.mapView.rotateEnabled=NO;
    self.mapView.zoomLevel=18;
    self.mapView.mapType=BMKMapTypeStandard;
    self.mapView.userTrackingMode=BMKUserTrackingModeFollow;
    
    _cityStr ? [self requestAddressLocation] : [self setlocation] ;
   
    self.codeSearch=[[BMKGeoCodeSearch alloc]init];
    
    self.mapViewMidImg=[[UIImageView alloc]init];
    self.mapViewMidImg.image=[UIImage imageNamed:@"location_red_icon"];
    self.locationBtn=[[UIButton alloc]init];
    [self.locationBtn setBackgroundImage:[UIImage imageNamed:@"req_myposition"] forState:UIControlStateNormal];
    [self.locationBtn addTarget:self action:@selector(locationBtnOnClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.mapViewView=[[UIView alloc]initWithFrame:CGRectMake(0, self.mapView.frame.size.height+self.mapView.frame.origin.y, Screen_Width,220)];
    self.mapViewView.backgroundColor=APPColor;
    self.mapView.frame=self.mapViewView.bounds;
    self.mapViewMidImg.frame=CGRectMake((self.mapView.frame.size.width-20)/2,self.mapView.frame.size.height/2-20 , 20, 40);
    self.locationBtn.frame=CGRectMake(Default_Space, self.mapView.frame.size.height-Default_Space-40, 40, 40);
    [self.mapViewView addSubview:self.mapView];
    [self.mapViewView addSubview:self.mapViewMidImg];
    [self.mapViewView addSubview:self.locationBtn];

    self.tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, self.mapViewView.frame.origin.y+self.mapViewView.frame.size.height, Screen_Width, Screen_Height-self.mapViewView.frame.size.height-self.mapViewView.frame.origin.y)];
    [self.tableView registerClass:[AddressLocationTC class] forCellReuseIdentifier:[AddressLocationTC getID]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.showsVerticalScrollIndicator=NO;
    
    [self.view addSubview:self.mapViewView];
    [self.view addSubview:self.tableView];
    
    [self loadData];
}

-(void)loadData{
    
    [self showData];
}

-(void)showData{
   

}

-(void)keyboardHide{
    [self.selectText resignFirstResponder];
}

-(void)requestAddressLocation{
   
    [self.geocoder geocodeAddressString:self.cityStr completionHandler:^(NSArray *placemarks, NSError *error) {
        //如果有错误信息，或者是数组中获取的地名元素数量为0，那么说明没有找到
        if (error || placemarks.count==0) {
            
            [self setlocation];
        }else   //  编码成功，找到了具体的位置信息
        {
            
            //打印查看找到的所有的位置信息
            /*
             name:名称
             locality:城市
             country:国家
             postalCode:邮政编码
             */
            for (CLPlacemark *placemark in placemarks) {
                NSLog(@"name=%@ locality=%@ country=%@ postalCode=%@",placemark.name,placemark.locality,placemark.country,placemark.postalCode);
            }
            
            //取出获取的地理信息数组中的第一个显示在界面上
            CLPlacemark *firstPlacemark=[placemarks firstObject];
            self.mapView.centerCoordinate = firstPlacemark.location.coordinate;
        }
    }];
}

-(void)setlocation{
    self.locService = [[BMKLocationService alloc]init];
    self.locService.delegate = self;
    [self.locService startUserLocationService];
}
#pragma mark 地址坐标检索
-(void)selectPOI:(CLLocationCoordinate2D)pt{
    BMKReverseGeoCodeOption *reverseGeoCodeSearchOption = [[BMKReverseGeoCodeOption alloc]init];
    reverseGeoCodeSearchOption.reverseGeoPoint = pt;
    BOOL flag = [self.codeSearch reverseGeoCode:reverseGeoCodeSearchOption];
    if(!flag)
    {
        [MBProgressHUD showError:@"网络异常" toView:self.view];
    }
}
-(void)selectCLL2D:(NSString *)str{
    BMKGeoCodeSearchOption *geoCodeSearchOption = [[BMKGeoCodeSearchOption alloc]init];
    geoCodeSearchOption.city= @"中国";
    geoCodeSearchOption.address=str;
    BOOL flag = [self.codeSearch geoCode:geoCodeSearchOption];
    if(!flag)
    {
        [MBProgressHUD showError:@"网络异常" toView:self.view];
    }
}



#pragma mark bar delegate
-(void)selectOnClick{
    if(self.selectText.text.length==0){
        [MBProgressHUD showError:@"请填写关键字" toView:self.view];
        return;
    }
    [self selectCLL2D:self.selectText.text];
    self.locService.delegate = nil;
    
}

#pragma mark tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.poiArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BMKPoiInfo *poi=[self.poiArray objectAtIndex:indexPath.row];
    AddressLocationTC *cell=[tableView dequeueReusableCellWithIdentifier:[AddressLocationTC getID] forIndexPath:indexPath];
    if(cell==nil){
        cell=[[AddressLocationTC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[AddressLocationTC getID]];
    }
    [cell showData:poi index:(int)indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [AddressLocationTC getHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    
    BMKPoiInfo *poi=[self.poiArray objectAtIndex:indexPath.row];
    self.block(poi);
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark location btn delegate
-(void)locationBtnOnClick{
    [self.mapView setCenterCoordinate:self.locService.userLocation.location.coordinate];
}

#pragma mark map delegate
- (void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    if (mapView.centerCoordinate.longitude < 1.0) {
        [self requestAddressLocation];
        return;
    }
    [self selectPOI:mapView.centerCoordinate];
}

#pragma mark location delegate
- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation{
    [self.mapView updateLocationData:userLocation];
    if(self.userLocation==nil){
        self.userLocation=userLocation;
        [self locationBtnOnClick];
    }
    self.userLocation=userLocation;
    [self selectPOI:self.mapView.centerCoordinate];
}

#pragma mark 地址坐标检索 delegate
- (void)onGetReverseGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKReverseGeoCodeResult *)result errorCode:(BMKSearchErrorCode)error{
    if (error == BMK_SEARCH_NO_ERROR) {
        self.poiArray=result.poiList;
        [self.tableView reloadData];
    }
    else {
        [MBProgressHUD showError:@"网络异常" toView:self.view];
    }
}

- (void)onGetGeoCodeResult:(BMKGeoCodeSearch *)searcher result:(BMKGeoCodeResult *)result errorCode:(BMKSearchErrorCode)error{
    if (error == BMK_SEARCH_NO_ERROR) {
        [self.mapView setCenterCoordinate:result.location];
    }
    else {
        [MBProgressHUD showError:@"网络异常" toView:self.view];
    }
}


-(CLGeocoder *)geocoder
{
    if (_geocoder==nil)
    {
        _geocoder=[[CLGeocoder alloc]init];
    }
    return _geocoder;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self selectOnClick];
    return YES;
}
@end
