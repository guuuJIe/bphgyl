//
//  OrderElseInfoView.h
//  519
//
//  Created by Macmini on 2018/1/12.
//  Copyright © 2018年 519. All rights reserved.
//
typedef void(^orderElseInfoBlock)(CGFloat height);
#import <UIKit/UIKit.h>

@interface OrderElseInfoView : UIView
@property (nonatomic,copy)orderElseInfoBlock block;
-(void)showdataOfOrderElseInfo:(NSDictionary *)dic;

@end
