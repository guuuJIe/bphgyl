//
//  OrderEndView.m
//  519
//
//  Created by Macmini on 2018/1/13.
//  Copyright © 2018年 519. All rights reserved.
//

#import "OrderEndView.h"

@interface OrderEndView()
@property (nonatomic,strong)NSMutableArray * btnMuArr;
@property (nonatomic,copy)NSString * phoneNum;
@end

@implementation OrderEndView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor =[ UIColor whiteColor];
        self.btnMuArr = [NSMutableArray new];
        [self createUI];
    }
    return self;
}

-(void)createUI{
    UIView * lineView = [[UIView alloc]init];
    lineView.backgroundColor = APPBGColor;
    [self addSubview:lineView];
    [lineView makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.offset(0);
        make.height.offset(1);
    }];
    
    UIButton * tellBtn = [[UIButton alloc]init];
    [tellBtn setTitle:@"联系客服" forState:(UIControlStateNormal)];
    [tellBtn setTitleColor:APPFourColor forState:(UIControlStateNormal)];
    [tellBtn setImage:[UIImage imageNamed:@"button_拨号"] forState:(UIControlState)UIControlStateNormal];
    [tellBtn addTarget:self action:@selector(tellBtnClick) forControlEvents:(UIControlEventTouchUpInside)];
    tellBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    tellBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -5);
    [self addSubview:tellBtn];
    [tellBtn makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.mas_centerY);
        make.centerX.offset(-self.width/4);
        make.height.offset(30);
        make.width.offset(self.width/4);
    }];
    
  
}
-(void)tellBtnClick{
    if (self.tellblock) {
        self.tellblock(@"");
    }
}
-(void)onClickBtn:(UIButton *)btn{
    if (self.block) {
        self.block(btn.titleLabel.text);
    }
}
-(void)showDataOfOrderEndView:(NSDictionary *)dic{
    
    [self getNowTimeWithString:[NSString stringWithFormat:@"%@",dic[@"create_time"]]];
    _phoneNum = dic[@""];
    NSArray * btnTitArr;
    NSString * order_type = dic[@"order_status_str"];
    if ([order_type isEqualToString:@"未付款"]) {
        btnTitArr=[[NSArray alloc] initWithObjects:@"立即支付",@"删除订单", nil];
    }
    if ([order_type isEqualToString:@"已付款"]) {
        btnTitArr=[[NSArray alloc] initWithObjects:@"我要退款", nil];
    }
    if ([order_type isEqualToString:@"待点评"]) {
        btnTitArr=[[NSArray alloc] initWithObjects:@"我要评价", nil];
    }
    if ([order_type isEqualToString:@"退款申请中"]) {
        
    }
    if ([order_type isEqualToString:@"退款完成"]) {
        //btnTitArr=[[NSArray alloc] initWithObjects:@"删除订单", nil];
    }
    if (btnTitArr.count>0) {
        for (NSInteger i = 0; i<btnTitArr.count; i++) {
            UIButton * btn = [[UIButton alloc]init];
            btn.layer.masksToBounds = YES;
            btn.layer.cornerRadius = 3;
            btn.tag = 4020+i;
            if (i == 0) {
                [btn setTitleColor:[UIColor  whiteColor] forState:(UIControlStateNormal)];
                btn.backgroundColor = APPColor;
                if (_ispay == 0) {
//                    [btn setTitleColor:[UIColor colorWithHexString:@"0xFFee11" andAlpha:.5] forState:(UIControlStateNormal)];
//                    btn.userInteractionEnabled = NO;
                }
            }else{
                [btn setTitleColor:APPColor forState:(UIControlStateNormal)];
                btn.layer.borderWidth = 1;
                btn.layer.borderColor = APPColor.CGColor;
            }
            [btn setTitle:btnTitArr[i] forState:(UIControlStateNormal)];
            btn.titleLabel.font = [UIFont systemFontOfSize:14];
            btn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
            [btn addTarget:self action:@selector(onClickBtn:) forControlEvents:(UIControlEventTouchUpInside)];
            [self addSubview:btn];
            [btn makeConstraints:^(MASConstraintMaker *make) {
                make.right.offset(-90*i-10);
                make.centerY.equalTo(self.mas_centerY);
                make.height.offset(30);
                make.width.offset(70);
            }];
            [_btnMuArr addObject:btn];
        }
    }
}

//时间处理
-(NSString *)getNowTimeWithString:(NSString *)aTimeString{
    NSDate  *nowDate = [NSDate date];
    NSTimeInterval timeinter = [nowDate timeIntervalSince1970];
    int endTime = (int)timeinter - [aTimeString intValue];
    int endtime2 = (3600*48)-endTime;
    if (endtime2 < 0) {
        _ispay=0;
        return @"超时未支付 订单已失效！";
    }
    return nil;
}


@end
