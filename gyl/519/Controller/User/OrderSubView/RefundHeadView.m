//
//  RefundHeadView.m
//  519
//
//  Created by Macmini on 2018/1/18.
//  Copyright © 2018年 519. All rights reserved.
//

#import "RefundHeadView.h"

@interface RefundHeadView()
@property (nonatomic,strong)UILabel * titleLabel;
@end

@implementation RefundHeadView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

-(void)createUI{
    UIImageView * bacImgV = [[UIImageView alloc]init];
    bacImgV.image = [UIImage imageNamed:@""];
    [self addSubview:bacImgV];
    [bacImgV makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.offset(0);
        make.height.offset(80);
    }];
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.textColor = [UIColor whiteColor];
    _titleLabel.textAlignment = NSTextAlignmentLeft;
    _titleLabel.font = [UIFont systemFontOfSize:14];
    [bacImgV addSubview:_titleLabel];
    [_titleLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(34);
        make.centerY.equalTo(bacImgV.mas_centerY);
        make.height.offset(20);
    }];
}
-(void)setTitleStr:(NSString *)titleStr{
    _titleStr = titleStr;
    _titleLabel.text = _titleStr;
}
@end
