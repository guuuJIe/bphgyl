//
//  OrderGoodsView.m
//  519
//
//  Created by Macmini on 2018/1/12.
//  Copyright © 2018年 519. All rights reserved.
//

#import "OrderGoodsView.h"
#import "OrderDetailTC.h"
#import "GoodsDetailModel.h"
@interface OrderGoodsView()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)UILabel * storeTitle;
@property (nonatomic,strong)UITableView * tableView;
@property (nonatomic,strong)NSMutableArray * dataArr;
@end

@implementation OrderGoodsView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.dataArr = [NSMutableArray new];
        self.backgroundColor = [UIColor whiteColor];
        [self createUI];
    }
    return self;
}

-(void)createUI{
    UIImageView * imageV = [[UIImageView alloc]init];
    imageV.image = [UIImage imageNamed:@"icon_自营"];
    [self addSubview:imageV];
    [imageV makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(10);
        make.top.offset(13);
        make.height.width.offset(15);
    }];
    
    _storeTitle = [[UILabel alloc]init];
    _storeTitle.text = @"泊啤汇自营";
    _storeTitle.textColor = APPFourColor;
    _storeTitle.textAlignment = NSTextAlignmentLeft;
    _storeTitle.font = [UIFont systemFontOfSize:13];
    [self addSubview:_storeTitle];
    [_storeTitle makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageV.mas_right).with.offset(10);
        make.top.offset(10);
        make.height.offset(20);
    }];
    
    [self addSubview:self.tableView];
    [_tableView makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_storeTitle.mas_bottom).with.offset(10);
        make.left.right.offset(0);
        make.bottom.offset(0);
    }];
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderDetailTC * cell = [tableView dequeueReusableCellWithIdentifier:[OrderDetailTC getID]];
    if (cell == nil) {
        cell = [[OrderDetailTC alloc]initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier:[OrderDetailTC getID]];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    GoodsDetailModel * model = _dataArr[indexPath.row];
    cell.model = model;
    return  cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

-(void)showdataOfGoodData:(NSDictionary *)dic{
    for ( NSDictionary * subdic in dic[@"deal_order_item"]) {
        GoodsDetailModel * model = [GoodsDetailModel new];
        [model setValuesForKeysWithDictionary:subdic];
        [_dataArr addObject:model];
    }
    
    [_tableView reloadData];
    
    if (self.block) {
        self.block(_dataArr.count);
    }
    
    
}















-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:(UITableViewStylePlain)];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.scrollEnabled = NO;
    }
    return _tableView;
    
}

@end
