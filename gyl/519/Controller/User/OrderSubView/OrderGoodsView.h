//
//  OrderGoodsView.h
//  519
//
//  Created by Macmini on 2018/1/12.
//  Copyright © 2018年 519. All rights reserved.
//
typedef void(^orderGoodsBlock)(NSInteger indes);
#import <UIKit/UIKit.h>

@interface OrderGoodsView : UIView
@property (nonatomic,copy)orderGoodsBlock block;
-(void)showdataOfGoodData:(NSDictionary *)dic;

@end
