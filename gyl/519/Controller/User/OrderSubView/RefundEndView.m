//
//  RefundEndView.m
//  519
//
//  Created by Macmini on 2018/1/18.
//  Copyright © 2018年 519. All rights reserved.
//

#import "RefundEndView.h"

@interface RefundEndView()
@property(nonatomic,strong)UILabel * contentLabel;
@end

@implementation RefundEndView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = APPBGColor;
        [self createUI];
    }
    return self;
}
-(void)createUI{
    UIView * contentView = [[UIView alloc]init];
    contentView.backgroundColor = [UIColor whiteColor];
    [self addSubview:contentView];
    [contentView makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.offset(0);
        make.height.offset(100);
    }];
    UILabel * titleLabel = [[UILabel alloc]init];
    titleLabel.text = @"退款说明";
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.textColor = APPFourColor;
    titleLabel.font = [UIFont systemFontOfSize:13];
    [contentView addSubview:titleLabel];
    [titleLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.offset(10);
        make.height.offset(20);
    }];
    UIView * lineView = [[UIView alloc]init];
    lineView.backgroundColor = APPBGColor;
    [contentView addSubview:lineView];
    [lineView makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.top.equalTo(titleLabel.mas_bottom).offset(5);
        make.height.offset(1);
    }];
    _contentLabel = [[UILabel alloc]init];
    _contentLabel.textColor = APPFourColor;
    _contentLabel.textAlignment = NSTextAlignmentLeft;
    _contentLabel.font = [UIFont systemFontOfSize:12];
    _contentLabel.numberOfLines = 2;
    [contentView addSubview:_contentLabel];
    [_contentLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(10);
        make.right.offset(-10);
        make.top.equalTo(lineView.mas_bottom).offset(8);
        make.height.offset(40);
    }];
    
    
    UIView * infoView = [[UIView alloc]init];
    infoView.backgroundColor = [UIColor whiteColor];
    [self addSubview:infoView];
    [infoView makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.top.equalTo(contentView.mas_bottom).offset(10);
        make.height.offset(65);
    }];
}
-(void)setRefundDic:(NSDictionary *)refundDic{
    _refundDic = refundDic;
    
    
    
}

@end
