//
//  OrderInfoView.m
//  519
//
//  Created by Macmini on 2018/1/11.
//  Copyright © 2018年 519. All rights reserved.
//

#import "OrderInfoView.h"

@interface OrderInfoView()
@property (nonatomic,strong)NSTimer * timer;

@property (nonatomic,strong)UIView * orderTypeView;
@property (nonatomic,strong)UILabel * orderTypeLabel;
@property (nonatomic,strong)UIView * addressView;
@property (nonatomic,strong)UILabel * nameLabel;
@property (nonatomic,strong)UILabel * phoneLabel;
@property (nonatomic,strong)UILabel * addressLabel;

@property (nonatomic,strong)UIView * contentView;
@property (nonatomic,strong)UILabel * contentLabel;
@property (nonatomic,strong)UILabel * contentTitle;

@end

@implementation OrderInfoView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame: frame];
    if (self) {
        self.backgroundColor = APPBGColor;
        [self createUI];
        
    }
    return self;
}

-(void)createUI{
    /*******/
    _orderTypeView = [[UIView alloc]init];
    [self addSubview:_orderTypeView];
    [_orderTypeView makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.offset(0);
        make.height.offset(80);
    }];
    UIImageView * imageView = [[UIImageView alloc]init];
    imageView.image = [UIImage imageNamed:@"img_状态背景"];
    [_orderTypeView addSubview:imageView];
    [imageView makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.offset(0);
    }];
    _orderTypeLabel = [[UILabel alloc]init];
    _orderTypeLabel.textColor = [UIColor whiteColor];
    _orderTypeLabel.textAlignment = NSTextAlignmentLeft;
    _orderTypeLabel.font = [UIFont systemFontOfSize:15];
    [_orderTypeView addSubview:_orderTypeLabel];
    [_orderTypeLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(34);
        make.top.offset(30);
        make.height.offset(20);
    }];
    
    /*******/
    _addressView = [[UIView alloc]init];
    _addressView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_addressView];
    [_addressView makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.top.equalTo(_orderTypeView.mas_bottom).with.offset(0);
        make.height.offset(60);
    }];
    UIImageView * locationImgV = [[UIImageView alloc]init];
    locationImgV.image = [UIImage imageNamed:@"icon_收货地址"];
    [_addressView addSubview:locationImgV];
    [locationImgV makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(10);
        make.centerY.equalTo(_addressView.mas_centerY);
        make.width.height.offset(14);
    }];
    _nameLabel = [[UILabel alloc]init];
    _nameLabel.textColor = APPFourColor;
    _nameLabel.text = @"收货人:xxxxxx";
    _nameLabel.textAlignment = NSTextAlignmentLeft;
    _nameLabel.font = [UIFont systemFontOfSize:13];
    [_addressView addSubview:_nameLabel];
    [_nameLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(locationImgV.mas_right).with.offset(10);
        make.top.offset(10);
        make.height.offset(20);
    }];
    _phoneLabel = [[UILabel alloc]init];
    _phoneLabel.text = @"13655552222";
    _phoneLabel.textAlignment = NSTextAlignmentRight;
    _phoneLabel.textColor = APPFourColor;
    _phoneLabel.font = [UIFont systemFontOfSize:13];
    [_addressView addSubview:_phoneLabel];
    [_phoneLabel makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(10);
        make.right.offset(-10);
        make.height.offset(20);
    }];
    _addressLabel = [[UILabel alloc]init];
    _addressLabel.text = @"中国 浙江 温州 撸曾zzzzzzzzzzzzzzzz";
    _addressLabel.textColor = APPFourColor;
    _addressLabel.textAlignment = NSTextAlignmentLeft;
    _addressLabel.font = [UIFont systemFontOfSize:13];
    [_addressView addSubview:_addressLabel];
    [_addressLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(locationImgV.mas_right).with.offset(10);
        make.top.equalTo(_nameLabel.mas_bottom).with.offset(5);
        make.width.offset(Screen_Width-50);
        make.height.offset(20);
    }];
    
    /*******/
    _contentView = [[UIView alloc]init];
    _contentView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_contentView];
    [_contentView makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.top.equalTo(_addressView.mas_bottom).with.offset(10);
        make.height.offset(60);
    }];
    UIImageView * iconImgV = [[UIImageView alloc]init];
    iconImgV.image = [UIImage imageNamed:@"icon_买家留言"];
    [_contentView addSubview:iconImgV];
    [iconImgV makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(10);
        make.centerY.equalTo(_contentView.mas_centerY);
        make.width.height.offset(14);
    }];
    _contentTitle = [[UILabel alloc]init];
    _contentTitle.text = @"买家留言:";
    _contentTitle.textColor = APPFourColor;
    _contentTitle.textAlignment = NSTextAlignmentLeft;
    _contentTitle.font = [UIFont systemFontOfSize:13];
    [_contentView addSubview:_contentTitle];
    [_contentTitle makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(iconImgV.mas_right).with.offset(10);
        make.top.offset(10);
        make.height.offset(20);
    }];
    _contentLabel = [[UILabel alloc]init];
    _contentLabel.textAlignment = NSTextAlignmentLeft;
    _contentLabel.textColor = APPThreeColor;
    _contentLabel.font = [UIFont systemFontOfSize:13];
    _contentLabel.numberOfLines = 0;
    _contentLabel.text = @"";
    [_contentView addSubview:_contentLabel];
    [_contentLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(iconImgV.mas_right).with.offset(10);
        make.top.equalTo(_contentTitle.mas_bottom).with.equalTo(5);
        make.width.offset(Screen_Width-40);
    }];
}

-(void)showOrderInfoData:(NSDictionary *)dic{
    _orderTypeLabel.text = dic[@"order_status_str"];
    if ([dic[@"order_status_str"] isEqualToString:@"未付款"]) {
        [_orderTypeLabel updateConstraints:^(MASConstraintMaker *make) {
            make.left.offset(34);
            make.top.offset(20);
            make.height.offset(20);
        }];
        
        UILabel * timeLabel = [[UILabel alloc]init];
        timeLabel.textAlignment = NSTextAlignmentLeft;
        timeLabel.textColor = [UIColor whiteColor];
        timeLabel.font = [UIFont systemFontOfSize:12];
        timeLabel.text = [self getNowTimeWithString:[NSString stringWithFormat:@"%@",dic[@"create_time"]]];
        [_orderTypeView addSubview:timeLabel];
        [timeLabel makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(34);
            make.top.equalTo(_orderTypeLabel.mas_bottom).offset(0);
            make.height.offset(20);
        }];
    }
    
    
    
    _nameLabel.text = dic[@"consignee"];
    _phoneLabel.text = dic[@"mobile"];
    _addressLabel.text = dic[@"address"];
    NSString * content = [NSString stringWithFormat:@"%@",dic[@"memo"]];
    if (content.length == 0) {
        content = @"未留言";
    }
    
    if ([_orderTypeLabel.text isEqualToString:@"退款成功"] || [_orderTypeLabel.text isEqualToString:@"退款申请中"]) {
        _contentTitle.text = @"退款原因";
        _contentLabel.text = dic[@"order_status_str"];
    }else{
        _contentLabel.text = content;
    }
    
    
    CGFloat height = [self heightForString:content andWidth:(Screen_Width-40)];
    [self.contentView updateConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.top.equalTo(_addressView.mas_bottom).with.offset(10);
        make.height.offset(50+height);
    }];
    if (self.block) {
        self.block(height);
    }
}
- (float) heightForString:(NSString *)value andWidth:(float)width{
    //获取当前文本的属性
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:value];
    NSRange range = NSMakeRange(0, attrStr.length);
    // 获取该段attributedString的属性字典
    NSDictionary *dic = [attrStr attributesAtIndex:0 effectiveRange:&range];
    // 计算文本的大小
    CGSize sizeToFit = [value boundingRectWithSize:CGSizeMake(width - 16.0, MAXFLOAT) // 用于计算文本绘制时占据的矩形块
                                           options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading // 文本绘制时的附加选项
                                        attributes:dic        // 文字的属性
                                           context:nil].size; // context上下文。包括一些信息，例如如何调整字间距以及缩放。该对象包含的信息将用于文本绘制。该参数可为nil
    return sizeToFit.height;
}

//时间处理
-(NSString *)getNowTimeWithString:(NSString *)aTimeString{
    NSDate  *nowDate = [NSDate date];
    NSTimeInterval timeinter = [nowDate timeIntervalSince1970];
    int endTime = (int)timeinter - [aTimeString intValue];
    int endtime2 = (3600*48)-endTime;
    if (endtime2 < 0) {

        return @"超时未支付 订单已失效！";
    }
    int hours = endtime2/3600;
    int minutes = endtime2 % 3600 / 60;
    
   NSString *hoursStr;NSString *minutesStr;
    //小时
    hoursStr = [NSString stringWithFormat:@"%d",hours];
    //分钟
    if(minutes<Default_Space)
        minutesStr = [NSString stringWithFormat:@"0%d",minutes];
    else
        minutesStr = [NSString stringWithFormat:@"%d",minutes];
    return [NSString stringWithFormat:@"请在%@小时%@分内完成支付，超时订单自动取消",hoursStr , minutesStr];
}
@end
