//
//  OrderInfoView.h
//  519
//
//  Created by Macmini on 2018/1/11.
//  Copyright © 2018年 519. All rights reserved.
//
typedef void(^orderHeightBlock)(CGFloat height);
#import <UIKit/UIKit.h>

@interface OrderInfoView : UIView
@property(nonatomic,copy)orderHeightBlock block;

-(void)showOrderInfoData:(NSDictionary *)dic;

@end
