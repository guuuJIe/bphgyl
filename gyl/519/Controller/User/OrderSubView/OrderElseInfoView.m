//
//  OrderElseInfoView.m
//  519
//
//  Created by Macmini on 2018/1/12.
//  Copyright © 2018年 519. All rights reserved.
//

#import "OrderElseInfoView.h"
#import "RefundEndView.m"
@interface OrderElseInfoView()

@property (nonatomic,strong)UIView * priceView;
@property (nonatomic,strong)UILabel * goodPriceLabel;
@property (nonatomic,strong)UILabel * yunfeiLabel;
@property (nonatomic,strong)UILabel * allPriceLabel;
@property (nonatomic,strong)UIView * infoView;
@property (nonatomic,strong)RefundEndView * endView;
@end

@implementation OrderElseInfoView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}
-(void)createUI{

    _priceView = [[UIView alloc]init];
    _priceView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_priceView];
    [_priceView makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.offset(0);
        make.height.offset(90);
    }];
    UILabel * oneTitle = [[UILabel alloc]init];
    oneTitle.text = @"商品总额";
    oneTitle.textColor = APPThreeColor;
    oneTitle.textAlignment = NSTextAlignmentLeft;
    oneTitle.font = [UIFont systemFontOfSize:13];
    [_priceView addSubview:oneTitle];
    [oneTitle makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.offset(10);
        make.height.offset(20);
    }];
    
    _goodPriceLabel = [[UILabel alloc]init];
    _goodPriceLabel.text = @"";
    _goodPriceLabel.textColor = APPThreeColor;
    _goodPriceLabel.textAlignment = NSTextAlignmentLeft;
    _goodPriceLabel.font = [UIFont systemFontOfSize:13];
    [_priceView addSubview:_goodPriceLabel];
    [_goodPriceLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(oneTitle.mas_centerY);
        make.right.offset(-10);
        make.height.offset(20);
    }];
    UILabel * twoTitle = [[UILabel alloc]init];
    twoTitle.text = @"运费:";
    twoTitle.textColor = APPThreeColor;
    twoTitle.textAlignment = NSTextAlignmentLeft;
    twoTitle.font = [UIFont systemFontOfSize:13];
    [_priceView addSubview:twoTitle];
    [twoTitle makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(oneTitle.mas_bottom).offset(0);
        make.left.offset(10);
        make.height.offset(20);
    }];
    
    _yunfeiLabel = [[UILabel alloc]init];
    _yunfeiLabel.text = @"111221";
    _yunfeiLabel.textColor = APPThreeColor;
    _yunfeiLabel.textAlignment = NSTextAlignmentLeft;
    _yunfeiLabel.font = [UIFont systemFontOfSize:13];
    [_priceView addSubview:_yunfeiLabel];
    [_yunfeiLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(twoTitle.mas_centerY);
        make.right.offset(-10);
        make.height.offset(20);
    }];
    UIView * lineView = [[UIView alloc]init];
    lineView.backgroundColor =APPBGColor;
    [_priceView addSubview:lineView];
    [lineView makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.top.equalTo(twoTitle.mas_bottom).with.offset(5);
        make.height.offset(1);
    }];
    
    UILabel * threeLabel = [[UILabel alloc]init];
    threeLabel.text = @"实付款:";
    threeLabel.textColor = APPFourColor;
    threeLabel.textAlignment = NSTextAlignmentLeft;
    threeLabel.font = [UIFont systemFontOfSize:14];
    [_priceView addSubview:threeLabel];
    [threeLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(10);
        make.top.equalTo(lineView.mas_bottom).with.offset(5);
        make.height.offset(20);
    }];
    
    _allPriceLabel = [[UILabel alloc]init];
    _allPriceLabel.text = @"111221";
    _allPriceLabel.textColor = APPColor;
    _allPriceLabel.textAlignment = NSTextAlignmentLeft;
    _allPriceLabel.font = [UIFont systemFontOfSize:14];
    [_priceView addSubview:_allPriceLabel];
    [_allPriceLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(threeLabel.mas_centerY);
        make.right.offset(-10);
        make.height.offset(20);
    }];
    
    
    _infoView = [[UIView alloc]init];
    _infoView.backgroundColor = [UIColor  whiteColor];
    [self addSubview:_infoView];
    [_infoView makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.top.equalTo(_priceView.mas_bottom).with.offset(10);
        make.height.offset(10);
    }];
    
    
  
    
}

-(void)showdataOfOrderElseInfo:(NSDictionary *)dic{
    
    
    if ([dic[@"return_total_score"] floatValue] > 0.0) {
        _goodPriceLabel.text = [NSString stringWithFormat:@"%@积分",dic[@"return_total_score"]];
        _yunfeiLabel.text = [NSString stringWithFormat:@"￥%@",dic[@"delivery_fee"]];
        _allPriceLabel.text = [NSString stringWithFormat:@"%@积分%@",dic[@"return_total_score"],[dic[@"pay_amount"] floatValue] > 0.0 ? [NSString stringWithFormat:@"%@元",dic[@"pay_amount"]]:@""];
    }else{
        _goodPriceLabel.text = [NSString stringWithFormat:@"￥%@",dic[@"deal_total_price"]];
        _yunfeiLabel.text = [NSString stringWithFormat:@"￥%@",dic[@"delivery_fee"]];
        _allPriceLabel.text = [NSString stringWithFormat:@"￥%@",dic[@"pay_amount"]];
    }
  
    
    NSMutableArray * detailData = [NSMutableArray new];
    [detailData addObject:[NSString stringWithFormat:@"订单编号: %@",dic[@"order_sn"]]];
    [detailData addObject:[NSString stringWithFormat:@"下单时间: %@",[self timerStampString:dic[@"create_time"]]]];
    NSString * pay_time = [NSString stringWithFormat:@"%@",dic[@"pay_time"]];
    if ([pay_time isEqualToString:@"0"]) {
        pay_time = [NSString stringWithFormat:@"支付时间: "];
        [detailData addObject:pay_time];
    }else{
        pay_time = [NSString stringWithFormat:@"支付时间: %@",[self timerStampString:pay_time]];
        [detailData addObject:pay_time];
    }
    if (![dic[@"pay_time"] isEqual:[NSNull null]]) {
       
    }
    NSString * order_sn = [NSString stringWithFormat:@"%@",dic[@"delivery_notice_sn"]];
    if (order_sn.length > 0) {
        [detailData addObject:[NSString stringWithFormat:@"订单编号: %@",order_sn]];
    }
    [detailData addObject:[NSString stringWithFormat:@"配送方式: %@",dic[@"delivery_name"]]];
    
    for (NSInteger i = 0; i<detailData.count; i++) {
        UILabel * label = [[UILabel alloc]init];
        label.text = detailData[i];
        label.textColor = APPThreeColor;
        label.textAlignment = NSTextAlignmentLeft;
        label.font = [UIFont systemFontOfSize:15];
        [_infoView addSubview:label];
        [label makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(i*30+5);
            make.left.offset(10);
            make.height.offset(30);
        }];
        
    }
    [_infoView updateConstraints:^(MASConstraintMaker *make) {
        make.left.right.offset(0);
        make.top.equalTo(_priceView.mas_bottom).with.offset(10);
        make.height.offset(30*detailData.count+10);
    }];
    
    if (self.block) {
        self.block(detailData.count);
    }
    
    
}

-(NSString *)timerStampString:(NSString *)timer{
    NSString *publishString = @"";
    
    double publishLong = [timer doubleValue];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [formatter setDateFormat:@"yyyy.MM.dd HH:mm:ss"];
    
    NSDate *publishDate = [NSDate dateWithTimeIntervalSince1970:publishLong+28800];
    
    NSDate *date = [NSDate date];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate:date];
    publishDate = [publishDate  dateByAddingTimeInterval: interval];
    
    publishString = [formatter stringFromDate:publishDate];
    
    return publishString;
}
@end
