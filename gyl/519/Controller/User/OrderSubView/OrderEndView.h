//
//  OrderEndView.h
//  519
//
//  Created by Macmini on 2018/1/13.
//  Copyright © 2018年 519. All rights reserved.
//
typedef void(^tellBlock)(NSString * phoneNum);
typedef void(^orderTypeBtnBlock)(NSString * str);
#import <UIKit/UIKit.h>

@interface OrderEndView : UIView
@property (nonatomic,copy)orderTypeBtnBlock block;
@property (nonatomic,copy)tellBlock tellblock;
@property(nonatomic,assign)NSInteger ispay;
-(void)showDataOfOrderEndView:(NSDictionary *)dic;
@end
