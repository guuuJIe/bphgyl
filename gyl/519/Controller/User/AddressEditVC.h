//
//  AddressEditVC.h
//  519
//
//  Created by 陈 on 16/9/19.
//  Copyright © 2016年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressEditVC : UIViewController

@property(nonatomic,copy)void(^pushBlock)(NSString * name,NSString * phone,NSString * address);
//编辑地址 新增地址
@property(nonatomic,copy)NSString *typeStr;

@property(nonatomic,copy)NSString *nameStr;
@property(nonatomic,copy)NSString *phoneStr;
@property(nonatomic,copy)NSString *addressStr;
@property(nonatomic,copy)NSString *regoinStr;
@property(nonatomic,copy)NSString *ids;
@property(nonatomic,copy)NSString *isdefualt;
@property(nonatomic,copy)NSString * isedit;
@property(nonatomic,copy)NSString * isBuy;

@end
