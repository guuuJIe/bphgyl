//
//  RefundVC.h
//  519
//
//  Created by Macmini on 16/12/13.
//  Copyright © 2016年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RefundVC : UIViewController
@property(nonatomic,copy)NSString * orderId;

@property(nonatomic,copy)NSString * icon;
@property(nonatomic,copy)NSString * goodsname;
@property(nonatomic,copy)NSString * goodsnumber;
@property(nonatomic,copy)NSString * goodsprice;

@property(nonatomic,strong)NSArray * refundArr;

@end
