//
//  OrderDetailVC.h
//  519
//
//  Created by Macmini on 16/12/12.
//  Copyright © 2016年 519. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailVC : UIViewController
@property(nonatomic,copy)NSString * orderDetailId;
@property(nonatomic,assign)NSInteger index;
@property(nonatomic,copy)NSString * vcName;
@property(nonatomic,strong)NSArray * detailArr;
@end
