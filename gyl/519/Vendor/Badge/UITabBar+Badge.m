//
//  UITabBar+Badge.m
//  519
//
//  Created by 陈 on 16/9/3.
//  Copyright © 2016年 519. All rights reserved.
//

#import "UITabBar+Badge.h"

@implementation UITabBar (Badge)

#define TabbarItemNums 4.0    //tabbar的数量 如果是5个设置为5.0

//显示小红点
- (void)showBadgeOnItemIndex:(int)index value:(NSString*)value{
    //移除之前的小红点
    [self removeBadgeOnItemIndex:index];
    
    //新建小红点
    UILabel *badgeView = [[UILabel alloc]init];
    badgeView.tag = 888 + index;
    CGRect tabFrame = self.frame;
    
//    if ([value integerValue] > 100) {
//
//    }else{
//
//    }
    badgeView.text=value;
    

    CGSize size = [badgeView.text sizeWithAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:FONT_SIZE_S]}];
    
    //ceilf()向上取整函数, 只要大于1就取整数2. floor()向下取整函数, 只要小于2就取整数1.
    
    CGSize adaptionSize = CGSizeMake(ceilf(size.width), ceilf(size.height));
    
    //确定小红点的位置
    float percentX = (index +0.5) / TabbarItemNums;
    CGFloat x = ceilf(percentX * tabFrame.size.width);
    CGFloat y = ceilf(0.07 * tabFrame.size.height);
    badgeView.frame = CGRectMake(x, y, adaptionSize.width+8, adaptionSize.width+8);//圆形大小为10
  
    badgeView.layer.cornerRadius = badgeView.frame.size.width/2;//圆形
    badgeView.layer.masksToBounds=YES;
    badgeView.backgroundColor = APPColor;
    badgeView.font=[UIFont systemFontOfSize:FONT_SIZE_S];
    badgeView.textColor=[UIColor whiteColor];
    badgeView.textAlignment=NSTextAlignmentCenter;
    [self addSubview:badgeView];
}

//隐藏小红点
- (void)hideBadgeOnItemIndex:(int)index{
    //移除小红点
    [self removeBadgeOnItemIndex:index];
}

//移除小红点
- (void)removeBadgeOnItemIndex:(int)index{
    //按照tag值进行移除
    for (UIView *subView in self.subviews) {
        if (subView.tag == 888+index) {
            [subView removeFromSuperview];
        }
    }
}
@end
