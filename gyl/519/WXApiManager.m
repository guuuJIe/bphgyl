//
//  WXApiManager.m
//  519
//
//  Created by Macmini on 17/1/7.
//  Copyright © 2017年 519. All rights reserved.
//

#import "WXApiManager.h"

@implementation WXApiManager
+(instancetype)sharedManager {
    static dispatch_once_t onceToken;
    static WXApiManager *instance;
    dispatch_once(&onceToken, ^{
        instance = [[WXApiManager alloc] init];
    });
    return instance;
}

- (void)dealloc {
    self.delegate = nil;
    
}

- (void)onResp:(BaseResp *)resp {
    if ([resp isKindOfClass:[SendAuthResp class]]) {
        if (_delegate
            && [_delegate respondsToSelector:@selector(managerDidRecvAuthResponse:)]) {
            SendAuthResp *authResp = (SendAuthResp *)resp;
            [_delegate managerDidRecvAuthResponse:authResp];
        }
    }if ([resp isKindOfClass:[PayResp class]]){
        if (_delegate && [_delegate respondsToSelector:@selector(managerJunpPay:)]) {
            PayResp *authResp = (PayResp *)resp;
            [_delegate managerJunpPay:authResp];
        }
        
    }
}

@end
