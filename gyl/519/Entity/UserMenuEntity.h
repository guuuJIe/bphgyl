//
//  UserMenuEntity.h
//  519
//
//  Created by 陈 on 16/9/3.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseEntity.h"

@interface UserMenuEntity : BaseEntity
@property(nonatomic,copy)NSString *icon;
@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *info;

@end
