//
//  EvaEntity.h
//  519
//
//  Created by 陈 on 16/9/6.
//  Copyright © 2016年 519. All rights reserved.
//

#import "BaseEntity.h"

@interface EvaEntity : BaseEntity

@property(nonatomic,copy)NSString *avatar;
@property(nonatomic,copy)NSString *user_name;
@property(nonatomic,copy)NSString *point;
@property(nonatomic,copy)NSString *content;
@property(nonatomic,copy)NSString *create_time;

@end
