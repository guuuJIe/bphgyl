//
//  RequestData.m
//  519
//
//  Created by Macmini on 16/11/30.
//  Copyright © 2016年 519. All rights reserved.
//

#import "RequestData.h"
#import "base64.h"


@implementation RequestData

+(void)requestDataUrl:(NSString *)urlString success:(void (^)(id))success failure:(void (^)(NSError *))failure{
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    // 设置超时时间
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10.f;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    NSDictionary *dic=manager.requestSerializer.HTTPRequestHeaders;
    NSString *oldStr=[dic valueForKey:@"User-Agent"];
    if (![oldStr containsString:@"bOpIhUi90_"]) {
        NSString *newStr=[oldStr stringByAppendingString:@"bOpIhUi90_"];
        [manager.requestSerializer setValue:newStr forHTTPHeaderField:@"User-Agent"];
    }
    NSLog(@"url -- %@",urlString);
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    

    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSString *base64Decoded = [[NSString alloc]
                                   initWithData:responseObject encoding:NSUTF8StringEncoding];
        
        NSData * jsondatastring = [Base64 decodeString:base64Decoded];
        
        NSDictionary * dic = [NSJSONSerialization JSONObjectWithData:jsondatastring options:NSJSONReadingMutableLeaves error:nil];
//        NSLog(@"%@",dic);
        success(dic);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        failure(error);
    }];
}

+(void)requestDataOfUrl:(NSString *)urlstring success:(void (^)(NSDictionary * dic))success failure:(void (^)(NSError * error))failure{
    
    [RequestData requestDataUrl:urlstring success:success failure:failure];
    
}

@end
