//
//  UIImage+LJImage.h
//  CCBao
//
//  Created by liuleijie on 15/2/9.
//  Copyright (c) 2015年 liuleijie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (LJImage)

- (UIImage *)fixOrientation;
-(UIImage*)scaleToSize:(CGSize)size;

@end
