//
//  NSString+StringHelper.h
//  CommonLibrary
//
//  Created by rang on 13-1-7.
//  Copyright (c) 2013年 rang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"

@interface NSString (TPCategory)
/** Returns a `NSString`.
 @return a guid string.
 */
+(NSString*)createGUID;

/** 判断字符串是否唯恐.
 @return 如果为nil或者为“”，返回YES
 */
+(NSString *)toUnix2ToTime:(NSString *)str;
-(BOOL)toBOOL;
-(int)toInt;
-(int)toInt : (int)defaultValue;
-(long)toLng;
-(float)toFloat;


+(NSString *)getHtml:(NSString *)str;

+(float) getHeight:(NSString *)value font:(UIFont*)font andWidth:(float)width;
+(CGSize)getCGSize:(NSString *)str font:(UIFont*)font;

+(NSString*)toTime:(NSString *)timeStr formatterNow:(NSString *)formatterNow formatterChange:(NSString *)formatterChange;

+(NSString*)toDateString : (NSString*)str;
+(NSString*)toDateTimeString : (NSString*)str;

-(NSString*)unixToDateStr:(NSString *)formatterStr;
-(NSDate*)toDate;
-(NSDate*)toDate : (NSString*)format;

+(NSString*)fromBOOL : (BOOL) value;
+(NSString*)fromInt : (int) value;
+(NSString*)fromLong : (long) value;
+(NSString*)fromFloat : (float) value;
+(NSString*)fromFloat : (float)value digits:(int)fractionalDigits;
+(NSString*)fromNumber : (NSNumber*) value;
+(NSString*)fromNumber : (NSNumber*)value digits:(int)fractionalDigits;
+(NSString*)fromObject : (id) value;
//判断是否为整形：
+(BOOL)isInt:(NSString*)str;
//判断是否为浮点形：
+(BOOL)isNumberic:(NSString*)str;
+(BOOL)isNULL : (NSString*)str;
+(BOOL)isEmpty : (NSString*)str;
+(BOOL)isEmptyOfJOSN : (NSString*)str;
+(BOOL)isAnyTypeOfJson : (NSString*)str;
-(int)compareTo : (NSString*)str;
-(BOOL)equals : (NSString*)str;
-(BOOL)equalsIgnoreCase : (NSString*)str;

//开始匹配区分大小写）
-(BOOL) startWith : (NSString*) prefix;

//结尾匹配（区分大小写）
-(BOOL) endWith : (NSString*) suffix;

-(NSString*)replace : (NSString*) oldStr : (NSString*) newStr;

-(NSString*) subString : (int)start;
-(NSString*) subString : (int)start : (int)count;

-(NSArray*) split : (NSString*) schar;
-(NSArray*) splitByChars : (NSCharacterSet*) schar;
/** 向前查找字符串.
 @return 查找到字符串的位置,否则返回-1
 */
-(NSInteger)indexOf:(NSString*)search;

-(NSInteger)indexOfChar:(unichar)searchChar;
-(NSInteger)indexOfChar:(unichar)searchChar : (int) startIndex;

/** 向后查找字符串.
 @return 查找到字符串的位置,否则返回-1
 */
-(NSInteger)lastIndexOf:(NSString*)search;
/** 去除字符串前后空格.
 @return 去除字符串前后空格的字符串
 */
-(NSString*)trim;

//读取资源图像的路径
+(NSString*)getResourceFileFullPath : (NSString*)fileName;

/** 根据字体与宽度计算字符串的大小.
 @return 获取文本大小
 */
-(CGSize)textSize:(UIFont*)f withWidth:(CGFloat)w;

/**
 *  计算文字的Size大小
 *
 *  @param text    文字内容
 *  @param font    文字字体
 *  @param maxSize 文字最大尺寸
 */
+ (CGSize)sizeWithText:(NSString *)text font:(UIFont *)font maxSize:(CGSize)maxSize;

/** Returns an MD5 string of from the given `NSString`.
 @return A MD5 string.
 */
- (NSString *) stringFromMD5;
/**
 Returns a string of the SHA1 sum of the receiver.
 @return The string of the SHA1 sum of the receiver.
 */
- (NSString *)SHA1Sum;
/**
 Returns a string of the SHA256 sum of the receiver.
 
 @return The string of the SHA256 sum of the receiver.
 */
- (NSString *)SHA256Sum;


- (NSString *)AES256EncryptWithKey;
- (NSString *)AES256EncryptWithKey:(NSString *)key;
- (NSString *)AES256DecryptWithKey;
- (NSString *)AES256DecryptWithKey:(NSString *)key;

/** Returns a `NSString` that is URL friendly.
 @return A URL encoded string.
 */
-(NSString*)URLEncode;
/**
 Returns a new string encoded for a URL parameter. (Deprecated)
 
 The following characters are encoded: `:/=,!$&'()*+;[]@#?`.
 
 @return A new string escaped for a URL parameter.
 */
- (NSString *)URLEncodedParameterString;

/**
 Returns a new string decoded from a URL.
 
 @return A new string decoded from a URL.
 */
- (NSString *)URLDecodedString;

/** Returns `YES` if a string is a valid email address, otherwise `NO`.
 @return True if the string is formatted properly as an email address.
 */
- (BOOL) isEmail;
- (BOOL) isPhone;
+ (BOOL)isPureInt:(NSString *)string;
- (BOOL) isURLString;
/** Returns a `NSString` that properly replaces HTML specific character sequences.
 @return An escaped HTML string.
 */
- (NSString *) escapeHTML;

/** Returns a `NSString` that properly formats text for HTML.
 @return An unescaped HTML string.
 */
- (NSString *) unescapeHTML;
///----------------------------------
/// @name URL Escaping and Unescaping
///----------------------------------

/**
 Returns a new string escaped for a URL query parameter.
 
 The following characters are escaped: `\n\r:/=,!$&'()*+;[]@#?%`. Spaces are escaped to the `+` character. (`+` is
 escaped to `%2B`).
 
 @return A new string escaped for a URL query parameter.
 
 @see stringByUnescapingFromURLQuery
 */
- (NSString *)stringByEscapingForURLQuery;

/**
 Returns a new string unescaped from a URL query parameter.
 
 `+` characters are unescaped to spaces.
 
 @return A new string escaped for a URL query parameter.
 
 @see stringByEscapingForURLQuery
 */
- (NSString *)stringByUnescapingFromURLQuery;

/** Returns a `NSString` that removes HTML elements.
 @return Returns a string without the HTML elements.
 */
- (NSString*) stringByRemovingHTML;
/** Returns `YES` is a string has the substring, otherwise `NO`.
 @param substring The substring.
 @return `YES` if the substring is contained in the string, otherwise `NO`.
 */
- (BOOL) hasString:(NSString*)substring;

///----------------------
/// @name Base64 Encoding
///----------------------

+(id)getJSONObject : (NSString*)jsonString;

/**
 Returns a string representation of the receiver Base64 encoded.
 
 @return A Base64 encoded string
 */
- (NSString *)base64EncodedString;

/**
 Returns a new string contained in the Base64 encoded string.
 
 This uses `NSData`'s `dataWithBase64String:` method to do the conversion then initializes a string with the resulting
 `NSData` object using `NSUTF8StringEncoding`.
 
 @param base64String A Base64 encoded string
 
 @return String contained in `base64String`
 */
+ (NSString *)stringWithBase64String:(NSString *)base64String;



- (CGSize)getSizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size;

- (CGFloat)getHeightWithFont:(UIFont *)font constrainedToSize:(CGSize)size;
- (CGFloat)getWidthWithFont:(UIFont *)font constrainedToSize:(CGSize)size;
//判断是否为手机号码
+ (NSString *)valiMobile:(NSString *)mobile;

+(NSString *)getHtmlData:(NSString *)str;
//url解析
+(NSString *) urlKeyValue:(NSString *)CS webaddress:(NSString *)webaddress;


@end
