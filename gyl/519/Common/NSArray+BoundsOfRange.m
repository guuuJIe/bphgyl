//
//  NSArray+BoundsOfRange.m
//  519
//
//  Created by Macmini on 2019/7/29.
//  Copyright © 2019 519. All rights reserved.
//

#import "NSArray+BoundsOfRange.h"
#import <objc/message.h>
@implementation NSArray (BoundsOfRange)

+ (void)load{
    
    [super load];
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
//        Method oldObjectAtIndex = class_getInstanceMethod(objc_getClass("__NSArrayI"), @selector(objectAtIndex:));
//        Method newObjectAtIndex = class_getInstanceMethod(objc_getClass("__NSArrayI"), @selector(newObjectAtIndex:));
//        method_exchangeImplementations(oldObjectAtIndex, newObjectAtIndex);
//
//        // 替换可变数组中的方法 objectAtIndex
//        Method oldMObjectAtIndex = class_getInstanceMethod(objc_getClass("__NSArrayM"), @selector(objectAtIndex:));
//        Method newMObjectAtIndex = class_getInstanceMethod(objc_getClass("__NSArrayM"), @selector(newMutableObjectAtIndex:));
//        method_exchangeImplementations(oldMObjectAtIndex, newMObjectAtIndex);
        
        
        // 替换可变数组中的方法 objectAtIndex
        Method oldSubScriptObjectAtIndex = class_getInstanceMethod(objc_getClass("__NSArrayM"), @selector(objectAtIndexedSubscript:));
        Method newSubScriptObjectAtIndex = class_getInstanceMethod(objc_getClass("__NSArrayM"), @selector(newMutableobjectAtIndexedSubscript:));
        method_exchangeImplementations(oldSubScriptObjectAtIndex, newSubScriptObjectAtIndex);
    });
}

- (id)newMutableobjectAtIndexedSubscript:(NSUInteger)index{
    
    if (index > self.count || self.count) {
        @try {
            return [self newMutableobjectAtIndexedSubscript:index];
        } @catch (NSException *exception) {
            NSLog(@"可变数组越界");
            return nil;
        } @finally {
            
        }
    }else{
        return [self newMutableobjectAtIndexedSubscript:index];
    }
    
}
@end
