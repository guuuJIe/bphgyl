
#import "ConfigHelper.h"

@interface ConfigHelper ()


@end

@implementation ConfigHelper



+(void)updateValue:(NSString*)itemName : (NSString*)itemValue{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:itemValue forKey:itemName];
    [userDefaults synchronize];
}

+(void)updateIntValue:(NSString*)itemName : (int)itemValue{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:[NSNumber numberWithInt:itemValue] forKey:itemName];
    [userDefaults synchronize];
}

+(void)updateObjValue:(NSString*)itemName : (id)itemValue{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:itemValue forKey:itemName];
    [userDefaults synchronize];
}

+(id)getValue:(NSString*)itemName{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    id value = [userDefaults objectForKey : itemName ];
    return value;
}


+(int)getIntValue:(NSString*)itemName{
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    return (int)[userDefaults integerForKey: itemName];
    
}


+(NSString*)getStringValue:(NSString*)itemName{
    id value = [self getValue:itemName];
    return [NSString stringWithFormat:@"%@", value];
}



@end