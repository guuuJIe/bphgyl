//
//  CALayer+CGColor.m
//  519
//
//  Created by Macmini on 2017/12/11.
//  Copyright © 2017年 519. All rights reserved.
//

#import "CALayer+CGColor.h"

@implementation CALayer (CGColor)
- (void)setBorderColorWithUIColor:(UIColor *)color
{
    self.borderColor = color.CGColor;
}
@end
