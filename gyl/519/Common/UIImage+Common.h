//
//  UIImage+Common.h
//  CarCool_iOS
//
//  Created by cliff on 14-8-4.
//  Copyright (c) 2014年 Coding. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>


@interface UIImage (Common)


+(UIImage *)imageChangeSize:(UIImage *)image size:(CGSize)size;
+(NSString *)imageTo64String:(UIImage *)image;

+(UIImage *)imageWithColor:(UIColor *)aColor;
+(UIImage *)imageWithColor:(UIColor *)aColor withFrame:(CGRect)aFrame;
-(UIImage*)scaledToSize:(CGSize)targetSize;
-(UIImage*)scaledToSize:(CGSize)targetSize highQuality:(BOOL)highQuality;
+ (UIImage *)fullResolutionImageFromALAsset:(ALAsset *)asset;
+ (UIImage *)image:(UIImage *)image rotation:(UIImageOrientation)orientation;
+(UIImage *)imageWithColors:(UIColor *)color;
@end
