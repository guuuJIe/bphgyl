//
//  NSString+StringHelper.m
//  CommonLibrary
//
//  Created by rang on 13-1-7.
//  Copyright (c) 2014 cliff. All rights reserved.
//

#import "NSDate+TPCategory.h"

@implementation NSDate (TPCategory)

-(NSString*)toDateString{
    return [self toDateString : @"yyyy-MM-dd"];
}
-(NSString*)toDateTimeString{
    return [self toDateString : @"yyyy-MM-dd HH:mm:ss"];
}

-(NSString*)toDateString : (NSString*)format{
    if(!self) return @"";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    NSString *strDate = [dateFormatter stringFromDate:self];
    return strDate;
}

+(NSDate*)dateFromString : (NSString*)dateString {
   return [self dateFromString:dateString withFormat:@"yyyy-MM-dd HH:mm:ss"];
}

+(NSDate*)dateFromString : (NSString*)dateString  withFormat : (NSString*)format{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    @try {
        
        NSDate *date = [dateFormatter dateFromString:dateString];
        return date;
    }
    @catch (NSException *exception) {
        return nil;
    }
    @finally {
    }
}

-(NSDate*)addSeconds : (int)seconds{
    
    return [self addDate:0 :0 :0 :0 :0 :seconds];
}

-(NSDate*)addMinutes : (int)minutes{
    
    return [self addDate:0 :0 :0 :0 :minutes :0];
}

-(NSDate*)addHours : (int)hours{
    
    return [self addDate:0 :0 :0 :hours :0 :0];
}

-(NSDate*)addDays : (int)days{
    return [self addDate:0 :0 :days :0 :0 :0];
}


-(NSDate*)addMonths : (int)months{
    
     return [self addDate:0 :months :0 :0 :0 :0];
}


-(NSDate*)addYears : (int)years{
    return [self addDate:years :0 :0 :0 :0 :0];
}

-(NSDate*)addDate : (int)years : (int)months : (int)days : (int)hours : (int)minutes : (int)seconds{
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
    [offsetComponents setYear:years];
    [offsetComponents setMonth:months];
    [offsetComponents setDay:days];
    [offsetComponents setHour:hours];
    [offsetComponents setMinute:minutes];
    [offsetComponents setSecond:seconds];
    
    NSDate *newdate = [gregorian dateByAddingComponents:offsetComponents toDate:self options:0];
    return newdate;
}

+(int)DateDiffOfDays :(NSDate*)fromDate toDate:(NSDate*)toDate{
    NSTimeInterval time=[toDate timeIntervalSinceDate:fromDate];
    
    return ((int)time)/(3600*24);
}


@end
