
#import <Foundation/Foundation.h>

@interface ConfigHelper : NSObject


+(void)updateValue:(NSString*)itemName : (NSString*)itemValue;
+(void)updateIntValue:(NSString*)itemName : (int)itemValue;
+(void)updateObjValue:(NSString*)itemName : (id)itemValue;

+(id)getValue:(NSString*)itemName;
+(int)getIntValue:(NSString*)itemName;
+(NSString*)getStringValue:(NSString*)itemName;


@end
