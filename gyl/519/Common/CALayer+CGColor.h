//
//  CALayer+CGColor.h
//  519
//
//  Created by Macmini on 2017/12/11.
//  Copyright © 2017年 519. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
@interface CALayer (CGColor)
- (void)setBorderColorWithUIColor:(UIColor *)color;
@end
