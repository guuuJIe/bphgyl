//
//  MainTabBarController.m
//  519
//
//  Created by Macmini on 17/6/10.
//  Copyright © 2017年 519. All rights reserved.
//

#import "MainTabBarController.h"
#import "MainVC.h"
#import "TypeVC.h"
#import "CarVC.h"
#import "UserVC.h"
#import "MainWebView.h"
#import "GoodInfoVC.h"
#import "GoodListVC.h"
#import "SCJumpViewController.h"
//#import "MainTabBar.h"
@interface MainTabBarController ()
//@property(nonatomic, weak)MainTabBar *mainTabBar;

@end

@implementation MainTabBarController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    //去掉tabBar顶部线条
//    CGRect rect = CGRectMake(0, 0, Screen_Width, Screen_Height);
//    UIGraphicsBeginImageContext(rect.size);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
//    CGContextFillRect(context, rect);
//    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    [self.tabBar setBackgroundImage:img];
//    [self.tabBar setShadowImage:img];
    
    
    
    //[self SetupMainTabBar];
    [self SetupAllControllers];

    self.tabBar.backgroundColor = [UIColor whiteColor];
    
//    [self.tabBar showBadgeOnItemIndex:2 value:@"4"];
    
    [self refreshCarNum];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshCarNum) name:@"getCarNum" object:nil];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)refreshCarNum{
    if(EMAIL){
        NSString * cardataUrl = [NSString stringWithFormat:@"%@ctl=cart&act=index&page=1&email=%@&pwd=%@",GYLUrl,EMAIL,USER_PWD];
        [RequestData requestDataOfUrl:cardataUrl success:^(NSDictionary *dic) {
            
            if ([dic[@"status"] isEqual:@1]) {
                
                if (![dic[@"cart_list"] isEqual:[NSNull null]]) {
                    //                NSArray *num = dic[@"cart_list"];
                    
                    [self.tabBar showBadgeOnItemIndex:2 value:[NSString stringWithFormat:@"%@",dic[@"total_number"]]];
                }else{
                    [self.tabBar hideBadgeOnItemIndex:2];
                }
                
                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",dic[@"total_number"]] forKey:@"num"];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshCart" object:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshNum" object:dic[@"total_number"]];
            }else{
                 [self.tabBar hideBadgeOnItemIndex:2];
            }
          
            
            
           
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }else{
        [self.tabBar hideBadgeOnItemIndex:2];
    }
}


/**
 *  增加tabbar中间按钮方法
 */
//- (void)SetupMainTabBar{
//    MainTabBar *tabBar = [[MainTabBar alloc] init];
//    [self setValue:tabBar forKey:@"tabBar"];
//    [tabBar setBlock:^{
//        NSDictionary*storedic = [[NSUserDefaults standardUserDefaults]objectForKey:@"storeInfo"];
//        MapNavigatorVC *vc = [[MapNavigatorVC alloc] init];
//        vc.xpoint = storedic[@"xpoint"];
//        vc.ypoint = storedic[@"ypoint"];
//        vc.phoneNum = storedic[@"tel"];
//        vc.storeName = storedic[@"shopName"];
//        BaseNC *nav = [[BaseNC alloc] initWithRootViewController:vc];
//        [self presentViewController:nav animated:YES completion:nil];
//    }];
//}

- (void)SetupAllControllers{
    NSArray *titles = @[@"首页",@"分类", @"进货单", @"我的"];
    NSArray *images = @[@"button_首页_默认",@"button_分类_默认", @"button_进货单_默认", @"button_我的_默认"];
    NSArray *selectedImages = @[@"button_首页_高亮",@"button_分类_高亮", @"button_进货单_高亮", @"button_我的_高亮"];
    
    MainVC *main1 = [[MainVC alloc] init];
    TypeVC *main2 = [[TypeVC alloc] init];
    CarVC *main3 = [[CarVC alloc] init];
    UserVC *main4 = [[UserVC alloc] init];
    NSArray *viewControllers = @[main1, main2, main3, main4];
    
    for (int i = 0; i < viewControllers.count; i++) {
        UIViewController *childVc = viewControllers[i];
        [self SetupChildVc:childVc title:titles[i] image:images[i] selectedImage:selectedImages[i]];
    }
}

- (void)SetupChildVc:(UIViewController *)VC title:(NSString *)title image:(NSString *)imageName selectedImage:(NSString *)selectedImageName{
    
    VC.tabBarItem.title = title;
    [VC.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName: APPColor} forState:UIControlStateSelected];
    VC.tabBarItem.image = [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    VC.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    BaseNC *nav = [[BaseNC alloc] initWithRootViewController:VC];
    [self addChildViewController:nav];
}


@end
