//
//  BaseNC.m
//  519
//
//  Created by 陈 on 16/8/2.
//  Copyright (c) 2016年 519. All rights reserved.
//

#import "BaseNC.h"
#import "UIImage+Common.h"
@interface BaseNC ()<UINavigationBarDelegate,UIGestureRecognizerDelegate>

@end

@implementation BaseNC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationBar.tintColor = [UIColor blackColor];
    [self.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:19], NSForegroundColorAttributeName:[UIColor blackColor]}];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"2px"] forBarMetrics:(UIBarMetricsDefault)];
    
    // 手势代理
    self.interactivePopGestureRecognizer.delegate = self;
}

/**
 决定是否触发手势
 */
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    // 排除根控制器，其他所有子控制器都要触发手势
    return self.childViewControllers.count > 1;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UMUtil mobclickAgentBeginLog:NSStringFromClass([self class])];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [UMUtil mobclickAgentEndLog:NSStringFromClass([self class])];
}
- (void)popToBack
{
    [self popViewControllerAnimated:YES];
}
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if (self.viewControllers.count > 0)
    {
        // 左上角的返回按钮
        UIView * btnView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 50,30)];

        UIButton *backButton = [[UIButton alloc]initWithFrame:btnView.bounds];
        [backButton setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
        [backButton setTitle:@"返回" forState:(UIControlStateNormal)];
        [backButton setTitleColor:APPFourColor forState:(UIControlState)UIControlStateNormal];
        backButton.titleLabel.font = [UIFont systemFontOfSize:15];
        backButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
        backButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [backButton addTarget:self action:@selector(onback) forControlEvents:UIControlEventTouchUpInside];
        backButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0); // 这里微调返回键的位置可以让它看上去和左边紧贴
        [btnView addSubview:backButton];
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnView];

        viewController.hidesBottomBarWhenPushed = YES; // 隐藏底部的工具条
    }
    [super pushViewController:viewController animated:animated];
}
-(void)onback{
    [self popViewControllerAnimated:YES];
}
- (UIViewController *)popViewControllerAnimated:(BOOL)animated
{
    return [super popViewControllerAnimated:animated];
}
-(BOOL)prefersStatusBarHidden {
    NSLog(@"--------[ViewController prefersStatusBarHidden]--------");
    return NO;
    //return YES;
}

-(UIViewController *)childViewControllerForStatusBarStyle {
    //    NSLog(@"--------[RootNavigationController childViewControllerForStatusBarStyle]--------");
    return self.topViewController;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
